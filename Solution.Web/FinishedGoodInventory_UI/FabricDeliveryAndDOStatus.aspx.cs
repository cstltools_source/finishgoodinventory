﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_FabricDeliveryAndDOStatus : System.Web.UI.Page
{
    QAStockReceiveDal aDal = new QAStockReceiveDal();


    private DoChargeDal aChargeDal = new DoChargeDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            //  LoadList(itemsGridView);
        }
    }

   

    private void LoadList(GridView gridView)
    {
        try
        {
            DataTable aDataTable = aChargeDal.RPT_DeliveryAndDoStatus(GenerateParameter());
            itemsGridView.DataSource = null;
            itemsGridView.DataBind();
            itemsGridView.DataSource = aDataTable;
            itemsGridView.DataBind();
        }
        catch (Exception )
        {
           
            throw;
        }

    
    }

    private string GenerateParameter()
    {
        string pram = "";

        if (txt_DeliveryDate.Text.Trim() != "")
        {
            
            pram = pram + " AND CONVERT(date,IFPM.DeliveryDate)  BETWEEN '" + txt_DeliveryDate.Text.Trim() +
                             "' AND '" + txt_DeliveryDate.Text.Trim() + "' ";

           // pram = txt_DeliveryDate.Text.Trim();

        }

        //if (ddlBuyer.SelectedValue != "")
        //{
        //    pram = pram + " AND BI.BuyerName LIKE '%" + ddlBuyer.SelectedItem.Text + "%'";
        //}

        //if (ddlVendor.SelectedValue != "")
        //{
        //    pram = pram + " AND VEN.BuyerName LIKE '%" + ddlBuyer.SelectedItem.Text + "%'";
        //}

        //if (ddlPi.SelectedValue != "")
        //{
        //    //pram = pram + " AND PI.ProformaInvNo LIKE '%" + ddlPi.SelectedValue + "%'";
        //    pram = pram + " AND PPI.ProformaMasterId = " + ddlPi.SelectedValue;
        //}

        //if (ddlfabric.SelectedValue != "")
        //{
        //    pram = pram + " AND QARD.FabricCodeId In (Select FabricId from tblFabricInfo Where LOWER(FabricName)= LOWER('" + ddlfabric.SelectedValue.Trim() + "'))";
        //}

        //if (ddlRollWise.SelectedValue != "")
        //{
        //    pram = pram + " AND QARD.RollNo LIKE '%" + ddlRollWise.SelectedValue + "%' ";
        //}

        //if (ddlSet.SelectedValue != "")
        //{
        //    pram = pram + "AND QARD.FabricSet LIKE '%" + ddlSet.SelectedValue + "%' ";
        //}

        //if (ddlShift.SelectedValue != "")
        //{
        //    pram = pram + " AND QARM.ShiftId = " + ddlShift.SelectedValue;
        //}

        //if (fromDateTextBox.Text.Trim() != "" && toDateTextBox.Text.Trim() != "")
        //{
        //    pram = pram + " AND PPI.ProformaDate BETWEEN '" + fromDateTextBox.Text.Trim() + "' AND '" + toDateTextBox.Text.Trim() + "'";
        //}

        //if (fromDateTextBox.Text.Trim() != "" && toDateTextBox.Text.Trim() == "")
        //{
        //    pram = pram + "  AND PPI.ProformaDate => '" + fromDateTextBox.Text.Trim() + "'";
        //}

        //if (fromDateTextBox.Text.Trim() == "" && toDateTextBox.Text.Trim() != "")
        //{
        //    pram = pram + " AND PPI.ProformaDate <= '" + toDateTextBox.Text.Trim() + "'";
        //}



        //if (txtProductionFromDate.Text.Trim() != "" && txtProductionToDate.Text.Trim() != "")
        //{
        //    pram = pram + " AND QARM.ProductionDate BETWEEN '" + txtProductionFromDate.Text.Trim() + "' AND '" + txtProductionToDate.Text.Trim() + "'";
        //}

        //if (txtProductionFromDate.Text.Trim() != "" && txtProductionToDate.Text.Trim() == "")
        //{
        //    pram = pram + "  AND QARM.ProductionDate => '" + txtProductionFromDate.Text.Trim() + "'";
        //}

        //if (txtProductionFromDate.Text.Trim() == "" && txtProductionToDate.Text.Trim() != "")
        //{
        //    pram = pram + " AND QARM.ProductionDate <= '" + toDateTextBox.Text.Trim() + "'";
        //}


        //if (txtEntryFromDate.Text.Trim() != "" && txtEntryToDate.Text.Trim() != "")
        //{
        //    pram = pram + " AND QARM.EntryDate BETWEEN '" + txtEntryFromDate.Text.Trim() + "' AND '" + txtEntryToDate.Text.Trim() + "'";
        //}

        //if (txtEntryFromDate.Text.Trim() != "" && txtEntryToDate.Text.Trim() == "")
        //{
        //    pram = pram + "  AND QARM.EntryDate => '" + txtEntryFromDate.Text.Trim() + "'";
        //}

        //if (txtEntryFromDate.Text.Trim() == "" && txtEntryToDate.Text.Trim() != "")
        //{
        //    pram = pram + " AND QARM.EntryDate <= '" + txtEntryToDate.Text.Trim() + "'";
        //}

        return pram;
    }

    

    private void PopUp(string rpt, string param)
    {
        string url = "../FinishedGoodInventory_RPTView/FinishedGoodReportViewer.aspx?rptType=" + rpt + "&rpt=" + param;
        string fullURL = "window.open('" + url + "', '_blank', 'height=600,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);

    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void loadGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        itemsGridView.PageIndex = e.NewPageIndex;
        this.LoadList(itemsGridView);
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {
        LoadList(itemsGridView);
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        //ddlBuyer.SelectedIndex = 0;
        //ddlVendor.SelectedIndex = 0;
        //ddlShift.SelectedIndex = 0;
        //ddlPi.SelectedIndex = 0;
        //ddlRcvType.SelectedIndex = 0;
        //txtProductionFromDate.Text = "";
        //txtProductionToDate.Text = "";
        //fromDateTextBox.Text = "";
        //toDateTextBox.Text = "";
        //ddlfabric.SelectedIndex = 0;
        //ddlRollWise.SelectedIndex = 0;
        //ddlSet.SelectedIndex = 0;
       
    }

    protected void ddlBuyer_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        // LoadPiNo();
        LoadList(itemsGridView);
    }

    protected void ddlVendor_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        // LoadPiNo();
        LoadList(itemsGridView);
    }

    protected void ddlPi_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        // LoadList(itemsGridView);

        FabricLoad();
    }

    private void FabricLoad()
    {
        //using (DataTable data = aDal.LoadSisCodeByPiD(Convert.ToInt32(ddlPi.SelectedValue)))
        //{
        //    ddlfabric.DataSource = data;
        //    ddlfabric.DataValueField = "FabricName";
        //    ddlfabric.DataTextField = "FabricDescription";
        //    ddlfabric.DataBind();
        //    ddlfabric.Items.Insert(0, new ListItem("Select from list", String.Empty));
        //}
    }


    protected void ddlWidth_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadList(itemsGridView);
    }

    private void LoadPiNo()
    {
        //string pram = "";

        //if (ddlBuyer.SelectedValue != "")
        //{
        //    pram = pram + " AND PFI.BuyerId = '" + ddlBuyer.SelectedValue + "'";
        //}

        //if (ddlVendor.SelectedValue != "")
        //{
        //    pram = pram + " AND PFI.VendorId = '" + ddlVendor.SelectedValue + "'";
        //}

        //aDal.LoadPiNo(ddlPi, pram);
    }


    public override void VerifyRenderingInServerForm(Control control)
    {
        //required to avoid the runtime error "  
        //Control 'GridView1' of type 'GridView' must be placed inside a form tag with runat=server."  
    }


    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        if (itemsGridView.Rows.Count > 0)
        {

           
                string attachment = "attachment; filename=FabricDelivery_&_DOStatus.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);

                //QaStockInformation
                DataTable aTable = aChargeDal.RPT_DeliveryAndDoStatus(GenerateParameter());

                itemsGridView.DataSource = aTable;
                itemsGridView.DataBind();

                // Create a form to contain the grid  
                HtmlForm frm = new HtmlForm();
                itemsGridView.Parent.Controls.Add(frm);
                //frm.Attributes["runat"] = "server";
                //frm.Controls.Add(loadGridView);
                //frm.RenderControl(htw);

                itemsGridView.HeaderRow.Style.Add("background-color", "#E5EEF1");

                // Set background color of each cell of GridView1 header row
                foreach (TableCell tableCell in itemsGridView.HeaderRow.Cells)
                {
                    tableCell.Style["background-color"] = "#E5EEF1";
                }

                // Set background color of each cell of each data row of GridView1
                foreach (GridViewRow gridViewRow in itemsGridView.Rows)
                {
                    gridViewRow.BackColor = System.Drawing.Color.White;

                    foreach (TableCell gridViewRowTableCell in gridViewRow.Cells)
                    {
                        gridViewRowTableCell.Style["background-color"] = "#FFFFFF";

                    }
                }

                itemsGridView.RenderControl(htw);

                Response.Write(sw.ToString());
                Response.End();
           


        }
        else
        {
            ShowMessageBox("No Data Found!!");
        }



    }



    protected void gv_DocumentUpload_PreRender(object sender, EventArgs e)
    {
        GridView gv = (GridView)sender;

        if ((gv.ShowHeader == true && gv.Rows.Count > 0)
            || (gv.ShowHeaderWhenEmpty == true))
        {
            //Force GridView to use <thead> instead of <tbody> - 11/03/2013 - MCR.
            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
}