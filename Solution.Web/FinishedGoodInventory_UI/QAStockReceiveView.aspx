﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" MasterPageFile="~/MasterPages/NewMasterPage.master"  EnableEventValidation="false" CodeFile="QAStockReceiveView.aspx.cs" Inherits="FinishedGoodInventory_UI_QAStockReceiveView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    

    

    <style>
        .large-table-container-3 {
            max-width: 100%;
            max-height: 500px;
            overflow-x: scroll;
            overflow-y: auto;
        }
       

    </style>
    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div id="popDiv">
            </div>

            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>QA Stock Receive</div>

                        <div class="ms-auto">
                            <div class="btn-group">
                                <a href="../FinishedGoodInventory_UI/QAStockReceive.aspx" class="btn btn-sm btn-outline-info "><i class="fa fa-plus" aria-hidden="true"></i>New Entry</a>
                            </div>
                        </div>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">
                            <div class="card border-top border-0 border-4 border-success">
                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-user me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Stock Receive List </h5>
                                        </div>
                                        <hr>

                                        <div class="row mb-2">

                                            <div class="col-md-4">
                                                <script type="text/javascript">
                                                    function pageLoad() {
                                                        $('.datepicker').pickadate({
                                                            selectMonths: true,
                                                            selectYears: true
                                                        });
                                                        $('.mySelect2').select2({
                                                            theme: 'bootstrap4',
                                                            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                                            placeholder: $(this).data('placeholder'),
                                                            allowClear: Boolean($(this).data('allow-clear')),
                                                        });
                                                    }
                                                </script>
                                                
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Receive Type </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2"  runat="server" ID="ddlRcvType"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Buyer Name</label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2" AutoPostBack="True" OnSelectedIndexChanged="ddlBuyer_OnSelectedIndexChanged" runat="server" ID="ddlBuyer"></asp:DropDownList>
                                                    </div>
                                                </div>


                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Vendor Name</label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2" AutoPostBack="True" OnSelectedIndexChanged="ddlVendor_OnSelectedIndexChanged" runat="server" ID="ddlVendor"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">SisCode/Fabric</label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlfabric"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                


                                            </div>
                                            
                                            <div class="col-md-4">
                                                
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">PI No </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2" AutoPostBack="True" OnSelectedIndexChanged="ddlPi_OnSelectedIndexChanged" runat="server" ID="ddlPi"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">From Date (PI) </label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="fromDateTextBox" runat="server" class="form-control form-control-sm datepicker"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">To Date (PI) </label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="toDateTextBox" runat="server" class="form-control form-control-sm datepicker"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Role Wise</label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlRollWise"></asp:DropDownList>
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Shift </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2" AutoPostBack="True" OnSelectedIndexChanged="ddlWidth_OnSelectedIndexChanged" runat="server" ID="ddlShift"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">From Date (Pro.)</label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtProductionFromDate" runat="server" CssClass="form-control form-control-sm datepicker"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">To Date (Pro.) </label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtProductionToDate" runat="server" CssClass="form-control form-control-sm datepicker"></asp:TextBox>
                                                    </div>
                                                </div>
                                                
                                                
                                                
                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">From Date (Cre.)</label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtEntryFromDate" runat="server" CssClass="form-control form-control-sm datepicker"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">To Date (Cre.) </label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtEntryToDate" runat="server" CssClass="form-control form-control-sm datepicker"></asp:TextBox>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Set No</label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlSet"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            


                                        </div>
                                        <hr />

                                        <div class="row mb-4">

                                            <div class="col-3"></div>
                                            <div class="col-6" style="">
                                                <asp:LinkButton runat="server" class="btn btn-outline-primary" ID="LinkButton1" OnClick="searchButton_Click">                                            
                                                            <i class="fa fa-search-plus"></i><span style="font-weight: bold !important">Search Information</span>
                                                </asp:LinkButton>
                                                <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="LinkButton2" OnClick="resetButton_Click">                                            
                                                            <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Information</span>
                                                </asp:LinkButton>
                                            </div>
                                            <div class="col-3"></div>

                                        </div>
                                        
                                    
                                    
                                    
                                    
                                    <hr/>
                                    
                                    <div class="row">
                                         
                                   
                                        <div class="col-md-2">
                                       
                                       
                                        </div>
                                         
                                        <div class="col-md-2">
                                       
                                       
                                        </div>
                                        <div class="col-md-2">
                                       
                                       
                                        </div>
                                        <div class="col-md-3">
                                       
                                       
                                        </div>
                              
                                       
                                        
                                       
                                  
                                  
                                        <div class="col-md-3">
                                            <asp:LinkButton ID="btnExportToExcel" runat="server" CssClass="btn btn-success pull-right" OnClick="btnExportToExcel_Click" ><span aria-hidden="true" class="fa fa-file-excel-o" ></span> &nbsp;Export To Excel</asp:LinkButton> 
                                       
                                      
                                       
        
                                        </div>
                                    </div>

                                        <hr />


                                        
                                        
                                        
                                    

                                        <div class="large-table-container-3">

                                            <asp:GridView ID="itemsGridView" runat="server" AutoGenerateColumns="False" 
                                                PageIndex="0" CssClass="table table-bordered table-condensed custom-table-style" DataKeyNames="StockReceiveMasterId,ApprovalStatus,QALocationId,StockReceiveDetailId" OnRowCommand="itemsGridView_RowCommand"
                                                          AllowPaging="True" OnPageIndexChanging="loadGridView_PageIndexChanging"  EmptyDataText="There are no data records to display.">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="SL">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="ProductionDate" HeaderText="Producttion Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                    <asp:BoundField DataField="ShiftTitle" HeaderText="Shift" />
                                                    <asp:BoundField DataField="ProformaInvNo" HeaderText="Proforma No" />
                                                    <asp:BoundField DataField="FabricName" HeaderText="SISCode" />
                                                    
                                                    <asp:TemplateField HeaderText="SETNo">
                                                        <ItemTemplate>

                                                          
                                                            <asp:TextBox ID="txtFabricSet" runat="server"  ReadOnly="true"  Text='<%#Eval("FabricSet") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>
                                                     
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    
                                                    <asp:TemplateField HeaderText="BeamNo">
                                                        <ItemTemplate>

                                                            <asp:TextBox ID="txtBeam" runat="server"  ReadOnly="true"  Text='<%#Eval("Beam") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>
                                                     
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    
                                                    <asp:TemplateField HeaderText="GreigeRoll No">
                                                        <ItemTemplate>
                                           
                                                            
                                                            <asp:TextBox ID="txtGreigeRollNo" runat="server"  ReadOnly="true"  Text='<%#Eval("GreigeRollNo") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>
                                                       
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    
                                                    <asp:TemplateField HeaderText="RollNo">
                                                        <ItemTemplate>
                                                    
                                                            <asp:TextBox ID="txtRollNo" runat="server"  ReadOnly="true"  Text='<%#Eval("RollNo") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>
                                                          
                                                        
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="DefectPoint">
                                                        <ItemTemplate>
                                             
                                                            <asp:TextBox ID="txtDefectPoint" runat="server"  ReadOnly="true"  Text='<%#Eval("DefectPoint") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Required Width">
                                                        <ItemTemplate>
                                               
                                                            
                                                            <asp:TextBox ID="txtRequiredWidth" runat="server"  ReadOnly="true"  Text='<%#Eval("RequiredWidth") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField HeaderText="ActualWidth">
                                                        <ItemTemplate>
                                                
                                                            
                                                            <asp:TextBox ID="txtActualWidth" runat="server"  ReadOnly="true"  Text='<%#Eval("ActualWidth") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Piece">
                                                        <ItemTemplate>
                                                          
                                                            <asp:TextBox ID="txtPiece" runat="server"  ReadOnly="true"  Text='<%#Eval("Piece") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                

                                                    <asp:TemplateField HeaderText="P.W.Length">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtPWLength" runat="server"  ReadOnly="true"  Text='<%#Eval("PWLength") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField HeaderText="Quantity">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtQuantity" runat="server"  ReadOnly="true"  Text='<%#Eval("Quantity") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>
                                                    
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField HeaderText="EntryBy">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtEntryBy" runat="server"  ReadOnly="true"  Text='<%#Eval("EntryBy") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField HeaderText="OperatorName">
                                                        <ItemTemplate>
                                                           
                                                            
                                                            <asp:TextBox ID="txtOperatorName" runat="server"  ReadOnly="true"  Text='<%#Eval("OperatorName") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>
                                                            

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField HeaderText="MCNo.">
                                                        <ItemTemplate>
                                                          
                                                            
                                                            <asp:TextBox ID="txtMCNo" runat="server"  ReadOnly="true"  Text='<%#Eval("MCNo") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    
                                                    <asp:TemplateField HeaderText="F.L.No.">
                                                        <ItemTemplate>
                                                            
                                                            
                                                            <asp:TextBox ID="txtFL" runat="server"  ReadOnly="true"  Text='<%#Eval("FL") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    
                                                    <asp:TemplateField HeaderText="PPHSY">
                                                        <ItemTemplate>
                                                            
                                                            
                                                            <asp:TextBox ID="txtPPHSY" runat="server"  ReadOnly="true"  Text='<%#Eval("PPHSY") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>
                                                            

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField HeaderText="GrossWeight">
                                                        <ItemTemplate>
                                                           
                                                            <asp:TextBox ID="txtGrossWeight" runat="server"  ReadOnly="true"  Text='<%#Eval("GrossWeight") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>
                                                            

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    
                                                    <asp:TemplateField HeaderText="NetWeight">
                                                        <ItemTemplate>
                                                           
                                                            
                                                            <asp:TextBox ID="txtNetWeight" runat="server"  ReadOnly="true"  Text='<%#Eval("NetWeight") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    
                                                <asp:TemplateField HeaderText="LocationName">
                                                    <ItemTemplate>
                                                     
                                                        
                                                        <asp:DropDownList Enabled="false"  CssClass="form-select form-select-sm mb-3 mySelect2"   runat="server" id="ddlLocationName" ></asp:DropDownList>
                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                
                                                <asp:TemplateField HeaderText="ProblemCause">
                                                    <ItemTemplate>
                                                    
                                                        
                                                        <asp:TextBox ID="txtProblemCause" runat="server"  ReadOnly="true"  Text='<%#Eval("ProblemCause") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>


                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                
                                                
                                                <asp:TemplateField HeaderText="Remarks">
                                                    <ItemTemplate>
                                                    
                                                        
                                                        <asp:TextBox ID="txtRemarks" runat="server"  ReadOnly="true"  Text='<%#Eval("Remarks") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>
                                                        
                                                         
                                                        <asp:LinkButton ID="lbtUpDate" runat="server" Visible="false" CssClass="btn-warning  btn-sm mb-1 mb-md-0"
                                                                        CommandArgument="<%# Container.DataItemIndex %>" CommandName="UpdateData"><i class='bx bxs-edit' aria-hidden='true'></i> Update</asp:LinkButton>


                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Edit">
                                                        <ItemTemplate>

                                                            <asp:LinkButton ID="LinkButton1"  Visible="False" runat="server" CssClass="btn-warning  btn-sm mb-1 mb-md-0"
                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="EditData"><i class='bx bxs-edit' aria-hidden='true'></i></asp:LinkButton>
                                             
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    

                                            <%--        <asp:TemplateField HeaderText="Actions">
                                                        <ItemTemplate>

                                                            <asp:ImageButton ID="PrintButton" runat="server" class="btn btn-outline-info btn-sm  " ToolTip="Print" CommandArgument='<%#Eval("StockReceiveMasterId") %>'
                                                                CommandName="PrintData" ImageUrl="~/assets/images/172530_print_icon.png" Height="30" Width="34" />

                                                            <asp:ImageButton ID="editImageButton" runat="server" class="btn btn-outline-info btn-sm  " ToolTip="Edit" CommandArgument='<%# Container.DataItemIndex + 1 %>'
                                                                CommandName="EditData" ImageUrl="~/assets/images/pencil-icon.png" Height="30" Width="34" />

                                                        </ItemTemplate>
                                                        <ItemStyle Width="12%"></ItemStyle>
                                                    </asp:TemplateField>--%>



                                                </Columns>
                                            </asp:GridView>

                                        </div>
                                        
                                    
                                    <div runat="server" Visible="False">


                                     <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"  OnPreRender="gv_DocumentUpload_PreRender"
                                                 CssClass="table table-bordered table-condensed custom-table-style" OnRowCommand="itemsGridView_RowCommand"
                                                           OnPageIndexChanging="loadGridView_PageIndexChanging" >
                                                <Columns>
                                                    <asp:TemplateField HeaderText="SL">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="ProductionDate" HeaderText="PRODUCTION DATE" DataFormatString="{0:dd-MMM-yyyy}" />
                                                    <asp:BoundField DataField="ShiftTitle" HeaderText="SHIFT" />
                                                    <asp:BoundField DataField="ProformaInvNo" HeaderText="PI NUMBER" />
                                                   <%-- <asp:BoundField DataField="FebricDescription" HeaderText="SISCode" />--%>
                                                
                                                    
                                                    
                                                    
                                                    <asp:TemplateField HeaderText="SET NUMBER">
                                                        <ItemTemplate>
                                                           
                                                            <asp:Label ID="lblService" runat="server" Text='<%#Eval("FabricSetNumber")%>'></asp:Label>  

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    

                                                    

                                          <%--      <asp:BoundField DataField="FabricSetNumber" HeaderText="SET NUMBER" />--%>
                                                
                                                
                                                <asp:BoundField DataField="Beam" HeaderText="BEAM NUMBER" />
                                                    
                                                <asp:BoundField DataField="GreigeRollNo" HeaderText="GRL NUMBER" />
                                                    <asp:BoundField DataField="RollNo" HeaderText="ROLL NUMBER" />
                                                    <asp:BoundField DataField="DefectPoint" HeaderText="DEFECT POINTS" />
                                                    <asp:BoundField DataField="CONTAMINATION" HeaderText="CONTAMINATION" />
                                                    <asp:BoundField DataField="RequiredWidth" HeaderText="REQUIRED WIDTH ()" />
                                                    <asp:BoundField DataField="ActualWidth" HeaderText="ACTUAL WIDTH ()" />
                                                    <asp:BoundField DataField="ShadeGradeing" HeaderText="SHADE" />
                                                    <asp:BoundField DataField="Piece" HeaderText="PIECE" />
                                                    <asp:BoundField DataField="PWLength" HeaderText="PIECE WISE LENGTH" />
                                                    <asp:BoundField DataField="ELITEA" HeaderText="ELITE-A(YARDS)" />

                                                    <asp:BoundField DataField="PPHSY" HeaderText="PPHSY" />
                                                    <asp:BoundField DataField="ELITEA1" HeaderText="ELITE-A1 (YARDS)" />
                                                    <asp:BoundField DataField="SHADY" HeaderText="SHADY (YARDS)" />
                                                    <asp:BoundField DataField="CUTPIECE" HeaderText="CUT PIECE (YARDS)" />
                                                    
                                                    <asp:BoundField DataField="EntryBy" HeaderText="Entry By" />
                                                    <asp:BoundField DataField="OperatorName" HeaderText="Operator Name" />
                                                    <asp:BoundField DataField="MCNo" HeaderText="MCNo" />
                                                    <asp:BoundField DataField="FL" HeaderText="FINISHING GRL NUMBER" />
                                                    <asp:BoundField DataField="GrossWeight" HeaderText="Gross Weight" />
                                                    <asp:BoundField DataField="NetWeight" HeaderText="Net Weight" />
                                                    <asp:BoundField DataField="LocationName" HeaderText="Zone & plate" />
                                                    <asp:BoundField DataField="SENDTOSTOREDATE" HeaderText="SEND TO STORE DATE" />
                                                    <asp:BoundField DataField="HEADENDSSENDINGDATE" HeaderText="HEADENDS SENDING DATE" />
                                                    <asp:BoundField DataField="HEADENDSAPPROVALDATE" HeaderText="HEADENDS APPROVAL DATE" />
                                                    <asp:BoundField DataField="HEADENDSREJECTDATE" HeaderText="HEADENDS REJECT DATE" />
                                                    <asp:BoundField DataField="DELIVERYDATE" HeaderText="DELIVERY DATE" />
                                                    <asp:BoundField DataField="PROBLEMATICCAUSE" HeaderText="PROBLEMATIC CAUSE" />
                                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                                

                                                </Columns>
                                            </asp:GridView>
                                    
                                    
                                    </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>

    <Triggers>
        <asp:PostBackTrigger ControlID="btnExportToExcel" /> 
    </Triggers>

    </asp:UpdatePanel>



    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                    Height="120" Width="120" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>

