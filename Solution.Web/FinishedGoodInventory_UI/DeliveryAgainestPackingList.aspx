﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/NewMasterPage.master" AutoEventWireup="true" CodeFile="DeliveryAgainestPackingList.aspx.cs" Inherits="FinishedGoodInventory_UI_DeliveryAgainestPackingList" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=3.0.20820.28364, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="popDiv">
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Delivery Pending List  </div>
                        <%--<div class="ms-auto">
                    <div class="btn-group">
                        <a href="../FinishedGoodInventory_UI/PIView.aspx" class="btn btn-sm btn-sm btn-outline-info"><i class="fa fa-backward"></i>&nbsp;Back to List</a>
                    </div>
                </div>--%>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">

                            <script type="text/javascript">
                                function pageLoad() {
                                    $('.datepicker').pickadate({
                                        selectMonths: true,
                                        selectYears: true
                                    });
                                    $('.mySelect2').select2({
                                        theme: 'bootstrap4',
                                        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                        placeholder: $(this).data('placeholder'),
                                        allowClear: Boolean($(this).data('allow-clear')),
                                    });
                                }
                            </script>

                            <div class="card border-top border-0 border-4 border-success">


                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-search me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary">Search Options </h5>
                                        </div>
                                        <hr>


                                        <div class="row">
                                            
                                            <div class="form-group row">
                                                    <label for="" class="col-sm-3 col-form-label col-form-label-sm">(Issue/Packing) From Date: </label>
                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="fromDateTextBox" runat="server" class="form-control form-control-sm datepicker"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-3 col-form-label col-form-label-sm">(Issue/Packing) To Date: </label>
                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="toDateTextBox" runat="server" class="form-control form-control-sm datepicker"></asp:TextBox>
                                                    </div>
                                                </div>
                                            
                                            <div class="form-group row mt-2">
                                                    <label for="" class="col-sm-3 col-form-label col-form-label-sm"> </label>
                                                    <div class="col-sm-9">
                                                        <asp:Button ID="Button1" CssClass="btn btn-sm btn-outline-success align-center" runat="server" Text="Search" OnClick="Button1_Click" />
                                                <asp:LinkButton runat="server" class="btn btn-sm btn-outline-warning" ID="resetButton" OnClick="resetButton_Click">
                                            
                                                    <span style="font-weight: bold !important">Reset</span>

                                                </asp:LinkButton>
                                                    </div>
                                                </div>
                                            
                                        </div>

                                        <hr />
                                        

                                        <div class="row ">

                                            <div id="maingridview" style="text-align: center; height: auto; overflow: scroll; width: 100%; overflow-y: scroll; overflow-x: scroll;">
                                                <asp:GridView ID="productGridView" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-condensed custom-table-style"
                                                    DataKeyNames="IssueMasterId" EmptyDataText="There are no data records to display.">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="# SL">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:BoundField DataField="PackegingIssueCode" HeaderText="Issue Code" />
                                                        <asp:BoundField DataField="IssueBy" HeaderText="Issue By" />
                                                        <asp:BoundField DataField="IssueDate" HeaderText="Issue Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                        <asp:BoundField DataField="TotalQuantity" HeaderText="Total Quantity" />
                                                        <asp:BoundField DataField="Reason" HeaderText="Reason/Remarks" />
                                                        <asp:TemplateField HeaderText="Actions">
                                                            <ItemTemplate>
                                                                <asp:Button ID="gotoinvoiceButton" runat="server" Text="Proceed to delivery >>" CssClass="btn btn-sm btn-outline-info"
                                                                    OnClick="gotoinvoiceButton_Click" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>



    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                    Height="120" Width="120" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>

