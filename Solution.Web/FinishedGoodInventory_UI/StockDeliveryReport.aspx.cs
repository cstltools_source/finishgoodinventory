﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;

public partial class FinishedGoodInventory_UI_StockDeliveryReport : System.Web.UI.Page
{


    RollWiseLocationAllocationDal aDal = new RollWiseLocationAllocationDal();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDownList();
        }
    }



    private void LoadDropDownList()
    {

        aDal.LoadFabricInfo(ddlFabric);

    }



    protected void Button1_Click(object sender, EventArgs e)
    {

        if (ddlFabric.SelectedValue != "")
        {
            var url = "../HTMLReport/StockDeliveryReport.aspx?fromDate=" + ddlFabric.SelectedValue;
            var fullURL = "var Mleft = (screen.width/2)-(950/2);var Mtop = (screen.height/2)-(700/2);window.open( '" + url + "', null, 'height=700,width=950,status=yes,toolbar=no,addressbar=no, scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
            ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
        }

    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }
}