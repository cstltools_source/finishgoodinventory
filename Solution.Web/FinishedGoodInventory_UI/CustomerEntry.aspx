﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/NewMasterPage.master" AutoEventWireup="true" CodeFile="CustomerEntry.aspx.cs" Inherits="FinishedGoodInventory_UI_CustomerEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
    <style>

        .star {
            color: red;
        }

    </style>
    
    
    
    <script
        src="https://code.jquery.com/jquery-3.6.0.js"
        integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="popDiv">
            </div>

            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Buyer/Vendor Setup</div>

                        <div class="ms-auto">
                            <div class="btn-group">

                                <a href="../FinishedGoodInventory_UI/CustomerView.aspx" class="btn btn-sm btn-sm btn-outline-info"><i class="fa fa-backward"></i>&nbsp;Back to List</a>

                            </div>
                        </div>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">

                            <div class="card border-top border-0 border-4 border-success">
                                

                                <script type="text/javascript">

                                </script>

                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-user me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Buyer/Vendor Information</h5>
                                        </div>
                                        <hr>
                                        <div class="row mb-2">
                                            <label class="col-sm-2 col-form-label">Is Buyer/Vendor: <span style='color: red !important;'>[*]</span> </label>
                                            <div class="col-sm-10">

                                                <asp:RadioButtonList ID="RadioButtonList" AutoPostBack="True" OnSelectedIndexChanged="RadioButtonList_OnSelectedIndexChanged" runat="server" RepeatDirection="Horizontal" CellPadding="5" Font-Bold="True" Font-Size="Larger">
                                                    <asp:ListItem Value="Isbuyer" Selected="True">&nbsp;Is buyer </asp:ListItem>
                                                    <asp:ListItem Value="Isvendor">&nbsp;Is vendor </asp:ListItem>
                                                    <asp:ListItem Value="Isboth">&nbsp;Is both </asp:ListItem>
                                                </asp:RadioButtonList>

                                            </div>
                                        </div>

                                        <div class="row mb-2">
                                            <%--<label class="col-sm-2 col-form-label" runat="server" ID="BuyerType"></label>--%>
                                            <div class="col-sm-2 col-form-label">
                                                <asp:Label runat="server" ID="BuyerType"> </asp:Label>
                                            </div>
                                            <div class="col-sm-10">
                                                <asp:DropDownList class="form-select mySelect2 " runat="server" ID="ddlBuyerType"></asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="row mb-2">
                                          
                                            
                                            <div class="col-sm-2 col-form-label">
                                                <asp:Label runat="server" ID="Name"></asp:Label>
                                            </div>

                                            <div class="col-sm-10">

                                                <asp:HiddenField runat="server" ID="hfBuyerId" />
                                                <asp:TextBox ID="txtBuyerName" runat="server" placeholder="Enter Buyer/Vendor Name" CssClass="form-control"></asp:TextBox>

                                            </div>
                                        </div>

                                        <div class="row mb-2">
                                           <%-- <label class="col-sm-2 col-form-label">Buyer/Vendor Contact No:<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>--%>
                                            
                                            <div class="col-sm-2 col-form-label">
                                                <asp:Label runat="server" ID="ContactNo"></asp:Label>
                                            </div>

                                            <div class="col-sm-10">
                                                <asp:TextBox ID="txtBuyerContactNo" runat="server" placeholder="Enter Buyer/Vendor Contact No" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="row mb-2">

                                     <%--       <label class="col-sm-2 col-form-label">Buyer/Vendor Email:<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>--%>
                                            
                                            
                                            
                                            <div class="col-sm-2 col-form-label">
                                                <asp:Label runat="server" ID="Email"></asp:Label>
                                            </div>
                                            

                                            <div class="col-sm-10">
                                                <asp:TextBox ID="txtBuyerEmail" runat="server" placeholder="Enter Buyer/Vendor Email" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>



                                        <div class="row mb-2">
                                           <%-- <label class="col-sm-2 col-form-label">Buyer/Vendor Address:<span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>--%>
                                            
                                            <div class="col-sm-2 col-form-label">
                                                <asp:Label runat="server" ID="Address"></asp:Label>
                                            </div>

                                            <div class="col-sm-10">
                                                <asp:TextBox TextMode="MultiLine" Columns="5" ID="txtBuyerAddress" runat="server" placeholder="Enter Buyer/Vendor Address" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <br />

                                        <div class="row">
                                            <label class="col-sm-2 col-form-label"></label>
                                            <div class="col-sm-10">




                                                <asp:LinkButton runat="server" class="btn btn-outline-primary" ID="submitButton" OnClientClick="return confirm('Are you really aware of this operation?');" OnClick="submitButton_Click">
                                            
                                                    <i class="bx bxs-save mr-1"></i><span style="font-weight: bold !important">Save Information</span>

                                                </asp:LinkButton>



                                                <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="resetButton" OnClick="resetButton_Click">
                                            
                                                    <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Information</span>

                                                </asp:LinkButton>





                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>



    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                    Height="120" Width="120" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>

