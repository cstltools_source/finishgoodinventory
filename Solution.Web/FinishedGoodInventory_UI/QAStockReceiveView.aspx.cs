﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_QAStockReceiveView : System.Web.UI.Page
{
    QAStockReceiveDal aDal = new QAStockReceiveDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDownlist();
          //  LoadList(itemsGridView);
        }
    }

    private void LoadDropDownlist()
    {
        aDal.LoadShift(ddlShift);
        aDal.LoadBuyerInfo(ddlBuyer);
        aDal.LoadVendorInfo(ddlVendor);
       // aDal.LoadFabricName(ddlfabric);
        aDal.LoadFabricSet(ddlSet);
        aDal.LoadRollName(ddlRollWise);
      

        using (DataTable dt = aDal.LoadPiNoAll())
        {
            ddlPi.DataSource = dt;
            ddlPi.DataValueField = "ProformaMasterId";
            ddlPi.DataTextField = "ProformaInvNo";
            ddlPi.DataBind();
            ddlPi.Items.Insert(0, new ListItem("Select from list", String.Empty));
            ddlPi.SelectedIndex = 0;

        }

        DataTable DT = aDal.GetReceiveMovementCategory("Receive");
        if (DT.Rows.Count > 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                ddlRcvType.DataSource = DT;
                ddlRcvType.DataTextField = "MovementCategory";
                ddlRcvType.DataValueField = "MovementCategoryId"; 
                ddlRcvType.DataBind();
                ddlRcvType.Items.Insert(0, new ListItem("Select from list", String.Empty));
                ddlRcvType.SelectedIndex = 0;
            }
        }
    }

    private void LoadList(GridView gridView)
    {

        DataTable aDataTable = aDal.GetStockReceiveInfo(GenerateParameter());

        gridView.DataSource = aDataTable;
        gridView.DataBind();

        if (aDataTable.Rows.Count > 0)
        {

            DataTable dt = aDal.GetLocationName();

            for (int i = 0; i < itemsGridView.Rows.Count; i++)
            {

                DropDownList ddlLocationName = (DropDownList)itemsGridView.Rows[i].FindControl("ddlLocationName");

                LinkButton LinkButton1 = (LinkButton)itemsGridView.Rows[i].FindControl("LinkButton1");

                string LocationId = itemsGridView.DataKeys[i][2].ToString();

                string Status = itemsGridView.DataKeys[i][1].ToString();

                if (Status != "Accepted")
                {
                    LinkButton1.Visible = true;
                }

                try
                {
                    ddlLocationName.DataSource = dt;
                    ddlLocationName.DataValueField = "LocationId";
                    ddlLocationName.DataTextField = "LocationName";
                    ddlLocationName.DataBind();
                    ddlLocationName.Items.Insert(0, new ListItem("Please Select From List", String.Empty));
                    ddlLocationName.SelectedIndex = 0;

                    try
                    {

                        ddlLocationName.SelectedValue = LocationId;
                    }
                    catch (Exception ex) { }

                }
                catch (Exception ex) { }
            }


        }
    }

    private string GenerateParameter()
    {
        string pram = "";

        if (ddlRcvType.SelectedValue != "")
        {
            pram = pram + " AND QARM.RcvdTypeId = " + ddlRcvType.SelectedValue;
        }

        if (ddlBuyer.SelectedValue != "")
        {
            pram = pram + " AND BI.BuyerName LIKE '%" + ddlBuyer.SelectedItem.Text + "%'";
        }
        
        if (ddlVendor.SelectedValue != "")
        {
            pram = pram + " AND VEN.BuyerName LIKE '%" + ddlBuyer.SelectedItem.Text + "%'";
        }

        if (ddlPi.SelectedValue != "")
        {
            //pram = pram + " AND PI.ProformaInvNo LIKE '%" + ddlPi.SelectedValue + "%'";
            pram = pram + " AND PPI.ProformaMasterId = " + ddlPi.SelectedValue;
        }

        if (ddlfabric.SelectedValue != "")
        {
            pram = pram + " AND QARD.FabricCodeId In (Select FabricId from tblFabricInfo Where LOWER(FabricName)= LOWER('"+ddlfabric.SelectedValue.Trim()+"'))";
        }

        if (ddlRollWise.SelectedValue != "")
        {
            pram = pram + " AND QARD.RollNo LIKE '%" + ddlRollWise.SelectedValue + "%' ";
        }

        if (ddlSet.SelectedValue != "")
        {
            pram = pram + "AND QARD.FabricSet LIKE '%" + ddlSet.SelectedValue + "%' ";
        }

        if (ddlShift.SelectedValue != "")
        {
            pram = pram + " AND QARM.ShiftId = " + ddlShift.SelectedValue;
        }
        
        if (fromDateTextBox.Text.Trim() != "" && toDateTextBox.Text.Trim() != "")
        {
            pram = pram + " AND PPI.ProformaDate BETWEEN '" + fromDateTextBox.Text.Trim() + "' AND '" + toDateTextBox.Text.Trim() + "'";
        }

        if (fromDateTextBox.Text.Trim() != "" && toDateTextBox.Text.Trim() == "")
        {
            pram = pram + "  AND PPI.ProformaDate => '" + fromDateTextBox.Text.Trim() + "'";
        }

        if (fromDateTextBox.Text.Trim() == "" && toDateTextBox.Text.Trim() != "")
        {
            pram = pram + " AND PPI.ProformaDate <= '" + toDateTextBox.Text.Trim() + "'";
        }

        

        if (txtProductionFromDate.Text.Trim() != "" && txtProductionToDate.Text.Trim() != "")
        {
            pram = pram + " AND QARM.ProductionDate BETWEEN '" + txtProductionFromDate.Text.Trim() + "' AND '" + txtProductionToDate.Text.Trim() + "'";
        }

        if (txtProductionFromDate.Text.Trim() != "" && txtProductionToDate.Text.Trim() == "")
        {
            pram = pram + "  AND QARM.ProductionDate => '" + txtProductionFromDate.Text.Trim() + "'";
        }

        if (txtProductionFromDate.Text.Trim() == "" && txtProductionToDate.Text.Trim() != "")
        {
            pram = pram + " AND QARM.ProductionDate <= '" + toDateTextBox.Text.Trim() + "'";
        }


        if (txtEntryFromDate.Text.Trim() != "" && txtEntryToDate.Text.Trim() != "")
        {
            pram = pram + " AND QARM.EntryDate BETWEEN '" + txtEntryFromDate.Text.Trim() + "' AND '" + txtEntryToDate.Text.Trim() + "'";
        }

        if (txtEntryFromDate.Text.Trim() != "" && txtEntryToDate.Text.Trim() == "")
        {
            pram = pram + "  AND QARM.EntryDate => '" + txtEntryFromDate.Text.Trim() + "'";
        }

        if (txtEntryFromDate.Text.Trim() == "" && txtEntryToDate.Text.Trim() != "")
        {
            pram = pram + " AND QARM.EntryDate <= '" + txtEntryToDate.Text.Trim() + "'";
        }

        return pram;
    }

    protected void itemsGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {

            int row = Convert.ToInt32(e.CommandArgument.ToString());

            DropDownList ddlLocationName = (DropDownList)itemsGridView.Rows[row].FindControl("ddlLocationName");
            ddlLocationName.Enabled= true;

            TextBox txtFabricSet = (TextBox)itemsGridView.Rows[row].FindControl("txtFabricSet");
            txtFabricSet.ReadOnly = false;

            TextBox txtGreigeRollNo = (TextBox)itemsGridView.Rows[row].FindControl("txtGreigeRollNo");
            txtGreigeRollNo.ReadOnly = false;

            TextBox txtBeam = (TextBox)itemsGridView.Rows[row].FindControl("txtBeam");
            txtBeam.ReadOnly = false;

            TextBox txtDefectPoint = (TextBox)itemsGridView.Rows[row].FindControl("txtDefectPoint");
            txtDefectPoint.ReadOnly = false;

            TextBox txtRequiredWidth = (TextBox)itemsGridView.Rows[row].FindControl("txtRequiredWidth");
            txtRequiredWidth.ReadOnly = false;

            TextBox txtActualWidth = (TextBox)itemsGridView.Rows[row].FindControl("txtActualWidth");
            txtActualWidth.ReadOnly = false;

            TextBox txtPiece = (TextBox)itemsGridView.Rows[row].FindControl("txtPiece");
            txtPiece.ReadOnly = false;

            TextBox txtPWLength = (TextBox)itemsGridView.Rows[row].FindControl("txtPWLength");
            txtPWLength.ReadOnly = false;

            TextBox txtQuantity = (TextBox)itemsGridView.Rows[row].FindControl("txtQuantity");
            txtQuantity.ReadOnly = false;

            TextBox txtOperatorName = (TextBox)itemsGridView.Rows[row].FindControl("txtOperatorName");
            txtOperatorName.ReadOnly = false;

            TextBox txtMCNo = (TextBox)itemsGridView.Rows[row].FindControl("txtMCNo");
            txtMCNo.ReadOnly = false;

            TextBox txtFL = (TextBox)itemsGridView.Rows[row].FindControl("txtFL");
            txtFL.ReadOnly = false;

            TextBox txtPPHSY = (TextBox)itemsGridView.Rows[row].FindControl("txtPPHSY");
            txtPPHSY.ReadOnly = false;

            TextBox txtGrossWeight = (TextBox)itemsGridView.Rows[row].FindControl("txtGrossWeight");
            txtGrossWeight.ReadOnly = false;

            TextBox txtNetWeight = (TextBox)itemsGridView.Rows[row].FindControl("txtNetWeight");
            txtNetWeight.ReadOnly = false;

            TextBox txtProblemCause = (TextBox)itemsGridView.Rows[row].FindControl("txtProblemCause");
            txtProblemCause.ReadOnly = false;

            TextBox txtRemarks = (TextBox)itemsGridView.Rows[row].FindControl("txtRemarks");
            txtRemarks.ReadOnly = false;

            LinkButton lbtUpDate = (LinkButton)itemsGridView.Rows[row].FindControl("lbtUpDate");
            lbtUpDate.Visible = true;

            lbtUpDate.Focus();

     
            //int row = 0;

            //if (itemsGridView.Rows.Count > 1)
            //{
            //    row = Convert.ToInt32(e.CommandArgument.ToString());
            //}

            //string Status = itemsGridView.DataKeys[row].Values[1].ToString();

            //if (Status != "Accepted")
            //{
            //    Session["StockReceiveMasterId"] = itemsGridView.DataKeys[row].Values[0].ToString();
            //    Response.Redirect("QAStockReceive.aspx");
            //}
            //else
            //{
            //    ShowMessageBox("Sorry!! You can not edit now");
            //}

        }




        if (e.CommandName == "UpdateData")
        {

            int row = Convert.ToInt32(e.CommandArgument.ToString());
            QaStockReceiveDetailDao adetail = new QaStockReceiveDetailDao();

            string MasterId = itemsGridView.DataKeys[row][0].ToString();
            string DetailsId = itemsGridView.DataKeys[row][3].ToString();
            DropDownList ddlLocationName = (DropDownList)itemsGridView.Rows[row].FindControl("ddlLocationName");
            TextBox txtFabricSet = (TextBox)itemsGridView.Rows[row].FindControl("txtFabricSet");
            TextBox txtGreigeRollNo = (TextBox)itemsGridView.Rows[row].FindControl("txtGreigeRollNo");
            TextBox txtBeam = (TextBox)itemsGridView.Rows[row].FindControl("txtBeam");
            TextBox txtDefectPoint = (TextBox)itemsGridView.Rows[row].FindControl("txtDefectPoint");
            TextBox txtActualWidth = (TextBox)itemsGridView.Rows[row].FindControl("txtActualWidth");
            TextBox txtPiece = (TextBox)itemsGridView.Rows[row].FindControl("txtPiece");
            TextBox txtPWLength = (TextBox)itemsGridView.Rows[row].FindControl("txtPWLength");
            TextBox txtQuantity = (TextBox)itemsGridView.Rows[row].FindControl("txtQuantity");
            TextBox txtOperatorName = (TextBox)itemsGridView.Rows[row].FindControl("txtOperatorName");
            TextBox txtMCNo = (TextBox)itemsGridView.Rows[row].FindControl("txtMCNo");
            TextBox txtFL = (TextBox)itemsGridView.Rows[row].FindControl("txtFL");
            TextBox txtPPHSY = (TextBox)itemsGridView.Rows[row].FindControl("txtPPHSY");
            TextBox txtGrossWeight = (TextBox)itemsGridView.Rows[row].FindControl("txtGrossWeight");
            TextBox txtNetWeight = (TextBox)itemsGridView.Rows[row].FindControl("txtNetWeight");
            TextBox txtProblemCause = (TextBox)itemsGridView.Rows[row].FindControl("txtProblemCause");
            TextBox txtRemarks = (TextBox)itemsGridView.Rows[row].FindControl("txtRemarks");

            //Data set in QaStockReceiveDetailDao model 
            adetail.StockReceiveMasterId = Convert.ToInt32(MasterId);
            adetail.StockReceiveDetailId = Convert.ToInt32(DetailsId);
            adetail.FabricSet = string.IsNullOrEmpty(txtFabricSet.Text) ? "" : txtFabricSet.Text.Trim();
            adetail.FL = string.IsNullOrEmpty(txtFL.Text) ? 0 : Convert.ToInt32(txtFL.Text.Trim());
            adetail.QALocationId = string.IsNullOrEmpty(ddlLocationName.SelectedValue) ? 0 : Convert.ToInt32(ddlLocationName.SelectedValue);
            adetail.Beam = string.IsNullOrEmpty(txtBeam.Text.Trim()) ? 0 : Convert.ToInt32(txtBeam.Text);
            adetail.GreigeRollNo = string.IsNullOrEmpty(txtGreigeRollNo.Text.Trim()) ? "" : txtGreigeRollNo.Text.Trim();
            adetail.DefectPoint = string.IsNullOrEmpty(txtDefectPoint.Text.Trim()) ? 0 : Convert.ToDecimal(txtDefectPoint.Text);
            adetail.ActualWidth = string.IsNullOrEmpty(txtActualWidth.Text) ? 0 : Convert.ToDecimal(txtActualWidth.Text);
            adetail.Piece =  string.IsNullOrEmpty(txtPiece.Text) ? 0 : Convert.ToDecimal(txtPiece.Text);
            adetail.PWLength = string.IsNullOrEmpty(txtPWLength.Text) ? "" : txtPWLength.Text;
            adetail.OperatorName = string.IsNullOrEmpty(txtOperatorName.Text) ? "" : txtOperatorName.Text;
            adetail.Quantity = string.IsNullOrEmpty(txtQuantity.Text) ? 0 : Convert.ToDecimal(txtQuantity.Text);
            adetail.PPHSY = string.IsNullOrEmpty(txtPPHSY.Text) ? 0 : Convert.ToDecimal(txtPPHSY.Text);
            adetail.MCNo = string.IsNullOrEmpty(txtMCNo.Text) ? 0 : Convert.ToInt32(txtMCNo.Text);
            adetail.GrossWeight = string.IsNullOrEmpty(txtGrossWeight.Text) ? 0 : Convert.ToDecimal(txtGrossWeight.Text);
            adetail.NetWeight = string.IsNullOrEmpty(txtNetWeight.Text) ? 0 : Convert.ToDecimal(txtNetWeight.Text);
            adetail.ProblemCause = string.IsNullOrEmpty(txtProblemCause.Text) ? "" : txtProblemCause.Text;
            adetail.Remarks = string.IsNullOrEmpty(txtRemarks.Text) ? "" : txtRemarks.Text;

            bool status = aDal.DetailsUpdate(adetail);

            if (status)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "alert",
                    "alert('Operation Successfully Done...');window.location ='QAStockReceiveView.aspx';",
                    true);

            }
        }

        if (e.CommandName == "DeleteData")
        {
            QaStockReceiveMasterDao aInfo = new QaStockReceiveMasterDao();

            aInfo.StockReceiveMasterId = Int32.Parse(e.CommandArgument.ToString());

            if (aDal.DeleteQAStockMasterById(aInfo))
            {
                ShowMessageBox("Successfully Deleted !!");
            }

            this.LoadList(itemsGridView);
        }


        if (e.CommandName == "PrintData")
        {
            string ID = e.CommandArgument.ToString();

            if (ID != "")
            {
                string param = "";
                param = param + " AND QARM.StockReceiveMasterId = " + ID;
                PopUp("QASR", param);
            }

        }
    }


    private void PopUp(string rpt, string param)
    {
        string url = "../FinishedGoodInventory_RPTView/FinishedGoodReportViewer.aspx?rptType=" + rpt + "&rpt=" + param;
        string fullURL = "window.open('" + url + "', '_blank', 'height=600,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);

    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void loadGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        itemsGridView.PageIndex = e.NewPageIndex;
        this.LoadList(itemsGridView);
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {
        LoadList(itemsGridView);
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        ddlBuyer.SelectedIndex = 0;
        ddlVendor.SelectedIndex = 0;
        ddlShift.SelectedIndex = 0;
        ddlPi.SelectedIndex = 0;
        ddlRcvType.SelectedIndex = 0;
        txtProductionFromDate.Text = "";
        txtProductionToDate.Text = "";
        fromDateTextBox.Text = "";
        toDateTextBox.Text = "";
        ddlfabric.SelectedIndex = 0;
        ddlRollWise.SelectedIndex = 0;
        ddlSet.SelectedIndex = 0;
        // LoadDropDownlist();
        //  LoadList(itemsGridView);
    }

    protected void ddlBuyer_OnSelectedIndexChanged(object sender, EventArgs e)
    {
       // LoadPiNo();
        LoadList(itemsGridView);
    }

    protected void ddlVendor_OnSelectedIndexChanged(object sender, EventArgs e)
    {
       // LoadPiNo();
        LoadList(itemsGridView);
    }

    protected void ddlPi_OnSelectedIndexChanged(object sender, EventArgs e)
    {
       // LoadList(itemsGridView);

       FabricLoad();
    }

    private void FabricLoad()
    {
        using (DataTable data = aDal.LoadSisCodeByPiD(Convert.ToInt32(ddlPi.SelectedValue)))
        {
            ddlfabric.DataSource = data;
            ddlfabric.DataValueField = "FabricName";
            ddlfabric.DataTextField = "FabricDescription";
            ddlfabric.DataBind();
            ddlfabric.Items.Insert(0, new ListItem("Select from list", String.Empty));
        }
    }


    protected void ddlWidth_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadList(itemsGridView);
    }

    private void LoadPiNo()
    {
        string pram = "";

        if (ddlBuyer.SelectedValue != "")
        {
            pram = pram + " AND PFI.BuyerId = '" + ddlBuyer.SelectedValue + "'";
        }

        if (ddlVendor.SelectedValue != "")
        {
            pram = pram + " AND PFI.VendorId = '" + ddlVendor.SelectedValue + "'";
        }

        aDal.LoadPiNo(ddlPi, pram);
    }

    
    public override void VerifyRenderingInServerForm(Control control)
    {
        //required to avoid the runtime error "  
        //Control 'GridView1' of type 'GridView' must be placed inside a form tag with runat=server."  
    }
   

    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        if (itemsGridView.Rows.Count > 0)
        {

            if (ddlfabric.SelectedValue != "")
            {
                string attachment = "attachment; filename=Production_QAStockReceive_Report.xls";

                //  Response.AddHeader("content-disposition", "attachment;filename=SqlExport.csv")

                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);

                //QaStockInformation
                DataTable aTable = aDal.GetStockReceiveInfo(GenerateParameter());

                for (int i = 0; i < aTable.Rows.Count; i++)
                {
                    aTable.Rows[i]["FabricSetNumber"] = "'" + aTable.Rows[i]["FabricSetNumber"].ToString();
                }

                aTable.Columns.Add("ELITEA", typeof(System.Decimal));
                aTable.Columns.Add("ELITEA1", typeof(System.Decimal));
                aTable.Columns.Add("SHADY", typeof(System.Decimal));
                aTable.Columns.Add("CUTPIECE", typeof(System.Decimal));
                DataTable aTable1 = aDal.GetStockReceiveCheck(ddlfabric.SelectedValue);

                ArrayList alist = new ArrayList();

                for (int i = 0; i < aTable1.Rows.Count; i++)
                {
                    alist.Add(int.Parse(aTable1.Rows[i]["FabricId"].ToString()));
                }

                for (int i = 0; i < aTable.Rows.Count; i++)
                {

                    if (aTable.Rows[i]["FabricCodeId"].ToString() == alist[0].ToString())
                    {
                        aTable.Rows[i]["ELITEA"] = aTable.Rows[i]["Quantity"].ToString();
                    }

                    if (aTable.Rows[i]["FabricCodeId"].ToString() == alist[1].ToString())
                    {
                        aTable.Rows[i]["ELITEA1"] = aTable.Rows[i]["Quantity"].ToString();
                    }

                    if (aTable.Rows[i]["FabricCodeId"].ToString() == alist[2].ToString())
                    {
                        aTable.Rows[i]["SHADY"] = aTable.Rows[i]["Quantity"].ToString();
                    }

                    if (aTable.Rows[i]["FabricCodeId"].ToString() == alist[3].ToString())
                    {
                        aTable.Rows[i]["CUTPIECE"] = aTable.Rows[i]["Quantity"].ToString();
                    }

                }


                GridView1.DataSource = aTable;
                GridView1.DataBind();



                // Create a form to contain the grid  
                HtmlForm frm = new HtmlForm();
                GridView1.Parent.Controls.Add(frm);
                //frm.Attributes["runat"] = "server";
                //frm.Controls.Add(loadGridView);
                //frm.RenderControl(htw);

                GridView1.HeaderRow.Style.Add("background-color", "#E5EEF1");

             //   GridView1

                // Set background color of each cell of GridView1 header row
                foreach (TableCell tableCell in GridView1.HeaderRow.Cells)
                {
                    tableCell.Style["background-color"] = "#E5EEF1";
                }

                // Set background color of each cell of each data row of GridView1
                foreach (GridViewRow gridViewRow in GridView1.Rows)
                {

                    gridViewRow.BackColor = System.Drawing.Color.White;

                    foreach (TableCell gridViewRowTableCell in gridViewRow.Cells)
                    {
                        gridViewRowTableCell.Style["background-color"] = "#FFFFFF";
                    }
                }

                GridView1.RenderControl(htw);
                string headerTable = @"
                <style>              
                table, th, td {
                 border: .1em solid black !important;
                 border-collapse: collapse !important;
                }
                th, td {                 
                  text-align: center;
                }               
                </style>                
                ";

                HttpContext.Current.Response.Write(headerTable);
                HttpContext.Current.Response.Write("<table style='width:100%;'>");
                HttpContext.Current.Response.Write("<tr>");
                HttpContext.Current.Response.Write("<th>FABRIC CODE: </th>");
                HttpContext.Current.Response.Write("<td colspan='5'> " + aTable.Rows[0]["FabricName"].ToString() + " </td>");
                HttpContext.Current.Response.Write("</tr>");

                HttpContext.Current.Response.Write("<tr>");
                HttpContext.Current.Response.Write("<th>FABRIC COLOR: </th>");
                HttpContext.Current.Response.Write("<td colspan='5'> " + aTable.Rows[0]["ColorName"].ToString() + " </td>");
                HttpContext.Current.Response.Write("</tr>");

                HttpContext.Current.Response.Write("<tr>");
                HttpContext.Current.Response.Write("<th>FABRIC WEIGHT/ OUNCE: </th>");
                HttpContext.Current.Response.Write("<td colspan='5'> " + aTable.Rows[0]["RequiredOunceYard"].ToString() + " </td>");
                HttpContext.Current.Response.Write("</tr>");

                HttpContext.Current.Response.Write("<tr>");
                HttpContext.Current.Response.Write("<th rowspan='2'>FABRIC SHRINKAGE:</th>");
                HttpContext.Current.Response.Write("<th colspan='2' style='text-align: center;'>LENGTH</th>");
                HttpContext.Current.Response.Write("<th colspan='3' style='text-align: center;'>WIDTH</th>");
                HttpContext.Current.Response.Write("</tr>");

                HttpContext.Current.Response.Write("<tr>");
                HttpContext.Current.Response.Write("<td style='text-align: center;'> " + aTable.Rows[0]["ShrinkageLengthMin"].ToString() + " </td>");
                HttpContext.Current.Response.Write("<td style='text-align: center;'> " + aTable.Rows[0]["ShrinkageLengthMax"].ToString() + " </td>");
                HttpContext.Current.Response.Write("<td style='text-align: center;'> " + aTable.Rows[0]["ShrinkageWidthMin"].ToString() + "</td>");
                HttpContext.Current.Response.Write("<td style='text-align: center;' colspan='2'> " + aTable.Rows[0]["ShrinkageWidthMax"].ToString() + "</td>");
                HttpContext.Current.Response.Write("</tr>");
                HttpContext.Current.Response.Write("</table>");
                HttpContext.Current.Response.Write("<br>");

                Response.Write(sw.ToString());
                Response.End();
            }
            else
            {
                ShowMessageBox("Please Select Fabric");
            }


        }
        else
        {
            ShowMessageBox("No Data Found!!");
        }

    }




    public static DataTable convertStringToDataTable(string data)
    {
        DataTable dataTable = new DataTable();
        bool columnsAdded = false;
        foreach (string row in data.Split('-'))
        {
            
        }
        return dataTable;
    }




    //Extra
    private void ExportGridToExcel()
    {

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.csv");
        Response.Charset = "";
        Response.ContentType = "text/csv";

        //To Export all pages.
        GridView1.AllowPaging = false;


        DataTable aTable = aDal.GetStockReceiveInfo(GenerateParameter());

        aTable.Columns.Add("ELITEA", typeof(System.Decimal));
        aTable.Columns.Add("ELITEA1", typeof(System.Decimal));
        aTable.Columns.Add("SHADY", typeof(System.Decimal));
        aTable.Columns.Add("CUTPIECE", typeof(System.Decimal));
        DataTable aTable1 = aDal.GetStockReceiveCheck(ddlfabric.SelectedValue);

        ArrayList alist = new ArrayList();

        for (int i = 0; i < aTable1.Rows.Count; i++)
        {
            alist.Add(int.Parse(aTable1.Rows[i]["FabricId"].ToString()));
        }

        for (int i = 0; i < aTable.Rows.Count; i++)
        {

            if (aTable.Rows[i]["FabricCodeId"].ToString() == alist[0].ToString())
            {
                aTable.Rows[i]["ELITEA"] = aTable.Rows[i]["Quantity"].ToString();
            }

            if (aTable.Rows[i]["FabricCodeId"].ToString() == alist[1].ToString())
            {
                aTable.Rows[i]["ELITEA1"] = aTable.Rows[i]["Quantity"].ToString();
            }

            if (aTable.Rows[i]["FabricCodeId"].ToString() == alist[2].ToString())
            {
                aTable.Rows[i]["SHADY"] = aTable.Rows[i]["Quantity"].ToString();
            }

            if (aTable.Rows[i]["FabricCodeId"].ToString() == alist[3].ToString())
            {
                aTable.Rows[i]["CUTPIECE"] = aTable.Rows[i]["Quantity"].ToString();
            }

        }

        GridView1.DataSource = aTable;
        GridView1.DataBind();


        

        StringBuilder sb = new StringBuilder();
        foreach (TableCell cell in GridView1.HeaderRow.Cells)
        {
            //Append data with separator.
            sb.Append(cell.Text + ',');
        }
        //Append new line character.
        sb.Append("\r\n");

        foreach (GridViewRow row in GridView1.Rows)
        {
            foreach (TableCell cell in row.Cells)
            {
                //Append data with separator.
                sb.Append(cell.Text + ',');
            }
            //Append new line character.
            sb.Append("\r\n");
        }





        //   GridView1.RenderControl(htw);
                string headerTable = @"
                <style>              
                table, th, td {
                 border: .1em solid black !important;
                 border-collapse: collapse !important;
                }
                th, td {
                 
                  text-align: center;
                }               
                </style>
                
                ";

               // HttpContext.Current.Response.Write(headerTable);
                HttpContext.Current.Response.Write("<table style='width:100%;'>");
                HttpContext.Current.Response.Write("<tr>");
                HttpContext.Current.Response.Write("<th>FABRIC CODE: </th>");
                HttpContext.Current.Response.Write("<td colspan='5'> " + aTable.Rows[0]["FabricName"].ToString() + " </td>");
                HttpContext.Current.Response.Write("</tr>");

                HttpContext.Current.Response.Write("<tr>");
                HttpContext.Current.Response.Write("<th>FABRIC COLOR: </th>");
                HttpContext.Current.Response.Write("<td colspan='5'> " + aTable.Rows[0]["ColorName"].ToString() + " </td>");
                HttpContext.Current.Response.Write("</tr>");

                HttpContext.Current.Response.Write("<tr>");
                HttpContext.Current.Response.Write("<th>FABRIC WEIGHT/ OUNCE: </th>");
                HttpContext.Current.Response.Write("<td colspan='5'> " + aTable.Rows[0]["RequiredOunceYard"].ToString() + " </td>");
                HttpContext.Current.Response.Write("</tr>");

                HttpContext.Current.Response.Write("<tr>");
                HttpContext.Current.Response.Write("<th rowspan='2'>FABRIC SHRINKAGE:</th>");
                HttpContext.Current.Response.Write("<th colspan='2' style='text-align: center;'>LENGTH</th>");
                HttpContext.Current.Response.Write("<th colspan='3' style='text-align: center;'>WIDTH</th>");
                HttpContext.Current.Response.Write("</tr>");

                HttpContext.Current.Response.Write("<tr>");
                HttpContext.Current.Response.Write("<td style='text-align: center;'> " + aTable.Rows[0]["ShrinkageLengthMin"].ToString() + " </td>");
                HttpContext.Current.Response.Write("<td style='text-align: center;'> " + aTable.Rows[0]["ShrinkageLengthMax"].ToString() + " </td>");
                HttpContext.Current.Response.Write("<td style='text-align: center;'> " + aTable.Rows[0]["ShrinkageWidthMin"].ToString() + "</td>");
                HttpContext.Current.Response.Write("<td style='text-align: center;' colspan='2'> " + aTable.Rows[0]["ShrinkageWidthMax"].ToString() + "</td>");
                HttpContext.Current.Response.Write("</tr>");
                HttpContext.Current.Response.Write("</table>");
                HttpContext.Current.Response.Write("<br>");

        Response.Output.Write(sb.ToString());
        Response.Flush();
        Response.End();
       


    } 

    protected void gv_DocumentUpload_PreRender(object sender, EventArgs e)
    {
        GridView gv = (GridView)sender;

        if ((gv.ShowHeader == true && gv.Rows.Count > 0)
            || (gv.ShowHeaderWhenEmpty == true))
        {
            //Force GridView to use <thead> instead of <tbody> - 11/03/2013 - MCR.
            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
   

}