﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/NewMasterPage.master" AutoEventWireup="true" CodeFile="StockShadeGradeUpdate.aspx.cs" Inherits="FinishedGoodInventory_UI_StockShadeGradeUpdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
    
  <%--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js" integrity="sha512-rMGGF4wg1R73ehtnxXBt5mbUfN9JUJwbk21KMlnLZDJh7BkPmeovBuddZCENJddHYYMkCh9hPFnPmS9sspki8g==" crossorigin="anonymous"></script>
    
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css" integrity="sha512-yVvxUQV0QESBt1SyZbNJMAwyKvFTLMyXSyBHDO4BG5t7k/Lw34tyqlSDlKIrIENIzCl+RVUNjmCPG+V/GMesRw==" crossorigin="anonymous" />
    
    <%--  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.3.28/jquery.sumoselect.min.js" integrity="sha512-GuYHN+OQvBwz9zmh4vLgZ8ICB2uIaZjrCLXeXumDV/u/xgRJEmWlMUzN1RUZgHB9O0llSTnTSS013N+1NK6wkQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.3.28/sumoselect.css" integrity="sha512-xE1tf9Jz0Zvmo6PnZXLRKJnLzCdP9/J4fgablbwouEHcznYd1uCMYRLkwtgxVeqwYJS31VrslRhoTdGx7p8EGg==" crossorigin="anonymous" referrerpolicy="no-referrer" />--%>
    <script src="jquery.sumoselect.min.js"></script>
    <link href="sumoselect.css" rel="stylesheet" />--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <style>
        .V1 {
            position: relative;
            display: inline-block;
            width: 50%;
        }


        #ContentPlaceHolder1_cbxShadeGrades_0,
        #ContentPlaceHolder1_cbxShadeGrades_1,
        #ContentPlaceHolder1_cbxShadeGrades_2,
        #ContentPlaceHolder1_cbxShadeGrades_3,
        #ContentPlaceHolder1_cbxShadeGrades_4,
        #ContentPlaceHolder1_cbxShadeGrades_5,
        #ContentPlaceHolder1_cbxShadeGrades_6, 
        #ContentPlaceHolder1_cbxShadeGrades_7, 
        #ContentPlaceHolder1_cbxShadeGrades_8, 
        #ContentPlaceHolder1_cbxShadeGrades_9 {
            margin-right: 5px !important;
            margin-left: 20px !important;
            
        }

        #ContentPlaceHolder1_cbxRemarks_0,
        #ContentPlaceHolder1_cbxRemarks_1,
        #ContentPlaceHolder1_cbxRemarks_2,
        #ContentPlaceHolder1_cbxRemarks_3,
        #ContentPlaceHolder1_cbxRemarks_4,
        #ContentPlaceHolder1_cbxRemarks_5,
        #ContentPlaceHolder1_cbxRemarks_6, 
        #ContentPlaceHolder1_cbxRemarks_7, 
        #ContentPlaceHolder1_cbxRemarks_8, 
        #ContentPlaceHolder1_cbxRemarks_9,
        #ContentPlaceHolder1_cbxRemarks_10,
        #ContentPlaceHolder1_cbxRemarks_11,
        #ContentPlaceHolder1_cbxRemarks_12,
        #ContentPlaceHolder1_cbxRemarks_13,
        #ContentPlaceHolder1_cbxRemarks_14,
        #ContentPlaceHolder1_cbxRemarks_15,
        #ContentPlaceHolder1_cbxRemarks_16, 
        #ContentPlaceHolder1_cbxRemarks_17, 
        #ContentPlaceHolder1_cbxRemarks_18, 
        #ContentPlaceHolder1_cbxRemarks_19,
        #ContentPlaceHolder1_cbxRemarks_20  
        {
            margin-right: 5px !important;
            margin-left: 5px !important;
            
        }

        #ContentPlaceHolder1_cbxRolls_0,
        #ContentPlaceHolder1_cbxRolls_1,
        #ContentPlaceHolder1_cbxRolls_2,
        #ContentPlaceHolder1_cbxRolls_3,
        #ContentPlaceHolder1_cbxRolls_4,
        #ContentPlaceHolder1_cbxRolls_5,
        #ContentPlaceHolder1_cbxRolls_6, 
        #ContentPlaceHolder1_cbxRolls_7, 
        #ContentPlaceHolder1_cbxRolls_8, 
        #ContentPlaceHolder1_cbxRolls_9,
        #ContentPlaceHolder1_cbxRolls_10,
        #ContentPlaceHolder1_cbxRolls_11,
        #ContentPlaceHolder1_cbxRolls_12,
        #ContentPlaceHolder1_cbxRolls_13,
        #ContentPlaceHolder1_cbxRolls_14,
        #ContentPlaceHolder1_cbxRolls_15,
        #ContentPlaceHolder1_cbxRolls_16, 
        #ContentPlaceHolder1_cbxRolls_17, 
        #ContentPlaceHolder1_cbxRolls_18, 
        #ContentPlaceHolder1_cbxRolls_19,
        #ContentPlaceHolder1_cbxRolls_20,
        #ContentPlaceHolder1_cbxRolls_30,
        #ContentPlaceHolder1_cbxRolls_21,
        #ContentPlaceHolder1_cbxRolls_22,
        #ContentPlaceHolder1_cbxRolls_23,
        #ContentPlaceHolder1_cbxRolls_24,
        #ContentPlaceHolder1_cbxRolls_25,
        #ContentPlaceHolder1_cbxRolls_26, 
        #ContentPlaceHolder1_cbxRolls_27, 
        #ContentPlaceHolder1_cbxRolls_28, 
        #ContentPlaceHolder1_cbxRolls_29  
        {
            margin-right: 5px !important;
            margin-left: 5px !important;
            
        }

    </style>



    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div id="popDiv">
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Stock Shade Grade </div>

                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">

                            <div class="card border-top border-0 border-4 border-success">
                            <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-search me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Search Options</h5>
                                        </div>
                                        <hr>

                                        <script type="text/javascript">
                                            function pageLoad() {
                                                $('.datepicker').pickadate({
                                                    selectMonths: true,
                                                    selectYears: true
                                                });
                                                $('.mySelect2').select2({
                                                    theme: 'bootstrap4',
                                                    width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                                    placeholder: $(this).data('placeholder'),
                                                    allowClear: Boolean($(this).data('allow-clear')),
                                                });
                                            }
                                        </script>
                                    
                                    
                                    <div class="row">
                                        <div class="col-md-6">


                                    <div class="row mt-1">

                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label  class="col-sm-4 col-form-label">PI Number</label>
                                                <div class="col-sm-7">

                                                    <asp:DropDownList runat="server" ID="ddlProformaNum" AutoPostBack="True" OnSelectedIndexChanged="ddlProformaNum_OnSelectedIndexChanged"  CssClass="form-control form-control-sm mySelect2" />
                                                 
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-1">
                                       
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label  class="col-sm-4 col-form-label">Fabric /SIS Code</label>
                                                <div class="col-sm-7">

                                                    <asp:DropDownList runat="server" ID="ddlFabric" AutoPostBack="False"     OnSelectedIndexChanged="ddlFabric_OnSelectedIndexChanged" CssClass="form-control form-control-sm mySelect2" />
                                                 
                                                </div>
                                                
                                            </div>

                                        </div>
                                    </div>
                                            
                                          <%--  <div class="row mt-1">
                                       
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label  class="col-sm-4 col-form-label">Category</label>
                                                        <div class="col-sm-7">

                                                            <asp:RadioButtonList id="cbxCategory" RepeatColumns="2" RepeatDirection="Horizontal" runat="server">
                                                                <asp:ListItem>Elite-A</asp:ListItem>
                                                                <asp:ListItem>Elite-A1</asp:ListItem>
                                                                <asp:ListItem>Shady</asp:ListItem>
                                                                <asp:ListItem>Insta</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                 
                                                        </div>
                                                
                                                    </div>

                                                </div>
                                            </div>--%>
                                            
                                            
                                            

                                    <div class="row mt-1">
                                       
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Order By</label>
                                                <div class="col-sm-7">

                                                    <asp:DropDownList ID="ddl_OrderBy"

                                                                      runat="server" CssClass="form-control form-control-sm mySelect2">

                                                        <asp:ListItem Value="ASC"> Ascending </asp:ListItem>
                                                        <asp:ListItem Value="DESC"> Descending </asp:ListItem>
                                                    </asp:DropDownList>
                                                 
                                                </div>
                                                
                                            </div>

                                        </div>
                                        
                                        
                                        
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Shade Grade</label>
                                                <div class="col-sm-7">

                                                    <asp:DropDownList ID="ddlShadeGrade" runat="server" CssClass="form-control form-control-sm mySelect2">
                                                        <asp:ListItem Value="">select from list</asp:ListItem>
                                                        <asp:ListItem Value="Without"> Without Shade </asp:ListItem>
                                                        <asp:ListItem Value="With"> With Shade </asp:ListItem>
                                                        <asp:ListItem Value="All"> All </asp:ListItem>
                                                    </asp:DropDownList>
                                                 
                                                </div>
                                                
                                            </div>

                                        </div>
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                    

                                    </div>
                                        

                                   <%-- <div class="row mt-1">
                                
                                    <div class="col-md-12">

                                        <div class="form-group row">
                                      
                                            <label  class="col-sm-4 col-form-label">Roll Name</label>
                                            
                                            <div class="col-sm-7">
                                                         <asp:ListBox runat="server"   ID="lbEmployee" CssClass="form-control form-control-sm selectme" SelectionMode="Multiple">
                                                                                </asp:ListBox>
                                                
                                            </div>
                                                                      <script type="text/javascript">
                                                                          $(document).ready(function () {

                                                                              $(<%=lbEmployee.ClientID%>).SumoSelect({ selectAll: true });


                                                                              Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
                                                                              function EndRequestHandler(sender, args) {

                                                                                  $(<%=lbEmployee.ClientID%>).SumoSelect({ selectAll: true });
                                                                              }
                                                                          });


    </script>
                                 
                                    
                                    
                                                                           <script type="text/javascript">
                                                                               function pageLoad() {



                                                                                   $('.selectme').chosen({ disable_search_threshold: 5, search_contains: true });
                                                                                 



                                                                               }
                                                        </script>
                                            
                                                    <script>
                                                        Sys.Application.add_load(lbEmployee);
                    </script>

                                          

                                        </div>
                                    </div>
                                </div>--%>
                                    
                                    
                                        </div>
                                        <div class="col-md-6">
                                            
                                            <div class="row mt-1">
                                               
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label  class="col-sm-4 col-form-label">Production From Date</label>
                                                        <div class="col-sm-7">

                                                            <asp:TextBox ID="txtProductionFromDate" runat="server" CssClass="form-control form-control-sm  datepicker"></asp:TextBox>
                                                 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mt-1">
                                              
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label  class="col-sm-4 col-form-label">Production From Date</label>
                                                        <div class="col-sm-7">

                                                            <asp:TextBox ID="txtProductionToDate" runat="server" CssClass="form-control form-control-sm  datepicker"></asp:TextBox>
                                                 
                                                        </div>
                                                
                                                    </div>

                                                </div>
                                            </div>
                                            
                                            
                                            
                                            <div class="row mt-1">
                                           
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label  class="col-sm-4 col-form-label">Entry From Date</label>
                                                        <div class="col-sm-7">

                                                            <asp:TextBox ID="txtEntryFromDate" runat="server" CssClass="form-control form-control-sm  datepicker"></asp:TextBox>
                                                 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mt-1">
                                               
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label for="" class="col-sm-4 col-form-label">Entry To Date</label>
                                                        <div class="col-sm-7">

                                                            <asp:TextBox ID="txtEntryToDate" runat="server" CssClass="form-control form-control-sm  datepicker"></asp:TextBox>
                                                 
                                                        </div>
                                                
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    
                                    
                                    

                                        <div class="row mb-1" runat="server" Visible="False">

                                            <div class="col-3">
                                                <div class="card" style="box-shadow: none !important;">
                                                    <div class="border p-2 rounded">
                                                        <div class="card-title">

                                                            <h6 class="mb-0 text-secondary" style="width: 100% !important">Shade Grades</h6>
                                                            <hr />

                                                        </div>

                                                        <div class="card-body" style="overflow: scroll !important; max-height: 130px !important" >
                                                            <div class="row mb-1">
                                                                <div style="display: inline !important">
                                                                    <asp:CheckBox ID="cbxShadeAll" style="margin-left: 20px !important" runat="server" AutoPostBack="True" OnCheckedChanged="cbxShadeAll_CheckedChanged" /><span>&nbsp;<b>Check All</b></span>
                                                                </div>                  
                                                            </div>
                                                            <div class="row mb-1"> 
                                                                <asp:CheckBoxList ID="cbxShadeGrades" AutoPostBack="True" CellPadding="5" CellSpacing="5" OnSelectedIndexChanged="cbxShadeGrades_OnSelectedIndexChanged"
                                                                    RepeatColumns="4" RepeatDirection="Horizontal" RepeatLayout="Flow" TextAlign="Right" runat="server">
                                                                </asp:CheckBoxList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-4">
                                                <div class="card" style="box-shadow: none !important;">
                                                    <div class="border p-2 rounded">
                                                    <div class="card-title">
                                                        
                                                            <h6 class="mb-0 text-secondary" style="width: 100% !important">Remarks</h6>
                                                            <hr />
                                                       
                                                    </div>
                                                    
                                                    <div class="card-body" style="overflow: scroll !important; max-height: 130px !important">
                                                        <div class="row mb-1">
                                                                <div style="display: inline !important">
                                                                    <asp:CheckBox ID="cbxRemarksAll" style="margin-left: 5px !important" runat="server" AutoPostBack="True" OnCheckedChanged="cbxRemarksAll_CheckedChanged" /><span>&nbsp;<b>Check All</b></span>
                                                                </div>                  
                                                            </div>
                                                            <div class="row mb-1">
                                                                <asp:CheckBoxList ID="cbxRemarks" AutoPostBack="True" CellPadding="5" CellSpacing="5" OnSelectedIndexChanged="cbxRemarks_OnSelectedIndexChanged"
                                                                    RepeatColumns="3" RepeatDirection="Vertical" RepeatLayout="Flow" TextAlign="Right" runat="server">
                                                                </asp:CheckBoxList>
                                                            </div>
                                                        </div>
                                                     </div>
                                                </div>
                                            </div>

                                            <div class="col-5">
                                                <div class="card" style="box-shadow: none !important;">
                                                    <div class="border p-2 rounded">
                                                    <div class="card-title">
                                                        
                                                            <h6 class="mb-0 text-secondary" style="width: 100% !important">Rolls</h6>
                                                            <hr />
                                                        </div>
                                                    
                                                    
                                                    <div class="card-body" style="overflow: scroll !important; max-height: 130px !important">
                                                        <div class="row mb-1">
                                                                <div style="display: inline !important">
                                                                    <asp:CheckBox ID="cbxRollsAll" style="margin-left: 5px !important" runat="server" AutoPostBack="True" OnCheckedChanged="cbxRollsAll_CheckedChanged" /><span >&nbsp;<b>Check All</b></span>
                                                                </div>                  
                                                            </div>
                                                            <div class="row mb-1">
                                                                <asp:CheckBoxList ID="cbxRolls" AutoPostBack="True" CellPadding="5" CellSpacing="5"
                                                                    RepeatColumns="6" RepeatDirection="Vertical" RepeatLayout="Flow" TextAlign="Right" runat="server">
                                                                </asp:CheckBoxList>
                                                            </div>
                                                        </div>
                                                        
                                                        </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row mb-1" runat="server" Visible="False">

                                            <div class="col-6">
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Fabric Code: </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList runat="server" ID="dldlFabric" AutoPostBack="True" OnSelectedIndexChanged="ddlFabric_OnSelectedIndexChanged" CssClass="form-control form-control-sm mySelect2" />
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Set No: </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList runat="server" ID="ddlSet" AutoPostBack="True" OnSelectedIndexChanged="ddlSet_OnSelectedIndexChanged" CssClass="form-control form-control-sm mySelect2" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Roll No: </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList runat="server" ID="ddlRoll" AutoPostBack="True" OnSelectedIndexChanged="ddlRoll_OnSelectedIndexChanged" CssClass="form-control form-control-sm mySelect2" />
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Order By: </label>
                                                    <div class="col-sm-8">

                                                        <asp:DropDownList ID="ddlOrderBy"
                                                            AutoPostBack="True"
                                                            OnSelectedIndexChanged="ddlOrderBy_OnSelectedIndexChanged"
                                                            runat="server" CssClass="form-control form-control-sm mySelect2">

                                                            <asp:ListItem Value="ASC"> Ascending </asp:ListItem>
                                                            <asp:ListItem Value="DESC"> Descending </asp:ListItem>
                                                        </asp:DropDownList>

                                                    </div>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>


                                        <hr />

                                        <div class="row mb-4">

                                            <div class="col-4"></div>
                                            <div class="col-5" style="">
                                                <asp:LinkButton runat="server" class="btn btn-outline-primary" ID="btnSearch" OnClick="searchButton_Click">                                            
                                                            <i class="fa fa-search-plus"></i><span style="font-weight: bold !important">Search Info</span>
                                                </asp:LinkButton>
                                                <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="LinkButton2" OnClick="resetButton_Click">                                            
                                                            <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Info</span>
                                                </asp:LinkButton>
                                            </div>
                                            <div class="col-3"></div>

                                        </div>
                                        <hr />


                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="card-title d-flex align-items-center text-center">


                                                    <div>
                                                        <i class="bx bxs-file me-1 font-22 text-secondary"></i>
                                                    </div>
                                                    <h5 class="mb-0 text-secondary ">Roll Description</h5>

                                                </div>

                                            </div>

                                            <div class="col-md-6">

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Shade Grade: </label>
                                                    <div class="col-sm-6">
                                                        <asp:DropDownList runat="server" ID="ddlShade" CssClass="form-control form-control-sm mySelect2" />
                                                    </div>

                                                    <div class="col-sm-2" style="padding-bottom: 5px">

                                                        <asp:LinkButton runat="server" class="btn btn-primary btn-sm" ID="BtnApply" OnClick="BtnApply_OnClick">

                                                        Apply

                                                        </asp:LinkButton>

                                                    </div>
                                                </div>


                                            </div>
                                        </div>


                                        <hr>

                                        <div class="row mb-4">


                                            <div id="maingridview" style="text-align: center; height: auto; overflow: scroll; width: 100%; overflow-y: scroll; overflow-x: scroll;">
                                                <asp:GridView ID="productGridView" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-condensed custom-table-style"
                                                    AllowPaging="True" OnPageIndexChanging="loadGridView_PageIndexChanging" PageSize="15"
                                                     EmptyDataText="There are no data records to display.">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="# SL">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                                <asp:HiddenField runat="server" ID="hfStockReceiveMasterId" Value='<%#Eval("StockReceiveMasterId") %>' />
                                                                <asp:HiddenField runat="server" ID="hfStockReceiveDetailId" Value='<%#Eval("StockReceiveDetailId") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="FabricName" HeaderText="Fabric / SIS Code" />
                                                        <asp:BoundField DataField="FabricSet" HeaderText="Set" />
                                                        <asp:BoundField DataField="Beam" HeaderText="Beam" />
                                                        <asp:BoundField DataField="GreigeRollNo" HeaderText="Greige Roll No" />
                                                        <asp:BoundField DataField="RollNo" HeaderText="Roll No" />
                                                        <asp:BoundField DataField="DefectPoint" HeaderText="Defect Point" />
                                                        <asp:BoundField DataField="ActualWidth" HeaderText="Actual Width" />
                                                        <asp:BoundField DataField="Piece" HeaderText="Piece" />
                                                        <asp:BoundField DataField="PWLength" HeaderText="P.W Length" />
                                                        <asp:BoundField DataField="PPHSY" HeaderText="PPHSY" />
                                                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" />
                                                        <asp:BoundField DataField="LocationName" HeaderText="Current Location" />
                                                        <asp:BoundField DataField="ShadeGradeing" HeaderText="Shade Grade" />
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True" OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" AutoPostBack="True" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Shade Grade">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server"  ID="txtShadeGrade" CssClass="form-control form-control-sm"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>

                                        <div class="row mb-4">

                                            <div class="col-4"></div>
                                            <div class="col-4">
                                                <asp:LinkButton Style="margin: 0 auto !important" runat="server" class="btn btn-outline-info" ID="submitButton" OnClientClick="return confirm('Are you really aware of this operation?');" OnClick="submitButton_Click">
                                            
                                                    <i class="bx bxs-save mr-1"></i><span style="font-weight: bold !important">Update Shade Grade</span>

                                                </asp:LinkButton>
                                            </div>
                                            <div class="col-4"></div>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    

<%--







<script>
        
        $(function () {

            GetRole(0);

            

        });

        function GetRole(id) {

           
           // var urlpath = 'UserPermission.aspx/Get_Roll_All';

            var urlpath = 'StockShadeGradeUpdate.aspx/Get_Roll_All';
      
            Selec2_Multiple_DisableOption(urlpath, $('#RollNoMultiSelectId'), 'RollNo', 'RollNo', id);
            $('#RollNoMultiSelectId').select2();
        }

        function Selec2_Multiple_DisableOption(urlpath, setControlId, bindId, bindName, setId) {
        
            $.ajax({
                url: urlpath,
                dataType: 'json',
                type: "POST", contentType: "application/json; charset=utf-8",
                async: true,
                success: function (data) {
                    var result = JSON.parse(data.d);
                    alert("Hello! I am an alert box!!");
                    console.log(result);
                    setControlId.empty();
                    for (var i = 0; i < result.length; i++) {
                 
                        setControlId.append($("<option></option>").val(result[i][bindId]).html(result[i][bindName]));
                        
                    }
                },
                complete: function () {
                    if (setId == 0) {

                    } else {
                        var arr = setId.split(',');
                        setControlId.val(arr).change();
                    }

                    setControlId.select2();
                    setControlId.val(setId);
                }
            });
        }






    </script>--%>




<%--    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                    Height="120" Width="120" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>

</asp:Content>

