﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_LocationView : System.Web.UI.Page
{
    private LocationDal aDal = new LocationDal();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Get_LocationFor();
            LoadLocationInfo(itemsGridView);

        }
    }


    private void Get_LocationFor()
    {

        DataTable Dt = aDal.GetLocationFor();
        if (Dt.Rows.Count > 0)
        {
            for (int i = 0; i < Dt.Rows.Count; i++)
            {
                ListItem item = new ListItem();
                item.Value = Dt.Rows[i]["LocationForId"].ToString();
                item.Text = Dt.Rows[i]["LocationForTitle"].ToString();
                rbLocationFor.Items.Add(item);
            }
        }
    }

    private void LoadLocationInfo(GridView gridView)
    {
        gridView.DataSource = aDal.GetLocationInfo(GenerateParameter());
        gridView.DataBind();
    }

    private string GenerateParameter()
    {
        string pram = "";

        if (txtLocationName.Text != "")
        {
            pram = pram + " AND L.LocationName LIKE " + "'%'" + "+'" + txtLocationName.Text.Trim() + "'+" + "'%'";
        }

        if (rbLocationFor.SelectedValue != "")
        {
            pram = pram + " AND L.LocationFor LIKE '%" + rbLocationFor.SelectedValue + "%'";
        }


        return pram;
    }

    protected void itemsGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            Session["LocationId"] = e.CommandArgument.ToString();
            Response.Redirect("LocationEntry.aspx");
        }

        if (e.CommandName == "ActiveInactiveData")
        {
            LocationDao aInfo = new LocationDao();

            aInfo.LocationId = Int32.Parse(e.CommandArgument.ToString());

            if (aDal.ActiveInactiveLocationInfo(aInfo))
            {
                ShowMessageBox("Successfully Updated !!");
            }

            this.LoadLocationInfo(itemsGridView);
        }

        
        if (e.CommandName == "DeleteData")
        {
            LocationDao aInfo = new LocationDao();

            aInfo.LocationId = Int32.Parse(e.CommandArgument.ToString());

            if (aDal.DeleteLocationInfo(aInfo))
            {
                ShowMessageBox("Successfully Deleted !!");
            }

            this.LoadLocationInfo(itemsGridView);
        }
    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void loadGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        itemsGridView.PageIndex = e.NewPageIndex;
        this.LoadLocationInfo(itemsGridView);
    }


    protected void searchButton_Click(object sender, EventArgs e)
    {
        LoadLocationInfo(itemsGridView);
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        txtLocationName.Text = "";

        rbLocationFor.Items.Clear();
        Get_LocationFor();

    }

    protected void rbLocationFor_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadLocationInfo(itemsGridView);
    }

    protected void txtLocationName_OnTextChanged(object sender, EventArgs e)
    {
        LoadLocationInfo(itemsGridView);
    }
}