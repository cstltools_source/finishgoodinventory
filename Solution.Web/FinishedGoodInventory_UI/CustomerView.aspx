﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/NewMasterPage.master" AutoEventWireup="true" CodeFile="CustomerView.aspx.cs" Inherits="FinishedGoodInventory_UI_CustomerView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div id="popDiv">
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Buyer/Vendor Setup</div>

                        <div class="ms-auto">
                            <div class="btn-group">
                                <a href="../FinishedGoodInventory_UI/CustomerEntry.aspx" class="btn btn-sm btn-outline-info "><i class="fa fa-plus" aria-hidden="true"></i>New Entry</a>
                            </div>
                        </div>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">
                            <div class="card border-top border-0 border-4 border-success">
                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-user me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Buyer/Vendor List </h5>
                                        </div>
                                        <hr>

                                        <div class="row">
                                            <div class="col">

                                                <div class="row mb-2">
                                                    <label class="col-sm-2 col-form-label">Buyer/Vendor Name:</label>
                                                    <div class="col-sm-10">
                                                        <asp:TextBox ID="txtBuyerName" runat="server" placeholder="Enter Buyer/Vendor Name" AutoPostBack="True" OnTextChanged="txtBuyerName_OnTextChanged" CssClass="form-control"></asp:TextBox>

                                                    </div>
                                                </div>

                                                <div class="row mb-2">
                                                    <label class="col-sm-2 col-form-label">Buyer/Vendor Type:</label>
                                                    <div class="col-sm-10">
                                                        <asp:DropDownList class="form-select" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBuyerType_OnSelectedIndexChanged" ID="ddlBuyerType"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <label class="col-sm-2 col-form-label"></label>
                                                    <div class="col-sm-10">
                                                        <asp:LinkButton runat="server" class="btn btn-outline-primary" ID="submitButton" OnClick="searchButton_Click">
                                                            <i class="fa fa-search-plus"></i><span style="font-weight: bold !important">Search Information</span>
                                                        </asp:LinkButton>

                                                        <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="resetButton" OnClick="resetButton_Click">
                                                            <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Information</span>
                                                        </asp:LinkButton>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <br />

                                        <div id="MainGradeDiv" style="text-align: center; height: auto; overflow: scroll; width: auto; overflow-y: scroll; overflow-x: scroll;">
                                            <asp:GridView ID="itemsGridView" runat="server" AutoGenerateColumns="False"
                                                PageIndex="0" CssClass="table table-bordered table-condensed custom-table-style text-center" DataKeyNames="BuyerId" OnRowCommand="itemsGridView_RowCommand"
                                                AllowPaging="True" OnPageIndexChanging="loadGridView_PageIndexChanging" PageSize="10" EmptyDataText="There are no data records to display.">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="# SL">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                            <asp:HiddenField runat="server" ID="hfBuyerId" Value='<%#Eval("BuyerId") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="BuyerTypeName" HeaderText="Type" />
                                                    <asp:BoundField DataField="Category" HeaderText="Buyer/Vendor" />
                                                    <asp:BoundField DataField="BuyerCode" HeaderText="Code" />
                                                    <asp:BoundField DataField="BuyerName" HeaderText="Name" />
                                                    <asp:BoundField DataField="BuyerAddress" HeaderText="Address" />
                                                    <asp:BoundField DataField="BuyerContactNo" HeaderText="Contact No" />
                                                    <asp:BoundField DataField="BuyerEmail" HeaderText="Email" />
                                                    <asp:BoundField DataField="EntryBy" HeaderText="Entry By" />
                                                    <asp:BoundField DataField="EntryDate" HeaderText="Entry Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                    <asp:BoundField DataField="UpdateBy" HeaderText="Update By" />
                                                    <asp:BoundField DataField="UpdateDate" HeaderText="Update Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                    <%--<asp:BoundField DataField="IsActive" HeaderText="Status" />--%>
                                                    <asp:BoundField DataField="InactiveBy" HeaderText="Inc. By" />
                                                    <asp:BoundField DataField="InactiveDate" HeaderText="Inc. Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                    <asp:TemplateField HeaderText="Actions">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="editImageButton" runat="server" class="btn btn-outline-info btn-sm  " ToolTip="Edit" CommandArgument='<%#Eval("BuyerId") %>'
                                                                CommandName="EditData" ImageUrl="~/assets/images/pencil-icon.png" Height="28" Width="28" />

                                                            <asp:ImageButton OnClientClick="return confirm('Are you really aware of this operation?');" ID="activeInactiveButton" runat="server" class="btn btn-outline-success btn-sm" ToolTip="Active or InActive"  Height="28" Width="28" CommandArgument='<%#Eval("BuyerId") %>'
                                                                CommandName="ActiveInactiveData" ImageUrl='<%#Eval("ActiveInactiveUrl") %>' />

                                                            <asp:ImageButton OnClientClick="return confirm('Are you really aware of this operation?');" ID="deleteButton" runat="server" class="btn btn-outline-danger btn-sm" ToolTip="Delete"  Height="28" Width="28" CommandArgument='<%#Eval("BuyerId") %>'
                                                                CommandName="DeleteData" ImageUrl="~/assets/images/delete.png" />
                                                        </ItemTemplate>

                                                        <ItemStyle Width="12%"></ItemStyle>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>



    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                    Height="120" Width="120" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>

