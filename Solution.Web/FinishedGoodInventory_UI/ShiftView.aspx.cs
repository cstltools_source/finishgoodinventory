﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_ShiftView : System.Web.UI.Page
{
    private ShiftDal aDal = new ShiftDal();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDownList();
            LoadShiftInfo(itemsGridView);
           
        }
    }


    private void LoadDropDownList()
    {
        aDal.LoadBuyerType(ddlShift);
    }


    private void LoadShiftInfo(GridView gridView)
    {
        gridView.DataSource = aDal.GetShiftInfo(GenerateParameter());
        gridView.DataBind();
    }

    private string GenerateParameter()
    {
        string pram = "";

        if (ddlShift.SelectedValue != "")
        {
            pram = pram + " And SFT.ShiftId =" + ddlShift.SelectedValue;
        }

        return pram;
    }

    protected void itemsGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            Session["ShiftId"] = e.CommandArgument.ToString();
            Response.Redirect("ShiftEntry.aspx");
        }

        if (e.CommandName == "ActiveInactiveData")
        {
            ShiftDao aInfo = new ShiftDao();

            aInfo.ShiftId = Int32.Parse(e.CommandArgument.ToString());

            if (aDal.ActiveInactiveShiftInfo(aInfo))
            {
                ShowMessageBox("Successfully Updated !!");
            }

            this.LoadShiftInfo(itemsGridView);
        }

        if (e.CommandName == "DeleteData")
        {
            ShiftDao aInfo = new ShiftDao();

            aInfo.ShiftId = Int32.Parse(e.CommandArgument.ToString());

            if (aDal.DeleteShiftInfo(aInfo))
            {
                ShowMessageBox("Successfully Deleted !!");
            }

            this.LoadShiftInfo(itemsGridView);
        }

        
    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void loadGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        itemsGridView.PageIndex = e.NewPageIndex;
        this.LoadShiftInfo(itemsGridView);
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {
        LoadShiftInfo(itemsGridView);
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        ddlShift.SelectedIndex = 0;
    }

}