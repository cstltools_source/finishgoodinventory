﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;

public partial class FinishedGoodInventory_UI_QAStockReceiveReport : System.Web.UI.Page
{
   private QAStockReceiveDal aDal = new QAStockReceiveDal();

   private FinishedGoodReportDal aGoodReportDal = new FinishedGoodReportDal();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitialDropdownlist();
        }
    }

    private void InitialDropdownlist()
    {
        using (DataTable dt = aDal.LoadFabricInfo())
        {
            ddlfabric.DataSource = dt;
            ddlfabric.DataValueField = "FabricName";
            ddlfabric.DataTextField = "FabricName";
            ddlfabric.DataBind();
            ddlfabric.Items.Insert(0, new ListItem("Select from list", String.Empty));
            ddlfabric.SelectedIndex = 0;
        }
     
    }

    private string GenerateParameter()
    {
        string pram = "";


        if (ddlfabric.SelectedValue != "")
        {
            pram = pram + " AND QARM.FabricCodeId In (Select FabricId from tblFabricInfo Where LOWER(FabricName)= LOWER('" + ddlfabric.SelectedValue.Trim() + "'))";
        }



        //if (ddlRollWise.SelectedValue != "")
        //{
        //    pram = pram + " AND QARD.RollNo = '" + ddlRollWise.SelectedValue + "' ";
        //}

        //if (ddlSet.SelectedValue != "")
        //{
        //    pram = pram + " AND QARD.FabricSet = '" + ddlSet.SelectedValue + "' ";
        //}

        //if (txtFromDate.Text != "" && txtToDate.Text != "")
        //{
        //    pram = pram + " AND CONVERT(date,QARM.ProductionDate)  BETWEEN '" + txtFromDate.Text.Trim() +
        //           "' AND '" + txtToDate.Text.Trim() + "' ";
        //}

        //if (txtFromDate.Text != "" && txtToDate.Text == "")
        //{
        //    pram = pram + " AND CONVERT(date,QARM.ProductionDate)  BETWEEN '" + txtFromDate.Text.Trim() +
        //           "' AND '" + DateTime.Now + "' ";
        //}

        //if (txtFromDate.Text == "" && txtToDate.Text != "")
        //{
        //    pram = pram + " AND CONVERT(date,QARM.ProductionDate)  <= '" + txtToDate.Text.Trim() + "' ";
        //}

        return pram;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
            Session["Param"] = "";
            Session["Param"] = GenerateParameter();

            PopUp("QA");
            
    }

    private void PopUp(string rpt)
    {

        string url = "../HTMLReport/QAStockReceiveViwer.aspx?rptType=" + rpt + "&rpt=" + 0;
        string fullURL = "window.open('" + url + "', '_blank', 'height=600,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
        
    }

    private void Clear()
    {
      
        ddlfabric.SelectedIndex = 0;

        itemsGridView.DataSource = null;
        itemsGridView.DataBind();

    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        Clear();
    }

    protected void loadGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        itemsGridView.PageIndex = e.NewPageIndex;
       // this.LoadList(itemsGridView);
    }

    public string GenerateString()
    {
        string param = "";

        if (ddlfabric.SelectedValue != "")
        {
            param = param + " AND QARM.FabricCodeId In (Select FabricId from tblFabricInfo Where LOWER(FabricName)= LOWER('" + ddlfabric.SelectedValue.Trim() + "'))";
        }

        //if (ddlRollWise.SelectedValue != "")
        //{
        //    param = param + " AND QARD.RollNo = '"+ddlRollWise.SelectedValue+"' ";
        //}

        //if (ddlSet.SelectedValue != "")
        //{
        //    param = param + " AND QARD.FabricSet = '"+ddlSet.SelectedValue+"' ";
        //}

        //if (txtFromDate.Text != "" && txtToDate.Text != "")
        //{
        //    param = param + " AND CONVERT(date,QARM.ProductionDate)  BETWEEN '" + txtFromDate.Text.Trim() +
        //            "' AND '" + txtToDate.Text.Trim() + "' ";
        //}

        //if (txtFromDate.Text != "" && txtToDate.Text == "")
        //{
        //    param = param + " AND CONVERT(date,QARM.ProductionDate)  BETWEEN '" + txtFromDate.Text.Trim() +
        //            "' AND '" + DateTime.Now + "' ";
        //}

        //if (txtFromDate.Text == "" && txtToDate.Text != "")
        //{
        //    param = param + " AND CONVERT(date,QARM.ProductionDate)  <= '" + txtToDate.Text.Trim() + "' ";
        //}

        return param;
    }

    protected void btn_List_OnClick(object sender, EventArgs e)
    {

        DataTable Dt = aGoodReportDal.RPT_StoreStock_Report(GenerateString());
        itemsGridView.DataSource = Dt;
        itemsGridView.DataBind();

    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //required to avoid the runtime error "  
        //Control 'GridView1' of type 'GridView' must be placed inside a form tag with runat=server."  
    }
    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        if (itemsGridView.Rows.Count != 0)
        {
            string attachment = "attachment; filename=Store_Stock_Report.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            DataTable aTable1 = aGoodReportDal.RPT_StoreStock_Report(GenerateString());
            itemsGridView.DataSource = aTable1;
            itemsGridView.DataBind();

            // Create a form to contain the grid  

            HtmlForm frm = new HtmlForm();
            itemsGridView.Parent.Controls.Add(frm);

            //frm.Attributes["runat"] = "server";
            //frm.Controls.Add(loadGridView);
            //frm.RenderControl(htw);

            itemsGridView.HeaderRow.Style.Add("background-color", "#E5EEF1");

            // Set background color of each cell of GridView1 header row
            foreach (TableCell tableCell in itemsGridView.HeaderRow.Cells)
            {
                tableCell.Style["background-color"] = "#E5EEF1";
            }

            // Set background color of each cell of each data row of GridView1
            foreach (GridViewRow gridViewRow in itemsGridView.Rows)
            {
                gridViewRow.BackColor = System.Drawing.Color.White;

                foreach (TableCell gridViewRowTableCell in gridViewRow.Cells)
                {
                    gridViewRowTableCell.Style["background-color"] = "#FFFFFF";

                }
            }

            itemsGridView.RenderControl(htw);

            string headerTable = @"<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
</style>
";
            HttpContext.Current.Response.Write(headerTable);
            HttpContext.Current.Response.Write("<table style='width:100%;'>");
            HttpContext.Current.Response.Write("<tr>");
            HttpContext.Current.Response.Write("<th colspan='20' style='text-align: center;'>" + aTable1.Rows[0]["FabricName"].ToString() + "</th>");
            HttpContext.Current.Response.Write("</tr>");
            HttpContext.Current.Response.Write("</table>");
            Response.Write(sw.ToString());
            Response.End();
        }

    }
}