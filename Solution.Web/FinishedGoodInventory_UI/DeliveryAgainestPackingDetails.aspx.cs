﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_DeliveryAgainestPackingDetails : System.Web.UI.Page
{

    DeliveryAgainestPackingListDal aDal = new DeliveryAgainestPackingListDal();

    private QAStockReceiveDal QADal = new QAStockReceiveDal();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            LoadDeliveryType();
            LoadDropdownList();

            txtDeliveryDate.Text = DateTime.Today.ToString("MM/dd/yyyy");

        }
        
    }

    private void LoadDropdownList()
    {
        aDal.LoadPackegingCode(ddlPackingGeneration);
        aDal.LoadBuyerInfo2(ddlBuyer);
        aDal.LoadVendorInfo2(ddlVendor);
        using (DataTable dt = QADal.LoadPiNoAll())
        {
            ddlPINumber.DataSource = dt;
            ddlPINumber.DataValueField = "ProformaMasterId";
            ddlPINumber.DataTextField = "ProformaInvNo";
            ddlPINumber.DataBind();
            ddlPINumber.Items.Insert(0, new ListItem("Please Select From this list", String.Empty));
            ddlPINumber.SelectedIndex = 0;

        }
        aDal.LoadDOCharge(ddlDOCharge);
    }

    private void LoadDeliveryType()
    {
        DataTable DT = aDal.GetReceiveMovementCategory("Delivery");

        if (DT.Rows.Count > 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                ListItem list = new ListItem();
                list.Text = DT.Rows[i]["MovementCategory"].ToString();
                list.Value = DT.Rows[i]["MovementCategoryId"].ToString();
                rbDeliveryType.Items.Add(list);
                rbDeliveryType.SelectedIndex = 0;
                rbDeliveryType_OnSelectedIndexChanged(null, null);

            }
        }
    }

    private void SetPackeginInfo(int issueMasterId,string OrderBy)
    {
        DataTable aTable = new DataTable();

        aTable = aDal.GetPackingListForDeliveryById(issueMasterId, OrderBy);

        if (aTable.Rows.Count > 0)
        {
            lblRemarks.Text = aTable.Rows[0].Field<String>("Reason");
            //labelBuyerName.Text = aTable.Rows[0].Field<String>("BuyerName");
            //labelProformaNo.Text = aTable.Rows[0].Field<String>("ProformaInvNo");
            //lblIssueCode.Text = aTable.Rows[0].Field<String>("PackegingIssueCode");
            //lblVendor.Text = aTable.Rows[0].Field<String>("Vendor");
            //labelIssueBy.Text = aTable.Rows[0].Field<String>("IssueBy");
            //labelIssueDate.Text = aTable.Rows[0].Field<DateTime>("IssueDate").ToString("dd-MMM-yyyy");
            //labelProformaDate.Text = aTable.Rows[0].Field<String>("ProformaDate");
            //lblIssueType.Text = aTable.Rows[0].Field<String>("MovementCategory");


            productGridView.DataSource = aTable;
            productGridView.DataBind();
        }
        else
        {
            productGridView.DataSource = null;
            productGridView.DataBind();
        }

    }

    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)productGridView.HeaderRow.FindControl("chkSelectAll");

        for (int i = 0; i < productGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");

            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
                productGridView.Rows[i].BorderColor = Color.Coral;
                productGridView.Rows[i].BackColor = Color.Cornsilk;
                
            }
            else
            {
                ChkBoxRows.Checked = false;
                productGridView.Rows[i].BorderColor = Color.LightGray;
                productGridView.Rows[i].BackColor = Color.White;
            }
        }

        CalculateTotalQuantity();
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            bool saveId = SaveChanges();
            if (saveId)
            {
               // ShowMessageBox("Delivery Confirmed Successfully !!");
                PopUpReport(Convert.ToInt32(productGridView.DataKeys[0][0].ToString()));
               // Response.Redirect("DeliveryAgainestPackingList.aspx");
            }
        }


        bool status = false;

        int masterId = 0;

        //if (Validation())
        //{
        //    DeliveryConfirmDao aDao = new DeliveryConfirmDao();

        //    aDao.ConfirmBy = Convert.ToInt32(Session["UserId"].ToString());
        //    aDao.IssueMasterId = Convert.ToInt32(productGridView.DataKeys[0][0].ToString());

        //    masterId = aDal.SaveDeliveryInfo(aDao);

        //    for (int i = 0; i < productGridView.Rows.Count; i++)
        //    {
        //        var chkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");

        //        if (chkBoxRows.Checked)
        //        {
        //            var txtDeliveryQty = (TextBox)productGridView.Rows[i].Cells[0].FindControl("txtDeliveryQty");

        //            var stockIssueId = Convert.ToInt32(productGridView.DataKeys[i][0].ToString());
        //            var deliveryQuantity = Convert.ToInt32(Convert.ToDecimal(txtDeliveryQty.Text));


        //            status = aDal.SaveDeliveryConfirmation(stockIssueId, deliveryQuantity, masterId);
        //        }

        //    }

        //    //if (status)
        //    //{
        //    //    ShowMessageBox("Delivery confirmed successfully !!!");

        //    //    PopUpReport(masterId);
        //    //    SetPackeginInfo(Convert.ToInt32(Session["IssueMasterId"]));
        //    //}
        //}
    }

    private bool SaveChanges()
    {
        bool retVal;
        try
        {
            retVal = aDal.SaveInfo(PrepareDataForSave(), PrepareDataForSaveDetail());

        }
        catch (Exception ex)
        {
            retVal = false;
            throw ex;
        }

        return retVal;
    }



    private List<IssueForPackegingDetailDao> PrepareDataForSaveDetail()
    {
        List<IssueForPackegingDetailDao> aList = new List<IssueForPackegingDetailDao>();

        for (int i = 0; i < productGridView.Rows.Count; i++)
        {
            var chkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");


            var aInfo = new IssueForPackegingDetailDao();

            aInfo.FGStockId = Convert.ToInt32(productGridView.DataKeys[i][2].ToString());

            if (chkBoxRows.Checked)
            {
                aInfo.IsDelivery = true;
            }
            else
            {
                aInfo.IsDelivery = false;
            }
            
            aInfo.IssueDetailId = Convert.ToInt32(productGridView.DataKeys[i][1].ToString());
            aInfo.TotalQuantity = Convert.ToDecimal(productGridView.DataKeys[i][3].ToString());
            aInfo.DeliveryTypeId = Int32.Parse(rbDeliveryType.SelectedValue.Trim());

            aList.Add(aInfo);
        }

        return aList;
    }

    private IssueForPackegingMasterDao PrepareDataForSave()
    {
        var aDao = new IssueForPackegingMasterDao();

        aDao.DeliveryTypeId = Int32.Parse(rbDeliveryType.SelectedValue.Trim());
        aDao.ProformaId = Int32.Parse(ddlPINumber.SelectedValue.Trim());
        aDao.DeliveryBy = Convert.ToInt32(Session["UserId"].ToString());
        aDao.DeliveryDate = Convert.ToDateTime(txtDeliveryDate.Text);
        aDao.DOChargeId = Convert.ToInt32(ddlDOCharge.SelectedValue);
        aDao.IssueMasterId = Convert.ToInt32(productGridView.DataKeys[0][0].ToString());
        aDao.DeliveryQuantity = Convert.ToDecimal(lblTotalQuantity.Text.Trim());

        return aDao;
    }

    private void PopUpReport(int masterId)
    {

        string pram = "";

        if (masterId.ToString() != "")
        {
            pram = pram + " AND D.IssueMasterId = " + masterId;
        }

        string url = "../FinishedGoodInventory_RPTView/FinishedGoodDeliveryReportViewer.aspx?&rpt=" + pram;
        string fullURL = "window.open('" + url + "', '_blank', 'height=600,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }

    private bool Validation()
    {

        if (ddlPINumber.SelectedValue == "")
        {
            ShowMessageBox("Please select PI No !!!");
            ddlPINumber.Focus();
            return false;
        }

        if (txtDeliveryDate.Text.Trim() == "")
        {
            ShowMessageBox("Please select delivery date !!!");
            txtDeliveryDate.Focus();
            return false;
        }

        if (ddlDOCharge.SelectedValue == "")
        {
            ShowMessageBox("Please DO !!!");
            ddlDOCharge.Focus();
            return false;
        }
        
        int count = 0;

        for (int i = 0; i < productGridView.Rows.Count; i++)
        {
            var chkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");

            if (chkBoxRows.Checked)
            {
                count++;
            }

            if (count > 0)
            {
                break;
            }

        }

        if (count == 0)
        {
            ShowMessageBox("Please select at least one item !!!");
            return false;
        }

        return true;
    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("DeliveryAgainestPackingList.aspx");
    }

    protected void rbDeliveryType_OnSelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ddlBuyer_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadBuyerVendorWisePI();
    }

    protected void ddlVendor_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadBuyerVendorWisePI();
    }

    private void CalculateTotalQuantity()
    {

        decimal totalQuantity = 0;

        for (int i = 0; i < productGridView.Rows.Count; i++)
        {
            var chkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");

            if (chkBoxRows.Checked)
            {
                totalQuantity = totalQuantity + Convert.ToDecimal(productGridView.DataKeys[i][3].ToString());
            }
        }

        lblTotalQuantity.Text = String.Format("{0:0.00}", totalQuantity);
    }


    private void GridRowColor()
    {
        for (int i = 0; i < productGridView.Rows.Count; i++)
        {
            var chkBoxRows = (CheckBox)productGridView.Rows[i].FindControl("chkSelect");

            if (chkBoxRows.Checked)
            {
                productGridView.Rows[i].BorderColor= Color.Coral;
                productGridView.Rows[i].BackColor = Color.Cornsilk;

            }
            else
            {
                productGridView.Rows[i].BorderColor = Color.LightGray;
                productGridView.Rows[i].BackColor = Color.White;
            }
        }

    }

    public void LoadBuyerVendorWisePI()
    {
        string pram = "";

        if (ddlBuyer.SelectedValue != "")
        {
            pram = pram + " AND PFI.BuyerId = " + ddlBuyer.SelectedValue;
        }

        if (ddlVendor.SelectedValue != "")
        {
            pram = pram + " AND PFI.VendorId = " + ddlVendor.SelectedValue;
        }

        aDal.LoadPiNo(ddlPINumber, pram);
    }

    protected void chkSelect_CheckedChanged(object sender, EventArgs e)
    {
        CalculateTotalQuantity();
        GridRowColor();
    }

    protected void ddlPINumber_OnTextChanged(object sender, EventArgs e)
    {
        if (ddlPINumber.SelectedValue != "")
        {
            DataTable dt = QADal.LoadBuyerVendor(ddlPINumber.SelectedValue);
            if (dt.Rows.Count > 0)
            {
                ddlBuyer.SelectedValue = dt.Rows[0]["BuyerId"].ToString();
                ddlVendor.SelectedValue = dt.Rows[0]["VendorId"].ToString();
            }
        }

    }

    protected void ddlPackingGeneration_OnTextChanged(object sender, EventArgs e)
    {
        if (ddlPackingGeneration.SelectedValue != "")
        {
            SetPackeginInfo(Convert.ToInt32(ddlPackingGeneration.SelectedValue), ddl_OrderBy.SelectedValue);
        }
    }

    protected void ddl_OrderBy_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPackingGeneration.SelectedValue != "")
        {
            SetPackeginInfo(Convert.ToInt32(ddlPackingGeneration.SelectedValue), ddl_OrderBy.SelectedValue);
        }
        else
        {
            ShowMessageBox("Please Select Fabric");
        }
    }
}