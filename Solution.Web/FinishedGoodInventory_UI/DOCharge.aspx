﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/NewMasterPage.master" AutoEventWireup="true" CodeFile="DOCharge.aspx.cs" Inherits="FinishedGoodInventory_UI_DOCharge" %>
<%@ Register TagPrefix="cc1" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=3.0.20820.28364, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style type="text/css">
        /*AutoComplete flyout */
        .autocomplete_completionListElement {
            margin: 0px !important;
            background-color: White;
            color: windowtext;
            border: buttonshadow;
            border-width: 1px;
            border-style: solid;
            cursor: default;
            overflow: auto;
            font-family: Calibri;
            font-size: 12px;
            text-align: left;
            list-style-type: none;
            margin-left: 0px;
            padding-left: 0px;
            max-height: 350px;
            width: 40% !important;
        }

        .AutoComplete .highlighted .item  .autocomplete_highlightedListItem {
            background-color: yellow;
            color: black;
            padding: 1px;
        }

        .AutoComplete .item .autocomplete_listItem {
            background-color: white;
            color: blue;
            padding: 0px;
        }

        #ContentPlaceHolder1_rbDeliveryType_0,
        #ContentPlaceHolder1_rbDeliveryType_1,
        #ContentPlaceHolder1_rbDeliveryType_2,
        #ContentPlaceHolder1_rbDeliveryType_3,
        #ContentPlaceHolder1_rbDeliveryType_4,
        #ContentPlaceHolder1_rbDeliveryType_5,
        #ContentPlaceHolder1_rbDeliveryType_6,
        #ContentPlaceHolder1_rbDeliveryType_7,
        #ContentPlaceHolder1_rbDeliveryType_8 {
            margin-right: 5px !important;
            margin-left: 5px !important;
        }

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="popDiv">
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Delivery Order</div>

                        <div class="ms-auto">
                            <div class="btn-group">


                                <a href="../FinishedGoodInventory_UI/DOChargeView.aspx" class="btn btn-sm btn-sm btn-outline-info"><i class="fa fa-backward"></i>&nbsp;Back to List</a>


                            </div>
                        </div>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">

                            <div class="card border-top border-0 border-4 border-success">


                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bx-file me-1 font-22 text-secondary"></i>
                                            </div>
                                            <%--<h5 class="mb-0 text-secondary ">Proforma Invoice (PI)</h5>--%>
                                        </div>
                                        <hr>
                                    <script type="text/javascript">
                                            function pageLoad() {
                                                $('.datepicker').pickadate({
                                                    selectMonths: true,
                                                    selectYears: true
                                                });
                                                $('.mySelect2').select2({
                                                    theme: 'bootstrap4',
                                                    width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                                    placeholder: $(this).data('placeholder'),
                                                    allowClear: Boolean($(this).data('allow-clear')),
                                                });
                                            }
                                        </script>

                                        <div class="row">
                                            <div class="col-6">
                                                <div class="row">
                                                    <label class="col-sm-3 col-form-label">DO NO:<span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>
                                                    <div class="col-sm-8">
                                                        <asp:HiddenField runat="server" ID="hfProformaId" />

                                                        <asp:TextBox ID="txt_DONO" runat="server" CssClass="form-control form-control-sm" ></asp:TextBox>
                                                     


                                                    </div>
                                                </div>

                                                <div class="row ">
                                                    <label class="col-sm-3 col-form-label">L/C No:<span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>
                                                    <div class="col-sm-8">
                                                      

                                                        <asp:TextBox ID="txt_LCNo" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>

                                                    </div>
                                                </div>

                                                <div class="row mb-1">
                                                    <label class="col-sm-3 col-form-label">Buyer:<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlBuyer"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="row mb-1">
                                                    <label class="col-sm-3 col-form-label">Proforma No:<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2" runat="server"  AutoPostBack="True" OnSelectedIndexChanged="ddlPINumber_OnSelectedIndexChanged" ID="ddlProforma"></asp:DropDownList>
                                                        <asp:HiddenField runat="server" ID="HFPIDate"/>
                                                        

                                                    </div>
                                                </div>
                                                

                                                <div class="row mb-1">
                                                    <label class="col-sm-3 col-form-label">SIS Code:<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2" runat="server"  AutoPostBack="True" OnSelectedIndexChanged="ddlSisCode_OnSelectedIndexChanged" ID="ddlSisCode"></asp:DropDownList>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-6">

                                                <div class="row mb-1">
                                                    <label class="col-sm-4 col-form-label">DO Date:<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>
                                                    <div class="col-sm-8">

                                                        <asp:TextBox ID="txt_DODate" runat="server" CssClass="form-control datepicker"></asp:TextBox>

                                                    </div>
                                                </div>

                                                

                                                <div class="row mb-1">
                                                    <label class="col-sm-4 col-form-label">L/C Date:<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txt_LCDate" runat="server" CssClass="form-control datepicker" ></asp:TextBox>
                                          
                                                    </div>
                                                </div>
                                                
                                                <div class="row mb-1">
                                                    <label class="col-sm-4 col-form-label">Shipment Date:<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txt_ShipmentDate" runat="server" CssClass="form-control datepicker" ></asp:TextBox>
                                          
                                                    </div>
                                                </div>
                                                
                                                <div class="row mb-1">
                                                    <label class="col-sm-4 col-form-label">Marketing Person:</label>
                                                    <div class="col-sm-8">
                                                      
                                                        <asp:DropDownList class="form-select mySelect2" runat="server"  ID="ddlMarketingPerson"></asp:DropDownList>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <br />
                                    

                                       <div runat="server" ID="SaveFunc" Visible="True">

                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-file me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Fabric Information</h5>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="MainGradeDiv" style="text-align: center !important">
                                                    <asp:GridView ID="itemsGridView" runat="server" AutoGenerateColumns="False"
                                                        PageIndex="0" CssClass="table table-bordered text-center table-condensed custom-table-style"  OnRowCommand="itemsGridView_RowCommand" 
                                                        DataKeyNames="ProformaMasterId" EmptyDataText="There are no data records to display.">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="SL">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Description of Goods">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtsisCode" runat="server" Text='<%#Eval("SISCode") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="PI No">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtPINo" runat="server" Text='<%#Eval("PINO") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            
                                                            <asp:TemplateField HeaderText="PI Date">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtPIDate" runat="server" Text='<%#Eval("PIDate") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            

                                                              <asp:TemplateField HeaderText="Quantity in YDS">
                                                                <ItemTemplate>

                                                                    <asp:TextBox ID="txtQuantity" runat="server" Text='<%#Eval("Quantity") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                             <asp:TemplateField HeaderText="Unit Price">
                                                                <ItemTemplate>
                                                                                                     
                                                                    <asp:TextBox ID="txtUnitPrice" runat="server" Text='<%#Eval("UnitPrice") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                              
                                                            <asp:TemplateField HeaderText="Amount">
                                                                <ItemTemplate>
                                                                                                     
                                                                    <asp:TextBox ID="txtAmount" runat="server" Text='<%#Eval("DOAmount") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            

                                                            <asp:TemplateField HeaderText="Remove">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="itemdeleteImageButton" runat="server" class="btn btn-danger btn-sm btnTextShadow" CommandName="DeleteData" OnClick="itemdeleteImageButton_Click"><i class="fa fa-trash" aria-hidden="true"></i>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                               
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <hr />

                                        <div class="row mb-4">

                                            <div class="col-md-5"></div>
                                            <div class="col-md-4">
                                                <asp:LinkButton runat="server" class="btn btn-outline-primary" ID="submitButton"  OnClick="submitButton_Click">
                                            
                                                    <i class="bx bxs-save mr-1"></i><span style="font-weight: bold !important">Save Info</span>

                                                </asp:LinkButton>



                                                <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="resetButton" OnClick="resetButton_Click">
                                            
                                                    <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Info</span>

                                                </asp:LinkButton>
                                            </div>
                                            <div class="col-md-4"></div>

                                        </div>
                                        
                                        
                                           
                                    </div>




                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                    Height="120" Width="120" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>

