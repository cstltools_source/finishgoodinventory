﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_QAPackingGeneration : System.Web.UI.Page
{
    QAPackingGenerationDal aDal = new QAPackingGenerationDal();

    private QAStockReceiveDal aStockReceiveDal = new QAStockReceiveDal();

    private readonly ShadeGradeUpdateDal aUpdateDal = new ShadeGradeUpdateDal();


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDownList();
          //  LoadRemarksCheckboxlist();
            if (Session["UserId"] != null)
            {
                tbxPackingBy.Text = Session["EmpName"].ToString();
                tbxPackingDate.Text = DateTime.Today.ToString("dd-MMM-yyyy");
            }
        }
    }

    private void LoadRemarksCheckboxlist()
    {
        // Remarks

        DataTable DT2 = aDal.GetRemarks(GenerateCbxParameter());

        cbxRemarks.Items.Clear();

        if (DT2.Rows.Count > 0)
        {
            for (int i = 0; i < DT2.Rows.Count; i++)
            {
                ListItem list = new ListItem();

                list.Text = DT2.Rows[i]["Remarks"].ToString();
                list.Value = DT2.Rows[i]["Remarks"].ToString();
                cbxRemarks.Items.Add(list);

            }
        }
    }

    private string GenerateCbxParameter()
    {
        string pram = "";

        if (cbxShadeGrades.Items.Cast<ListItem>().Count(li => li.Selected) > 0)
        {
            pram = pram + " AND FGS.ShadeGradeing IN ( ";

            foreach (ListItem lst in cbxShadeGrades.Items)
            {
                if (lst.Selected)
                {
                    pram = pram + "'" + lst.Text.Trim() + "',";
                }
            }
            pram = pram.Remove(pram.Length - 1, 1) + ")";
        }
        else
        {
            pram = pram + " AND FGS.ShadeGradeing IS NULL";
        }
        return pram;
    }

    private void LoadDropDownList()
    {
        // Shade Grade


        using (DataTable dt = aStockReceiveDal.LoadPiNoAll())
        {
            ddlProforma.DataSource = dt;
            ddlProforma.DataValueField = "ProformaMasterId";
            ddlProforma.DataTextField = "ProformaInvNo";
            ddlProforma.DataBind();
            ddlProforma.Items.Insert(0, new ListItem("Select from list", String.Empty));
            ddlProforma.SelectedIndex = 0;

        }


        //cbxShadeGrades.Items.Clear();
        //DataTable DT = aDal.GetShadeGrades();

        //if (DT.Rows.Count > 0)
        //{
        //    for (int i = 0; i < DT.Rows.Count; i++)
        //    {
        //        ListItem list = new ListItem();
        //        list.Text = DT.Rows[i]["ShadeGrade"].ToString();
        //        list.Value = DT.Rows[i]["ShadeGradeId"].ToString();
        //        cbxShadeGrades.Items.Add(list);

        //    }
        //}


      //  aDal.LoadFabricInfo(ddlFabric);
        aDal.LoadShadeGradeInfo(ddlShadeGrade);

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if (ddlFabric.SelectedValue != "")
        {
            DataTable aTable = new DataTable();

            aTable = aDal.GetFGStockView(GenerateParameter());

            productGridView.DataSource = null;
            productGridView.DataBind();

            if (aTable.Rows.Count > 0)
            {
                productGridView.DataSource = aTable;
                productGridView.DataBind();

            }
        }
        else
        {
            ShowMessageBox("Please select Fabric/ SIS Code !!");
            productGridView.DataSource = null;
            productGridView.DataBind();
        }
    }


    private string GenerateParameter()
    {
        string pram = "";


        if (cbxShadeGrades.Items.Cast<ListItem>().Count(li => li.Selected) > 0)
        {
            pram = pram + " AND FGST.ShadeGradeing IN ( ";

            foreach (ListItem lst in cbxShadeGrades.Items)
            {

                if (lst.Selected)
                {
                    pram = pram + "'" + lst.Text.Trim() + "',";
                }

            }

            pram = pram.Remove(pram.Length - 1, 1) + ")";

        }
        //else
        //{
        //    pram = pram + " AND FGST.ShadeGradeing IS NOT NULL";
        //}


        if (cbxSETNumber.Items.Cast<ListItem>().Count(li => li.Selected) > 0)
        {
            pram = pram + " AND FGST.FabricSet IN ( ";

            foreach (ListItem lst in cbxSETNumber.Items)
            {

                if (lst.Selected)
                {
                    pram = pram + "'" + lst.Text.Trim() + "',";
                }

            }

            pram = pram.Remove(pram.Length - 1, 1) + ")";

        }
        //else
        //{
        //    pram = pram + " AND FGST.FabricSet IS NOT NULL";
        //}



        if (CkPPHSY.Items.Cast<ListItem>().Count(li => li.Selected) > 0)
        {
            pram = pram + " AND FGST.PPHSY IN ( ";

            foreach (ListItem lst in CkPPHSY.Items)
            {

                if (lst.Selected)
                {
                    pram = pram + "'" + lst.Text.Trim() + "',";
                }

            }

            pram = pram.Remove(pram.Length - 1, 1) + ")";

        }
        //else
        //{
        //    pram = pram + " AND FGST.PPHSY IS NOT NULL";
        //}



        if (cbxRemarks.Items.Cast<ListItem>().Count(li => li.Selected) > 0)
        {
            pram = pram + " AND FGST.Remarks IN ( ";

            foreach (ListItem lst in cbxRemarks.Items)
            {

                if (lst.Selected)
                {
                    pram = pram + "'" + lst.Text.Trim() + "',";
                }

            }

            pram = pram.Remove(pram.Length - 1, 1) + ")";

        }
        //else
        //{
        //    pram = pram + " AND FGST.Remarks IS NOT NULL";
        //}

        if (ckDEFECTPOINTS.Items.Cast<ListItem>().Count(li => li.Selected) > 0)
        {
            pram = pram + " AND FGST.DefectPoint IN ( ";

            foreach (ListItem lst in ckDEFECTPOINTS.Items)
            {

                if (lst.Selected)
                {
                    pram = pram + "'" + lst.Text.Trim() + "',";
                }

            }

            pram = pram.Remove(pram.Length - 1, 1) + ")";

        }
        //else
        //{
        //    pram = pram + " AND FGST.DefectPoint IS NOT NULL";
        //}

        
        if (ddlFabric.SelectedValue != "")
        {

            pram = pram + " AND FGST.FabricCodeId In (Select FabricId from tblFabricInfo Where LOWER(FabricName)= LOWER('" + ddlFabric.SelectedValue + "'))";
        }



        if (ddlShadeGrade.SelectedValue != "")
        {
            pram = pram + " AND FGST.ShadeGradeing = '" + ddlShadeGrade.SelectedItem.Text.Trim() + "'";
        }

        if (tbxActualWidthMin.Text.Trim() != "" && tbxActualWidthMax.Text.Trim() == "" )
        {
            pram = pram + " AND (FGST.ActualWidth BETWEEN '" + tbxActualWidthMin.Text.Trim() + "' AND '" + tbxActualWidthMax.Text.Trim() + "')"; 
        }

        if (tbxRollLengthMin.Text.Trim() == "" && tbxRollLengthMax.Text.Trim() != "")
        {
            pram = pram + " AND (FGST.RollNo BETWEEN '" + tbxRollLengthMin.Text.Trim() + "' AND '" + tbxRollLengthMax.Text.Trim() + "')"; 
        }
        
        //if (tbxActualWidthMin.Text.Trim() != "" && tbxActualWidthMax.Text.Trim() != "" )
        //{
        //    pram = pram + " AND (FGST.ActualWidth BETWEEN '" + tbxActualWidthMin.Text.Trim() + "' AND '" + tbxActualWidthMax.Text.Trim() + "')"; 
        //}

        //if (tbxOunceYardMin.Text.Trim() != "" && tbxOunceYardMax.Text.Trim() == "")
        //{
        //    pram = pram + " AND (FGST.RequiredOunceYard BETWEEN '" + tbxOunceYardMin.Text.Trim() + "' AND '" + tbxOunceYardMin.Text.Trim() + "')";
        //}

        //if (tbxOunceYardMin.Text.Trim() == "" && tbxOunceYardMax.Text.Trim() != "")
        //{
        //    pram = pram + " AND (FGST.RequiredOunceYard BETWEEN '" + tbxOunceYardMin.Text.Trim() + "' AND '" + tbxOunceYardMax.Text.Trim() + "')";
        //}

        //if (tbxOunceYardMin.Text.Trim() != "" && tbxOunceYardMax.Text.Trim() != "")
        //{
        //    pram = pram + " AND (FGST.RequiredOunceYard BETWEEN '" + tbxOunceYardMin.Text.Trim() + "' AND '" + tbxOunceYardMax.Text.Trim() + "')";
        //}

        //if (tbxShrinkageMin.Text.Trim() != "" && tbxShrinkageMax.Text.Trim() == "")
        //{
        //    pram = pram + " AND (FGST.Shrinkage BETWEEN '" + tbxShrinkageMin.Text.Trim() + "' AND '" + tbxShrinkageMin.Text.Trim() + "')";
        //}

        //if (tbxShrinkageMin.Text.Trim() == "" && tbxShrinkageMax.Text.Trim() != "")
        //{
        //    pram = pram + " AND (FGST.Shrinkage BETWEEN '" + tbxShrinkageMax.Text.Trim() + "' AND '" + tbxShrinkageMax.Text.Trim() + "')";
        //}

        //if (tbxShrinkageMin.Text.Trim() != "" && tbxOunceYardMax.Text.Trim() != "")
        //{
        //    pram = pram + " AND (FGST.Shrinkage BETWEEN '" + tbxShrinkageMin.Text.Trim() + "' AND '" + tbxShrinkageMax.Text.Trim() + "')";
        //}

           
        return pram;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            int saveId = 0;
            saveId = SaveChanges();

            if (saveId > 0)
            {
                PopupReport(saveId);
                
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "alert",
                    "alert('Issue for delivery successfully done !!');window.location ='QAPackingGeneration.aspx';",
                    true);

                //if (aDal.InsertIntoPackegingTranscationTable(saveId))
                //{
                //    ShowMessageBox("Issue for delivery successfully done !!");
                //    PopupReport(saveId);
                //}
            }
            else
            {
                ShowMessageBox("Issue for delivery isn't success!!");
            }

            Clear();
        }
        
    }

    public void Clear()
    {
        LoadDropDownList();
        tbxActualWidthMax.Text = "";
        tbxActualWidthMin.Text = "";
        tbxOunceYardMax.Text = "";
        tbxOunceYardMin.Text = "";
        tbxShrinkageMax.Text = "";
        tbxShrinkageMin.Text = "";
        tbxPackingReason.Text = "";

        productGridView.DataSource = null;
        productGridView.DataBind();
    }

    private Int32 SaveChanges()
    {
        Int32 retVal;
        try
        {
            retVal = aDal.SaveInfo(PrepareDataForSave(), PrepareDataForSaveDetail());

        }
        catch (Exception ex)
        {
            retVal = 0;
            throw ex;
        }

        return retVal;
    }

    private List<IssueForPackegingDetailDao> PrepareDataForSaveDetail()
    {
        List<IssueForPackegingDetailDao> aList = new List<IssueForPackegingDetailDao>();

        for (int i = 0; i < productGridView.Rows.Count; i++)
        {
            var chkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");

            if (chkBoxRows.Checked)
            {
                var aInfo = new IssueForPackegingDetailDao();
                aInfo.FGStockId = Convert.ToInt32(productGridView.DataKeys[i][0].ToString());
                aList.Add(aInfo);
            }
        }
        return aList;
    }

    private IssueForPackegingMasterDao PrepareDataForSave()
    {
        var aDao = new IssueForPackegingMasterDao();

        aDao.IssueDate = DateTime.Today;
        aDao.Reason = tbxPackingReason.Text;
        aDao.TotalQuantity = Convert.ToDecimal(tbxPackingQuantity.Text.Trim());
        aDao.IssueBy = Convert.ToInt32(Session["UserId"].ToString());

        return aDao;
    }

    private void PopupReport(int masterId)
    {
        string pram = "";
        if (masterId.ToString(CultureInfo.InvariantCulture) != "")
        {
            pram = pram + " AND M.IssueMasterId = " + masterId;
        }

        string url = "../FinishedGoodInventory_RPTView/FinishedGoodPackegingReportViewer.aspx?rpt=" + pram;
        string fullURL = "window.open('" + url + "', '_blank', 'height=600,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }

    private int SaveIssuePackegingMaster()
    {
        IssueForPackegingMasterDao aDao = new IssueForPackegingMasterDao();

        aDao.IssueDate = DateTime.Today;
        aDao.Reason = tbxPackingReason.Text;
        aDao.TotalQuantity = Convert.ToDecimal(tbxPackingQuantity.Text.Trim());
        aDao.IssueBy = Convert.ToInt32(Session["UserId"].ToString());

        return aDal.SaveIssueForPackeging(aDao);

    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private bool Validation()
    {
        if (tbxPackingBy.Text.Trim() == "")
        {
            ShowMessageBox("Please select packing by !!!");
            return false;
        }
        
        if (tbxPackingDate.Text.Trim() == "")
        {
            ShowMessageBox("Please select packing date !!!");
            return false;
        }
        
        if (tbxPackingReason.Text.Trim() == "")
        {
            ShowMessageBox("Please select packing reason !!!");
            return false;
        }

        int count = 0;

        for (int i = 0; i < productGridView.Rows.Count; i++)
        {
            var chkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");

            if (chkBoxRows.Checked)
            {
                count++;
            }

            if (count > 0)
            {
                break;
            }

        }

        if (count == 0)
        {
            ShowMessageBox("Please select at least one item !!!");
            return false;
        }

        

        return true;
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        Clear();
    }

    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)productGridView.HeaderRow.FindControl("chkSelectAll");

        for (int i = 0; i < productGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");

            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
        }

        CalculateTotalQuantity();
    }

    private void CalculateTotalQuantity()
    {

        decimal totalQuantity = 0;

        for (int i = 0; i < productGridView.Rows.Count; i++)
        {
            var chkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");

            if (chkBoxRows.Checked)
            {
                totalQuantity = totalQuantity + Convert.ToDecimal(productGridView.DataKeys[i][1].ToString());
            }
        }

        tbxPackingQuantity.Text = String.Format("{0:0.00}", totalQuantity);
    }


    protected void ddlBuyer_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Button1_Click(null, null);
    }

    protected void ddlFabric_OnSelectedIndexChanged(object sender, EventArgs e)
    {

        //txtRequiredShrinkageLenthMin.Text = "";
        //txtRequiredShrinkageLenthMax.Text = "";
        //txtRequiredShrinkageWidthMin.Text = "";
        //txtRequiredShrinkageWidthMax.Text = "";


        //if (ddlFabricInfo.SelectedValue != "")
        //{
        //    //string fabric = ddlFabricInfo.SelectedItem.Text.Trim();
        //    //string[] fabrics = fabric.Split(':');

        //    DataTable aTable = aDal.GetFabricInfoById(Convert.ToInt32(ddlPINumber.SelectedValue), ddlFabricInfo.SelectedItem.Text.Trim());

        //    if (aTable.Rows.Count > 0)
        //    {
        //        ddlColor.SelectedValue = aTable.Rows[0].Field<Int32>("ColorId").ToString(CultureInfo.InvariantCulture);
        //        tbxRequiredWidth.Text = aTable.Rows[0].Field<Decimal>("RequiredWidth").ToString(CultureInfo.InvariantCulture);
        //        tbxOunceYard.Text = aTable.Rows[0].Field<Decimal>("RequiredOunceOryard").ToString(CultureInfo.InvariantCulture);
        //        txtRequiredShrinkageLenthMin.Text = aTable.Rows[0].Field<Decimal>("ShrinkageLengthMin").ToString(CultureInfo.InvariantCulture);
        //        txtRequiredShrinkageLenthMax.Text = aTable.Rows[0].Field<Decimal>("ShrinkageLengthMax").ToString(CultureInfo.InvariantCulture);
        //        txtRequiredShrinkageWidthMin.Text = aTable.Rows[0].Field<Decimal>("ShrinkageWidthMin").ToString(CultureInfo.InvariantCulture);
        //        txtRequiredShrinkageWidthMax.Text = aTable.Rows[0].Field<Decimal>("ShrinkageWidthMax").ToString(CultureInfo.InvariantCulture);
        //    }
        //}
        //else
        //{
        //    ddlColor.SelectedValue = "";
        //    tbxRequiredWidth.Text = "";
        //    tbxOunceYard.Text = "";
        //    //   tbxShrinkageLength.Text = "";
        //    //   tbxShrinkageWidth.Text = "";
        //    txtRequiredShrinkageLenthMin.Text = "";
        //    txtRequiredShrinkageLenthMax.Text = "";
        //    txtRequiredShrinkageWidthMin.Text = "";
        //    txtRequiredShrinkageWidthMax.Text = "";



        //}


        if (ddlFabric.SelectedValue != "")
        {
            DataTable aTable = new DataTable();

            aTable = aDal.GetFGStockView(GenerateParameter());

            if (aTable.Rows.Count > 0)
            {
                CkPPHSY.Items.Clear();
                cbxSETNumber.Items.Clear();
                cbxRemarks.Items.Clear();
                cbxShadeGrades.Items.Clear();
                ckDEFECTPOINTS.Items.Clear();
                CKActualwith.Items.Clear();
                
                for (int i = 0; i < aTable.Rows.Count; i++)
                {
                    ListItem list = new ListItem();
                    list.Text = aTable.Rows[i]["PPHSY"].ToString();
                    list.Value = aTable.Rows[i]["PPHSY"].ToString();
                    CkPPHSY.Items.Add(list);
                    

                    ListItem SetNumber = new ListItem();
                    SetNumber.Text = aTable.Rows[i]["FabricSet"].ToString();
                    SetNumber.Value = aTable.Rows[i]["FabricSet"].ToString();
                    cbxSETNumber.Items.Add(SetNumber);


                    ListItem remarklist = new ListItem();
                    remarklist.Text = aTable.Rows[i]["Remarks"].ToString();
                    remarklist.Value = aTable.Rows[i]["Remarks"].ToString();
                    cbxRemarks.Items.Add(remarklist);

                    ListItem Shadelist = new ListItem();
                    Shadelist.Text = aTable.Rows[i]["ShadeGradeing"].ToString();
                    Shadelist.Value = aTable.Rows[i]["ShadeGradeing"].ToString();
                    cbxShadeGrades.Items.Add(Shadelist);

                    
                    ListItem DefectPointlist = new ListItem();
                    DefectPointlist.Text = aTable.Rows[i]["DefectPoint"].ToString();
                    DefectPointlist.Value = aTable.Rows[i]["DefectPoint"].ToString();
                    ckDEFECTPOINTS.Items.Add(DefectPointlist);


                    ListItem ActualWithlist = new ListItem();
                    ActualWithlist.Text = aTable.Rows[i]["ActualWidth"].ToString();
                    ActualWithlist.Value = aTable.Rows[i]["ActualWidth"].ToString();
                    CKActualwith.Items.Add(ActualWithlist);

                }


                List<object> items = new List<object>();
                List<object> duplicateItems = new List<object>();
                for (int i = cbxShadeGrades.Items.Count - 1; i >= 0; i--)
                {
                    object item = cbxShadeGrades.Items[i];
                    if (!items.Contains(item))
                        items.Add(item);
                    else
                    {
                        duplicateItems.Add(item);
                        cbxShadeGrades.Items.RemoveAt(i);
                    }
                }


                items.Clear();
                duplicateItems.Clear();

                for (int i = CKActualwith.Items.Count - 1; i >= 0; i--)
                {
                    object item = CKActualwith.Items[i];
                    if (!items.Contains(item))
                        items.Add(item);
                    else
                    {
                        duplicateItems.Add(item);
                        CKActualwith.Items.RemoveAt(i);
                    }
                }

                items.Clear();
                duplicateItems.Clear();

                for (int i = cbxRemarks.Items.Count - 1; i >= 0; i--)
                {
                    object item = cbxRemarks.Items[i];
                    if (!items.Contains(item))
                        items.Add(item);
                    else
                    {
                        duplicateItems.Add(item);
                        cbxRemarks.Items.RemoveAt(i);
                    }
                }

                items.Clear();
                duplicateItems.Clear();

                for (int i = CkPPHSY.Items.Count - 1; i >= 0; i--)
                {
                    object item = CkPPHSY.Items[i];
                    if (!items.Contains(item))
                        items.Add(item);
                    else
                    {
                        duplicateItems.Add(item);
                        CkPPHSY.Items.RemoveAt(i);
                    }
                }

                items.Clear();
                duplicateItems.Clear();

                for (int i = CkPPHSY.Items.Count - 1; i >= 0; i--)
                {
                    object item = CkPPHSY.Items[i];
                    if (!items.Contains(item))
                        items.Add(item);
                    else
                    {
                        duplicateItems.Add(item);
                        CkPPHSY.Items.RemoveAt(i);
                    }
                }


                items.Clear();
                duplicateItems.Clear();

                for (int i = cbxSETNumber.Items.Count - 1; i >= 0; i--)
                {
                    object item = cbxSETNumber.Items[i];
                    if (!items.Contains(item))
                        items.Add(item);
                    else
                    {
                        duplicateItems.Add(item);
                        cbxSETNumber.Items.RemoveAt(i);
                    }
                }

                items.Clear();
                duplicateItems.Clear();

                for (int i = ckDEFECTPOINTS.Items.Count - 1; i >= 0; i--)
                {
                    object item = ckDEFECTPOINTS.Items[i];
                    if (!items.Contains(item))
                        items.Add(item);
                    else
                    {
                        duplicateItems.Add(item);
                        ckDEFECTPOINTS.Items.RemoveAt(i);
                    }
                }


            }
        }



        Button1_Click(null, null);
    }

    protected void chkSelect_CheckedChanged(object sender, EventArgs e)
    {
        CalculateTotalQuantity();
    }



    private void MaxMin(double max, double min)
    {
        if (min > max)
        {
            ShowMessageBox("Max value can not be less then min value");
        }
          
    }

    protected void tbxActualWidthMax_OnTextChanged(object sender, EventArgs e)
    {
        if (tbxActualWidthMax.Text.Trim() != "" && tbxActualWidthMin.Text.Trim() != "")
        {
            MaxMin(Convert.ToDouble(tbxActualWidthMax.Text.Trim()), Convert.ToDouble(tbxActualWidthMin.Text.Trim()));

            if (Convert.ToDouble(tbxActualWidthMax.Text.Trim()) < Convert.ToDouble(tbxActualWidthMin.Text.Trim()))
            {
                tbxActualWidthMax.Text = "";
            }

        }
    }

    protected void tbxActualWidthMin_OnTextChanged(object sender, EventArgs e)
    {
        if (tbxActualWidthMax.Text.Trim() != "" && tbxActualWidthMin.Text.Trim() != "")
        {
            MaxMin(Convert.ToDouble(tbxActualWidthMax.Text.Trim()), Convert.ToDouble(tbxActualWidthMin.Text.Trim()));

            if (Convert.ToDouble(tbxActualWidthMax.Text.Trim()) < Convert.ToDouble(tbxActualWidthMin.Text.Trim()))
            {
                tbxActualWidthMin.Text = "";
            }
        }
    }

    protected void tbxOunceYardMin_OnTextChanged(object sender, EventArgs e)
    {
        if (tbxOunceYardMax.Text.Trim() != "" && tbxOunceYardMin.Text.Trim() != "")
        {
            MaxMin(Convert.ToDouble(tbxOunceYardMax.Text.Trim()), Convert.ToDouble(tbxOunceYardMin.Text.Trim()));

            if (Convert.ToDouble(tbxOunceYardMax.Text.Trim()) < Convert.ToDouble(tbxOunceYardMin.Text.Trim()))
            {
                tbxOunceYardMin.Text = "";
            }
        }
    }

    protected void tbxOunceYardMax_OnTextChanged(object sender, EventArgs e)
    {
        if (tbxOunceYardMax.Text.Trim() != "" && tbxOunceYardMin.Text.Trim() != "")
        {
            MaxMin(Convert.ToDouble(tbxOunceYardMax.Text.Trim()), Convert.ToDouble(tbxOunceYardMin.Text.Trim()));
            if (Convert.ToDouble(tbxOunceYardMax.Text.Trim()) < Convert.ToDouble(tbxOunceYardMin.Text.Trim()))
            {
                tbxOunceYardMax.Text = "";
            }
        }
    }

    protected void tbxShrinkageMin_OnTextChanged(object sender, EventArgs e)
    {
        if (tbxShrinkageMax.Text.Trim() != "" && tbxShrinkageMin.Text.Trim() != "")
        {
            MaxMin(Convert.ToDouble(tbxShrinkageMax.Text.Trim()), Convert.ToDouble(tbxShrinkageMin.Text.Trim()));
            if (Convert.ToDouble(tbxShrinkageMin.Text.Trim()) > Convert.ToDouble(tbxShrinkageMax.Text.Trim()))
            {
                tbxShrinkageMin.Text = "";
            }
           
        }
    }

    protected void tbxShrinkageMax_OnTextChanged(object sender, EventArgs e)
    {
        if (tbxShrinkageMax.Text.Trim() != "" && tbxShrinkageMin.Text.Trim() != "")
        {
            MaxMin(Convert.ToDouble(tbxShrinkageMax.Text.Trim()), Convert.ToDouble(tbxShrinkageMin.Text.Trim()));
            if (Convert.ToDouble(tbxShrinkageMax.Text.Trim()) < Convert.ToDouble(tbxShrinkageMin.Text.Trim()))
            {
                tbxShrinkageMax.Text = "";
            }
        }
    }

    protected void cbxShadeAll_CheckedChanged(object sender, EventArgs e)
    {
        if (cbxShadeAll.Checked)
        {
            foreach (ListItem lst in cbxShadeGrades.Items)
            {
                lst.Selected = true;
            }
        }
        else
        {
            foreach (ListItem lst in cbxShadeGrades.Items)
            {
                lst.Selected = false;
            }
        }

        LoadRemarksCheckboxlist();
        //LoadRollList();
    }


    protected void cbxSETNumber_CheckedChanged(object sender, EventArgs e)
    {
        if (cbxSETNumberAll.Checked)
        {
            foreach (ListItem lst in cbxSETNumber.Items)
            {
                lst.Selected = true;
            }
        }
        else
        {
            foreach (ListItem lst in cbxSETNumber.Items)
            {
                lst.Selected = false;
            }
        }

        // LoadRemarksCheckboxlist();
        //LoadRollList();
    }





    protected void cbxPPHSYAll_CheckedChanged(object sender, EventArgs e)
    {
        if (CkPPHSYAll.Checked)
        {
            foreach (ListItem lst in CkPPHSY.Items)
            {
                lst.Selected = true;
            }
        }
        else
        {
            foreach (ListItem lst in CkPPHSY.Items)
            {
                lst.Selected = false;
            }
        }

       // LoadRemarksCheckboxlist();
        //LoadRollList();
    }



    protected void cbxShadeGrades_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        
    }

    protected void cbxRemarksAll_CheckedChanged(object sender, EventArgs e)
    {
        if (cbxRemarksAll.Checked)
        {
            foreach (ListItem lst in cbxRemarks.Items)
            {
                lst.Selected = true;
            }
        }
        else
        {
            foreach (ListItem lst in cbxRemarks.Items)
            {
                lst.Selected = false;
            }
        }

        //LoadRollList();
    }

    //protected void cbxRemarks_OnSelectedIndexChanged(object sender, EventArgs e)
    //{
    //    throw new NotImplementedException();
    //}
    protected void tbxRollLengthMin_OnTextChanged(object sender, EventArgs e)
    {
        if (tbxShrinkageMax.Text.Trim() != "" && tbxRollLengthMin.Text.Trim() != "")
        {
            MaxMin(Convert.ToDouble(tbxRollLengthMax.Text.Trim()), Convert.ToDouble(tbxRollLengthMin.Text.Trim()));
            if (Convert.ToDouble(tbxRollLengthMin.Text.Trim()) > Convert.ToDouble(tbxRollLengthMax.Text.Trim()))
            {
                tbxRollLengthMin.Text = "";
            }

        }
    }

    protected void tbxRollLengthMax_OnTextChanged(object sender, EventArgs e)
    {
        if (tbxRollLengthMax.Text.Trim() != "" && tbxRollLengthMin.Text.Trim() != "")
        {
            MaxMin(Convert.ToDouble(tbxRollLengthMax.Text.Trim()), Convert.ToDouble(tbxRollLengthMin.Text.Trim()));

            if (Convert.ToDouble(tbxRollLengthMax.Text.Trim()) < Convert.ToDouble(tbxRollLengthMin.Text.Trim()))
            {
                tbxRollLengthMax.Text = "";
            }
        }
    }

    protected void ddlProforma_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlProforma.SelectedValue != "")
        {
            aUpdateDal.LoadSisCodeByPi_ShadeGrade(ddlFabric, Convert.ToInt32(ddlProforma.SelectedValue));

            int fabricCount = 0;
            fabricCount = ddlFabric.Items.Count;

            if (fabricCount == 2)
            {
                ddlFabric.SelectedIndex = 1;
               ddlFabric_OnSelectedIndexChanged(null, null);
            }

        }
    }

    protected void ckDEFECTPOINTSAll_OnCheckedChanged(object sender, EventArgs e)
    {
        if (ckDEFECTPOINTSAll.Checked)
        {
            foreach (ListItem lst in ckDEFECTPOINTS.Items)
            {
                lst.Selected = true;
            }
        }
        else
        {
            foreach (ListItem lst in ckDEFECTPOINTS.Items)
            {
                lst.Selected = false;
            }
        }
    }

    protected void ckActualWith_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (CkActualWithAll.Checked)
        {
            foreach (ListItem lst in CKActualwith.Items)
            {
                lst.Selected = true;
            }
        }
        else
        {
            foreach (ListItem lst in CKActualwith.Items)
            {
                lst.Selected = false;
            }
        }
    }
}