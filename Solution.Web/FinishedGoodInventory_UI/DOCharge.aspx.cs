﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.ReportAppServer.DataDefModel;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_DOCharge : System.Web.UI.Page
{

    ProformaInvoiceDal aDal = new ProformaInvoiceDal();
    private QAStockReceiveDal qDal = new QAStockReceiveDal();

    private DoChargeDal aChargeDal = new DoChargeDal();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDownList();

            if (Session["ProformaId"] != null)
            {
                GetOneRecord(Convert.ToInt32(Session["ProformaId"].ToString()));
                Session["ProformaId"] = null;
            }
        }
    }



    # region Edit
    private void GetOneRecord(int proformaId)
    {
        var aTable = new DataTable();

        aTable = aDal.GetProformaInfoById(proformaId);

        if (aTable.Rows.Count > 0)
        {
            hfProformaId.Value = aTable.Rows[0].Field<int>("ProformaMasterId").ToString(CultureInfo.InvariantCulture);
           // txt_Buyer.Text = aTable.Rows[0].Field<String>("BuyerName");
           
          //  txtRemarks.Text = aTable.Rows[0].Field<String>("Remarks");

            itemsGridView.DataSource = aTable;
            itemsGridView.DataBind();


            if (itemsGridView.Rows.Count > 0)
            {
                for (int i = 0; i < itemsGridView.Rows.Count; i++)
                {
                    var itemdeleteImageButton = (LinkButton)itemsGridView.Rows[i].Cells[1].FindControl("itemdeleteImageButton");


                    var t = itemsGridView.DataKeys[i][12].ToString();

                    if (Convert.ToBoolean(itemsGridView.DataKeys[i][12]))
                    {
                        itemdeleteImageButton.Enabled = true;
                    }
                    else
                    {
                        itemdeleteImageButton.Enabled = false;
                    }
                }
            }

            submitButton.Text = "Update Information";
        }
    }

    #endregion Edit
    private void LoadDropDownList()
    {
        aDal.LoadMarketingPerson(ddlMarketingPerson);
        aDal.Load_Buyer_Info(ddlBuyer);

        aChargeDal.LoadPIInfo(ddlProforma);
    
        using (DataTable dt = qDal.LoadPiNoAll())
        {
            ddlProforma.DataSource = dt;
            ddlProforma.DataValueField = "ProformaMasterId";
            ddlProforma.DataTextField = "ProformaInvNo";
            ddlProforma.DataBind();
            ddlProforma.Items.Insert(0, new ListItem("Select from list", String.Empty));
            ddlProforma.SelectedIndex = 0;

        }
    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private bool Validation()
    {


        if (txt_DONO.Text.Trim() == "")
        {
            ShowMessageBox("Please Input DO No");
            txt_DONO.Focus();
            return false;
        }

        if (txt_DODate.Text.Trim() == "")
        {
            ShowMessageBox("Please Input DO Date");
            txt_DODate.Focus();
            return false;
        }

        if (txt_LCNo.Text.Trim() == "")
        {
            ShowMessageBox("Please Input LC NO");
            txt_LCNo.Focus();
            return false;
        }

        if (txt_LCDate.Text.Trim() == "")
        {
            ShowMessageBox("Please Input LC Date");
            txt_LCDate.Focus();
            return false;
        }

        if (txt_ShipmentDate.Text.Trim() == "")
        {
            ShowMessageBox("Please Input Shipment Date");
            txt_ShipmentDate.Focus();
            return false;
        }

        if (ddlBuyer.SelectedValue == "")
        {
            ShowMessageBox("Please Select Buyer");
            ddlBuyer.Focus();
            return false;
        }

        if (ddlProforma.SelectedValue == "")
        {
            ShowMessageBox("Please Select PI No");
            ddlProforma.Focus();
            return false;
        }

        if (ddlSisCode.SelectedValue == "")
        {
            ShowMessageBox("Please Select SIS Code");
            ddlSisCode.Focus();
            return false;
        }

        if (ddlMarketingPerson.SelectedValue == "")
        {
            ShowMessageBox("Please Select Marketing Person");
            ddlMarketingPerson.Focus();
            return false;
        }


        int rowsCount = 0;
        rowsCount = itemsGridView.Rows.Count;

        if (rowsCount == 0)
        {
            ShowMessageBox("Please input fabric info !!");
            return false;
        }

        return true;
    }


    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
           Int32 Id=  SaveChanges();

           if (Id > 0)
           {

               PopupReport(Id);

               ScriptManager.RegisterStartupScript(this, this.GetType(),
                   "alert",
                   "alert('Operation Successful...!');window.location ='DOChargeView.aspx';",
                   true);
           }

        }
    }



    private void PopupReport(int masterId)
    {
        string pram = "";
        if (masterId.ToString(CultureInfo.InvariantCulture) != "")
        {
            pram = pram + " AND DO.DOChargeId = " + masterId;
        }

        string url = "../FinishedGoodInventory_RPTView/DOChargeReportViewer.aspx?rpt=" + pram;
        string fullURL = "window.open('" + url + "', '_blank', 'height=600,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }


    private void PopUp(string rpt, string param)
    {
        string url = "../FinishedGoodInventory_RPTView/FinishedGoodReportViewer.aspx?rptType=" + rpt + "&rpt=" + param;
        string fullURL = "window.open('" + url + "', '_blank', 'height=600,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);

    }


    private ProformaMasterDao PrepareDataForUpdatee()
    {
        var aInfo = new ProformaMasterDao();

        aInfo.ProformaMasterId = Convert.ToInt32(hfProformaId.Value);
        
       // aInfo.Remarks = txtRemarks.Text;

        return aInfo;
    }


    private Int32 SaveChanges()
    {
        Int32 retVal;
        try
        {
            retVal = aChargeDal.SaveDOCharge(PrepareMasterDataForSave(), PrepareDataForSaveDetail());
        }
        catch (Exception ex)
        {
            retVal = 0;
            throw ex;
        }

        return retVal;
    }

    private List<DOChargeDetails> PrepareDataForSaveDetail()
    {
        List<DOChargeDetails> aList = new List<DOChargeDetails>();
        for (int i = 0; i < itemsGridView.Rows.Count; i++)
        {
            var aInfo = new DOChargeDetails();

            TextBox ItemDescription = (TextBox) itemsGridView.Rows[i].FindControl("txtsisCode");

            aInfo.ItemDescription = ItemDescription.Text;
            aInfo.ProformaMasterId = Convert.ToInt32(itemsGridView.DataKeys[i][0].ToString());

            TextBox Pidate = (TextBox)itemsGridView.Rows[i].FindControl("txtPIDate");
            aInfo.PIDate = Convert.ToDateTime(Pidate.Text);

            TextBox qty = (TextBox)itemsGridView.Rows[i].FindControl("txtQuantity");
            aInfo.Quantity = string.IsNullOrEmpty(qty.Text.Trim()) ? 0 : Convert.ToDecimal(qty.Text.Trim());

            TextBox UnitPrice = (TextBox)itemsGridView.Rows[i].FindControl("txtUnitPrice");
            aInfo.UnitPrice =  string.IsNullOrEmpty(UnitPrice.Text.Trim()) ? 0 : Convert.ToDecimal(UnitPrice.Text.Trim());

            TextBox Amount = (TextBox)itemsGridView.Rows[i].FindControl("txtUnitPrice");
            aInfo.DOAmount =  string.IsNullOrEmpty(Amount.Text.Trim()) ? 0 : Convert.ToDecimal(Amount.Text.Trim());
            aList.Add(aInfo);
        }
        return aList;
    }


    private DOCharge PrepareMasterDataForSave()
    {
        var aInfo = new DOCharge();
        aInfo.DOChargeId = 0;
        aInfo.DONo = txt_DONO.Text;
        aInfo.DODate = Convert.ToDateTime(txt_DODate.Text);
        aInfo.LCNo = txt_LCNo.Text;
        aInfo.LCDate = Convert.ToDateTime(txt_LCDate.Text);
        aInfo.ShipmentDate = Convert.ToDateTime(txt_ShipmentDate.Text);
        aInfo.BuyerId = Convert.ToInt32(ddlBuyer.SelectedValue);
        aInfo.MarketingPersonId = Convert.ToInt32(ddlMarketingPerson.SelectedValue);
        return aInfo;
    }

    private void Clear()
    {
        //  txtPINo.Text = "";
        //  txtPIDate.Text = "";
        //  txt_Buyer.Text = "";
        //  txt_vendor.Text = "";
        //  txtRemarks.Text = "";
        ////  ddlVendor.SelectedValue = "";
        //  //ddlColor.SelectedValue = "";
        //  txtRequiredWidth.Text = "";
        //  txtRequiredOunceOrYard.Text = "";
        //  txtRequiredShrinkageLenthMin.Text = "";
        //  hfProformaId.Value = "";

        //  itemsGridView.DataSource = null;
        //  itemsGridView.DataBind();
        //  submitButton.Text = "Save Information";


        Response.Redirect("DOCharge.aspx");
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        Clear();
    }

    public bool HasSisCodeId(string dcstoreId)
    {
        if (dcstoreId != "")
        {
            for (int i = 0; i < itemsGridView.Rows.Count; i++)
            {
                if (itemsGridView.Rows[i].FindControl("").ToString() == dcstoreId)
                {
                    return false;
                    break;

                }
            }
        }

        return true;
    }

    protected void addToListButton_Click(object sender, EventArgs e)
    {
       // add_To_List();
    }

    private void add_To_List()
    {
        if (AddToListValidation())
        {
            DataTable aDataTable = new DataTable();

            aDataTable.Columns.Add("DOChargeId");
            aDataTable.Columns.Add("DOChargeDetailsId");
            aDataTable.Columns.Add("SISCode");
            aDataTable.Columns.Add("ProformaMasterId");
            aDataTable.Columns.Add("PINO");
            aDataTable.Columns.Add("PIDate");
            aDataTable.Columns.Add("Quantity");
            aDataTable.Columns.Add("UnitPrice");
            aDataTable.Columns.Add("DOAmount");
            aDataTable.Columns.Add("CurrencyId");

            DataRow dataRow = null;

            string fabCategory = "";

                dataRow = aDataTable.NewRow();
                dataRow["ProformaMasterId"] = ddlProforma.SelectedValue;
                dataRow["SISCode"] = ddlSisCode.SelectedItem.Text.Trim();
                dataRow["PINO"] = ddlProforma.SelectedItem.Text.Trim();
                dataRow["PIDate"] = Convert.ToDateTime(HFPIDate.Value).ToString("MM/dd/yyyy");
                aDataTable.Rows.Add(dataRow);
                
            for (int i = 0; i < itemsGridView.Rows.Count; i++)
            {
                dataRow = aDataTable.NewRow();

                dataRow["ProformaMasterId"] = itemsGridView.DataKeys[i][0].ToString();
                dataRow["SISCode"] = itemsGridView.Rows[i].FindControl("SISCode").ToString();
                dataRow["PINO"] = itemsGridView.Rows[i].FindControl("PINO").ToString();
                dataRow["PIDate"] = itemsGridView.Rows[i].FindControl("PIDate").ToString();
                dataRow["Quantity"] = itemsGridView.Rows[i].FindControl("Quantity").ToString();
                dataRow["UnitPrice"] = itemsGridView.Rows[i].FindControl("UnitPrice").ToString();
                dataRow["DOAmount"] = itemsGridView.Rows[i].FindControl("DOAmount").ToString();
                aDataTable.Rows.Add(dataRow);
            }

            itemsGridView.DataSource = aDataTable;
            itemsGridView.DataBind();

        }
    }

    private bool AddToListValidation()
    {
        if (ddlSisCode.SelectedValue == "")
        {
            ShowMessageBox("Please select fabric/siscode !!");
            return false;
        }

        return true;
    }

    protected void itemdeleteImageButton_Click(object sender, EventArgs e)
    {
        LinkButton productCodeTextBox = (LinkButton)sender;
        GridViewRow currentRow = (GridViewRow)productCodeTextBox.Parent.Parent;
        int rowindex = 0;
        rowindex = currentRow.RowIndex;

        DataTable aDataTable = new DataTable();

        aDataTable.Columns.Add("DOChargeId");
        aDataTable.Columns.Add("DOChargeDetailsId");
        aDataTable.Columns.Add("SISCode");
        aDataTable.Columns.Add("ProformaMasterId");
        aDataTable.Columns.Add("PINO");
        aDataTable.Columns.Add("PIDate");
        aDataTable.Columns.Add("Quantity");
        aDataTable.Columns.Add("UnitPrice");
        aDataTable.Columns.Add("DOAmount");
        aDataTable.Columns.Add("CurrencyId");

        DataRow dataRow = null;
        for (int i = 0; i < itemsGridView.Rows.Count; i++)
        {
            if (i != rowindex)
            {
                dataRow = aDataTable.NewRow();

                dataRow["ProformaMasterId"] = itemsGridView.DataKeys[i][0].ToString();
                dataRow["SISCode"] = itemsGridView.Rows[i].FindControl("SISCode").ToString();
                dataRow["PINO"] = itemsGridView.Rows[i].FindControl("PINO").ToString();
                dataRow["PIDate"] = itemsGridView.Rows[i].FindControl("PIDate").ToString();
                dataRow["Quantity"] = itemsGridView.Rows[i].FindControl("Quantity").ToString();
                dataRow["UnitPrice"] = itemsGridView.Rows[i].FindControl("UnitPrice").ToString();
                dataRow["DOAmount"] = itemsGridView.Rows[i].FindControl("DOAmount").ToString();
                
                aDataTable.Rows.Add(dataRow);
            }
            //else
            //{
            //    bool s = aDal.Delete_Proforma_DetailById(MasterId, DetailsId);
            //}
        }

        itemsGridView.DataSource = aDataTable;
        itemsGridView.DataBind();


    }

    private void MaxMin(double max, double min)
    {
        if (min > max)
        {
            ShowMessageBox("Max value can not be less then min value");
        }

    }

    protected void txt_Buyer_OnTextChanged(object sender, EventArgs e)
    {
        
    }

    protected void txt_vendor_OnTextChanged(object sender, EventArgs e)
    {
        
    }

    protected void txt_siscode_OnTextChanged(object sender, EventArgs e)
    {
       
    }

    protected void txtColor_OnTextChanged(object sender, EventArgs e)
    {
        

       
    }

    protected void itemsGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int row = Convert.ToInt32(e.CommandArgument.ToString());

            TextBox txtRequiredWidth = (TextBox)itemsGridView.Rows[row].FindControl("txtRequiredWidth");
            txtRequiredWidth.ReadOnly = false;

            TextBox txtRequiredOunceYard = (TextBox)itemsGridView.Rows[row].FindControl("txtRequiredOunceYard");
            txtRequiredOunceYard.ReadOnly = false;

            TextBox txtShrinkageLength = (TextBox)itemsGridView.Rows[row].FindControl("txtShrinkageLength");
            txtShrinkageLength.ReadOnly = false;

            TextBox txtShrinkageWidth = (TextBox)itemsGridView.Rows[row].FindControl("txtShrinkageWidth");
            txtShrinkageWidth.ReadOnly = false;

            LinkButton alinkButton = (LinkButton)itemsGridView.Rows[row].FindControl("lbtUpDate");
            alinkButton.Visible = true;

        }

        if (e.CommandName == "UpdateData")
        {

            int row = Convert.ToInt32(e.CommandArgument.ToString());
            ProformaDetailDao aInfo = new ProformaDetailDao();

            string ProformaMasterId = itemsGridView.DataKeys[row][11].ToString();

            TextBox txtRequiredWidth = (TextBox)itemsGridView.Rows[row].FindControl("txtRequiredWidth");
            TextBox txtRequiredOunceYard = (TextBox)itemsGridView.Rows[row].FindControl("txtRequiredOunceYard");
            TextBox txtShrinkageLength = (TextBox)itemsGridView.Rows[row].FindControl("txtShrinkageLength");

            string ShrinkageLength = txtShrinkageLength.Text.Trim();
            if (ShrinkageLength.Contains('-'))
            {
                string[] productInfo = ShrinkageLength.Split('-');
                aInfo.ShrinkageLengthMin = Convert.ToDecimal(productInfo[0]);
                aInfo.ShrinkageLengthMax = Convert.ToDecimal(productInfo[1]);
            }

            TextBox txtShrinkageWidth = (TextBox)itemsGridView.Rows[row].FindControl("txtShrinkageWidth");
            string ShrinkageWidth = txtShrinkageWidth.Text.Trim();
            if (ShrinkageWidth.Contains('-'))
            {
                string[] productInfo = ShrinkageWidth.Split('-');
                aInfo.ShrinkageWidthMin = Convert.ToDecimal(productInfo[0].Trim());
                aInfo.ShrinkageWidthMax = Convert.ToDecimal(productInfo[1].Trim());
            }

            aInfo.SisCode = itemsGridView.Rows[row].Cells[1].Text.Trim();
            aInfo.RequiredOunceOryard = Convert.ToDecimal(txtRequiredOunceYard.Text);
            aInfo.RequiredWidth = Convert.ToDecimal(txtRequiredWidth.Text);
            aInfo.ProformaMasterId = Convert.ToInt32(ProformaMasterId);

            aDal.RowWise_UpdateInfo(aInfo);

        }

    }


  

    protected void ddlPINumber_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        
        if (ddlProforma.SelectedValue != "")
        {
            DataTable aTable = aDal.GetProformaInfoById(int.Parse(ddlProforma.SelectedValue));
            if (aTable.Rows.Count > 0)
            {
                HFPIDate.Value = aTable.Rows[0]["ProformaDate"].ToString();

                if (aTable.Rows[0]["MarketingPersonId"] != null)
                {
                    ddlMarketingPerson.SelectedValue = aTable.Rows[0]["MarketingPersonId"].ToString();
                }
            }
            qDal.LoadSisCodeByPi(ddlSisCode, Convert.ToInt32(ddlProforma.SelectedValue));

            //DataTable dt = qDal.LoadBuyerVendor(ddlProforma.SelectedValue);

            //if (dt.Rows.Count > 0)
            //{

            //    int fabricCount = 0;
            //    fabricCount = ddlSisCode.Items.Count;

            //    if (fabricCount == 2)
            //    {
            //        ddlSisCode.SelectedIndex = 1;
            //    }
            //}
        }


    }

    protected void ddlSisCode_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        add_To_List();
    }
}