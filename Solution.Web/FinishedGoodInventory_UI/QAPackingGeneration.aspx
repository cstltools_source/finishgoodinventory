﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/NewMasterPage.master" AutoEventWireup="true" CodeFile="QAPackingGeneration.aspx.cs" Inherits="FinishedGoodInventory_UI_QAPackingGeneration" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=3.0.20820.28364, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style>
        fieldset {
            /*border:1px solid grey !important;*/
            padding: 10px !important;
        }

        #ContentPlaceHolder1_rbDeliveryType_0,
        #ContentPlaceHolder1_rbDeliveryType_1,
        #ContentPlaceHolder1_rbDeliveryType_2,
        #ContentPlaceHolder1_rbDeliveryType_3,
        #ContentPlaceHolder1_rbDeliveryType_4,
        #ContentPlaceHolder1_rbDeliveryType_5,
        #ContentPlaceHolder1_rbDeliveryType_6,
        #ContentPlaceHolder1_rbDeliveryType_7,
        #ContentPlaceHolder1_rbDeliveryType_8 {
            margin-right: 5px !important;
        }


        #ContentPlaceHolder1_cbxShadeGrades_0,
        #ContentPlaceHolder1_cbxShadeGrades_1,
        #ContentPlaceHolder1_cbxShadeGrades_2,
        #ContentPlaceHolder1_cbxShadeGrades_3,
        #ContentPlaceHolder1_cbxShadeGrades_4,
        #ContentPlaceHolder1_cbxShadeGrades_5,
        #ContentPlaceHolder1_cbxShadeGrades_6, 
        #ContentPlaceHolder1_cbxShadeGrades_7, 
        #ContentPlaceHolder1_cbxShadeGrades_8, 
        #ContentPlaceHolder1_cbxShadeGrades_9 {
            margin-right: 5px !important;
            margin-left: 20px !important;
            
        }

        #ContentPlaceHolder1_cbxRemarks_0,
        #ContentPlaceHolder1_cbxRemarks_1,
        #ContentPlaceHolder1_cbxRemarks_2,
        #ContentPlaceHolder1_cbxRemarks_3,
        #ContentPlaceHolder1_cbxRemarks_4,
        #ContentPlaceHolder1_cbxRemarks_5,
        #ContentPlaceHolder1_cbxRemarks_6, 
        #ContentPlaceHolder1_cbxRemarks_7, 
        #ContentPlaceHolder1_cbxRemarks_8, 
        #ContentPlaceHolder1_cbxRemarks_9,
        #ContentPlaceHolder1_cbxRemarks_10,
        #ContentPlaceHolder1_cbxRemarks_11,
        #ContentPlaceHolder1_cbxRemarks_12,
        #ContentPlaceHolder1_cbxRemarks_13,
        #ContentPlaceHolder1_cbxRemarks_14,
        #ContentPlaceHolder1_cbxRemarks_15,
        #ContentPlaceHolder1_cbxRemarks_16, 
        #ContentPlaceHolder1_cbxRemarks_17, 
        #ContentPlaceHolder1_cbxRemarks_18, 
        #ContentPlaceHolder1_cbxRemarks_19,
        #ContentPlaceHolder1_cbxRemarks_20  
        {
            margin-right: 5px !important;
            margin-left: 5px !important;
            
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div id="popDiv">
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>QA Packing Generation</div>
                        <%--<div class="ms-auto">
                    <div class="btn-group">
                        <a href="../FinishedGoodInventory_UI/PIView.aspx" class="btn btn-sm btn-sm btn-outline-info"><i class="fa fa-backward"></i>&nbsp;Back to List</a>
                    </div>
                </div>--%>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">

                            <script type="text/javascript">
                                function pageLoad() {
                                    $('.datepicker').pickadate({
                                        selectMonths: true,
                                        selectYears: true
                                    });
                                    $('.mySelect2').select2({
                                        theme: 'bootstrap4',
                                        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                        placeholder: $(this).data('placeholder'),
                                        allowClear: Boolean($(this).data('allow-clear')),
                                    });
                                }
                            </script>

                            <div class="card border-top border-0 border-4 border-success">


                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <%-- <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-search me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Search Options</h5>
                                        </div>
                                        <hr>--%>

                                        <div class="row mb-2">

                                            <div class="col-5">
                                                <div class="card border-top border-0 border-4 pb-2">
                                                    <div class="card-body" style="padding-bottom: 1px !important;">
                                                        <div class="card-title">
                                                            <h6 class="mb-0 text-secondary">Packing Info </h6>
                                                            <hr />
                                                        </div>

                                                        <div class="row form-group">
                                                            <label for="" class="col-sm-4 col-form-label col-form-label-sm">Packing By:<span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox class="form-control" ReadOnly="True" CssClass="form-control form-control-sm" runat="server" ID="tbxPackingBy"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="row form-group">
                                                            <label for="" class="col-sm-4 col-form-label col-form-label-sm">Packing Date:<span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="tbxPackingDate" runat="server" ReadOnly="True" class="form-control form-control-sm datepicker"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="row form-group">
                                                            <label for="" class="col-sm-4 col-form-label col-form-label-sm">Reason:<span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="tbxPackingReason" TextMode="MultiLine" Rows="6" runat="server" CssClass="form-control form-control-sm" placeholder="Delivery remarks/instructions"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="row form-group mt-1">
                                                            <label for="" class="col-sm-4 col-form-label col-form-label-sm">Total Quantity:<span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="tbxPackingQuantity" runat="server" ReadOnly="True" CssClass="form-control form-control-sm"></asp:TextBox>
                                                            </div>
                                                        </div>


                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-7">
                                                <div class="card border-top border-0 border-4">
                                                    <div class="card-body">
                                                        <div class="card-title">
                                                            <h6 class="mb-0 text-secondary ">Search options</h6>
                                                            <hr />
                                                        </div>
                                                        
                                                        
                                                        <div class="row form-group" runat="server">
                                                            <label for="" class="col-sm-4 col-form-label col-form-label-sm"> PI NO: <span style="color: orangered">[*]</span></label>
                                                            <div class="col-sm-8">
                                                                <asp:DropDownList class="form-select mySelect2" AutoPostBack="True" OnSelectedIndexChanged="ddlProforma_OnSelectedIndexChanged" runat="server" ID="ddlProforma"></asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="row form-group" runat="server">
                                                            <label for="" class="col-sm-4 col-form-label col-form-label-sm">Fabric/SIS Code: <span style="color: orangered">[*]</span></label>
                                                            <div class="col-sm-8">
                                                                <asp:DropDownList class="form-select mySelect2" AutoPostBack="True" OnSelectedIndexChanged="ddlFabric_OnSelectedIndexChanged" runat="server" ID="ddlFabric"></asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row ">
                                                            <label for="" class="col-sm-4 col-form-label col-form-label-sm">Actual/Cuttable Width: </label>
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="tbxActualWidthMin" runat="server" placeholder="Min" AutoPostBack="True" OnTextChanged="tbxActualWidthMin_OnTextChanged" CssClass="form-control form-control-sm "></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="tbxActualWidthMin" FilterType="Custom, Numbers" ValidChars="." />
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="tbxActualWidthMax" AutoPostBack="True" OnTextChanged="tbxActualWidthMax_OnTextChanged" runat="server" placeholder="Max" CssClass="form-control form-control-sm "></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="tbxActualWidthMax" FilterType="Custom, Numbers" ValidChars="." />
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group row ">
                                                            <label for="" class="col-sm-4 col-form-label col-form-label-sm">Roll Length: </label>
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="tbxRollLengthMin" runat="server" placeholder="Min" AutoPostBack="True" OnTextChanged="tbxRollLengthMin_OnTextChanged" CssClass="form-control form-control-sm "></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="tbxRollLengthMin" FilterType="Custom, Numbers" ValidChars="." />
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="tbxRollLengthMax" AutoPostBack="True" OnTextChanged="tbxRollLengthMax_OnTextChanged" runat="server" placeholder="Max" CssClass="form-control form-control-sm "></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="tbxRollLengthMax" FilterType="Custom, Numbers" ValidChars="." />
                                                            </div>
                                                        </div>

                                                 
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                      
                                                        

                                                        


                                                        <div class="form-group row " runat="server" Visible="False">
                                                            <label for="" class="col-sm-4 col-form-label col-form-label-sm">Required Ounce/Yard<sup>2</sup>: </label>
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="tbxOunceYardMin" AutoPostBack="True" OnTextChanged="tbxOunceYardMin_OnTextChanged" runat="server" placeholder="Min" CssClass="form-control form-control-sm "></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="tbxOunceYardMin" FilterType="Custom, Numbers" ValidChars="." />
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="tbxOunceYardMax" runat="server" AutoPostBack="True" OnTextChanged="tbxOunceYardMax_OnTextChanged" placeholder="Max" CssClass="form-control form-control-sm "></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="tbxOunceYardMax" FilterType="Custom, Numbers" ValidChars="." />
                                                            </div>
                                                        </div>

                                                        <div class="form-group row " runat="server" Visible="False">
                                                            <label for="" class="col-sm-4 col-form-label col-form-label-sm">Shrinkage (%): </label>
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="tbxShrinkageMin" AutoPostBack="True" OnTextChanged="tbxShrinkageMin_OnTextChanged" runat="server" placeholder="Min" CssClass="form-control form-control-sm "></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="freqQtyTextBox1" runat="server" TargetControlID="tbxShrinkageMin" FilterType="Custom, Numbers" ValidChars="." />
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="tbxShrinkageMax" AutoPostBack="True" OnTextChanged="tbxShrinkageMax_OnTextChanged" runat="server" placeholder="Max" CssClass="form-control form-control-sm "></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="tbxShrinkageMax" FilterType="Custom, Numbers" ValidChars="." />
                                                            </div>
                                                        </div>

                                                        <div class="row form-group" runat="server" Visible="False">
                                                            <label for="" class="col-sm-4 col-form-label col-form-label-sm">Shade Grade:</label>
                                                            <div class="col-sm-8">
                                                                <asp:DropDownList class="form-select mySelect2" AutoPostBack="True" OnSelectedIndexChanged="ddlBuyer_OnSelectedIndexChanged" runat="server" ID="ddlShadeGrade"></asp:DropDownList>

                                                            </div>
                                                        </div>



                                                        <hr />
                                                        <div class="row mb-4">

                                                            <div class="col-4"></div>
                                                            <div class="col-4" style="margin: 0 auto !important;">
                                                                <asp:Button ID="Button1" CssClass="btn btn-sm btn-outline-success align-center" runat="server" Text="Search" OnClick="Button1_Click" />
                                                                <asp:LinkButton runat="server" class="btn btn-sm btn-outline-warning" ID="resetButton" OnClick="resetButton_Click"><span style="font-weight: bold !important">Reset</span></asp:LinkButton>
                                                            </div>
                                                            <div class="col-4"></div>

                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    
                                    <div class="row">

                                        <div class="row form-group">

                                                            <div class="col-4">
                                                                <div class="card" style="box-shadow: none !important;">
                                                                    <div class="border p-2 rounded">
                                                                        <div class="card-title">

                                                                            <h6 class="mb-0 text-secondary" style="width: 100% !important">Shade Grades</h6>
                                                                            <hr />

                                                                        </div>

                                                                        <div class="card-body" style="overflow: scroll !important; max-height: 130px !important">
                                                                            <div class="row mb-1">
                                                                                <div style="display: inline !important">
                                                                                    <asp:CheckBox ID="cbxShadeAll" Style="margin-left: 5px !important" runat="server" AutoPostBack="True" OnCheckedChanged="cbxShadeAll_CheckedChanged" /><span>&nbsp;<b>Check All</b></span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row mb-1">
                                                                                <asp:CheckBoxList ID="cbxShadeGrades" AutoPostBack="True" CellPadding="5" CellSpacing="5" OnSelectedIndexChanged="cbxShadeGrades_OnSelectedIndexChanged"
                                                                                    RepeatColumns="5" RepeatDirection="Horizontal" RepeatLayout="Flow" TextAlign="Right" runat="server">
                                                                                </asp:CheckBoxList>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-4">
                                                                <div class="card" style="box-shadow: none !important;">
                                                                    <div class="border p-2 rounded">
                                                                        <div class="card-title">

                                                                            <h6 class="mb-0 text-secondary" style="width: 100% !important">Remarks</h6>
                                                                            <hr />

                                                                        </div>

                                                                        <div class="card-body" style="overflow: scroll !important; max-height: 130px !important">
                                                                            <div class="row mb-1">
                                                                                <div style="display: inline !important">
                                                                                    <asp:CheckBox ID="cbxRemarksAll" Style="margin-left: 5px !important" runat="server" AutoPostBack="True" OnCheckedChanged="cbxRemarksAll_CheckedChanged" /><span>&nbsp;<b>Check All</b></span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row mb-1">
                                                                                <asp:CheckBoxList ID="cbxRemarks"  CellPadding="5" CellSpacing="5" 
                                                                                   
                                                                                    RepeatColumns="5" RepeatDirection="Horizontal" RepeatLayout="Flow" TextAlign="Right" runat="server">
                                                                                </asp:CheckBoxList>
                                                                                
                                                                                 <%--OnSelectedIndexChanged="cbxRemarks_OnSelectedIndexChanged"--%>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                            
                                            
                                            
                                              <div class="col-4">
                                                                <div class="card" style="box-shadow: none !important;">
                                                                    <div class="border p-2 rounded">
                                                                        <div class="card-title">

                                                                            <h6 class="mb-0 text-secondary" style="width: 100% !important">Actual/Cuttable Width</h6>
                                                                            <hr />

                                                                        </div>

                                                                        <div class="card-body" style="overflow: scroll !important; max-height: 130px !important">
                                                                            <div class="row mb-1">
                                                                                <div style="display: inline !important">
                                                                                    <asp:CheckBox ID="CkActualWithAll" Style="margin-left: 5px !important" runat="server" AutoPostBack="True" OnCheckedChanged="ckActualWith_OnSelectedIndexChanged" /><span>&nbsp;<b>Check All</b></span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row mb-1">
                                                                                <asp:CheckBoxList ID="CKActualwith" AutoPostBack="True" CellPadding="5" CellSpacing="5" OnSelectedIndexChanged="cbxShadeGrades_OnSelectedIndexChanged"
                                                                                    RepeatColumns="5" RepeatDirection="Horizontal" RepeatLayout="Flow" TextAlign="Right" runat="server">
                                                                                </asp:CheckBoxList>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                            
                                            

                                                        </div>

                                        <div class="row form-group">

                                                            <div class="col-md-4">
                                                                <div class="card" style="box-shadow: none !important;">
                                                                    <div class="border p-2 rounded">
                                                                        <div class="card-title">

                                                                            <h6 class="mb-0 text-secondary" style="width: 100% !important">SET Number</h6>
                                                                            <hr />

                                                                        </div>

                                                                        <div class="card-body" style="overflow: scroll !important; max-height: 130px !important">
                                                                            <div class="row mb-1">
                                                                                <div style="display: inline !important">
                                                                                    <asp:CheckBox ID="cbxSETNumberAll" Style="margin-left: 5px !important" runat="server" AutoPostBack="True" OnCheckedChanged="cbxSETNumber_CheckedChanged" /><span>&nbsp;<b>Check All</b></span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row mb-1">
                                                                                <asp:CheckBoxList ID="cbxSETNumber" CellPadding="5" CellSpacing="5" 
                                                                                    RepeatColumns="5" RepeatDirection="Horizontal" RepeatLayout="Flow" TextAlign="Right" runat="server">
                                                                                </asp:CheckBoxList>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="card" style="box-shadow: none !important;">
                                                                    <div class="border p-2 rounded">
                                                                        <div class="card-title">

                                                                            <h6 class="mb-0 text-secondary" style="width: 100% !important">PPHSY</h6>
                                                                            <hr />

                                                                        </div>

                                                                        <div class="card-body" style="overflow: scroll !important; max-height: 130px !important">
                                                                            <div class="row mb-1">
                                                                                <div style="display: inline !important">
                                                                                    <asp:CheckBox ID="CkPPHSYAll" Style="margin-left: 5px !important" runat="server" AutoPostBack="True" OnCheckedChanged="cbxPPHSYAll_CheckedChanged" /><span>&nbsp;<b>Check All</b></span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row mb-1">
                                                                                <asp:CheckBoxList ID="CkPPHSY"  CellPadding="5" CellSpacing="5" 
                                                                                   
                                                                                    RepeatColumns="5" RepeatDirection="Horizontal" RepeatLayout="Flow" TextAlign="Right" runat="server">
                                                                                </asp:CheckBoxList>
                                                                                
                                                                                 <%--OnSelectedIndexChanged="cbxRemarks_OnSelectedIndexChanged"--%>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                            
                                            
                                            
                                             <div class="col-md-4">
                                                                <div class="card" style="box-shadow: none !important;">
                                                                    <div class="border p-2 rounded">
                                                                        <div class="card-title">

                                                                            <h6 class="mb-0 text-secondary" style="width: 100% !important">DEFECT POINTS</h6>
                                                                            <hr />

                                                                        </div>

                                                                        <div class="card-body" style="overflow: scroll !important; max-height: 130px !important">
                                                                            <div class="row mb-1">
                                                                                <div style="display: inline !important">
                                                                                    <asp:CheckBox ID="ckDEFECTPOINTSAll" Style="margin-left:5px !important" runat="server" AutoPostBack="True" OnCheckedChanged="ckDEFECTPOINTSAll_OnCheckedChanged" /><span>&nbsp;<b>Check All</b></span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row mb-1">
                                                                                <asp:CheckBoxList ID="ckDEFECTPOINTS"  CellPadding="5" CellSpacing="5" 
                                                                                    RepeatColumns="5" RepeatDirection="Horizontal" RepeatLayout="Flow" TextAlign="Right" runat="server">
                                                                                </asp:CheckBoxList>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                        


                                    </div>
                                    


                                        <hr />
                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-file me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Roll Description</h5>
                                        </div>
                                        <hr>


                                        <div class="row ">

                                            <div id="maingridview" style="text-align: center; height: auto; overflow: scroll; width: 100%; overflow-y: scroll; overflow-x: scroll;">
                                                <asp:GridView ID="productGridView" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-condensed custom-table-style"
                                                    DataKeyNames="FGStockId,StockQuantity" EmptyDataText="There are no data records to display.">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="# SL">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="FabricName" HeaderText="Fabric / SIS Code" />
                                                        <asp:BoundField DataField="ProductionDate" HeaderText="ProductionDate" />
                                                        <asp:BoundField DataField="RequiredOunceYard" HeaderText="Ounce/Yard" />
                                                        <asp:BoundField DataField="ActualWidth" HeaderText="Cuttable Width" />
                                                        <asp:BoundField DataField="DefectPoint" HeaderText="DefectPoint" />
                                                        <asp:BoundField DataField="ShadeGradeing" HeaderText="Shade Grade" />
                                                        <asp:BoundField DataField="FabricSet" HeaderText="Set" />
                                                        <asp:BoundField DataField="PPHSY" HeaderText="PPHSY" />
                                                        <asp:BoundField DataField="Piece" HeaderText="Piece" />
                                                        <asp:BoundField DataField="PWLength" HeaderText="P.W Length" />
                                                        <asp:BoundField DataField="GreigeRollNo" HeaderText="GRL No" />
                                                        <asp:BoundField DataField="RollNo" HeaderText="Roll No" />
                                                        <asp:BoundField DataField="StockQuantity" HeaderText="Quantity" />
                                                        <asp:BoundField DataField="LocationName" HeaderText="Current Location" />
                                                        <asp:BoundField DataField="ProblemCause" HeaderText="Problem Cause" />
                                                        <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True" OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" AutoPostBack="True" OnCheckedChanged="chkSelect_CheckedChanged" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>


                                        </div>
                                        <br />
                                        <div class="row">

                                            <div class="col-4"></div>
                                            <div class="col-3" style="">
                                                <asp:LinkButton runat="server" class="btn btn-outline-primary" ID="submitButton" OnClientClick="return confirm('Are you really aware of this operation?');" OnClick="submitButton_Click">
                                                    <span style="font-weight: bold !important">Issue For Delivery </span> <i class="bx bxs-right-arrow-circle mr-1"></i>
                                                </asp:LinkButton>
                                            </div>
                                            <div class="col-4" style=""></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                    Height="120" Width="120" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>

