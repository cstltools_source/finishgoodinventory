﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/NewMasterPage.master" AutoEventWireup="true" CodeFile="DOChargeView.aspx.cs" Inherits="FinishedGoodInventory_UI_DOChargeView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    
    
       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div id="popDiv">
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Delivery Order View</div>

                        <div class="ms-auto">
                            <div class="btn-group">
                                <a href="../FinishedGoodInventory_UI/DOCharge.aspx" class="btn btn-sm btn-outline-info "><i class="fa fa-plus" aria-hidden="true"></i>New Entry</a>
                            </div>
                        </div>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">

                            <div class="card border-top border-0 border-4 border-success">

                               <%-- <script type="text/javascript">
                                    function pageLoad() {
                                        $('.datepicker').pickadate({
                                            selectMonths: true,
                                            selectYears: true
                                        });
                                        $('.mySelect2').select2({
                                            theme: 'bootstrap4',
                                            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                            placeholder: $(this).data('placeholder'),
                                            allowClear: Boolean($(this).data('allow-clear')),
                                        });
                                    }
                                </script>


                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bx-list-ul me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Delivery Order List</h5>
                                        </div>
                                        <hr>

                                        <div class="row mb-2">

                                            <div class="col-md-4">
                                                <script type="text/javascript">
                                                    function pageLoad() {
                                                        $('.datepicker').pickadate({
                                                            selectMonths: true,
                                                            selectYears: true
                                                        });
                                                        $('.mySelect2').select2({
                                                            theme: 'bootstrap4',
                                                            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                                            placeholder: $(this).data('placeholder'),
                                                            allowClear: Boolean($(this).data('allow-clear')),
                                                        });
                                                    }
                                                </script>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Buyer Name </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2" AutoPostBack="True" OnSelectedIndexChanged="ddlBuyer_OnSelectedIndexChanged" runat="server" ID="ddlBuyer"></asp:DropDownList>
                                                    </div>
                                                </div>


                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Vendor Name </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2" AutoPostBack="True" OnSelectedIndexChanged="ddlVendor_OnSelectedIndexChanged" runat="server" ID="ddlVendor"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">PI No </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2" AutoPostBack="True" OnSelectedIndexChanged="ddlPi_OnSelectedIndexChanged" runat="server" ID="ddlPi"></asp:DropDownList>
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Required Width </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2" AutoPostBack="True" OnSelectedIndexChanged="ddlWidth_OnSelectedIndexChanged" runat="server" ID="ddlWidth"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Ounce/Yard<sup>2</sup> </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2" AutoPostBack="True" OnSelectedIndexChanged="ddlOunceYard_OnSelectedIndexChanged" runat="server" ID="ddlOunceYard"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm"> Shrinkage </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2" AutoPostBack="True" OnSelectedIndexChanged="ddlShrinkage_OnSelectedIndexChanged" runat="server" ID="ddlShrinkage"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">From Date (PI) </label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="fromDateTextBox" runat="server" class="form-control form-control-sm datepicker"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">To Date (PI) </label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="toDateTextBox" runat="server" class="form-control form-control-sm datepicker"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <hr />
                                        
                                         <div class="row mb-4">

                                            <div class="col-3"></div>
                                            <div class="col-6" style="">
                                                <asp:LinkButton runat="server" class="btn btn-outline-primary" ID="submitButton" OnClick="searchButton_Click">                                            
                                                            <i class="fa fa-search-plus"></i><span style="font-weight: bold !important">Search Information</span>
                                                </asp:LinkButton>
                                                <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="resetButton" OnClick="resetButton_Click">                                            
                                                            <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Information</span>
                                                </asp:LinkButton>
                                            </div>
                                            <div class="col-3"></div>

                                        </div>--%>

                                        <hr />

                                        <div id="MainGradeDiv" style="text-align: center !important">
                                            <asp:GridView ID="itemsGridView" runat="server" AutoGenerateColumns="False"
                                                PageIndex="0" CssClass="table table-bordered text-center table-condensed custom-table-style" DataKeyNames="DOChargeId" OnRowCommand="itemsGridView_RowCommand"
                                                AllowPaging="True" OnPageIndexChanging="loadGridView_PageIndexChanging" PageSize="10" EmptyDataText="There are no data records to display.">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="SL">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="DONo" HeaderText="Buyer" />
                                                    <asp:BoundField DataField="DODate" HeaderText="Vendor" DataFormatString="{0:dd-MMM-yyyy}" />
                                                    <asp:BoundField DataField="LCNo" HeaderText="Proforma Code" />
                                                    <asp:BoundField DataField="LCDate" HeaderText="Proforma Name/No"  DataFormatString="{0:dd-MMM-yyyy}"/>
                                                    <asp:BoundField DataField="ShipmentDate" HeaderText="Proforma Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                    <asp:BoundField DataField="ItemDescription" HeaderText="Description of Good" />
                                                    <asp:BoundField DataField="ProformaInvNo" HeaderText="PI No" />
                                                    <asp:BoundField DataField="PIDate" HeaderText="PI Date" />
                                                    <asp:BoundField DataField="Quantity" HeaderText="Quantity" />
                                                    <asp:BoundField DataField="UnitPrice" HeaderText="Unit Price" />
                                                    <asp:BoundField DataField="DOAmount" HeaderText="Amount" />

                                                <%--    <asp:TemplateField HeaderText="Actions">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="editImageButton" runat="server" class="btn btn-outline-info btn-sm  " ToolTip="Edit" CommandArgument='<%#Eval("DOChargeId") %>'
                                                                CommandName="EditData" ImageUrl="~/assets/images/pencil-icon.png" Height="30" Width="34" />

                                                        </ItemTemplate>
                                                        <ItemStyle Width="10%"></ItemStyle>
                                                    </asp:TemplateField>--%>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>



    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                    Height="120" Width="120" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    

</asp:Content>

