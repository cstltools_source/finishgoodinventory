﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_StoreApprovalDetail : System.Web.UI.Page
{
    private readonly QAStockVerificationDal aDal = new QAStockVerificationDal();
    private readonly StoreReceiveApprovalDal approvalDal = new StoreReceiveApprovalDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["StockReceiveMasterId"] != null)
            {
                GetOneRecord(Convert.ToInt32(Session["StockReceiveMasterId"].ToString()));
                Session["StockReceiveMasterId"] = null;
            }
            else
            {
                Response.Redirect("StoreApprovalList.aspx");
            }
        }
    }

    private void GetOneRecord(int stockReceiveMasterId)
    {
        var aTable = new DataTable();

        aTable = aDal.GetQAStockReceiveMasterById(stockReceiveMasterId, "verified");

        if (aTable.Rows.Count > 0)
        {
            QAStockRId.Value = aTable.Rows[0]["StockReceiveMasterId"].ToString();
            lblReceiveType.Text = aTable.Rows[0]["MovementCategory"].ToString();
            lblProductiondate.Text = Convert.ToDateTime(aTable.Rows[0]["ProductionDate"]).ToString("dd-MMM-yyyy");
            lblProductionShift.Text = aTable.Rows[0]["ShiftTitle"].ToString();
            lblEntryBy.Text = aTable.Rows[0]["EntryBy"].ToString();
          //  lblVerifiedBy.Text = aTable.Rows[0]["VerifiedBy"].ToString();
            lblEntryDate.Text = Convert.ToDateTime(aTable.Rows[0]["EntryDate"]).ToString("dd-MMM-yyyy");
           // lblVerifiedDate.Text = Convert.ToDateTime(aTable.Rows[0]["VerifiedDate"]).ToString("dd-MMM-yyyy");
            lblFabricName.Text = aTable.Rows[0]["FabricName"].ToString();
            lblColor.Text = aTable.Rows[0]["ColorName"].ToString();
            lblSetNo.Text = aTable.Rows[0]["FabricSet"].ToString();
            lblFLNo.Text = aTable.Rows[0]["FL"].ToString();
            lblQaLocation.Text = aTable.Rows[0]["LocationName"].ToString();
            lblRequiredWidth.Text = aTable.Rows[0]["RequiredWidth"].ToString();
            lblOunceYard.Text = aTable.Rows[0]["RequiredOunceYard"].ToString();
            lblShrinkage.Text = aTable.Rows[0]["ShrinkageLength"].ToString();
            lblShrinkageWidth.Text = aTable.Rows[0]["ShrinkageWidth"].ToString();
            QAStockDetails.DataSource = aTable;
            QAStockDetails.DataBind();
            Line_Total_Count();
        }
    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private bool Validation()
    {
        decimal temValue = 0;
        decimal value = 0;
        decimal temPhyQuantity = 0;
        decimal PhyQuantity = 0;
        for (int k = 0; k < QAStockDetails.Rows.Count; k++)
        {
            string LTotal = QAStockDetails.DataKeys[k].Values[0].ToString();
            TextBox Quantity = (TextBox)QAStockDetails.Rows[k].FindControl("TxtQuantity");
            if (LTotal != "")
            {
                value = decimal.Parse(LTotal.Trim());
                temValue += value;
                PhyQuantity = string.IsNullOrEmpty(Quantity.Text.Trim()) ? 0 : Decimal.Parse(Quantity.Text.Trim());
                temPhyQuantity += PhyQuantity;
            }
        }

        if (temValue != temPhyQuantity)
        {
            return false;
        }

        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            QaStockReceiveMasterDao aMasterDao = new QaStockReceiveMasterDao();
            aMasterDao.StockReceiveMasterId = Convert.ToInt32(QAStockRId.Value);
            bool status = approvalDal.Approved_QAStockReceive(aMasterDao);
            if (status)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "alert",
                    "alert('Successfully Approved...!');window.location ='StoreApprovalList.aspx';",
                    true);
            }
        }
        else
        {
            ShowMessageBox("Difference between received & physical quantity must be 0 !!");
        }


    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }



    private void Line_Total_Count()
    {
        QAStockDetails.FooterRow.Cells[11].Text = "Total";
        QAStockDetails.FooterRow.Cells[11].HorizontalAlign = HorizontalAlign.Right;
        decimal temValue = 0;
        decimal value = 0;

        decimal temPhyQuantity = 0;
        decimal PhyQuantity = 0;

        for (int k = 0; k < QAStockDetails.Rows.Count; k++)
        {
            string LTotal = QAStockDetails.DataKeys[k].Values[0].ToString();
            TextBox Quantity = (TextBox)QAStockDetails.Rows[k].FindControl("TxtQuantity");

            if (LTotal != "")
            {
                value = decimal.Parse(LTotal);
                temValue += value;
                PhyQuantity = string.IsNullOrEmpty(Quantity.Text.Trim()) ? 0 : Decimal.Parse(Quantity.Text);

                temPhyQuantity += PhyQuantity;

                QAStockDetails.FooterRow.Cells[10].Text = temValue.ToString();
                QAStockDetails.FooterRow.Cells[10].Font.Bold = true;
                QAStockDetails.FooterRow.Cells[10].HorizontalAlign = HorizontalAlign.Right;
                QAStockDetails.FooterRow.BackColor = System.Drawing.Color.Beige;

                QAStockDetails.FooterRow.Cells[11].Text = temPhyQuantity.ToString();
                QAStockDetails.FooterRow.Cells[11].Font.Bold = true;
                QAStockDetails.FooterRow.Cells[11].HorizontalAlign = HorizontalAlign.Right;
                QAStockDetails.FooterRow.BackColor = System.Drawing.Color.Beige;

            }

        }
    }

    protected void TxtQuantity_OnTextChanged(object sender, EventArgs e)
    {
        Line_Total_Count();
    }



    
}