﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/NewMasterPage.master" AutoEventWireup="true" CodeFile="FabricDeliveryAndDOStatus.aspx.cs" Inherits="FinishedGoodInventory_UI_FabricDeliveryAndDOStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    <style>
        .large-table-container-3 {
            max-width: 100%;
            max-height: 500px;
            overflow-x: scroll;
            overflow-y: auto;
        }
       

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    
      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div id="popDiv">
            </div>

            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Daily Fabrics Delivery & D.O Status </div>

                       <%-- <div class="ms-auto">
                            <div class="btn-group">
                                <a href="../FinishedGoodInventory_UI/QAStockReceive.aspx" class="btn btn-sm btn-outline-info "><i class="fa fa-plus" aria-hidden="true"></i>New Entry</a>
                            </div>
                        </div>--%>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">
                            <div class="card border-top border-0 border-4 border-success">
                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        
                                        
                                        <script type="text/javascript">
                                            function pageLoad() {
                                                $('.datepicker').pickadate({
                                                    selectMonths: true,
                                                    selectYears: true
                                                });
                                                $('.mySelect2').select2({
                                                    theme: 'bootstrap4',
                                                    width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                                    placeholder: $(this).data('placeholder'),
                                                    allowClear: Boolean($(this).data('allow-clear')),
                                                });
                                            }
                                        </script>
                                        

                                        
                                        <div class="row mb-2">
                                            <label for="" class="col-sm-2 col-form-label">Delivery Date :</label>
                                            <div class="col-sm-6">
                                               
                                                <asp:TextBox ID="txt_DeliveryDate" runat="server" class="form-control  datepicker"></asp:TextBox>
                                            </div>
                                        </div>


                                          

                                        </div>

                                        <hr />

                                        <div class="row mb-4">

                                            <div class="col-3"></div>
                                            <div class="col-6" style="">
                                                <asp:LinkButton runat="server" class="btn btn-outline-primary" ID="LinkButton1" OnClick="searchButton_Click">                                            
                                                            <i class="fa fa-search-plus"></i><span style="font-weight: bold !important">Search Information</span>
                                                </asp:LinkButton>
                                                <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="LinkButton2" OnClick="resetButton_Click">                                            
                                                            <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Information</span>
                                                </asp:LinkButton>
                                            </div>
                                            <div class="col-3"></div>

                                        </div>
                                        
                                    
                                    
                                    
                                    
                                    <hr/>
                                    
                                    <div class="row">
                                         
                                   
                                        <div class="col-md-2">
                                       
                                       
                                        </div>
                                         
                                        <div class="col-md-2">
                                       
                                       
                                        </div>
                                        <div class="col-md-2">
                                       
                                       
                                        </div>
                                        <div class="col-md-3">
                                       
                                       
                                        </div>
                              

                                        <div class="col-md-3">
                                            <asp:LinkButton ID="btnExportToExcel" runat="server" CssClass="btn btn-success pull-right" OnClick="btnExportToExcel_Click" ><span aria-hidden="true" class="fa fa-file-excel-o" ></span> &nbsp;Export To Excel</asp:LinkButton> 
                                       
                                      
                                       
        
                                        </div>
                                    </div>

                                        <hr />

                                        <div class="large-table-container-3">

                                            <asp:GridView ID="itemsGridView" runat="server" AutoGenerateColumns="False" 
                                                PageIndex="0" CssClass="table table-bordered table-condensed custom-table-style" 
                                                          AllowPaging="True" OnPageIndexChanging="loadGridView_PageIndexChanging"  EmptyDataText="There are no data records to display.">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="SL No">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="Vendor" HeaderText="Vendor Name" />

                                          <%--<asp:BoundField DataField="VendorAddres" HeaderText="Vendor Location" />--%>
                                                    <asp:BoundField DataField="ItemDescription" HeaderText="Fabric Code" />
                                                    <asp:BoundField DataField="ProformaInvNo" HeaderText="PI No" />
                                                    <asp:BoundField DataField="BuyerName" HeaderText="Buyer" />
                                                    <asp:BoundField DataField="DODate" HeaderText="D.O Date" />
                                                    <asp:BoundField DataField="DONo" HeaderText="DO. No" />
                                                    <asp:BoundField DataField="DOQuantity" HeaderText="DO. Qty(Yds)" />
                                                    <asp:BoundField DataField="DeliveryQuantity" HeaderText="Total Delivery(Yds) " />
                                                <%--    <asp:BoundField DataField="DOAmount" HeaderText="D.O Balance" />--%>
                                                    <asp:BoundField DataField="DOPrice" HeaderText="DO PRICE" />
                                                    <asp:BoundField DataField="POPrice" HeaderText="PI PRICE" />
                                                   <%-- <asp:BoundField DataField="Variance" HeaderText="VARIANCE" />--%>
                                                    <asp:BoundField DataField="DOAmount" HeaderText="Delivery Value $" />
                                                    <asp:BoundField DataField="MarketingPersonName" HeaderText="Account Holder" />

                                                </Columns>
                                            </asp:GridView>

                                        </div>

                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>

    <Triggers>
        <asp:PostBackTrigger ControlID="btnExportToExcel" /> 
    </Triggers>

    </asp:UpdatePanel>



    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                    Height="120" Width="120" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>

