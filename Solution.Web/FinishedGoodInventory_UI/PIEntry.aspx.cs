﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_PIEntry : System.Web.UI.Page
{
    ProformaInvoiceDal aDal = new ProformaInvoiceDal();

    private QAStockReceiveDal qDal = new QAStockReceiveDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDownList();

           
            if (Session["ProformaId"] != null)
            {
                GetOneRecord(Convert.ToInt32(Session["ProformaId"].ToString()));
                Session["ProformaId"] = null;
            }
        }
    }



    # region Edit
    private void GetOneRecord(int proformaId)
    {
        var aTable = new DataTable();

        aTable = aDal.GetProformaInfoById(proformaId);

        if (aTable.Rows.Count > 0) 
        {
            hfProformaId.Value = aTable.Rows[0].Field<int>("ProformaMasterId").ToString(CultureInfo.InvariantCulture);
            txt_Buyer.Text = aTable.Rows[0].Field<String>("BuyerName");
            txt_vendor.Text = aTable.Rows[0].Field<String>("VendorName");
            HFBuyerName.Value = aTable.Rows[0].Field<String>("BuyerName");
            HFVendorName.Value = aTable.Rows[0].Field<String>("VendorName");
            HFBuyerId.Value = aTable.Rows[0].Field<int>("BuyerId").ToString(CultureInfo.InvariantCulture);
            HFVendorId.Value = aTable.Rows[0].Field<int>("VendorId").ToString(CultureInfo.InvariantCulture);
            txtPIDate.Text = aTable.Rows[0].Field<DateTime>("ProformaDate").ToString("dd-MMM-yyyy");
            txtPINo.Text = aTable.Rows[0].Field<String>("ProformaInvNo");
            txtRemarks.Text = aTable.Rows[0].Field<String>("Remarks");  

            itemsGridView.DataSource = aTable;
            itemsGridView.DataBind();


            if (itemsGridView.Rows.Count > 0)
            {
                for (int i = 0; i < itemsGridView.Rows.Count; i++)
                {
                    var itemdeleteImageButton = (LinkButton)itemsGridView.Rows[i].Cells[1].FindControl("itemdeleteImageButton");


                    var t = itemsGridView.DataKeys[i][12].ToString();

                    if (Convert.ToBoolean(itemsGridView.DataKeys[i][12]))
                    {
                        itemdeleteImageButton.Enabled = true;
                    }
                    else
                    {
                        itemdeleteImageButton.Enabled = false; 
                    }
                }
            }

            submitButton.Text = "Update Information";
        }
    }

    #endregion Edit
    private void LoadDropDownList()
    {
        aDal.LoadMarketingPerson(ddlMarketingPerson);
        //aDal.LoadVendorInfo2(ddlVendor);
        //aDal.LoadColor(ddlColor);
        //aDal.LoadFabric(ddlSisCode);


        using (DataTable dt = qDal.LoadPiNoAll())
        {
            ddlPINumber.DataSource = dt;
            ddlPINumber.DataValueField = "ProformaMasterId";
            ddlPINumber.DataTextField = "ProformaInvNo";
            ddlPINumber.DataBind();
            ddlPINumber.Items.Insert(0, new ListItem("Select from list", String.Empty));
            ddlPINumber.SelectedIndex = 0;

        }
    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private bool Validation()
    {
        //if (ddlBuyer.SelectedValue == "")
        //{
        //    ShowMessageBox("You should select buyer !!");
        //    ddlBuyer.Focus();
        //    ddlBuyer.BackColor = Color.GhostWhite;
        //    return false;
        //}

        //if (ddlVendor.SelectedValue == "")
        //{
        //    ShowMessageBox("You should select vendor !!");
        //    ddlVendor.Focus();
        //    ddlVendor.BackColor = Color.GhostWhite;
        //    return false;
        //}

        if (txt_Buyer.Text.Trim() == "")
        {
            ShowMessageBox("You should enter Buyer !!");
            txt_Buyer.Focus();
            txt_Buyer.BackColor = Color.GhostWhite;
            return false;
        }

        if (txt_vendor.Text.Trim() == "")
        {
            ShowMessageBox("You should enter Vendor !!");
            txt_vendor.Focus();
            txt_vendor.BackColor = Color.GhostWhite;
            return false;
        }


        if (txtPINo.Text.Trim() == "")
        {
            ShowMessageBox("You should enter PI No !!");
            txtPINo.Focus();
            txtPINo.BackColor = Color.GhostWhite;
            return false;
        }

        if (txtPIDate.Text.Trim() == "")
        {
            ShowMessageBox("You should enter PI Date !!");
            txtPIDate.Focus();
            txtPIDate.BackColor = Color.GhostWhite;
            return false;
        }

        int rowsCount = 0;
        rowsCount = itemsGridView.Rows.Count;

        if (rowsCount == 0)
        {
            ShowMessageBox("Please select fabric info !!");
            return false;
        }

        //if (ddlColor.SelectedValue == "")
        //{
        //    ShowMessageBox("You should select color !!");
        //    ddlColor.Focus();
        //    ddlColor.BackColor = Color.GhostWhite;
        //    return false;
        //}
        
        //if (txtRequiredWidth.Text.Trim() == "")
        //{
        //    ShowMessageBox("You should enter required width !!");
        //    txtRequiredWidth.Focus();
        //    txtRequiredWidth.BackColor = Color.GhostWhite;
        //    return false;
        //}
        
        
        //if (txtRequiredOunceOrYard.Text.Trim() == "")
        //{
        //    ShowMessageBox("You should enter required Ounce/Yard !!");
        //    txtRequiredOunceOrYard.Focus();
        //    txtRequiredOunceOrYard.BackColor = Color.GhostWhite;
        //    return false;
        //}
        
        
        //if (txtRequiredShrinkage.Text.Trim() == "")
        //{
        //    ShowMessageBox("You should enter required Shrinkage !!");
        //    txtRequiredShrinkage.Focus();
        //    txtRequiredShrinkage.BackColor = Color.GhostWhite;
        //    return false;
        //}

        return true;
    }



    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {

            if (hfProformaId.Value == "")
            {
                Int32 saveId = SaveChanges();

                if (saveId > 0)
                {
                   

                    ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "alert",
                    "alert('Proforma Invoice Saved Successfully...');window.location ='PIEntry.aspx';",
                    true);
                }
                else
                {
                    ShowMessageBox("Data does not save successfully !!");
                }
            }
            else
            {
                bool saveId = UpdateChanges();

                if (saveId)
                {
                    
                   
                    ScriptManager.RegisterStartupScript(this, this.GetType(),
                   "alert",
                   "alert('Proforma Invoice updated Successfully...');window.location ='PIEntry.aspx';",
                   true);
                }
                else
                {
                    ShowMessageBox("Data does not updated successfully !!");
                }
            }

        }
    }

    private bool UpdateChanges()
    {
        bool retVal;
        try
        {
            retVal = aDal.UpdateInfo(PrepareDataForUpdatee(), PrepareDataForSaveDetail());

        }
        catch (Exception ex)
        {
            retVal = false;
            throw ex;
        }

        return retVal;
    }

    private ProformaMasterDao PrepareDataForUpdatee()
    {
        var aInfo = new ProformaMasterDao();

        aInfo.ProformaMasterId = Convert.ToInt32(hfProformaId.Value);
        aInfo.ProformaInvNo = txtPINo.Text;
        aInfo.ProformaDate = Convert.ToDateTime(txtPIDate.Text);

        if (HFBuyerName.Value.ToUpper().Trim() != txt_Buyer.Text.ToUpper().Trim())
        {
            aInfo.BuyerId = 0;
        }
        else
        {
            aInfo.BuyerId = Convert.ToInt32(HFBuyerId.Value);
        }

        if (HFVendorName.Value.ToUpper().Trim() != txt_vendor.Text.ToUpper().Trim())
        {
            aInfo.VendorId = 0;
        }
        else
        {
            aInfo.VendorId = Convert.ToInt32(HFVendorId.Value);
        }

        aInfo.Remarks = txtRemarks.Text;

        return aInfo;
    }


    private Int32 SaveChanges()
    {
        Int32 retVal;
        try
        {
            retVal = aDal.SaveInfo(PrepareDataForSave(), PrepareDataForSaveDetail());

        }
        catch (Exception ex)
        {
            retVal = 0;
            throw ex;
        }

        return retVal;
    }

    private List<ProformaDetailDao> PrepareDataForSaveDetail()
    {
        List<ProformaDetailDao> aList = new List<ProformaDetailDao>();

        for (int i = 0; i < itemsGridView.Rows.Count; i++)
        {
            //string masterId = string.IsNullOrEmpty(itemsGridView.DataKeys[i][12].ToString()) ? "" : itemsGridView.DataKeys[i][12].ToString();

            //if (masterId.Trim() == "")
            //{
                var aInfo = new ProformaDetailDao();

                if (hfProformaId.Value == "")
                {
                    aInfo.SisCodeId = 0;
                }
                else
                {
                    aInfo.SisCodeId = 0;
                }
                aInfo.FabricName = string.IsNullOrEmpty(itemsGridView.DataKeys[i][5].ToString()) ? null : itemsGridView.DataKeys[i][5].ToString();
                aInfo.ColorName = string.IsNullOrEmpty(itemsGridView.DataKeys[i][4].ToString()) ? null : itemsGridView.DataKeys[i][4].ToString();
                aInfo.ColorId = Convert.ToInt32(itemsGridView.DataKeys[i][0].ToString());
                aInfo.RequiredOunceOryard = Convert.ToDecimal(itemsGridView.DataKeys[i][2].ToString());
                aInfo.RequiredWidth = Convert.ToDecimal(itemsGridView.DataKeys[i][1].ToString());
                aInfo.ShrinkageLengthMin = Convert.ToDecimal(itemsGridView.DataKeys[i][3].ToString());
                aInfo.ShrinkageLengthMax = Convert.ToDecimal(itemsGridView.DataKeys[i][7].ToString());
                aInfo.ShrinkageWidthMin = string.IsNullOrEmpty(itemsGridView.DataKeys[i][8].ToString()) ? 0 : Convert.ToDecimal(itemsGridView.DataKeys[i][8].ToString());
                aInfo.ShrinkageWidthMax = string.IsNullOrEmpty(itemsGridView.DataKeys[i][9].ToString()) ? 0 : Convert.ToDecimal(itemsGridView.DataKeys[i][9].ToString());

                aInfo.ShrinkageWidthMin = string.IsNullOrEmpty(itemsGridView.DataKeys[i][13].ToString()) ? 0 : Convert.ToDecimal(itemsGridView.DataKeys[i][13].ToString());
                aInfo.ShrinkageWidthMax = string.IsNullOrEmpty(itemsGridView.DataKeys[i][14].ToString()) ? 0 : Convert.ToDecimal(itemsGridView.DataKeys[i][14].ToString());
                aInfo.Quantity = string.IsNullOrEmpty(txtQuantity.Text.Trim()) ? 0 : Convert.ToDecimal(txtQuantity.Text.Trim());
                aInfo.UnitPrice = string.IsNullOrEmpty(txtUnitPrice.Text) ? 0 : Convert.ToDecimal(txtUnitPrice.Text.Trim());

                aList.Add(aInfo);

           // }
        }
        return aList;
    }




    private ProformaDetailDao PrepareDataForUpdateDetail()
    {
            var aInfo = new ProformaDetailDao();

            if (HFSisCodeId.Value == "")
            {
                aInfo.SisCodeId = 0;
            }
            else
            {
                aInfo.SisCodeId = int.Parse(HFSisCodeId.Value);
            }
            aInfo.FabricName = string.IsNullOrEmpty(txt_siscode.Text.Trim()) ? null : txt_siscode.Text.Trim();
            aInfo.ColorName = string.IsNullOrEmpty(txtColor.Text.Trim()) ? null : txtColor.Text.Trim();

           if(hfColorId.Value != "")
            {
               aInfo.ColorId = int.Parse(hfColorId.Value);
            } else
            {
              aInfo.ColorId = 0;
            }

            aInfo.ProformaMasterId = Convert.ToInt32(ddlPINumber.SelectedValue);
            aInfo.RequiredOunceOryard = Convert.ToDecimal(txtRequiredOunceOrYard.Text);
            aInfo.RequiredWidth = Convert.ToDecimal(txtRequiredWidth.Text);
            aInfo.ShrinkageLengthMin = Convert.ToDecimal(txtRequiredShrinkageLenthMin.Text);
            aInfo.ShrinkageLengthMax = Convert.ToDecimal(txtRequiredShrinkageLenthMax.Text);
            aInfo.ShrinkageWidthMin = string.IsNullOrEmpty(txtRequiredShrinkageWidthMin.Text.Trim()) ? 0 : Convert.ToDecimal(txtRequiredShrinkageWidthMin.Text.Trim());
            aInfo.ShrinkageWidthMax = string.IsNullOrEmpty(txtRequiredShrinkageWidthMax.Text.Trim()) ? 0 : Convert.ToDecimal(txtRequiredShrinkageWidthMax.Text.Trim());
            aInfo.Quantity = string.IsNullOrEmpty(txtQuantity.Text.Trim()) ? 0 : Convert.ToDecimal(txtQuantity.Text.Trim());
            aInfo.UnitPrice = string.IsNullOrEmpty(txtUnitPrice.Text) ? 0 : Convert.ToDecimal(txtUnitPrice.Text.Trim());

        return aInfo;

    }




    private bool UpdateChangesSingle()
    {
        bool retVal;
        try
        {
            retVal = aDal.UpdateSingleInfo(PrepareDataForUpdateDetail());

        }
        catch (Exception ex)
        {
            retVal = false;
            throw ex;
        }

        return retVal;
    }

    
    private ProformaMasterDao PrepareDataForSave()
    {
        var aInfo = new ProformaMasterDao();

        aInfo.ProformaInvNo = txtPINo.Text.Trim();
        aInfo.ProformaDate = Convert.ToDateTime(txtPIDate.Text);

        if (HFBuyerId.Value == "")
        {
            aInfo.BuyerId = 0;
            aInfo.BuyerName = txt_Buyer.Text.ToString();
        }
        else
        {
            aInfo.BuyerId = Convert.ToInt32(HFBuyerId.Value);
            aInfo.BuyerName = "";
           
        }

        if (HFVendorId.Value == "")
        {
            aInfo.VendorId = 0;
            aInfo.VendorName = txt_vendor.Text.ToString();
        }
        else
        {
            aInfo.VendorId = Convert.ToInt32(HFVendorId.Value);
            aInfo.VendorName = "";
        }

      
        aInfo.Remarks = txtRemarks.Text;

        if (ddlMarketingPerson.SelectedValue =="")
        {
            aInfo.ProformaMasterId = 0;
        }
        else
        {
            aInfo.ProformaMasterId = int.Parse(ddlMarketingPerson.SelectedValue);
        }

        


      
        return aInfo;
    }

    private void Clear()
    {
      //  txtPINo.Text = "";
      //  txtPIDate.Text = "";
      //  txt_Buyer.Text = "";
      //  txt_vendor.Text = "";
      //  txtRemarks.Text = "";
      ////  ddlVendor.SelectedValue = "";
      //  //ddlColor.SelectedValue = "";
      //  txtRequiredWidth.Text = "";
      //  txtRequiredOunceOrYard.Text = "";
      //  txtRequiredShrinkageLenthMin.Text = "";
      //  hfProformaId.Value = "";

      //  itemsGridView.DataSource = null;
      //  itemsGridView.DataBind();
      //  submitButton.Text = "Save Information";


        Response.Redirect("PIEntry.aspx");
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        Clear();
    }

    public bool HasSisCodeId(string dcstoreId)
    {
        if (dcstoreId != "")
        {
            for (int i = 0; i < itemsGridView.Rows.Count; i++)
            {
                if (itemsGridView.DataKeys[i][5].ToString() == dcstoreId)
                {
                    return false;
                    break;

                }
            }
        }
        
        return true;
    }

    protected void addToListButton_Click(object sender, EventArgs e)
    {
        add_To_List();
    }

    private void add_To_List()
    {
        if (AddToListValidation())
        {
            DataTable aDataTable = new DataTable();

            aDataTable.Columns.Add("FabricId");
            aDataTable.Columns.Add("FabricCatagory");
            aDataTable.Columns.Add("ColorId");
            aDataTable.Columns.Add("RequiredWidth");
            aDataTable.Columns.Add("RequiredOunceYard");
            aDataTable.Columns.Add("ShrinkageLengthMin");
            aDataTable.Columns.Add("ShrinkageLengthMax");
            aDataTable.Columns.Add("ShrinkageWidthMin");
            aDataTable.Columns.Add("ShrinkageWidthMax");
            aDataTable.Columns.Add("ShrinkageLength");
            aDataTable.Columns.Add("ShrinkageWidth");
            aDataTable.Columns.Add("Color");
            aDataTable.Columns.Add("SISCode");
            aDataTable.Columns.Add("ProformaMasterId");
            aDataTable.Columns.Add("Quantity");
            aDataTable.Columns.Add("UnitPrice");
            //aDataTable.Columns.Add("ProformaDetailId");
            aDataTable.Columns.Add("DeleteStatus");

            DataRow dataRow = null;

            string fabCategory = "";

            if (HasSisCodeId(HFSisCodeId.Value))
            {
                dataRow = aDataTable.NewRow();

                dataRow["FabricId"] = HFSisCodeId.Value;
                dataRow["ColorId"] = hfColorId.Value;
                dataRow["RequiredWidth"] = Convert.ToDecimal(txtRequiredWidth.Text.Trim()).ToString("F");
                dataRow["RequiredOunceYard"] = Convert.ToDecimal(txtRequiredOunceOrYard.Text.Trim()).ToString("F");
                dataRow["ShrinkageLengthMin"] = txtRequiredShrinkageLenthMin.Text.Trim();
                dataRow["ShrinkageLengthMax"] = txtRequiredShrinkageLenthMax.Text.Trim();
                dataRow["ShrinkageWidthMin"] = txtRequiredShrinkageWidthMin.Text.Trim();
                dataRow["ShrinkageWidthMax"] = txtRequiredShrinkageWidthMax.Text.Trim();
                dataRow["ShrinkageLength"] = Convert.ToDecimal(txtRequiredShrinkageLenthMin.Text.Trim()).ToString("F") + " - " + Convert.ToDecimal(txtRequiredShrinkageLenthMax.Text.Trim()).ToString("F");
                dataRow["ShrinkageWidth"] = Convert.ToDecimal(txtRequiredShrinkageWidthMin.Text.Trim()).ToString("F") + " - " + Convert.ToDecimal(txtRequiredShrinkageWidthMax.Text.Trim()).ToString("F");
                dataRow["Color"] = txtColor.Text;
                dataRow["SISCode"] = txt_siscode.Text;

                dataRow["Quantity"] = string.IsNullOrEmpty(txtQuantity.Text) ? "" : txtQuantity.Text;
                dataRow["UnitPrice"] = string.IsNullOrEmpty(txtUnitPrice.Text) ? "" : txtUnitPrice.Text;

              
                dataRow["ProformaMasterId"] = "";
                //dataRow["ProformaDetailId"] = "";
                dataRow["DeleteStatus"] = 1;
                aDataTable.Rows.Add(dataRow);
            }
            else
            {
                ShowMessageBox("Fabric/SisCode already exist !!");
            }

            for (int i = 0; i < itemsGridView.Rows.Count; i++)
            {
                dataRow = aDataTable.NewRow();

                dataRow["FabricId"] = 0;
                dataRow["ColorId"] = itemsGridView.DataKeys[i][0].ToString();
                dataRow["RequiredWidth"] = itemsGridView.DataKeys[i][1].ToString();
                dataRow["RequiredOunceYard"] = itemsGridView.DataKeys[i][2].ToString();
                dataRow["ShrinkageLengthMin"] = itemsGridView.DataKeys[i][3].ToString();
                dataRow["ShrinkageLengthMax"] = itemsGridView.DataKeys[i][7].ToString();
                dataRow["ShrinkageWidthMin"] = itemsGridView.DataKeys[i][8].ToString();
                dataRow["ShrinkageWidthMax"] = itemsGridView.DataKeys[i][9].ToString();
                dataRow["ShrinkageLength"] = itemsGridView.DataKeys[i][3].ToString() + " - " + itemsGridView.DataKeys[i][7].ToString();
                dataRow["ShrinkageWidth"] = itemsGridView.DataKeys[i][8].ToString() + " - " + itemsGridView.DataKeys[i][9].ToString();
                dataRow["Color"] = itemsGridView.DataKeys[i][4].ToString();
                dataRow["SISCode"] = itemsGridView.DataKeys[i][5].ToString();

                //dataRow["FabricCatagory"] = itemsGridView.DataKeys[i][8].ToString();
                dataRow["ProformaMasterId"] = itemsGridView.DataKeys[i][12].ToString();
                //dataRow["ProformaDetailId"] = itemsGridView.DataKeys[i][13].ToString();

                dataRow["Quantity"] = string.IsNullOrEmpty(itemsGridView.DataKeys[i][13].ToString()) ? "" : itemsGridView.DataKeys[i][13].ToString();
                dataRow["UnitPrice"] = string.IsNullOrEmpty(itemsGridView.DataKeys[i][14].ToString()) ? "" : itemsGridView.DataKeys[i][14].ToString();
                dataRow["DeleteStatus"] = 1;
                aDataTable.Rows.Add(dataRow);
            }

            itemsGridView.DataSource = aDataTable;
            itemsGridView.DataBind();

            txt_siscode.Text = "";

            txtColor.Text = "";
            txtRequiredWidth.Text = "";
            txtRequiredOunceOrYard.Text = "";
            txtRequiredShrinkageLenthMin.Text = "";
            txtRequiredShrinkageLenthMax.Text = "";
            txtRequiredShrinkageWidthMin.Text = "";
            txtRequiredShrinkageWidthMax.Text = "";
            txtQuantity.Text = "";
            txtUnitPrice.Text = "";
        }
    }

    private bool AddToListValidation()
    {
        if (txt_siscode.Text.Trim() == "")
        {
            ShowMessageBox("Please select fabric/siscode !!");
            return false;
        }

        //int count = 0;
        //
        //if (HFSisCodeId.Value == 0.ToString(CultureInfo.InvariantCulture))
        //{
        //    foreach (ListItem item in rbDeliveryType.Items)
        //    {
        //        if (item.Selected)
        //        {
        //            count++;
        //        }
        //    }
        //
        //    if (count == 0)
        //    {
        //        ShowMessageBox("Please select fabric category !!");
        //        rbDeliveryType.Focus();
        //        return false;
        //    }
        //}

        //if (ddlColor.SelectedValue == "")
        //{
        //    ShowMessageBox("Please select color !!");
        //    return false;
        //}

        if (txtColor.Text.Trim() == "")
        {
            ShowMessageBox("Please Enter color !!");
            return false;
        }

        if (txtRequiredWidth.Text.Trim() == "")
        {
            ShowMessageBox("Please Enter required width !!");
            return false;
        }

        if (txtRequiredOunceOrYard.Text.Trim() == "")
        {
            ShowMessageBox("Please Enter required Ounce/Yard !!");
            return false;
        }

        if (txtRequiredShrinkageLenthMin.Text.Trim() == "")
        {
            ShowMessageBox("Please Enter required Shrinkage length !!");
            return false;
        }


        if (txtRequiredShrinkageLenthMin.Text.Trim() == "")
        {
            ShowMessageBox("Please Enter required Shrinkage Width !!");
            return false;
        }

        return true;
    }

    protected void itemdeleteImageButton_Click(object sender, EventArgs e)
    {
        LinkButton productCodeTextBox = (LinkButton)sender;
        GridViewRow currentRow = (GridViewRow)productCodeTextBox.Parent.Parent;
        int rowindex = 0;
        rowindex = currentRow.RowIndex;


        DataTable aDataTable = new DataTable();
        aDataTable.Columns.Add("FabricId");
        aDataTable.Columns.Add("ColorId");
        aDataTable.Columns.Add("RequiredWidth");
        aDataTable.Columns.Add("RequiredOunceYard");
        aDataTable.Columns.Add("ShrinkageLengthMin");
        aDataTable.Columns.Add("ShrinkageLengthMax");
        aDataTable.Columns.Add("ShrinkageWidthMin");
        aDataTable.Columns.Add("ShrinkageWidthMax");
        aDataTable.Columns.Add("ShrinkageLength");
        aDataTable.Columns.Add("ShrinkageWidth");
        aDataTable.Columns.Add("Color");
        aDataTable.Columns.Add("SISCode");
        aDataTable.Columns.Add("ProformaMasterId");
        aDataTable.Columns.Add("ProformaDetailId");
        aDataTable.Columns.Add("DeleteStatus");
        aDataTable.Columns.Add("Quantity");
        aDataTable.Columns.Add("UnitPrice");

        DataRow dataRow = null;
        for (int i = 0; i < itemsGridView.Rows.Count; i++)
        {
            if (i != rowindex)
            {
                dataRow = aDataTable.NewRow();

                dataRow["FabricId"] = 0;
                dataRow["ColorId"] = itemsGridView.DataKeys[i][0].ToString();
                dataRow["RequiredWidth"] = itemsGridView.DataKeys[i][1].ToString();
                dataRow["RequiredOunceYard"] = itemsGridView.DataKeys[i][2].ToString();
                dataRow["ShrinkageLengthMin"] = itemsGridView.DataKeys[i][3].ToString();
                dataRow["ShrinkageLengthMax"] = itemsGridView.DataKeys[i][7].ToString();
                dataRow["ShrinkageWidthMin"] = itemsGridView.DataKeys[i][8].ToString();
                dataRow["ShrinkageWidthMax"] = itemsGridView.DataKeys[i][9].ToString();
                dataRow["ShrinkageLength"] = itemsGridView.DataKeys[i][3].ToString() + " - " + itemsGridView.DataKeys[i][7].ToString();
                dataRow["ShrinkageWidth"] = itemsGridView.DataKeys[i][8].ToString() + " - " + itemsGridView.DataKeys[i][9].ToString();
                dataRow["Color"] = itemsGridView.DataKeys[i][4].ToString();
                dataRow["SISCode"] = itemsGridView.DataKeys[i][5].ToString();
                //dataRow["FabricCatagory"] = itemsGridView.DataKeys[i][8].ToString();
                dataRow["ProformaMasterId"] = itemsGridView.DataKeys[i][12].ToString();
                //dataRow["ProformaDetailId"] = itemsGridView.DataKeys[i][13].ToString();

                dataRow["Quantity"] = string.IsNullOrEmpty(itemsGridView.DataKeys[i][13].ToString()) ? "" : itemsGridView.DataKeys[i][13].ToString();
                dataRow["UnitPrice"] = string.IsNullOrEmpty(itemsGridView.DataKeys[i][14].ToString()) ? "" : itemsGridView.DataKeys[i][14].ToString();
                dataRow["DeleteStatus"] = 1;
                aDataTable.Rows.Add(dataRow);
            }
            //else
            //{
            //    bool s = aDal.Delete_Proforma_DetailById(MasterId, DetailsId);
            //}
        }

        itemsGridView.DataSource = aDataTable;
        itemsGridView.DataBind();







     //string  MasterId =  string.IsNullOrEmpty(itemsGridView.DataKeys[rowindex][12].ToString()) ? "" : itemsGridView.DataKeys[rowindex][12].ToString();
     //
     //string  DetailsId =  string.IsNullOrEmpty(itemsGridView.DataKeys[rowindex][0].ToString()) ? "" : itemsGridView.DataKeys[rowindex][0].ToString();
     //
     //DataTable dt = aDal.StockVerification(MasterId, DetailsId);
     //
     //if (dt.Rows.Count > 0)
     //{
     //    ShowMessageBox("Already Received In QAStockReceive");
     //}
     //else
     //{
     //    //Start Here
     //}

    }

    private void MaxMin(double max, double min)
    {
        if (min > max)
        {
            ShowMessageBox("Max value can not be less then min value");
        }

    }

    protected void txt_Buyer_OnTextChanged(object sender, EventArgs e)
    {
        if (txt_Buyer.Text != "")
        {
            string productName = txt_Buyer.Text.Trim();
            if (productName.Contains(':'))
            {
                string[] productInfo = productName.Split(':');
                HFBuyerId.Value = productInfo[0];
                txt_Buyer.Text = productInfo[1];
            }
        }

        txt_vendor.Focus();
    }

    protected void txt_vendor_OnTextChanged(object sender, EventArgs e)
    {
        if (txt_vendor.Text != "")
        {
            string productName = txt_vendor.Text.Trim();
            if (productName.Contains(':'))
            {
                string[] productInfo = productName.Split(':');
                HFVendorId.Value = productInfo[0];
                txt_vendor.Text = productInfo[1];
            }
        }

        txtPIDate.Focus();
    }

    protected void txt_siscode_OnTextChanged(object sender, EventArgs e)
    {
        if (txt_siscode.Text != "")
        {
            string productName = txt_siscode.Text.Trim();
            if (productName.Contains(':'))
            {
                string[] productInfo = productName.Split(':');
                HFSisCodeId.Value = productInfo[0];
                txt_siscode.Text = productInfo[1];
            }
            else
            {
                HFSisCodeId.Value = txt_siscode.Text;
            }
        }
    }

    protected void txtColor_OnTextChanged(object sender, EventArgs e)
    {
        if (txtColor.Text != "")
        {
            string productName = txtColor.Text.Trim();

            if (productName.Contains('|'))
            {
                string[] productInfo = productName.Split('|');
                hfColorId.Value = productInfo[0];
                txtColor.Text = productInfo[1];
            }
            else
            {
                hfColorId.Value = 0.ToString(CultureInfo.InvariantCulture);
            }
        }

        txtRequiredWidth.Focus();
    }

    protected void itemsGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {

            int row = Convert.ToInt32(e.CommandArgument.ToString());

            TextBox txtRequiredWidth = (TextBox)itemsGridView.Rows[row].FindControl("txtRequiredWidth");
            txtRequiredWidth.ReadOnly = false;

            TextBox txtRequiredOunceYard = (TextBox)itemsGridView.Rows[row].FindControl("txtRequiredOunceYard");
            txtRequiredOunceYard.ReadOnly = false;

            TextBox txtShrinkageLength = (TextBox)itemsGridView.Rows[row].FindControl("txtShrinkageLength");
            txtShrinkageLength.ReadOnly = false;

            TextBox txtShrinkageWidth = (TextBox)itemsGridView.Rows[row].FindControl("txtShrinkageWidth");
            txtShrinkageWidth.ReadOnly = false;

            LinkButton alinkButton = (LinkButton) itemsGridView.Rows[row].FindControl("lbtUpDate");
            alinkButton.Visible = true;

        }

        if (e.CommandName == "UpdateData")
        {

            int row = Convert.ToInt32(e.CommandArgument.ToString());
            ProformaDetailDao aInfo = new ProformaDetailDao();

            string ProformaMasterId = itemsGridView.DataKeys[row][11].ToString();

            TextBox txtRequiredWidth = (TextBox)itemsGridView.Rows[row].FindControl("txtRequiredWidth");
            TextBox txtRequiredOunceYard = (TextBox)itemsGridView.Rows[row].FindControl("txtRequiredOunceYard");
            TextBox txtShrinkageLength = (TextBox)itemsGridView.Rows[row].FindControl("txtShrinkageLength");

            string ShrinkageLength = txtShrinkageLength.Text.Trim();
            if (ShrinkageLength.Contains('-'))
            {
                string[] productInfo = ShrinkageLength.Split('-');
                aInfo.ShrinkageLengthMin = Convert.ToDecimal(productInfo[0]);
                aInfo.ShrinkageLengthMax = Convert.ToDecimal(productInfo[1]);
            }

            TextBox txtShrinkageWidth = (TextBox)itemsGridView.Rows[row].FindControl("txtShrinkageWidth");
            string ShrinkageWidth = txtShrinkageWidth.Text.Trim();
            if (ShrinkageWidth.Contains('-'))
            {
                string[] productInfo = ShrinkageWidth.Split('-');
                aInfo.ShrinkageWidthMin = Convert.ToDecimal(productInfo[0].Trim());
                aInfo.ShrinkageWidthMax = Convert.ToDecimal(productInfo[1].Trim());
            }

            aInfo.SisCode = itemsGridView.Rows[row].Cells[1].Text.Trim();
            aInfo.RequiredOunceOryard = Convert.ToDecimal(txtRequiredOunceYard.Text);
            aInfo.RequiredWidth = Convert.ToDecimal(txtRequiredWidth.Text);
            aInfo.ProformaMasterId = Convert.ToInt32(ProformaMasterId);


             aDal.RowWise_UpdateInfo(aInfo);

           
            

            
        }


    }


    protected void ddlPINumber_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        //ddlColor.SelectedValue = "";
        //tbxRequiredWidth.Text = "";
        //tbxOunceYard.Text = "";
        //tbxShrinkageLength.Text = "";
        //tbxShrinkageWidth.Text = "";
        txtRequiredShrinkageLenthMin.Text = "";
        txtRequiredShrinkageLenthMax.Text = "";
        txtRequiredShrinkageWidthMin.Text = "";
        txtRequiredShrinkageWidthMax.Text = "";


        if (ddlPINumber.SelectedValue != "")
        {


            SaveFunc.Visible = false;
            UpdateFunc.Visible = true;
           
           
         DataTable  aTable = aDal.GetProformaInfoById(int.Parse(ddlPINumber.SelectedValue));

            if (aTable.Rows.Count > 0)
            {

                update.Visible = true;
                addtolist.Visible = false;

                hfProformaId.Value = aTable.Rows[0].Field<int>("ProformaMasterId")
                .ToString(CultureInfo.InvariantCulture);
                txt_Buyer.Text = aTable.Rows[0].Field<String>("BuyerName");
                txt_vendor.Text = aTable.Rows[0].Field<String>("VendorName");
                HFBuyerName.Value = aTable.Rows[0].Field<String>("BuyerName");
                HFVendorName.Value = aTable.Rows[0].Field<String>("VendorName");
                HFBuyerId.Value = aTable.Rows[0].Field<int>("BuyerId").ToString(CultureInfo.InvariantCulture);
                HFVendorId.Value = aTable.Rows[0].Field<int>("VendorId").ToString(CultureInfo.InvariantCulture);
                txtPIDate.Text = aTable.Rows[0].Field<DateTime>("ProformaDate").ToString("dd-MMM-yyyy");
                txtPINo.Text = aTable.Rows[0].Field<String>("ProformaInvNo");
                txtRemarks.Text = aTable.Rows[0].Field<String>("Remarks");

            }

            qDal.LoadSisCodeByPi(ddlFabricInfo, Convert.ToInt32(ddlPINumber.SelectedValue));

            DataTable dt = qDal.LoadBuyerVendor(ddlPINumber.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                int fabricCount = 0;
                fabricCount = ddlFabricInfo.Items.Count;

                if (fabricCount == 2)
                {
                    ddlFabricInfo.SelectedIndex = 1;
                    ddlFabric_OnSelectedIndexChanged(null, null);
                }
                else
                {
                    txtColor.Text = "";
                }
            }
        }

    }

    protected void ddlFabric_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        //ddlColor.SelectedValue = "";
        //tbxRequiredWidth.Text = "";
        //tbxOunceYard.Text = "";
        //tbxShrinkageLength.Text = "";
        //tbxShrinkageWidth.Text = "";
        txtRequiredShrinkageLenthMin.Text = "";
        txtRequiredShrinkageLenthMax.Text = "";
        txtRequiredShrinkageWidthMin.Text = "";
        txtRequiredShrinkageWidthMax.Text = "";


        if (ddlFabricInfo.SelectedValue != "")
        {
            //string fabric = ddlFabricInfo.SelectedItem.Text.Trim();
            //string[] fabrics = fabric.Split(':');

            DataTable aTable = qDal.GetFabricInfoById(Convert.ToInt32(ddlPINumber.SelectedValue), ddlFabricInfo.SelectedItem.Text.Trim());

            if (aTable.Rows.Count > 0)
            {
                //ddlColor.SelectedValue = aTable.Rows[0].Field<Int32>("ColorId").ToString(CultureInfo.InvariantCulture);
                hfColorId.Value = aTable.Rows[0].Field<Int32>("ColorId").ToString(CultureInfo.InvariantCulture);
                hfColorName.Value = aTable.Rows[0].Field<String>("ColorName").ToString(CultureInfo.InvariantCulture);
                HFSisCodeId.Value = aTable.Rows[0].Field<Int32>("SisCodeId").ToString(CultureInfo.InvariantCulture);
                HFSisCodeName.Value = aTable.Rows[0].Field<string>("FabricName").ToString(CultureInfo.InvariantCulture);
                txtColor.Text = aTable.Rows[0].Field<string>("ColorName").ToString(CultureInfo.InvariantCulture);
                txt_siscode.Text = aTable.Rows[0].Field<string>("FabricName").ToString(CultureInfo.InvariantCulture);
                txtRequiredWidth.Text = aTable.Rows[0].Field<Decimal>("RequiredWidth").ToString(CultureInfo.InvariantCulture);
                txtRequiredOunceOrYard.Text = aTable.Rows[0].Field<Decimal>("RequiredOunceOryard").ToString(CultureInfo.InvariantCulture);
                txtRequiredShrinkageLenthMin.Text = aTable.Rows[0].Field<Decimal>("ShrinkageLengthMin").ToString(CultureInfo.InvariantCulture);
                txtRequiredShrinkageLenthMax.Text = aTable.Rows[0].Field<Decimal>("ShrinkageLengthMax").ToString(CultureInfo.InvariantCulture);
                txtRequiredShrinkageWidthMin.Text = aTable.Rows[0].Field<Decimal>("ShrinkageWidthMin").ToString(CultureInfo.InvariantCulture);
                txtRequiredShrinkageWidthMax.Text = aTable.Rows[0].Field<Decimal>("ShrinkageWidthMax").ToString(CultureInfo.InvariantCulture);

                txtQuantity.Text = aTable.Rows[0].Field<Decimal>("Quantity").ToString(CultureInfo.InvariantCulture);
                txtUnitPrice.Text = aTable.Rows[0].Field<Decimal>("UnitPrice").ToString(CultureInfo.InvariantCulture);
            }
        }
        else
        {
            
            txtRequiredShrinkageLenthMin.Text = "";
            txtRequiredShrinkageLenthMax.Text = "";
            txtRequiredShrinkageWidthMin.Text = "";
            txtRequiredShrinkageWidthMax.Text = "";
        }
    }

    protected void btnSingleUpdate_OnClick(object sender, EventArgs e)
    {

        if (AddToListValidation())
        {

            bool updatestatus = UpdateChangesSingle();

            if (updatestatus)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "alert",
                    "alert('Operation Successfully Done...');window.location ='PIEntry.aspx';",
                    true);
            }

      
        }
            
    }
}