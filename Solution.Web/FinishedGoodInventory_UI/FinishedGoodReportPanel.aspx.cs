﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;

public partial class FinishedGoodInventory_UI_FinishedGoodReportPanel : System.Web.UI.Page
{
    private FinishedGoodReportDal aReportDal = new FinishedGoodReportDal();
    private QAStockReceiveDal aStockReceiveDal = new QAStockReceiveDal();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDownList();
        }
    }



    private void LoadDropDownList()
    {
        aReportDal.LoadBuyerInfoForddl(ddlBuyer);
        aStockReceiveDal.LoadProformaInvoice(ddlProforma);
        aStockReceiveDal.LoadShift(ddlShift);
        aStockReceiveDal.LoadQALocation(ddlLocation);
    }


    private string GenerateParameter()
    {
        string pram = "";

        if (rblRecordStatus.SelectedValue == "FGRR")
        {
            if (ddlBuyer.SelectedValue != "")
            {
                pram = pram + " AND BI.BuyerId = " + ddlBuyer.SelectedValue;
            }


            if (ddlShift.SelectedValue != "")
            {
                pram = pram + " AND STRM.ShiftId = " + ddlShift.SelectedValue;
            }


            if (txtFromDate.Text != "" && txtToDate.Text != "")
            {
                pram = pram + " AND CONVERT(date,STRM.ProductionDate)  BETWEEN '" + txtFromDate.Text.Trim() +
                       "' AND '" + txtToDate.Text.Trim() + "' ";
            }

            if (txtFromDate.Text != "" && txtToDate.Text == "")
            {
                pram = pram + " AND CONVERT(date,STRM.ProductionDate)  BETWEEN '" + txtFromDate.Text.Trim() +
                       "' AND '" + DateTime.Now + "' ";
            }


            if (txtFromDate.Text == "" && txtToDate.Text != "")
            {
                pram = pram + " AND CONVERT(date,STRM.ProductionDate)  <= '" + txtToDate.Text.Trim() + "' ";
            }
        }

        //if (rblRecordStatus.SelectedValue == "FGPKG")
        //{

        //}

        return pram;
    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {


        if (rblRecordStatus.SelectedValue == "")
        {
            ShowMessageBox("You should select Report Name!!");
            rblRecordStatus.Focus();
            rblRecordStatus.BackColor = Color.GhostWhite;
            return false;
        }


        //if (ddlProforma.SelectedValue == "")
        //{
        //    ShowMessageBox("You should select PI No!!");
        //    ddlProforma.Focus();
        //    ddlProforma.BackColor = Color.GhostWhite;
        //    return false;
        //}

        //if (ddlShift.Text == "")
        //{
        //    ShowMessageBox("You should enter Shift Name !!");
        //    ddlShift.Focus();
        //    ddlShift.BackColor = Color.GhostWhite;
        //    return false;
        //}

        return true;
    }


    protected void searchButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            PopUp(rblRecordStatus.SelectedValue, GenerateParameter());
        }
    }

    private void PopUp(string rpt, string param)
    {
        string url = "../FinishedGoodInventory_RPTView/FinishedGoodReportViewer.aspx?rptType=" + rpt + "&rpt=" + param;
        string fullURL = "window.open('" + url + "', '_blank', 'height=600,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }


    private void Clear()
    {
        ddlShift.SelectedIndex = 0;
        ddlBuyer.SelectedIndex = 0;
        ddlProforma.SelectedIndex = 0;
        txtToDate.Text = "";
        txtFromDate.Text = "";
       // rblRecordStatus.Items.Clear(); 

    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        Clear();
    }

    protected void rblRecordStatus_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblRecordStatus.SelectedValue == "PWFS")
        {
            
        }
    }
}