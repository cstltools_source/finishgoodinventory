﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/NewMasterPage.master" AutoEventWireup="true" CodeFile="QAStockReceiveReport.aspx.cs" Inherits="FinishedGoodInventory_UI_QAStockReceiveReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    
    
     

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    

    
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Store Stock Report</div>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">


                            <div class="card border-top border-0 border-4 border-success">
                                <script type="text/javascript">
                                    function pageLoad() {
                                        $('.datepicker').pickadate({
                                            selectMonths: true,
                                            selectYears: true
                                        });
                                        $('.mySelect2').select2({
                                            theme: 'bootstrap4',
                                            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                            placeholder: $(this).data('placeholder'),
                                            allowClear: Boolean($(this).data('allow-clear')),
                                        });
                                    }
                            </script>

                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-search me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Search Options</h5>
                                        </div>
                                        <hr>
                                        
                                        
                                        <div class="row mb-2">
                                            <label for="" class="col-sm-2 col-form-label">Sis Code</label>
                                            <div class="col-sm-10">
                                                <asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlfabric"></asp:DropDownList>
                                            </div>
                                        </div>


                                        <hr />
                                        <div class="row mb-4">

                                            <div class="col-4"></div>
                                            <div class="col-4" style="">



                                                <asp:LinkButton runat="server" class="btn btn-outline-info" ID="btn_List" OnClick="btn_List_OnClick">
                                                 
                                                    
                                                    <i class="fa fa-list" aria-hidden="true"></i>

                                                   

                                                    <span style="font-weight: bold !important">List View </span>
                                                </asp:LinkButton>

                                                <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="resetButton" OnClick="resetButton_Click">
                                                    <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset </span>
                                                </asp:LinkButton>

                                            </div>
                                            <div class="col-4"></div>

                                        </div>
                                        <hr />
                                        
                                        
                                        <div class="row">

                                            <div class="col-md-2">

                                            </div>
                                         
                                            <div class="col-md-2">

                                            </div>
                                            <div class="col-md-2">

                                            </div>
                                            <div class="col-md-3">

                                            </div>
                                            <div class="col-md-3">
                                                <asp:LinkButton ID="btnExportToExcel" runat="server" CssClass="btn btn-success pull-right" OnClick="btnExportToExcel_Click" ><span aria-hidden="true" class="fa fa-file-excel-o" ></span> &nbsp;Export To Excel</asp:LinkButton> 

                                            </div>
                                        </div>
                                        
                                        
                                        <br/>

                                          <div id="MainGradeDiv" style="text-align: center; height: 20%; overflow: scroll; width: auto; overflow-y: scroll; overflow-x: scroll;">
                                            <asp:GridView ID="itemsGridView" runat="server" AutoGenerateColumns="False"
                                                CssClass="table table-bordered table-condensed custom-table-style" 
                                                 OnPageIndexChanging="loadGridView_PageIndexChanging"  EmptyDataText="There are no data records to display.">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="SL.">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="FabricName" HeaderText="Fabric Code" />
                                                    <asp:BoundField DataField="ShiftTitle" HeaderText="Shift" />
                                                    <asp:BoundField DataField="ProformaInvNo" HeaderText="PI No" />
                                                    <asp:BoundField DataField="FabricSet" HeaderText="Set No" />
                                                    <asp:BoundField DataField="RollNo" HeaderText="Roll No" />
                                                    <asp:BoundField DataField="Quantity" HeaderText="Qty in (Yds)" />
                                                    <asp:BoundField DataField="DefectPoint" HeaderText="Defect Point" />
                                                    <asp:BoundField DataField="RequiredWidth" HeaderText="Req. Width" />
                                                    <asp:BoundField DataField="ActualWidth" HeaderText="With()" />
                                                    <asp:BoundField DataField="NetWeight" HeaderText="Weight(Kg)" />
                                                    <asp:BoundField DataField="PPHSY" HeaderText="PPHSY" />
                                                    <asp:BoundField DataField="Piece" HeaderText="Piece" />
                                                    <asp:BoundField DataField="PWLength" HeaderText="Pcs Wise Leangth" />
                                                    <asp:BoundField DataField="LocationName" HeaderText="Location Or Plate" />
                                                    <asp:BoundField DataField="ProductionDate" HeaderText="Producttion Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                    <asp:BoundField DataField="ReceiveBy" HeaderText="Received By" />
                                                    <asp:BoundField DataField="RecheckDate" HeaderText="Recheck Date" />
                                                    <asp:BoundField DataField="RecheckBy" HeaderText="Recheck By" />
                                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
    </ContentTemplate>
    
<Triggers>
    <asp:PostBackTrigger ControlID="btnExportToExcel" /> 
</Triggers>
</asp:UpdatePanel>
    
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                           Height="120" Width="120" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>

