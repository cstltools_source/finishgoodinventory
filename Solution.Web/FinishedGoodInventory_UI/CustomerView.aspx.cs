﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_CustomerView : System.Web.UI.Page
{
    CustomerDal aDal = new CustomerDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDownlist();
            LoadBuyerInfo(itemsGridView);
        }
    }

    private void LoadDropDownlist()
    {
        aDal.LoadBuyerType(ddlBuyerType);
    }

    private void LoadBuyerInfo(GridView gridView)
    {
        gridView.DataSource = aDal.GetBuyerInfo(GenerateParameter());
        gridView.DataBind();
    }

    private string GenerateParameter()
    {
        string pram = "";

        if (ddlBuyerType.SelectedValue != "")
        {
            pram = pram + " AND BI.BuyerTypeId =" + ddlBuyerType.SelectedValue;
        }

        if (txtBuyerName.Text != "")
        {
            pram = pram + " AND BI.BuyerName LIKE '%" + txtBuyerName.Text + "%'"; ;
        }

        return pram;
    }

    protected void itemsGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            Session["BuyerId"] = e.CommandArgument.ToString();
            Response.Redirect("CustomerEntry.aspx");
        }

        if (e.CommandName == "ActiveInactiveData")
        {
           BuyerInfo aInfo = new BuyerInfo();

           aInfo.BuyerId = Int32.Parse(e.CommandArgument.ToString());

           if (aDal.ActiveInactiveBuyerInfo(aInfo))
           {
               ShowMessageBox("Successfully Updated !!");
           }
            
            this.LoadBuyerInfo(itemsGridView);
        }
        
        if (e.CommandName == "DeleteData")
        {
           BuyerInfo aInfo = new BuyerInfo();

           aInfo.BuyerId = Int32.Parse(e.CommandArgument.ToString());

           if (aDal.DeleteBuyerInfoById(aInfo))
           {
               ShowMessageBox("Successfully Deleted !!");
           }
            
            this.LoadBuyerInfo(itemsGridView);
        }
    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void loadGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        itemsGridView.PageIndex = e.NewPageIndex;
        this.LoadBuyerInfo(itemsGridView);
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {
        LoadBuyerInfo(itemsGridView);
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        LoadDropDownlist();
        txtBuyerName.Text = "";
        LoadBuyerInfo(itemsGridView);
    }

    protected void txtBuyerName_OnTextChanged(object sender, EventArgs e)
    {
        LoadBuyerInfo(itemsGridView);
    }

    protected void ddlBuyerType_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadBuyerInfo(itemsGridView);
    }
}