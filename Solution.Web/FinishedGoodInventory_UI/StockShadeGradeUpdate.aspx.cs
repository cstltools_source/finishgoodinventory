﻿using System;
using System.Activities.Expressions;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;
using SalesSolution.Web.DataLayer;

public partial class FinishedGoodInventory_UI_StockShadeGradeUpdate : System.Web.UI.Page
{
    RollWiseLocationAllocationDal aDal = new RollWiseLocationAllocationDal();

    private readonly ShadeGradeUpdateDal aUpdateDal = new ShadeGradeUpdateDal();




    public static RollWiseLocationAllocationDal aLocation = new RollWiseLocationAllocationDal();

    QAStockReceiveDal QaStockReceiveDal = new QAStockReceiveDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDownList();
            LoadRemarksCheckboxlist();
            LoadRollList();
        }
    }

    private void LoadRollList()
    {
        // Roll No

        //DataTable DT2 = aDal.GetRollList(GenerateCbxParameterForRoll());

        //cbxRolls.Items.Clear();

        //if (DT2.Rows.Count > 0)
        //{
        //    for (int i = 0; i < DT2.Rows.Count; i++)
        //    {
        //        ListItem list = new ListItem();

        //        list.Text = DT2.Rows[i]["RollNo"].ToString();
        //        list.Value = DT2.Rows[i]["RollNo"].ToString();
        //        cbxRolls.Items.Add(list);

        //    }
        //}
    }

    private string GenerateCbxParameterForRoll()
    {
        string pram = "";

        if (cbxShadeGrades.Items.Cast<ListItem>().Count(li => li.Selected) > 0)
        {
            pram = pram + " AND FGS.ShadeGradeing IN ( ";

            foreach (ListItem lst in cbxShadeGrades.Items)
            {

                if (lst.Selected)
                {
                    pram = pram + "'" + lst.Text.Trim() + "',";
                }

            }

            pram = pram.Remove(pram.Length - 1, 1) + ")";

        }
        else
        {
            pram = pram + " AND FGS.ShadeGradeing IS NULL";
        }

        if (cbxRemarks.Items.Cast<ListItem>().Count(li => li.Selected) > 0)
        {
            pram = pram + " AND FGS.Remarks IN ( ";

            foreach (ListItem lst in cbxRemarks.Items)
            {

                if (lst.Selected)
                {
                    pram = pram + "'" + lst.Text.Trim() + "',";
                }

            }

            pram = pram.Remove(pram.Length - 1, 1) + ")";

        }
        

        return pram;
    }

    private void LoadRemarksCheckboxlist()
    {
        // Remarks

        DataTable DT2 = aDal.GetRemarks(GenerateCbxParameter());

        cbxRemarks.Items.Clear();

        if (DT2.Rows.Count > 0)
        {
            for (int i = 0; i < DT2.Rows.Count; i++)
            {
                ListItem list = new ListItem();

                list.Text = DT2.Rows[i]["Remarks"].ToString();
                list.Value = DT2.Rows[i]["Remarks"].ToString();
                cbxRemarks.Items.Add(list);

            }
        }
    }


    private void LoadDropDownList()
    {
        // Shade Grade

        cbxShadeGrades.Items.Clear();
        DataTable DT = aDal.GetShadeGrades();

        if (DT.Rows.Count > 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                ListItem list = new ListItem();
                list.Text = DT.Rows[i]["ShadeGrade"].ToString();
                list.Value = DT.Rows[i]["ShadeGradeId"].ToString();
                cbxShadeGrades.Items.Add(list);

            }
        }


        using (DataTable dt = QaStockReceiveDal.LoadPiNoAll())
        {
            ddlProformaNum.DataSource = dt;
            ddlProformaNum.DataValueField = "ProformaMasterId";
            ddlProformaNum.DataTextField = "ProformaInvNo";
            ddlProformaNum.DataBind();
            ddlProformaNum.Items.Insert(0, new ListItem("Select from list", String.Empty));
            ddlProformaNum.SelectedIndex = 0;

        }


        using (DataTable dt = aDal.GetRollList()
        )
        {

            //lbEmployee.DataSource = dt;
            //lbEmployee.DataValueField = "RollNo";
            //lbEmployee.DataTextField = "RollNo";
            //lbEmployee.DataBind();
          
            //lbEmployee.SelectedIndex = -1;


            //ddlEmp1.Items.Insert(0, new ListItem("Please Select an Employee.....", String.Empty));
            //ddlEmp1.SelectedIndex = 0;
        }

        

        aDal.LoadFabricSetInfo(ddlSet);
        aDal.LoadRollNoInfo(ddlRoll);
      //  aDal.LoadFabricInfo(ddlFabric);
      using (DataTable dt = aDal.LoadFabricInfoForShadeGradeUpdate())
        {

            ddlFabric.DataSource = dt;
            ddlFabric.DataValueField = "FabricName";
            ddlFabric.DataTextField = "FabricName";
            ddlFabric.DataBind();
            ddlFabric.Items.Insert(0, new ListItem("Select from list", String.Empty));
            ddlFabric.SelectedIndex = 0;
        }


        aDal.LoadShadeGradeInfo(ddlShade);

    }

    private string GenerateCbxParameter()
    {
        string pram = "";

        if(cbxShadeGrades.Items.Cast<ListItem>().Count(li => li.Selected) > 0)
        {
            pram = pram + " AND FGS.ShadeGradeing IN ( ";

            foreach (ListItem lst in cbxShadeGrades.Items)
            {
                
                if (lst.Selected)
                {
                    pram = pram + "'" + lst.Text.Trim() + "',";
                }

            }

            pram = pram.Remove(pram.Length - 1, 1) +")";
            
        }
        else
        {
            pram = pram + " AND FGS.ShadeGradeing IS NULL";
        }

        return pram;
    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private string GenerateParameter()
    {
        string pram = "";


        if (ddlProformaNum.SelectedValue != "")
        {
            pram = pram + " AND M.ProformaInvId = '" + ddlProformaNum.SelectedValue + "'";
        }

        if (ddlFabric.SelectedValue != "")
        {
           
            pram = pram + " AND D.FabricCodeId In (Select FabricId from tblFabricInfo Where LOWER(FabricName)= LOWER('" + ddlFabric.SelectedValue.Trim() + "'))";
        }

        if (txtProductionFromDate.Text.Trim() != "" && txtProductionToDate.Text.Trim() != "")
        {
            pram = pram + " AND M.ProductionDate BETWEEN '" + txtProductionFromDate.Text.Trim() + "' AND '" + txtProductionToDate.Text.Trim() + "'";
        }

        if (txtProductionFromDate.Text.Trim() != "" && txtProductionToDate.Text.Trim() == "")
        {
            pram = pram + "  AND M.ProductionDate => '" + txtProductionFromDate.Text.Trim() + "'";
        }

        if (txtProductionFromDate.Text.Trim() == "" && txtProductionToDate.Text.Trim() != "")
        {
            pram = pram + " AND M.ProductionDate <= '" + txtProductionToDate.Text.Trim() + "'";
        }

        if (txtEntryFromDate.Text.Trim() != "" && txtEntryToDate.Text.Trim() != "")
        {
            pram = pram + " AND M.EntryDate BETWEEN '" + txtEntryFromDate.Text.Trim() + "' AND '" + txtEntryToDate.Text.Trim() + "'";
        }

        if (txtEntryFromDate.Text.Trim() != "" && txtEntryToDate.Text.Trim() == "")
        {
            pram = pram + "  AND M.EntryDate => '" + txtEntryFromDate.Text.Trim() + "'";
        }

        if (txtEntryFromDate.Text.Trim() == "" && txtEntryToDate.Text.Trim() != "")
        {
            pram = pram + " AND M.EntryDate <= '" + txtEntryToDate.Text.Trim() + "'";
        }


        if (ddlShadeGrade.SelectedValue == "Without")
        {
            pram = pram + " AND D.ShadeGradeing IS NULL  ";
        }

        if (ddlShadeGrade.SelectedValue == "With")
        {
            pram = pram + "AND D.ShadeGradeing IS NOT NULL  ";
        }

        if (ddl_OrderBy.SelectedValue != "")
        {
            pram = pram + "ORDER BY D.RollNo " + ddl_OrderBy.SelectedValue + "";
        }

        //if (lbEmployee.Items.Cast<ListItem>().Count(li => li.Selected) > 0)
        //{

        //    string message = "";
        //    foreach (ListItem item in lbEmployee.Items)
        //    {
        //        if (item.Selected)
        //        {
        //            message += item.Value + ",";
        //        }
        //    }
        //    pram = pram + " AND FGST.RollNo IN (" + message.TrimEnd(',') + ")";
        //}


        return pram;
    }



    
    protected void submitButton_Click(object sender, EventArgs e)
    {

        if (Validation())
        {
            List<QaStockReceiveDetailDao> alist = new List<QaStockReceiveDetailDao>();
            for (int i = 0; i < productGridView.Rows.Count; i++)
            {
                var check = (CheckBox)productGridView.Rows[i].FindControl("chkSelect");
                if (check.Checked)
                {
                    QaStockReceiveDetailDao ashade = new QaStockReceiveDetailDao();
                    var StockRMasterId = (HiddenField)productGridView.Rows[i].FindControl("hfStockReceiveMasterId");
                    var StockRDetailId = (HiddenField)productGridView.Rows[i].FindControl("hfStockReceiveDetailId");
                    var Shade = (TextBox)productGridView.Rows[i].FindControl("txtShadeGrade");
                    ashade.StockReceiveMasterId = int.Parse(StockRMasterId.Value);
                    ashade.StockReceiveDetailId = int.Parse(StockRDetailId.Value);
                    ashade.ShadeGradeing = Shade.Text;
                    alist.Add(ashade);
                }
            }

            var status = aUpdateDal.UpdateShadeGrade(alist);

            if (status)
            {
                Clear();
                ShowMessageBox("Shade Grade Update Successfully");
            }

        } 

    }

    private bool Validation()
    {
        int count = 0;

        for (int i = 0; i < productGridView.Rows.Count; i++)
        {
            var chkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");

            if (chkBoxRows.Checked)
            {
                count++;
            }

            if (count > 0)
            {
                break;
            }

        }

        if (count == 0)
        {
            ShowMessageBox("Please select at least one item !!!");
            return false;
        }


        for (int i = 0; i < productGridView.Rows.Count; i++)
        {
            var chkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");

            if (chkBoxRows.Checked)
            {
                var Shade = (TextBox)productGridView.Rows[i].FindControl("txtShadeGrade");

                if (Shade.Text.Trim() == "")
                {
                    ShowMessageBox("Please Input Shade Grade !!");
                    return false;
                }
            }
        }

        return true;
    }


    protected void resetButton_Click(object sender, EventArgs e)
    {
        Clear();
    }

    private void Clear()
    {
        ddlProformaNum.SelectedIndex = 0;
        ddlFabric.SelectedIndex = 0;
        ddl_OrderBy.SelectedIndex = 0;
        txtProductionFromDate.Text = "";
        txtProductionToDate.Text = "";
        txtEntryFromDate.Text = "";
        txtEntryToDate.Text = "";

        productGridView.DataSource = null;
        productGridView.DataBind();
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {
        LoadList(productGridView);
    }

    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)productGridView.HeaderRow.FindControl("chkSelectAll");

        for (int i = 0; i < productGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");

            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
        }
    }

    protected void ddlSet_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        searchButton_Click(null, null);
    }

    protected void ddlRoll_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        searchButton_Click(null, null);
    }


    protected void loadGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        productGridView.PageIndex = e.NewPageIndex;
        this.LoadList(productGridView);
    }

    private void LoadList(GridView gridView)
    {

        DataTable dtdata = aUpdateDal.GetFinishedGoodStock(GenerateParameter());

        if (dtdata.Rows.Count > 0)
        {
            gridView.DataSource = dtdata;
            gridView.DataBind();
        }
        else
        {
            gridView.DataSource = null;
            gridView.DataBind();
        }


    }

    private bool ApplyValidation()
    {
        if (productGridView.Rows.Count == 0)
        {
            ShowMessageBox("Please Search Information");
            return false;
        }

        if (ddlShade.SelectedIndex == 0)
        {
            ShowMessageBox("Please Select Shade Grade");
            ddlShade.Focus();
            return false;
        }
        
        
        return true;
    }

    
    protected void BtnApply_OnClick(object sender, EventArgs e)
    {
        if (ApplyValidation())
        {
            for (int i = 0; i < productGridView.Rows.Count; i++)
            {
                TextBox aBox = (TextBox)productGridView.Rows[i].FindControl("txtShadeGrade");
                CheckBox aCheckBox = (CheckBox)productGridView.Rows[i].FindControl("chkSelect");

                if (aBox.Text.Trim() == "" && aCheckBox.Checked)
                {
                    aBox.Text = ddlShade.SelectedItem.Text;
                }

            }
            ddlShade.SelectedIndex = 0;
        }
        
    }

    protected void ddlFabric_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        searchButton_Click(null, null);
    }

    protected void ddlOrderBy_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        searchButton_Click(null, null);
    }

    protected void cbxShadeGrades_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadRemarksCheckboxlist();
        LoadRemarksCheckboxlist();
        LoadRollList();
    }

    protected void cbxShadeAll_CheckedChanged(object sender, EventArgs e)
    {
        if (cbxShadeAll.Checked)
        {
            foreach (ListItem lst in cbxShadeGrades.Items)
            {
                lst.Selected = true;
            }
        }
        else
        {
            foreach (ListItem lst in cbxShadeGrades.Items)
            {
                lst.Selected = false;
            }
        }

        LoadRemarksCheckboxlist();
        LoadRollList();
    }

    protected void cbxRollsAll_CheckedChanged(object sender, EventArgs e)
    {
        if (cbxRollsAll.Checked)
        {
            foreach (ListItem lst in cbxRolls.Items)
            {
                lst.Selected = true;
            }
        }
        else
        {
            foreach (ListItem lst in cbxRolls.Items)
            {
                lst.Selected = false;
            }
        }
    }

    protected void cbxRemarksAll_CheckedChanged(object sender, EventArgs e)
    {
        if (cbxRemarksAll.Checked)
        {
            foreach (ListItem lst in cbxRemarks.Items)
            {
                lst.Selected = true;
            }
        }
        else
        {
            foreach (ListItem lst in cbxRemarks.Items)
            {
                lst.Selected = false;
            }
        }

        LoadRollList();
       
    }

    protected void cbxRemarks_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadRollList();
    }


    [WebMethod]
    public static dynamic Get_Roll_All()
    {
        var list = aLocation.GetRollList();
        return list;
    }

    //protected void ddlProformaNum_OnSelectedIndexChanged(object sender, EventArgs e)
    //{
       
    //}

    protected void ddlProformaNum_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        
        if (ddlProformaNum.SelectedValue != "")
        {
            aUpdateDal.LoadSisCodeByPi_ShadeGrade(ddlFabric, Convert.ToInt32(ddlProformaNum.SelectedValue));

            int fabricCount = 0;
            fabricCount = ddlFabric.Items.Count;

            if (fabricCount == 2)
            {
                ddlFabric.SelectedIndex = 1;
                ddlFabric_OnSelectedIndexChanged(null, null);
            }
                
        }

    }
}