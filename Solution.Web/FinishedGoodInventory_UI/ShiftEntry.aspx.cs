﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_ShiftEntry : System.Web.UI.Page
{
    private ShiftDal aShiftDal = new ShiftDal();


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Session["ShiftId"] != null)
            {
                GetOneRecord(Convert.ToInt32(Session["ShiftId"].ToString()));
                Session["ShiftId"] = null;
            }
            else
            {
                ShiftTime();
            }
        }
    }



    # region Edit
    private void GetOneRecord(int buyerId)
    {
        var aTable = new DataTable();

        aTable = aShiftDal.GetShiftInfoById(buyerId);

        if (aTable.Rows.Count > 0)
        {
            hfShiftId.Value = aTable.Rows[0].Field<int>("ShiftId").ToString(CultureInfo.InvariantCulture);
            TxtShiftName.Text = aTable.Rows[0].Field<String>("ShiftTitle");
            txtShiftInTime.Text = aTable.Rows[0]["ShiftTitleInTime"].ToString();
            txtShiftEndTime.Text = aTable.Rows[0]["ShiftTitleInEndTime"].ToString();
            submitButton.Text = "Update Information";
        }

    }

    #endregion Edit


    public bool ShiftInTime()
    {
        try
        {
            TimeSpan aTimeSpan = new TimeSpan();
            aTimeSpan = Convert.ToDateTime(txtShiftInTime.Text).TimeOfDay;
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }

    public bool ShiftEndTime()
    {
        try
        {
            TimeSpan aTimeSpan = new TimeSpan();
            aTimeSpan = Convert.ToDateTime(txtShiftInTime.Text).TimeOfDay;
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }



    private void ShiftTime()
    {
        txtShiftInTime.Text = "08:00 AM";
        txtShiftEndTime.Text = "08:00 PM";

    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private bool Validation()
    {
        if (TxtShiftName.Text == "")
        {
            ShowMessageBox("You should enter Shift Name !!");
            TxtShiftName.Focus();
            TxtShiftName.BackColor = Color.GhostWhite;
            return false;
        }

        if (txtShiftInTime.Text == "")
        {
            ShowMessageBox("You should enter Shift In Time !!");
            txtShiftInTime.Focus();
            txtShiftInTime.BackColor = Color.GhostWhite;
            return false;
        }


        if (txtShiftEndTime.Text == "")
        {
            ShowMessageBox("You should enter Shift In Time !!");
            txtShiftEndTime.Focus();
            txtShiftEndTime.BackColor = Color.GhostWhite;
            return false;
        }

        if (ShiftInTime() == false)
        {
            ShowMessageBox("Please give a valid Shift In Time.Example:- 08:00 PM OR 08:00 AM !!!");
            return false;
        }

        if (ShiftEndTime() == false)
        {
            ShowMessageBox("Please give a valid Shift End Time.Example:- 08:00 PM OR 08:00 AM !!!");
            return false;
        }

        return true;
    }


    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {

            if (hfShiftId.Value == "")
            {
                int saveId = SaveChanges();

                if (saveId > 0)
                {
                    ShowMessageBox("Shift info saved successfully !!");
                    Clear();
                }
                else
                {
                    ShowMessageBox("Data does not save successfully !!");
                }
            }
            else
            {
                bool saveId = UpdateChanges();

                if (saveId)
                {
                    ShowMessageBox("Shift info updated successfully !!");
                    Clear();
                }
                else
                {
                    ShowMessageBox("Data does not updated successfully !!");
                }
            }
        }
    }

    private bool UpdateChanges()
    {
        bool retVal;
        try
        {
            retVal = aShiftDal.UpdateShiftInfo(PrepareDataForUpdatee());

        }
        catch (Exception ex)
        {
            retVal = false;
            throw ex;
        }

        return retVal;
    }

    private ShiftDao PrepareDataForUpdatee()
    {
        var aInfo = new ShiftDao();

        aInfo.ShiftId = Convert.ToInt32(hfShiftId.Value);
        aInfo.ShiftTitle = TxtShiftName.Text.Trim();
        aInfo.ShiftTitleInTime = Convert.ToDateTime(txtShiftInTime.Text.Trim()).TimeOfDay;
        aInfo.ShiftTitleInEndTime = Convert.ToDateTime(txtShiftEndTime.Text.Trim()).TimeOfDay;
        return aInfo;
    }


    private Int32 SaveChanges()
    {
        Int32 retVal;
        try
        {
            retVal = aShiftDal.SaveShift(PrepareDataForSave());

        }
        catch (Exception ex)
        {
            retVal = 0;
            throw ex;
        }

        return retVal;
    }

    private ShiftDao PrepareDataForSave()
    {
        var aInfo = new ShiftDao();

        aInfo.ShiftTitle = TxtShiftName.Text.Trim();
        aInfo.ShiftTitleInTime = Convert.ToDateTime(txtShiftInTime.Text.Trim()).TimeOfDay;
        aInfo.ShiftTitleInEndTime = Convert.ToDateTime(txtShiftEndTime.Text.Trim()).TimeOfDay;  
        
        return aInfo;
    }

    private void Clear()
    {
        TxtShiftName.Text = "";
        txtShiftInTime.Text = "";
        txtShiftEndTime.Text = "";
        ShiftTime();

    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        Clear();
    }
}