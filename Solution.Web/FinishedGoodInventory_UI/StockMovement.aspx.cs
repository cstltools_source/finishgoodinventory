﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_StockMovement : System.Web.UI.Page
{

    private readonly StockMovementDal aMovementDal = new StockMovementDal();
    private QAStockReceiveDal aDal = new QAStockReceiveDal();

    RollWiseLocationAllocationDal aRollDal = new RollWiseLocationAllocationDal();

    private readonly ShadeGradeUpdateDal aUpdateDal = new ShadeGradeUpdateDal();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropdownList();
        }
    }


    private void LoadDropdownList()
    {
        DataTable DT = aDal.GetReceiveMovementCategory("Stock");
        if (DT.Rows.Count > 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                ListItem list = new ListItem();
                list.Text = DT.Rows[i]["MovementCategory"].ToString();
                list.Value = DT.Rows[i]["MovementCategoryId"].ToString();
                rbStockType.Items.Add(list);
                rbStockType.SelectedValue = 1.ToString();
            }
        }

        aRollDal.LoadFabricSetInfo(ddlSet);
        aRollDal.LoadRollNoInfo(ddlRoll);
       // aRollDal.LoadFabricInfo(ddlFabric);
        //aRollDal.LoadRequiredWidthInfo(ddlRequiredWidth);
        //aRollDal.LoadRequiredOunceOryardInfo(ddlRequiredOunceOrYerd);
        //aRollDal.LoadShrinkageInfo(ddlShrinkage);
      //  aRollDal.LoadPWLengthInfo(ddlPWLength);

    }

    protected void ddlSet_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        searchButton_Click(null, null);
    }

    protected void ddlRoll_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        searchButton_Click(null, null);
    }

    protected void ddlRequiredWidth_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        searchButton_Click(null, null);
    }

    protected void ddlRequiredWidth_ddlRequiredOunceOrYerd(object sender, EventArgs e)
    {
        searchButton_Click(null, null);
    }

    
    protected void ddlShrinkage_ddlRequiredOunceOrYerd(object sender, EventArgs e)
    {
        searchButton_Click(null, null);
    }


    private string GenerateParameter()
    {
        string pram = "";


        if (ddlSet.SelectedValue != "")
        {
            pram = pram + " AND FGST.FabricSet = '" + ddlSet.SelectedValue + "'";
        }

        if (ddlRoll.SelectedValue != "")
        {
            pram = pram + " AND FGST.RollNo = '" + ddlRoll.SelectedValue + "'";
        }

        if (ddlFabric.SelectedValue != "")
        {
            pram = pram + " AND FGST.FabricCodeId = '" + ddlFabric.SelectedValue + "'";
        }

        if (ddlOrderBy.SelectedValue != "")
        {
            pram = pram + " ORDER BY FGST.RollNo " + ddlOrderBy.SelectedValue;
        }


        return pram;
    }



    protected void searchButton_Click(object sender, EventArgs e)
    {

        LoadList(productGridView);
    }

    private void LoadList(GridView gridView)
    {
        DataTable dtdata = aMovementDal.GetStockMovement(GenerateParameter());
        if (dtdata.Rows.Count > 0)
        {
            gridView.DataSource = dtdata;
            gridView.DataBind();

            for (int i = 0; i < productGridView.Rows.Count; i++)
            {
                var ddlFabric = (DropDownList)productGridView.Rows[i].FindControl("ddlFabric");
                Label alabel = (Label)productGridView.Rows[i].FindControl("LblFabricSISCode");
                string[] dta = alabel.Text.Split(':');
                string[] param = dta[1].Split('-');
                using (DataTable dt = aMovementDal.Get_Fabric_Info(param[0].Trim()))
                {
                    ddlFabric.DataSource = dt;
                    ddlFabric.DataValueField = "FabricId";
                    ddlFabric.DataTextField = "FebricDescription";
                    ddlFabric.DataBind();
                    ddlFabric.Items.Insert(0, new ListItem("Please Select From this list", String.Empty));
                    ddlFabric.SelectedIndex = 0;

                }
                
            }
            gridView.Columns[2].Visible = false;
            productGridView.Columns[7].Visible = false;
            productGridView.Columns[12].Visible = false;
            GridColumnControl();
        }
        else
        {
            gridView.DataSource = null;
            gridView.DataBind();
        }

    }

    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)productGridView.HeaderRow.FindControl("chkSelectAll");

        for (int i = 0; i < productGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");

            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
        }
    }
    protected void rbStockType_OnTextChanged(object sender, EventArgs e)
    {

        if (rbStockType.SelectedValue != "")
        {
            txtRemarks.Text = rbStockType.SelectedItem.Text;

        }

        GridColumnControl();
    }


    private void GridColumnControl()
    {
        if (rbStockType.SelectedValue != "")
        {
            if (productGridView.Rows.Count > 0)
            {
                var caseSwitch = rbStockType.SelectedItem.Text.Trim();
                switch (caseSwitch)
                {
                    case "Stock Adjustment":
                        productGridView.Columns[2].Visible = false;
                        productGridView.Columns[7].Visible = false;
                        productGridView.Columns[12].Visible = true;
                        break;
                    case "Roll No Change":
                        productGridView.Columns[2].Visible = false;
                        productGridView.Columns[7].Visible = true;
                        productGridView.Columns[12].Visible = false;
                        break;
                    default:
                        productGridView.Columns[2].Visible = true;
                        productGridView.Columns[7].Visible = true;
                        productGridView.Columns[12].Visible = false;
                        break;
                }
            }
        }

    }


    protected void loadGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        productGridView.PageIndex = e.NewPageIndex;
        this.LoadList(productGridView);
    }


    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }


    private bool Validation()
    {

        if (rbStockType.SelectedValue == "")
        {
            ShowMessageBox("Please Select Stock Type");
            return false;
        }


        int count = 0;

        for (int i = 0; i < productGridView.Rows.Count; i++)
        {
            var chkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");

            if (chkBoxRows.Checked)
            {
                count++;
            }

            if (count > 0)
            {
                break;
            }

        }

        if (count == 0)
        {
            ShowMessageBox("Please select at least one item !!!");
            return false;
        }


        if (rbStockType.SelectedItem.Text.Trim() != "Stock Adjustment" || rbStockType.SelectedItem.Text.Trim() != "Roll No Change")
        {
            for (int i = 0; i < productGridView.Rows.Count; i++)
            {
                var chkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");

                if (chkBoxRows.Checked)
                {
                    var Fabric = (DropDownList)productGridView.Rows[i].FindControl("ddlFabric");

                    if (Fabric.SelectedValue == "")
                    {
                        ShowMessageBox("Please Select Change to Fabric / Sis Code !!");
                        return false;
                    }
                }
            }
        }



        //if (rbStockType.SelectedItem.Text.Trim() == "Roll No Change")
        //{
            for (int i = 0; i < productGridView.Rows.Count; i++)
            {
                var chkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");

                if (chkBoxRows.Checked)
                {
                    var NewRoll = (TextBox)productGridView.Rows[i].FindControl("txtChangeRollNo");

                    if (NewRoll.Text.Trim() == "")
                    {
                        ShowMessageBox("You should enter Roll No. !!");
                        return false;
                    }
                }
            }
        //}


        
        return true;
    }


    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            
                List<StockMovement> aList = new List<StockMovement>();

                for (int i = 0; i < productGridView.Rows.Count; i++)
                {
                    StockMovement aMovement = new StockMovement();
                    var check = (CheckBox)productGridView.Rows[i].FindControl("chkSelect");
                    if (check.Checked)
                    {
                        int StockId = int.Parse(productGridView.DataKeys[i]["FGStockId"].ToString());
                        DropDownList fabricId = (DropDownList)productGridView.Rows[i].FindControl("ddlFabric");
                        double Qty = Convert.ToDouble(productGridView.DataKeys[i]["StockQuantity"].ToString());
                        var RollChange = (TextBox)productGridView.Rows[i].FindControl("txtChangeRollNo");
                        //var NewStock = (TextBox)productGridView.Rows[i].FindControl("txtQuantity");

                        aMovement.StockId = StockId;
                        aMovement.StockQty = Qty;
                        aMovement.fabricId = string.IsNullOrEmpty(fabricId.SelectedValue) ? 0:Convert.ToInt32(fabricId.SelectedValue);
                        //aMovement.NewStockQty = string.IsNullOrEmpty(NewStock.Text) ? 0 : double.Parse(NewStock.Text);
                        aMovement.RollChange = string.IsNullOrEmpty(RollChange.Text) ? null : RollChange.Text.ToString();
                        aMovement.MovementTypeId = int.Parse(rbStockType.SelectedValue);
                        aMovement.MovementType = rbStockType.SelectedItem.Text.Trim().ToString();
                        aList.Add(aMovement);
                    }

                }

                bool status;

                var caseSwitch = rbStockType.SelectedItem.Text.Trim();
                switch (caseSwitch)
                {
                    case "Stock Adjustment":

                      //  ShowMessageBox("No operation occurred");

                        break;
                    case "Roll No Change":
                        status= aMovementDal.StockMovement_RollNoChange(aList);
                        if (status)
                        {
                            ShowMessageBox("Execute Successfully");
                        }
                        break;
                    //case "Program Change":
                    //    status= aMovementDal.StockMovement_ProgramChange(aList);
                    //    if (status)
                    //    {
                    //        ShowMessageBox("Execute Successfully");
                    //    }
                    //    break;
                    default:

                        status= aMovementDal.StockMovement_ProgramChange(aList);
                        if (status)
                        {
                            ShowMessageBox("Execute Successfully");
                        }

                        break;
                }

            
        }

        searchButton_Click(null, null);

    }


    protected void resetButton_Click(object sender, EventArgs e)
    {
        
    }


    private void Clear()
    {
        rbStockType.SelectedValue = "";
        txtRemarks.Text = "";

        ddlFabric.SelectedValue = "";
        ddlSet.SelectedValue = "";
        ddlRoll.SelectedValue = "";

        productGridView.DataSource = null;
        productGridView.DataBind();
    }


    protected void ddlFabric_OnSelectedIndexChanged(object sender, EventArgs e)
    {

        DropDownList qtyTextBox = (DropDownList)sender;
        GridViewRow currentRow = (GridViewRow)qtyTextBox.Parent.Parent;
        int rowindex = 0;
        rowindex = currentRow.RowIndex;

        Int32 FabricId = int.Parse(productGridView.DataKeys[rowindex]["FabricCodeId"].ToString());
        var changeFabricId = (DropDownList)productGridView.Rows[rowindex].FindControl("ddlFabric");

            if (Convert.ToInt32(changeFabricId.SelectedValue) == (FabricId))
            {
                ShowMessageBox("You can not select Same Fabric/Sis code !!");
                changeFabricId.SelectedValue = "";
            }
        
    }

    protected void txtChangeRollNo_OnTextChanged(object sender, EventArgs e)
    {

        TextBox qtyTextBox = (TextBox)sender;
        GridViewRow currentRow = (GridViewRow)qtyTextBox.Parent.Parent;
        int rowindex = 0;
        rowindex = currentRow.RowIndex;

        var Roll = (TextBox)productGridView.Rows[rowindex].FindControl("txtChangeRollNo");

        DataTable Dt = aMovementDal.GetRollNoValidation(Roll.Text.Trim());

        if (Dt.Rows.Count > 0)
        {
            ShowMessageBox("Roll number already exists !!");
            Roll.Text="";

        }

    }

    protected void ddlOrderBy_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        searchButton_Click(null, null);
    }
}