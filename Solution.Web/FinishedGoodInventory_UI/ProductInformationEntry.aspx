﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/NewMasterPage.master" AutoEventWireup="true" CodeFile="ProductInformationEntry.aspx.cs" Inherits="FinishedGoodInventory_UI_ProductInformationEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div id="popDiv">
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Fabric Setup</div>

                        <div class="ms-auto">
                            <div class="btn-group">


                                <a href="../FinishedGoodInventory_UI/ProductView.aspx" class="btn btn-sm btn-sm btn-outline-info"><i class="fa fa-backward"></i>&nbsp;Back to List</a>


                            </div>
                        </div>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">

                            <div class="card border-top border-0 border-4 border-success">

                                <script type="text/javascript">
                                    function pageLoad() {
                                        $('.datepicker').pickadate({
                                            selectMonths: true,
                                            selectYears: true
                                        });
                                        $('.mySelect2').select2({
                                            theme: 'bootstrap4',
                                            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                            placeholder: $(this).data('placeholder'),
                                            allowClear: Boolean($(this).data('allow-clear')),
                                        });
                                    }
                                </script>


                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bx-info-circle me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Fabric Information</h5>
                                        </div>
                                        <hr>

                                        <%--<div class="row mb-2">
                                    <label for="inputEnterYourName" class="col-sm-2 col-form-label">Fabric Code(Auto/text):<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" readonly="" id="inputEnterYourName" placeholder="Fabric Code">
                                    </div>
                                </div>--%>

                                        <div class="row form-group mt-1">
                                            <label class="col-sm-2 col-form-label">Fabric Name:<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>
                                            <div class="col-sm-10">

                                                <asp:HiddenField runat="server" ID="hfFabricId" />
                                                <asp:TextBox ID="txtFabricName" runat="server" placeholder="Enter Fabric Name" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="row form-group mt-1">
                                            <label class="col-sm-2 col-form-label">Fabric Short Name:<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>
                                            <div class="col-sm-10">
                                                <asp:TextBox ID="txtFebricShortName" runat="server" placeholder="Enter Fabric Short Name" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="row form-group mt-1">
                                            <label class="col-sm-2 col-form-label">Fabric Color:<span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>
                                            <div class="col-sm-10">
                                                <asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlFabricColor"></asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="row form-group mt-1">
                                            <label class="col-sm-2 col-form-label">Fabric Description:<span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>
                                            <div class="col-sm-10">
                                                <asp:TextBox TextMode="MultiLine" Columns="4" ID="txtFabricDescription" runat="server" placeholder="Enter Fabric Description" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>





                                        <div class="row  mt-3">
                                            <label class="col-sm-2 col-form-label"></label>
                                            <div class="col-sm-10">


                                                <%--    <asp:LinkButton runat="server" class="btn btn-outline-primary" ID="submitButton"  OnClientClick="return confirm('Are you really aware of this operation?');" OnClick="submitButton_Click">
                                            
                                                    <i class="bx bxs-save mr-1"></i><span style="font-weight: bold !important">Save Information</span>

                                                </asp:LinkButton>--%>


                                                <asp:LinkButton runat="server" class="btn btn-outline-primary" ID="submitButton" OnClientClick="return confirm('Are you really aware of this operation?');" OnClick="submitButton_Click">
                                            
                                                    <i class="bx bxs-save mr-1"></i><span style="font-weight: bold !important">Save Information</span>

                                                </asp:LinkButton>



                                                <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="resetButton" OnClick="resetButton_Click">
                                            
                                                    <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Information</span>

                                                </asp:LinkButton>





                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>



    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                    Height="120" Width="120" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>

