﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_StoreApproval : System.Web.UI.Page
{
    private readonly StoreReceiveApprovalDal aDal = new StoreReceiveApprovalDal();
    QAStockReceiveDal aStockReceiveDal = new QAStockReceiveDal();

    private string MasterId="";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDownlist();
            LoadList(itemsGridView);
        }
    }

    private void LoadDropDownlist()
    {
        aDal.LoadShift(ddlShift);
        aDal.LoadBuyerInfo(ddlBuyer);
        aDal.LoadVendorInfo(ddlVendor);
        aDal.LoadProformaInvoice(ddlPi);
    }

    private void LoadList(GridView gridView)
    {
        gridView.DataSource = aDal.GetStockReceiveApprovalList(GenerateParameter());
        gridView.DataBind();
    }

    protected void gotoinvoiceButton_Click(object sender, EventArgs e)
    {
        Button button = (Button)sender;
        GridViewRow currentRow = (GridViewRow)button.Parent.Parent;
        int rowindex = 0;
        rowindex = currentRow.RowIndex;

        Session["StockReceiveMasterId"] = itemsGridView.DataKeys[rowindex]["StockReceiveMasterId"].ToString();
        Response.Redirect("StoreApprovalDetail.aspx");

    }

    private string GenerateParameter()
    {
        string pram = "";

        if (ddlBuyer.SelectedValue != "")
        {
            pram = pram + " AND BI.BuyerName LIKE '%" + ddlBuyer.SelectedItem.Text + "%'";
        }

        if (ddlVendor.SelectedValue != "")
        {
            pram = pram + " AND VEN.BuyerName LIKE '%" + ddlBuyer.SelectedItem.Text + "%'";
        }

        if (ddlShift.SelectedValue != "")
        {
            pram = pram + " AND M.ShiftId = " + ddlShift.SelectedValue;
        }

        if (fromDateTextBox.Text.Trim() != "" && toDateTextBox.Text.Trim() != "")
        {
            pram = pram + " AND CONVERT(date,PI.ProformaDate) BETWEEN '" + fromDateTextBox.Text.Trim() + "' AND '" + toDateTextBox.Text.Trim() + "'";
        }

        if (fromDateTextBox.Text.Trim() != "" && toDateTextBox.Text.Trim() == "")
        {
            pram = pram + "  AND CONVERT(date,PI.ProformaDate) BETWEEN => '" + fromDateTextBox.Text.Trim() + "'";
        }

        if (fromDateTextBox.Text.Trim() == "" && toDateTextBox.Text.Trim() != "")
        {
            pram = pram + " AND CONVERT(date,PI.ProformaDate) BETWEEN <= '" + toDateTextBox.Text.Trim() + "'";
        }

        if (ddlPi.SelectedValue != "")
        {
            pram = pram + " AND PI.ProformaMasterId ='" + ddlPi.SelectedValue + "'";
        }

        if (txtProductionFromDate.Text.Trim() != "" && txtProductionToDate.Text.Trim() != "")
        {
            pram = pram + " AND CONVERT(date,M.ProductionDate) BETWEEN '" + txtProductionFromDate.Text.Trim() + "' AND '" + txtProductionToDate.Text.Trim() + "'";
        }

        if (txtProductionFromDate.Text.Trim() != "" && txtProductionToDate.Text.Trim() == "")
        {
            pram = pram + "  AND CONVERT(date,M.ProductionDate) BETWEEN => '" + txtProductionFromDate.Text.Trim() + "'";
        }

        if (txtProductionFromDate.Text.Trim() == "" && txtProductionToDate.Text.Trim() != "")
        {
            pram = pram + " AND CONVERT(date,M.ProductionDate) BETWEEN <= '" + toDateTextBox.Text.Trim() + "'";
        }

        

        return pram;


    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void loadGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        itemsGridView.PageIndex = e.NewPageIndex;
        this.LoadList(itemsGridView);
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {
        LoadList(itemsGridView);
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        ddlBuyer.SelectedIndex = 0;
        ddlVendor.SelectedIndex = 0;
        ddlShift.SelectedIndex = 0;
        ddlPi.SelectedIndex = 0;
        txtProductionFromDate.Text = "";
        txtProductionToDate.Text = "";
        fromDateTextBox.Text = "";
        toDateTextBox.Text = "";
      //  LoadDropDownlist();
        LoadList(itemsGridView);
    }

    protected void ddlBuyer_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadList(itemsGridView);
    }

    protected void ddlVendor_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadList(itemsGridView);
    }

    protected void ddlPi_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadList(itemsGridView);
    }

    protected void ddlWidth_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadList(itemsGridView);
    }

    protected void checkAll_OnCheckedChanged(object sender, EventArgs e)
    {
        CheckBox chckheader = (CheckBox)itemsGridView.HeaderRow.FindControl("checkAll");
            
        for (int i = 0; i < itemsGridView.Rows.Count; i++)
        {
            CheckBox chckrw = (CheckBox)itemsGridView.Rows[i].FindControl("checkbox");
            if (chckheader.Checked)
            {
                chckrw.Checked = true;
            }
            else
            {
                chckrw.Checked = false;
            }
        }
    }


    private bool Validation()
    {
        int count = 0;

        for (int i = 0; i < itemsGridView.Rows.Count; i++)
        {
            CheckBox check = (CheckBox)itemsGridView.Rows[i].FindControl("checkbox");

            string M_id = itemsGridView.DataKeys[i]["StockReceiveMasterId"].ToString();

            if (check.Checked)
            {
                MasterId += M_id + ',';
                count++;

            }
        }
        if (count == 0)
        {
            ShowMessageBox("Please Select Minimum One");
            return false;
        }

        return true;
    }


    protected void btn_Approved_OnClick(object sender, EventArgs e)
    {
        if (Validation())
        {
            string MultipleMasterId= MasterId.TrimEnd(',');

           bool status = aDal.Approved_QAStockReceive_Multiple(MultipleMasterId);

           if (status)
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(),
                   "alert",
                   "alert('Operation Successfully Done...');window.location ='StoreApprovalList.aspx';",
                   true);
           }

        }
    }
}