﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;

public partial class FinishedGoodInventory_UI_DeliveryAgainestPackingList : System.Web.UI.Page
{
    DeliveryAgainestPackingListDal aDal = new DeliveryAgainestPackingListDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropdownList();
        }
    }

    private void LoadDropdownList()
    {
        //aDal.LoadBuyerInfo(ddlBuyer);
        //aDal.LoadVendorInfo(ddlVendor);
        //aDal.LoadMovementCatagories(ddlDeliveryType);
    }

    protected void ddlBuyer_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadPiNo();
        Button1_Click(null, null);
    }

    private void LoadPiNo()
    {
        
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        DataTable aTable = new DataTable();

        aTable = aDal.GetPackingListForDelivery(GenerateParameter());

        if (aTable.Rows.Count > 0)
        {
            productGridView.DataSource = aTable;
            productGridView.DataBind();
        }
    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }


    private string GenerateParameter()
    {
        string pram = "";


        if (fromDateTextBox.Text.Trim() != "" && toDateTextBox.Text.Trim() != "")
        {
            pram = pram + " AND M.IssueDate BETWEEN '" + fromDateTextBox.Text.Trim() + "' AND '" + toDateTextBox.Text.Trim() + "'";
        }

        if (fromDateTextBox.Text.Trim() != "" && toDateTextBox.Text.Trim() == "")
        {
            pram = pram + "  AND M.IssueDate => '" + fromDateTextBox.Text.Trim() + "'";
        }

        if (fromDateTextBox.Text.Trim() == "" && toDateTextBox.Text.Trim() != "")
        {
            pram = pram + " AND M.IssueDate <= '" + toDateTextBox.Text.Trim() + "'";
        }

        return pram;
    }


    protected void gotoinvoiceButton_Click(object sender, EventArgs e)
    {
        Button button = (Button)sender;
        GridViewRow currentRow = (GridViewRow)button.Parent.Parent;
        int rowindex = 0;
        rowindex = currentRow.RowIndex;

        Session["IssueMasterId"] = productGridView.DataKeys[rowindex]["IssueMasterId"].ToString();
        Response.Redirect("DeliveryAgainestPackingDetails.aspx");

    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        LoadDropdownList();
        productGridView.DataSource = null;
        productGridView.DataBind();
    }

    protected void ddlPINumber_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Button1_Click(null, null);
    }

    protected void ddlVendor_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadPiNo();
        Button1_Click(null, null);
    }

    protected void ddlDeliveryType_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Button1_Click(null,null);
    }
}