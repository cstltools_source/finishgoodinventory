﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_ProductView : System.Web.UI.Page
{
    FabricDal aDal = new FabricDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDownlist();
            LoadListInfo(itemsGridView);
        }
    }

    private void LoadDropDownlist()
    {
        aDal.LoadFabricColor(ddlFabricColor);
        aDal.LoadFabricCategory(ddlFabricCategory);
    }

    private void LoadListInfo(GridView gridView)
    {
        gridView.DataSource = aDal.GetFabricInfo(GenerateParameter());
        gridView.DataBind();
    }

    private string GenerateParameter()
    {
        string pram = "";

        if (ddlFabricColor.SelectedValue != "")
        {
            pram = pram + " AND FabricColor =" + ddlFabricColor.SelectedValue;
        }

        if (ddlFabricCategory.SelectedValue != "")
        {
            pram = pram + " AND FBCI.CategoryId =" + ddlFabricCategory.SelectedValue;
        }

        if (txtFabricName.Text != "")
        {
            pram = pram + " AND FabricName LIKE '%" + txtFabricName.Text.Trim() + "%'";
        }
        
        if (txtFebricShortName.Text != "")
        {

            pram = pram + " AND FabricName LIKE '%" + txtFebricShortName.Text.Trim() + "%'";

        }

        return pram;
    }

    protected void itemsGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            Session["FabricId"] = e.CommandArgument.ToString();
            Response.Redirect("ProductInformationEntry.aspx");
        }

        if (e.CommandName == "ActiveInactiveData")
        {
            FabricDao aInfo = new FabricDao();

            aInfo.FabricId = Int32.Parse(e.CommandArgument.ToString());

            if (aDal.ActiveInactiveBuyerInfo(aInfo))
            {
                ShowMessageBox("Successfully Updated !!");
            }

            this.LoadListInfo(itemsGridView);
        }

        if (e.CommandName == "DeleteData")
        {
            FabricDao aInfo = new FabricDao();

            aInfo.FabricId = Int32.Parse(e.CommandArgument.ToString());

            if (aDal.DeleteFabricInfoById(aInfo))
            {
                ShowMessageBox("Successfully Deleted !!");
            }

            this.LoadListInfo(itemsGridView);
        }
    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void loadGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        itemsGridView.PageIndex = e.NewPageIndex;
        this.LoadListInfo(itemsGridView);
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {
        LoadListInfo(itemsGridView);
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        LoadDropDownlist();
        txtFabricName.Text = "";
        txtFebricShortName.Text = "";
        LoadListInfo(itemsGridView);
    }

    protected void txtFabricName_OnTextChanged(object sender, EventArgs e)
    {
        LoadListInfo(itemsGridView);
    }

    protected void txtFebricShortName_OnTextChanged(object sender, EventArgs e)
    {
        LoadListInfo(itemsGridView);
    }

    protected void ddlFabricCategory_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadListInfo(itemsGridView);
    }

    protected void ddlFabricColor_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadListInfo(itemsGridView);
    }
}