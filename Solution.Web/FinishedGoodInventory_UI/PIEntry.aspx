﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/NewMasterPage.master" AutoEventWireup="true" CodeFile="PIEntry.aspx.cs" Inherits="FinishedGoodInventory_UI_PIEntry" %>

<%@ Register TagPrefix="cc1" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=3.0.20820.28364, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        /*AutoComplete flyout */
        .autocomplete_completionListElement {
            margin: 0px !important;
            background-color: White;
            color: windowtext;
            border: buttonshadow;
            border-width: 1px;
            border-style: solid;
            cursor: default;
            overflow: auto;
            font-family: Calibri;
            font-size: 12px;
            text-align: left;
            list-style-type: none;
            margin-left: 0px;
            padding-left: 0px;
            max-height: 350px;
            width: 40% !important;
        }

        .AutoComplete .highlighted .item  .autocomplete_highlightedListItem {
            background-color: yellow;
            color: black;
            padding: 1px;
        }

         .AutoComplete .item .autocomplete_listItem {
            background-color: white;
            color: blue;
            padding: 0px;
        }

      #ContentPlaceHolder1_rbDeliveryType_0,
        #ContentPlaceHolder1_rbDeliveryType_1,
        #ContentPlaceHolder1_rbDeliveryType_2,
        #ContentPlaceHolder1_rbDeliveryType_3,
        #ContentPlaceHolder1_rbDeliveryType_4,
        #ContentPlaceHolder1_rbDeliveryType_5,
        #ContentPlaceHolder1_rbDeliveryType_6,
        #ContentPlaceHolder1_rbDeliveryType_7,
        #ContentPlaceHolder1_rbDeliveryType_8 {
            margin-right: 5px !important;
            margin-left: 5px !important;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="popDiv">
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Proforma Invoice </div>

                        <div class="ms-auto">
                            <div class="btn-group">


                                <a href="../FinishedGoodInventory_UI/PIView.aspx" class="btn btn-sm btn-sm btn-outline-info"><i class="fa fa-backward"></i>&nbsp;Back to List</a>


                            </div>
                        </div>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">

                            <div class="card border-top border-0 border-4 border-success">


                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bx-file me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Proforma Invoice (PI)</h5>
                                        </div>
                                        <hr>



                                        <script type="text/javascript">
                                            function pageLoad() {
                                                $('.datepicker').pickadate({
                                                    selectMonths: true,
                                                    selectYears: true
                                                });
                                                $('.mySelect2').select2({
                                                    theme: 'bootstrap4',
                                                    width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                                    placeholder: $(this).data('placeholder'),
                                                    allowClear: Boolean($(this).data('allow-clear')),
                                                });
                                            }
                                        </script>

                                        <div class="row">
                                            <div class="col-6">
                                                <div class="row">
                                                    <label class="col-sm-3 col-form-label">Buyer Name:<span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>
                                                    <div class="col-sm-8">
                                                        <asp:HiddenField runat="server" ID="hfProformaId" />

                                                        <asp:TextBox ID="txt_Buyer" runat="server" CssClass="form-control form-control-sm" AutoPostBack="True" OnTextChanged="txt_Buyer_OnTextChanged"></asp:TextBox>
                                                        <cc1:AutoCompleteExtender ID="TxtFabricName_AutoCompleteExtender" runat="server"
                                                            DelimiterCharacters="" EnableCaching="true" Enabled="True" MinimumPrefixLength="1"
                                                            CompletionSetCount="10" ServiceMethod="GetBuyerList" ServicePath="~/SInventoryWebService.asmx"
                                                            TargetControlID="txt_Buyer" CompletionListCssClass="autocomplete_completionListElement"
                                                            CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                            ShowOnlyCurrentWordInCompletionListItem="true">
                                                        </cc1:AutoCompleteExtender>


                                                        <asp:HiddenField runat="server" ID="HFBuyerId" />
                                                        <asp:HiddenField runat="server" ID="HFBuyerName" />


                                                    </div>
                                                </div>

                                                <div class="row ">
                                                    <label class="col-sm-3 col-form-label">Vendor Name:<span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>
                                                    <div class="col-sm-8">
                                                        <%--<asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlVendor"></asp:DropDownList>--%>



                                                        <asp:TextBox ID="txt_vendor" runat="server" CssClass="form-control form-control-sm" AutoPostBack="True" OnTextChanged="txt_vendor_OnTextChanged"></asp:TextBox>
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server"
                                                            DelimiterCharacters="" EnableCaching="true" Enabled="True" MinimumPrefixLength="1"
                                                            CompletionSetCount="10" ServiceMethod="GetVendorList" ServicePath="~/SInventoryWebService.asmx"
                                                            TargetControlID="txt_vendor" CompletionListCssClass="autocomplete_completionListElement"
                                                            CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                            ShowOnlyCurrentWordInCompletionListItem="true">
                                                        </cc1:AutoCompleteExtender>

                                                        <asp:HiddenField runat="server" ID="HFVendorId" />
                                                        <asp:HiddenField runat="server" ID="HFVendorName" />
                                                    </div>
                                                </div>

                                                <div class="row mb-1">
                                                    <label class="col-sm-3 col-form-label">Proforma Date:<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox class="form-control  datepicker" placeholder="Enter proforma date" runat="server" ID="txtPIDate"></asp:TextBox>


                                                    </div>
                                                </div>

                                                <div class="row mb-1">
                                                    <label class="col-sm-3 col-form-label">Proforma No:<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtPINo" runat="server" placeholder="Enter proforma no" CssClass="form-control "></asp:TextBox>
                                                    </div>
                                                </div>
                                                
                                                 <div class="row mb-1">
                                                    <label class="col-sm-3 col-form-label">Marketing Person:</label>
                                                    <div class="col-sm-8">
                                                      
                                                        <asp:DropDownList class="form-select mySelect2" runat="server"  ID="ddlMarketingPerson"></asp:DropDownList>

                                                    </div>
                                                </div>

                                                <div class="row mb-1">
                                                    <label class="col-sm-3 col-form-label">Remarks:</label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtRemarks" TextMode="MultiLine" Rows="4" runat="server" placeholder="Enter remarks" CssClass="form-control "></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                                <div class="row mb-1">
                                                    <label class="col-sm-4 col-form-label">Fabric/SIS Code:<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>
                                                    <div class="col-sm-8">
                                                        <%--<asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlSisCode"></asp:DropDownList>--%>



                                                        <asp:TextBox ID="txt_siscode" runat="server" CssClass="form-control form-control-sm" AutoPostBack="True" OnTextChanged="txt_siscode_OnTextChanged"></asp:TextBox>
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender7" runat="server"
                                                            DelimiterCharacters="" EnableCaching="true" Enabled="True" MinimumPrefixLength="1"
                                                            CompletionSetCount="10" ServiceMethod="GetFabrics_PI" ServicePath="~/SInventoryWebService.asmx"
                                                            TargetControlID="txt_siscode" CompletionListCssClass="autocomplete_completionListElement"
                                                            CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                            ShowOnlyCurrentWordInCompletionListItem="true">
                                                        </cc1:AutoCompleteExtender>

                                                        <asp:HiddenField runat="server" ID="HFSisCodeId" />
                                                        <asp:HiddenField runat="server" ID="HFSisCodeName" />
                                                        <asp:HiddenField runat="server" ID="HFIsNew" />


                                                    </div>
                                                </div>

                                                

                                                <div class="row mb-1">
                                                    <label class="col-sm-4 col-form-label">Color:<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>
                                                    <div class="col-sm-8">
                                                        <%--<asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlColor"></asp:DropDownList>--%>
                                                        <asp:TextBox ID="txtColor" runat="server" CssClass="form-control form-control-sm" AutoPostBack="True" OnTextChanged="txtColor_OnTextChanged"></asp:TextBox>
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server"
                                                            DelimiterCharacters="" EnableCaching="true" Enabled="True" MinimumPrefixLength="1"
                                                            CompletionSetCount="10" ServiceMethod="GetColorList_PI" ServicePath="~/SInventoryWebService.asmx"
                                                            TargetControlID="txtColor" CompletionListCssClass="autocomplete_completionListElement"
                                                            CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                            ShowOnlyCurrentWordInCompletionListItem="true">
                                                        </cc1:AutoCompleteExtender>

                                                        <asp:HiddenField runat="server" ID="hfColorId" />
                                                        <asp:HiddenField runat="server" ID="hfColorName" />
                                                    </div>
                                                </div>

                                                <div class="row mb-1">
                                                    <label class="col-sm-4 col-form-label">Required Width:<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtRequiredWidth" runat="server" placeholder="Enter Required Width" CssClass="form-control "></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server"
                                                            TargetControlID="txtRequiredWidth"
                                                            FilterType="Custom, Numbers"
                                                            ValidChars="." />
                                                    </div>
                                                </div>

                                                <div class="row mb-1">
                                                    <label class="col-sm-4 col-form-label">Required Ounce/Yard<sup>2</sup>:<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtRequiredOunceOrYard" runat="server" placeholder="Enter Required Ounce/Yard" CssClass="form-control "></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                                            TargetControlID="txtRequiredOunceOrYard"
                                                            FilterType="Custom, Numbers"
                                                            ValidChars="." />
                                                    </div>
                                                </div>

                                                <div class="row mb-1">


                                                    <label class="col-sm-4 col-form-label">Shrinkage (Length %):<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>

                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="txtRequiredShrinkageLenthMin" runat="server" placeholder="min" CssClass="form-control form-control-sm "></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="freqQtyTextBox1" runat="server" TargetControlID="txtRequiredShrinkageLenthMin" FilterType="Custom, Numbers" ValidChars="." />
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="txtRequiredShrinkageLenthMax" runat="server" placeholder="max" CssClass="form-control form-control-sm "></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtRequiredShrinkageLenthMax" FilterType="Custom, Numbers" ValidChars="." />
                                                    </div>


                                                </div>
                                                
                                                <div class="row mb-1">


                                                    <label class="col-sm-4 col-form-label">Shrinkage (Width %):<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>

                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="txtRequiredShrinkageWidthMin" runat="server" placeholder="min" CssClass="form-control form-control-sm "></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtRequiredShrinkageWidthMin" FilterType="Custom, Numbers" ValidChars="." />
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="txtRequiredShrinkageWidthMax" runat="server" placeholder="max" CssClass="form-control form-control-sm "></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtRequiredShrinkageWidthMax" FilterType="Custom, Numbers" ValidChars="." />
                                                    </div>


                                                </div>
                                                
                                                
                                                <div class="row mb-1">
                                                    <label class="col-sm-4 col-form-label">Quantity:</label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID=txtQuantity runat="server" placeholder="Quantity" CssClass="form-control "></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
                                                            TargetControlID="txtRequiredOunceOrYard"
                                                            FilterType="Custom, Numbers"
                                                            ValidChars="." />
                                                    </div>
                                                </div>
                                                
                                                 <div class="row mb-1">
                                                    <label class="col-sm-4 col-form-label">Unit Price:</label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtUnitPrice" runat="server" placeholder="UnitPrice" CssClass="form-control "></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
                                                            TargetControlID="txtUnitPrice"
                                                            FilterType="Custom, Numbers"
                                                            ValidChars="." />
                                                    </div>
                                                </div>


                                                <div class="row mt-3" runat="server" ID="addtolist" Visible="True">
                                                    <label class="col-sm-4 col-form-label">&nbsp;</label>
                                                    <div class="col-sm-8">
                                                        <asp:LinkButton runat="server" class="btn btn-outline-secondary" ID="addToListButton" OnClick="addToListButton_Click">
                                                               <i class="bx bxs-plus-circle mr-1"></i><span style="font-weight: bold !important">Add to list</span>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="row mt-3"  runat="server" id="update" Visible="False">
                                      
                                            
                                                   

                                            
                                                </div>
                                                
                                                
                                                

                                            </div>
                                        </div>
                                        <br />
                                    
                                    
                                    
                                    
                                    <div class="row mb-4" runat="server" ID="UpdateFunc" Visible="False">

                                        <div class="col-md-4"></div>
                                        <div class="col-md-4">
                                            <asp:LinkButton runat="server" class="btn btn-outline-primary" ID="LinkButton1"  OnClick="btnSingleUpdate_OnClick">
                                            
                                                <i class="bx bxs-save mr-1"></i><span style="font-weight: bold !important">Update Info</span>

                                            </asp:LinkButton>



                                            <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="LinkButton2" OnClick="resetButton_Click">
                                            
                                                <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Info</span>

                                            </asp:LinkButton>
                                        </div>
                                        <div class="col-md-4"></div>

                                    </div>
                                    
                                    
                                    
                                    
                                    
                                    <div runat="server" ID="SaveFunc" Visible="True">

                                   

                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-file me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Fabric Information</h5>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="MainGradeDiv" style="text-align: center !important">
                                                    <asp:GridView ID="itemsGridView" runat="server" AutoGenerateColumns="False"
                                                        PageIndex="0" CssClass="table table-bordered text-center table-condensed custom-table-style"  OnRowCommand="itemsGridView_RowCommand" 
                                                        DataKeyNames="ColorId,RequiredWidth,RequiredOunceYard,ShrinkageLengthMin,Color,SISCode,ShrinkageWidth,ShrinkageLengthMax,ShrinkageWidthMin,ShrinkageWidthMax,ShrinkageLength,ProformaMasterId,DeleteStatus,Quantity,UnitPrice" EmptyDataText="There are no data records to display.">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="SL">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="SISCode" HeaderText="Fabric/SIS Code" />
                                                            <asp:BoundField DataField="Color" HeaderText="Color" />
                                                            <asp:TemplateField HeaderText="Required Width">
                                                                <ItemTemplate>
                                                    
                                                        
                                                                    <asp:TextBox ID="txtRequiredWidth" runat="server"  ReadOnly="true"  Text='<%#Eval("RequiredWidth") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>


                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Required Ounce/Yard">
                                                                <ItemTemplate>
                                                    
                                                        
                                                                    <asp:TextBox ID="txtRequiredOunceYard" runat="server"  ReadOnly="true"  Text='<%#Eval("RequiredOunceYard") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>


                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Shrinkage Length (%)">
                                                                <ItemTemplate>

                                                                    <asp:TextBox ID="txtShrinkageLength" runat="server"  ReadOnly="true"  Text='<%#Eval("ShrinkageLength") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Shrinkage Width (%)">
                                                                <ItemTemplate>
                                                                    
                                                                    
                                                                    
                                                                    <div class="row">
                                                                        <div class="col-md-8">
                                                                            <asp:TextBox ID="txtShrinkageWidth" runat="server"  ReadOnly="true"  Text='<%#Eval("ShrinkageWidth") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>

                                                                        </div>
                                                                        <div class="col-md-4">
                                                                        
                                                                            
                                                                            <asp:LinkButton ID="lbtUpDate" runat="server" Visible="false" CssClass="btn-warning  btn-sm mb-1 mb-md-0"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="UpdateData"><i class='bx bxs-edit' aria-hidden='true'></i> Update</asp:LinkButton>
                                                                        </div>
                                                                    </div>


                                                                </ItemTemplate>

                                                            </asp:TemplateField>
                                                            
                                                            
                                                              <asp:TemplateField HeaderText="Quantity">
                                                                <ItemTemplate>
                                                    
                                                       
                                                                    <asp:TextBox ID="txtQuantity" runat="server"   Text='<%#Eval("Quantity") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                             <asp:TemplateField HeaderText="Unit Price">
                                                                <ItemTemplate>
                                                                                                     
                                                                    <asp:TextBox ID="txtUnitPrice" runat="server"   Text='<%#Eval("UnitPrice") %>' CssClass="form-control form-control-sm mb-3"></asp:TextBox>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            


                                                            <asp:TemplateField HeaderText="Remove">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="itemdeleteImageButton" runat="server" class="btn btn-danger btn-sm btnTextShadow" CommandName="DeleteData" OnClick="itemdeleteImageButton_Click"><i class="fa fa-trash" aria-hidden="true"></i>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField HeaderText="Edit">
                                                                <ItemTemplate>

                                                                    <asp:LinkButton ID="EditButton" Visible="False"  runat="server" CssClass="btn-warning  btn-sm mb-1 mb-md-0"
                                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="EditData"><i class='bx bxs-edit' aria-hidden='true'></i></asp:LinkButton>
                                             
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <hr />

                                        <div class="row mb-4">

                                            <div class="col-md-5"></div>
                                            <div class="col-md-4">
                                                <asp:LinkButton runat="server" class="btn btn-outline-primary" ID="submitButton"  OnClick="submitButton_Click">
                                            
                                                    <i class="bx bxs-save mr-1"></i><span style="font-weight: bold !important">Save Info</span>

                                                </asp:LinkButton>



                                                <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="resetButton" OnClick="resetButton_Click">
                                            
                                                    <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Info</span>

                                                </asp:LinkButton>
                                            </div>
                                            <div class="col-md-4"></div>

                                        </div>
                                        
                                        
                                           
                                    </div>


                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-file me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Edit </h5>
                                        </div>
                                        <hr>
                                        
                                        
                                        <div class="row">
                                            <div class="col-6">
                                               
                                              
                                                   <div class="row mb-1">
                                                    <label class="col-sm-3 col-form-label">Proforma No:</label>
                                                    <div class="col-sm-8">
                                                      
                                                        <asp:DropDownList class="form-select mySelect2" AutoPostBack="True" runat="server" OnSelectedIndexChanged="ddlPINumber_OnSelectedIndexChanged" ID="ddlPINumber"></asp:DropDownList>


                                                    </div>
                                                </div>


                                              
                                            </div>
                                            <div class="col-md-6">

                                              

                                                                                                                                   
                                                
                                                        <div class="row mb-1">
                                                    <label class="col-sm-3 col-form-label">Sis Code :</label>
                                                    <div class="col-sm-8">
                                                      
                                                        <asp:DropDownList class="form-select mySelect2" AutoPostBack="True" runat="server" ID="ddlFabricInfo" OnSelectedIndexChanged="ddlFabric_OnSelectedIndexChanged"></asp:DropDownList>


                                                    </div>
                                                </div>

                                               
                                            </div>
                                        </div>


                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>



    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                    Height="120" Width="120" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>

