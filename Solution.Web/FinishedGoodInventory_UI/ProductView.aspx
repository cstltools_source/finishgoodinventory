﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/NewMasterPage.master" AutoEventWireup="true" CodeFile="ProductView.aspx.cs" Inherits="FinishedGoodInventory_UI_ProductView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


    <style>
        tbody tr td a {
            cursor: pointer !important;
        }

        tbody tr td a {
            position: relative;
            display: block;
            padding: 0.5rem 0.75rem;
            margin-left: -1px;
            line-height: 1.25;
            color: #007bff;
            background-color: #fff;
            border: 1px solid #dee2e6;
        }

       tbody tr td a {
            color: #007bff;
            text-decoration: none;
            background-color: transparent;
            -webkit-text-decoration-skip: objects;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div id="popDiv">
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Fabric Setup</div>

                        <div class="ms-auto">
                            <div class="btn-group">
                                <a href="../FinishedGoodInventory_UI/ProductInformationEntry.aspx" class="btn btn-sm btn-outline-info "><i class="fa fa-plus" aria-hidden="true"></i>New Entry</a>
                            </div>
                        </div>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">

                            <div class="card border-top border-0 border-4 border-success">


                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bx-list-ul me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Fabric List</h5>
                                        </div>
                                        <hr>


                                        <div class="row mb-2">

                                            <div class="col-md-6">
                                                <script type="text/javascript">
                                                    function pageLoad() {
                                                        $('.datepicker').pickadate({
                                                            selectMonths: true,
                                                            selectYears: true
                                                        });
                                                        $('.mySelect2').select2({
                                                            theme: 'bootstrap4',
                                                            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                                            placeholder: $(this).data('placeholder'),
                                                            allowClear: Boolean($(this).data('allow-clear')),
                                                        });
                                                    }
                                                </script>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Fabric Name: </label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtFabricName" runat="server" AutoPostBack="True" OnTextChanged="txtFabricName_OnTextChanged" placeholder="Enter Fabric Name" CssClass="form-control form-control-sm"></asp:TextBox>
                                                    </div>
                                                </div>


                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Fabric Short Name: </label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtFebricShortName" runat="server" AutoPostBack="True" OnTextChanged="txtFebricShortName_OnTextChanged" placeholder="Enter Fabric Short Name" CssClass="form-control form-control-sm"></asp:TextBox>
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="col-md-6">

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Fabric Category: </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2 " AutoPostBack="True" OnSelectedIndexChanged="ddlFabricCategory_OnSelectedIndexChanged" runat="server" ID="ddlFabricCategory"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Fabric Color: </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2 " AutoPostBack="True" OnSelectedIndexChanged="ddlFabricColor_OnSelectedIndexChanged" runat="server" ID="ddlFabricColor"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">

                                            <div class="col-3"></div>
                                            <div class="col-6">
                                                <asp:LinkButton runat="server" class="btn btn-outline-primary" ID="submitButton" OnClick="searchButton_Click">                                            
                                                     <i class="fa fa-search-plus"></i><span style="font-weight: bold !important">Search Information</span>
                                                </asp:LinkButton>

                                                <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="resetButton" OnClick="resetButton_Click">
                                                     <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Information</span>
                                                </asp:LinkButton>
                                            </div>
                                            <div class="col-3"></div>

                                        </div>
                                        <br />
                                        <br />

                                        <div id="MainGradeDiv" style="text-align: center !important">
                                            <asp:GridView ID="itemsGridView" runat="server" AutoGenerateColumns="False"
                                                PageIndex="0" CssClass="table table-bordered table-condensed custom-table-style" DataKeyNames="FabricId" OnRowCommand="itemsGridView_RowCommand"
                                                AllowPaging="True" OnPageIndexChanging="loadGridView_PageIndexChanging" PageSize="10" EmptyDataText="There are no data records to display.">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="SL">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                            <asp:HiddenField runat="server" ID="hfBuyerId" Value='<%#Eval("FabricId") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="FabricCode" HeaderText="Fabric Code" />
                                                    <asp:BoundField DataField="FabricShortName" HeaderText="Short Name" />
                                                    <asp:BoundField DataField="CategoryName" HeaderText="Category" />
                                                    <asp:BoundField DataField="FabricName" HeaderText="Fabric Name" />
                                                    <asp:BoundField DataField="FebricDescription" HeaderText="Febric Description" />
                                                    <asp:BoundField DataField="ColorName" HeaderText="Color" />
                                                    <asp:BoundField DataField="IsActive" HeaderText="Status" />
                                                    <asp:BoundField DataField="EntryBy" HeaderText="Entry By" />
                                                    <asp:BoundField DataField="EntryDate" HeaderText="Entry Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                    <%--<asp:BoundField DataField="UpdateBy" HeaderText="Update By" />
                                                    <asp:BoundField DataField="UpdateDate" HeaderText="Update Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                    <asp:BoundField DataField="InactiveBy" HeaderText="Inactive By" />
                                                    <asp:BoundField DataField="InactiveDate" HeaderText="Inactive Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                    <asp:TemplateField HeaderText="Actions">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="editImageButton" runat="server" class="btn btn-outline-info btn-sm  " ToolTip="Edit" CommandArgument='<%#Eval("FabricId") %>'
                                                                CommandName="EditData" ImageUrl="~/assets/images/pencil-icon.png" Height="30" Width="34" />

                                                            <asp:ImageButton OnClientClick="return confirm('Are you really aware of this operation?');" ID="activeInactiveButton" runat="server" class="btn btn-outline-success btn-sm" ToolTip="Active or InActive" Height="30" Width="34" CommandArgument='<%#Eval("FabricId") %>'
                                                                CommandName="ActiveInactiveData" ImageUrl='<%#Eval("ActiveInactiveUrl") %>' />

                                                            <asp:ImageButton OnClientClick="return confirm('Are you really aware of this operation?');" ID="deleteButton" runat="server" class="btn btn-outline-danger btn-sm" ToolTip="Delete" Height="30" Width="34" CommandArgument='<%#Eval("FabricId") %>'
                                                                CommandName="DeleteData" ImageUrl="~/assets/images/delete.png" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="12%"></ItemStyle>
                                                    </asp:TemplateField>--%>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>



    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                    Height="120" Width="120" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>

