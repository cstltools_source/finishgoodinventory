﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/NewMasterPage.master" AutoEventWireup="true" CodeFile="ProductCategoryView.aspx.cs" Inherits="FinishedGoodInventory_UI_ProductCategoryView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    
    <div id="popDiv">
    </div>
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Fabric Category</div>

                <%--<div class="ms-auto">
                    <div class="btn-group">
                        <a href="../FinishedGoodInventory_UI/LocationEntry.aspx" class="btn btn-sm btn-outline-info "><i class="fa fa-plus" aria-hidden="true"></i> New Entry</a>
                    </div>
                </div>--%>
            </div>
            <!--end breadcrumb-->
            <div class="row">
                <div class="col">

                    <div class="card border-top border-0 border-4 border-success">


                        <div class="card-body custom-height">
                            <div class="border p-4 rounded">
                                <div class="card-title d-flex align-items-center text-center">
                                    <div>
                                        <i class="bx bx-list-ul me-1 font-22 text-secondary"></i>
                                    </div>
                                    <h5 class="mb-0 text-secondary ">Fabric Category List</h5>
                                </div>
                                <hr>
                                
                                
                                     <div id="MainGradeDiv">
                                            <asp:GridView ID="itemsGridView" runat="server" AutoGenerateColumns="False"
                                                PageIndex="0" CssClass="table table-bordered table-condensed"
                                                AllowPaging="True" OnPageIndexChanging="loadGridView_PageIndexChanging" PageSize="10" EmptyDataText="There are no data records to display.">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="SL">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                         
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="ShortForm" HeaderText="Short Code" />
                                                    <asp:BoundField DataField="CategoryName" HeaderText="Fabric Category" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    

</asp:Content>

