﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FinishedGoodInventory_UI_BeenCardReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string pram = "";

        if (txtFromDate.Text != "")
        {
            if (txtToDate.Text != "")
            {
                var fromDate = Convert.ToDateTime(txtFromDate.Text.Trim());
                var toDate = Convert.ToDateTime(txtToDate.Text.Trim());

                var url = "../HTMLReport/BeenCardReportViewer.aspx?fromDate=" + fromDate + "&toDate=" + toDate;
                var fullURL = "var Mleft = (screen.width/2)-(950/2);var Mtop = (screen.height/2)-(700/2);window.open( '" + url + "', null, 'height=700,width=950,status=yes,toolbar=no,addressbar=no, scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
                ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
            }
            else
            {
                ShowMessageBox("Please select To Date !!");
            }

        }
        else
        {
            ShowMessageBox("Please select From Date !!");
        }


        //if (txtFromDate.Text != "" && txtToDate.Text != "")
        //{
        //    pram = pram + " AND CONVERT(date,FGSV.ProductionDate)  BETWEEN '" + txtFromDate.Text.Trim() +
        //           "' AND '" + txtToDate.Text.Trim() + "' ";
        //}

        //if (txtFromDate.Text != "" && txtToDate.Text == "")
        //{
        //    pram = pram + " AND CONVERT(date,FGSV.ProductionDate)  BETWEEN '" + txtFromDate.Text.Trim() +
        //           "' AND '" + DateTime.Now + "' ";
        //}

        //if (txtFromDate.Text == "" && txtToDate.Text != "")
        //{
        //    pram = pram + " AND CONVERT(date,FGSV.ProductionDate)  <= '" + txtToDate.Text.Trim() + "' ";
        //}
    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);

        
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }
}