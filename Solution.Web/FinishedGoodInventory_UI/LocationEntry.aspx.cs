﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_LocationEntry : System.Web.UI.Page
{
    private LocationDal aDal = new LocationDal();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            Get_LocationFor();

            if (Session["LocationId"] != null)
            {
                GetOneRecord(Convert.ToInt32(Session["LocationId"].ToString()));
                Session["LocationId"] = null;
            }

        }

    }




    # region Edit
    private void GetOneRecord(int locationId)
    {
        var aTable = new DataTable();

        aTable = aDal.GetLocationInfoById(locationId);

        if (aTable.Rows.Count > 0)
        {
            HFLocationId.Value = aTable.Rows[0].Field<int>("LocationId").ToString(CultureInfo.InvariantCulture);
            txtLocationName.Text = aTable.Rows[0].Field<String>("LocationName");
            rbLocationFor.SelectedValue = aTable.Rows[0].Field<Int32>("LocationFor").ToString(CultureInfo.InvariantCulture);
            submitButton.Text = "Update Information";
        }

    }

    #endregion Edit


    private void  Get_LocationFor()
    {

       DataTable Dt = aDal.GetLocationFor();
       if (Dt.Rows.Count > 0)
       {
           for (int i = 0; i < Dt.Rows.Count; i++)
           {
               ListItem item = new ListItem();
               item.Value = Dt.Rows[i]["LocationForId"].ToString();
               item.Text = Dt.Rows[i]["LocationForTitle"].ToString();
               rbLocationFor.Items.Add(item);
           }
       }
    }


    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private bool Validation()
    {
        if (txtLocationName.Text == "")
        {
            ShowMessageBox("You should enter Location !!");
            txtLocationName.Focus();
            txtLocationName.BackColor = Color.GhostWhite;
            return false;
        }
        
        if (rbLocationFor.SelectedValue == "")
        {
            ShowMessageBox("You should select Location For !!");
            rbLocationFor.Focus();
            rbLocationFor.BackColor = Color.GhostWhite;
            return false;
        }

        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {

            if (HFLocationId.Value == "")
            {
                bool saveId = SaveChanges();

                if (saveId)
                {
                    ShowMessageBox("Location info saved successfully !!");
                    Clear();
                }
                else
                {
                    ShowMessageBox("Data does not save successfully !!");
                }
            }
            else
            {
                bool saveId = UpdateChanges();

                if (saveId)
                {
                    ShowMessageBox("Location info updated successfully !!");
                    Clear();
                }
                else
                {
                    ShowMessageBox("Data does not updated successfully !!");
                }
            }

        }
    }

    private bool UpdateChanges()
    {
        bool retVal;
        try
        {
            retVal = aDal.UpdateLocationInfo(PrepareDataForUpdatee());

        }
        catch (Exception ex)
        {
            retVal = false;
            throw ex;
        }

        return retVal;
    }

    private LocationDao PrepareDataForUpdatee()
    {
        var aInfo = new LocationDao();

        aInfo.LocationId = Convert.ToInt32(HFLocationId.Value);
        aInfo.LocationName = txtLocationName.Text;
        aInfo.LocationFor = Convert.ToInt32(rbLocationFor.SelectedValue);

        return aInfo;
    }

 

    private bool SaveChanges()
    {
        bool retVal;
        try
        {
            retVal = aDal.SaveLocationInfo(PrepareDataForSave());

        }
        catch (Exception ex)
        {
            retVal = false;
            throw ex;
        }

        return retVal;
    }

    private LocationDao PrepareDataForSave()
    {
        var aInfo = new LocationDao();

        aInfo.LocationName = txtLocationName.Text;
        aInfo.LocationFor = Convert.ToInt32(rbLocationFor.SelectedValue);
        return aInfo;
    }

    private void Clear()
    {
        txtLocationName.Text = "";
        rbLocationFor.Items.Clear();
        Get_LocationFor();


    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        Clear();
    }
}