﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;

public partial class FinishedGoodInventory_UI_PalletWiseFabricLocationReport : System.Web.UI.Page
{
    private QAStockReceiveDal aDal = new QAStockReceiveDal();

    private FinishedGoodReportDal aGoodReportDal = new FinishedGoodReportDal();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //string date = Convert.ToString(DateTime.Now);
            InitialDropdownlist();
        }
    }

    private void InitialDropdownlist()
    {

        aGoodReportDal.LoadLocationInfoForddl(ddlLocation);

        aDal.LoadFabricName(ddlfabric);
        aDal.LoadFabricSet(ddlSet);
        aDal.LoadRollName(ddlRollWise);
    }

 

    protected void Button1_Click(object sender, EventArgs e)
    {
       

    }

  

    private void Clear()
    {
        ddlRollWise.SelectedIndex = 0;
        ddlfabric.SelectedIndex = 0;
        ddlSet.SelectedIndex = 0;
      //  txtFromDate.Text = "";
       // txtToDate.Text = "";
        itemsGridView.DataSource = null;
        itemsGridView.DataBind();

    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        Clear();
    }

    protected void loadGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        itemsGridView.PageIndex = e.NewPageIndex;
        // this.LoadList(itemsGridView);
    }

    private string GenerateString()
    {
        string param = "";

        if (ddlLocation.SelectedValue != "")
        {
            param = param + " AND FGS.QALocationId=" + ddlLocation.SelectedValue;
        }
        //if (txtFromDate.Text != "" && txtToDate.Text != "")
        //{
        //    param = param + " AND CONVERT(date,FGS.StockReceiveDate)  BETWEEN '" + txtFromDate.Text.Trim() +
        //            "' AND '" + txtToDate.Text.Trim() + "' ";
        //}
        //if (txtFromDate.Text != "" && txtToDate.Text == "")
        //{
        //    param = param + " AND CONVERT(date,FGS.StockReceiveDate)  BETWEEN '" + txtFromDate.Text.Trim() +
        //            "' AND '" + DateTime.Now + "' ";
        //}
        //if (txtFromDate.Text == "" && txtToDate.Text != "")
        //{
        //    param = param + " AND CONVERT(date,FGS.StockReceiveDate)  <= '" + txtToDate.Text.Trim() + "' ";
        //}
        return param;
    }

    protected void btn_List_OnClick(object sender, EventArgs e)
    {

        DataTable Dt = aGoodReportDal.Get_LocationWiseFabricStatus(GenerateString());
        itemsGridView.DataSource = Dt;
        itemsGridView.DataBind();

    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //required to avoid the runtime error "  
        //Control 'GridView1' of type 'GridView' must be placed inside a form tag with runat=server."  
    }

    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        
                string attachment = "attachment; filename=Pallet_Wise_Fabric_Status.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                DataTable aTable1 = aGoodReportDal.Get_LocationWiseFabricStatus(GenerateString());
                itemsGridView.DataSource = aTable1;
                itemsGridView.DataBind();

                // Create a form to contain the grid  

                HtmlForm frm = new HtmlForm();
                itemsGridView.Parent.Controls.Add(frm);

                //frm.Attributes["runat"] = "server";
                //frm.Controls.Add(loadGridView);
                //frm.RenderControl(htw);

                itemsGridView.HeaderRow.Style.Add("background-color", "#E5EEF1");

                // Set background color of each cell of GridView1 header row
                foreach (TableCell tableCell in itemsGridView.HeaderRow.Cells)
                {
                    tableCell.Style["background-color"] = "#E5EEF1";
                }

                // Set background color of each cell of each data row of GridView1
                foreach (GridViewRow gridViewRow in itemsGridView.Rows)
                {
                    gridViewRow.BackColor = System.Drawing.Color.White;

                    foreach (TableCell gridViewRowTableCell in gridViewRow.Cells)
                    {
                        gridViewRowTableCell.Style["background-color"] = "#FFFFFF";

                    }
                }

                itemsGridView.RenderControl(htw);

                string headerTable = @"
<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
</style>

";
                HttpContext.Current.Response.Write(headerTable);
                HttpContext.Current.Response.Write("<table style='width:100%;'>");
                HttpContext.Current.Response.Write("<tr>");
                HttpContext.Current.Response.Write("<th colspan='2'>Pallet No:</th> ");
                HttpContext.Current.Response.Write("<th>" + aTable1.Rows[0]["LocationName"].ToString() + "</th>");
                HttpContext.Current.Response.Write("<th colspan='4'>Pallet Wise Fabric Roll Status, Reporting Date:-</th>");
                HttpContext.Current.Response.Write("<th></th>");
                HttpContext.Current.Response.Write("<th></th>");
                HttpContext.Current.Response.Write("<th></th>");
                HttpContext.Current.Response.Write("<th colspan='2'>"+DateTime.Now.ToString("MM/dd/yyyy")+"</th>");
                HttpContext.Current.Response.Write("</tr>");
                HttpContext.Current.Response.Write("</table>");
              //  HttpContext.Current.Response.Write("<br>");
              Response.Write(sw.ToString());
                Response.End();
            

    }
   
}