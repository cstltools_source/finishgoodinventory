﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;

public partial class FinishedGoodInventory_UI_RoleWiseStoreLocationSetup : System.Web.UI.Page
{
    RollWiseLocationAllocationDal aDal = new RollWiseLocationAllocationDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDownList();
        }
    }


    private void LoadDropDownList()                                                                
    {
        aDal.LoadFabricSetInfo(ddlSet);
        aDal.LoadRollNoInfo(ddlRoll);
        aDal.LoadFabricInfo(ddlFabric);
        aDal.LoadSearchStoreLocation(ddlLocation);
        aDal.LoadStoreLocation(ddlStockLocation);
    }
    
    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }


    private string GenerateParameter()
    {
        string pram = "";

        if (ddlSet.SelectedValue != "")
        {
            pram = pram + " AND FGST.FabricSet = '" + ddlSet.SelectedValue + "'";
        }
        
        if (ddlRoll.SelectedValue != "")
        {
            pram = pram + " AND FGST.RollNo = '" + ddlRoll.SelectedValue + "'";
        }

       if (ddlFabric.SelectedValue != "")
        {
            pram = pram + " AND FGST.FabricCodeId = '" + ddlFabric.SelectedValue + "'";
        }

       if (ddlLocation.SelectedValue != "")
       {
           pram = pram + " AND FGST.QALocationId = '" + ddlLocation.SelectedValue + "'";
       }

       return pram;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            for (int i = 0; i < productGridView.Rows.Count; i++)
            {
                var storeLocationDropDownList = (DropDownList) productGridView.Rows[i].Cells[3].FindControl("ddlStoreLocation");

                if (storeLocationDropDownList.SelectedValue != "")
                {
                    string fgStockId = productGridView.DataKeys[i][0].ToString();

                    aDal.UpdateStockLocation(fgStockId, storeLocationDropDownList.SelectedValue,Convert.ToInt32(Session["UserId"].ToString()));
                }
            }

            ShowMessageBox("Location Updated Successfully !!");
            searchButton_Click(null, null);

        }
        else
        {
            ShowMessageBox("Please select location for at least one row !!");
        }

        
    }


    private bool Validation()
    {
        int count = 0;

        for (int i = 0; i < productGridView.Rows.Count; i++)
        {
            var chkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");

            if (chkBoxRows.Checked)
            {
                count++;
            }

            if (count > 0)
            {
                break;
            }

        }

        if (count == 0)
        {
            ShowMessageBox("Please select at least one item !!!");
            return false;
        }


        for (int i = 0; i < productGridView.Rows.Count; i++)
        {
            var chkBoxRows = (CheckBox) productGridView.Rows[i].Cells[0].FindControl("chkSelect");

            if (chkBoxRows.Checked)
            {
                var storeLocationDropDownList = (DropDownList)productGridView.Rows[i].Cells[3].FindControl("ddlStoreLocation");

                if (storeLocationDropDownList.SelectedValue == "")
                {
                    ShowMessageBox("Please Select Stock Location !!");
                    return false;
                }
            }
        }

        return true;
    }


    protected void resetButton_Click(object sender, EventArgs e)
    {   
        LoadDropDownList();
        productGridView.DataSource = null;
        productGridView.DataBind();
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {

        LoadList(productGridView);
    }

    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)productGridView.HeaderRow.FindControl("chkSelectAll");

        for (int i = 0; i < productGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");

            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
        }
    }

    protected void ddlSet_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        searchButton_Click(null,null);
    }

    protected void ddlRoll_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        searchButton_Click(null, null);
    }

    protected void loadGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        productGridView.PageIndex = e.NewPageIndex;
        this.LoadList(productGridView);
    }

    private void LoadList(GridView gridView)
    {

        DataTable dtdata = aDal.GetFinishedGoodStock(GenerateParameter());

        if (dtdata.Rows.Count > 0)
        {
            gridView.DataSource = dtdata;
            gridView.DataBind();


            for (int i = 0; i < productGridView.Rows.Count; i++)
            {
                var storeLocationDropDownList =
                    (DropDownList)productGridView.Rows[i].Cells[3].FindControl("ddlStoreLocation");

                aDal.LoadStoreLocation(storeLocationDropDownList);
            }
        }
        else
        {
            gridView.DataSource = null;
            gridView.DataBind();
        }

        
    }

    protected void ddlFabric_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        searchButton_Click(null, null);
    }

    protected void ddlLocation_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        searchButton_Click(null, null);
    }

    protected void BtnApply_OnClick(object sender, EventArgs e)
    {

        if (ddlStockLocation.SelectedValue != "")
        {
            for (int i = 0; i < productGridView.Rows.Count; i++)
            {
                var chkBoxRows = (CheckBox)productGridView.Rows[i].Cells[0].FindControl("chkSelect");

                if (chkBoxRows.Checked)
                {
                    var storeLocationDropDownList = (DropDownList)productGridView.Rows[i].Cells[3].FindControl("ddlStoreLocation");
                    storeLocationDropDownList.SelectedValue = ddlStockLocation.SelectedValue;
                }
            }
        }
        else
        {
            ShowMessageBox("You should select stock location !!");
            ddlStockLocation.Focus();
        }
        
    }
}