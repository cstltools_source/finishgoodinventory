﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/NewMasterPage.master" AutoEventWireup="true" CodeFile="DeliveryAgainestPackingDetails.aspx.cs" Inherits="FinishedGoodInventory_UI_DeliveryAgainestPackingDetails" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=3.0.20820.28364, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


    <style>
        #ContentPlaceHolder1_labelBuyerName {
            padding-top: 35px !important;
            vertical-align: central !important;
            font-size: 1.1em !important;
        }

        #ContentPlaceHolder1_rbDeliveryType_0,
        #ContentPlaceHolder1_rbDeliveryType_1,
        #ContentPlaceHolder1_rbDeliveryType_2,
        #ContentPlaceHolder1_rbDeliveryType_3,
        #ContentPlaceHolder1_rbDeliveryType_4,
        #ContentPlaceHolder1_rbDeliveryType_5,
        #ContentPlaceHolder1_rbDeliveryType_6,
        #ContentPlaceHolder1_rbDeliveryType_7,
        #ContentPlaceHolder1_rbDeliveryType_8 {
            margin-right: 5px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div id="popDiv">
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Delivery to Customer </div>
                        <%--<div class="ms-auto">
                    <div class="btn-group">
                        <a href="../FinishedGoodInventory_UI/PIView.aspx" class="btn btn-sm btn-sm btn-outline-info"><i class="fa fa-backward"></i>&nbsp;Back to List</a>
                    </div>
                </div>--%>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">

                            <div class="card border-top border-0 border-4 border-success">

                                <script type="text/javascript">
                                    function pageLoad() {
                                        $('.datepicker').pickadate({
                                            selectMonths: true,
                                            selectYears: true
                                        });
                                        $('.mySelect2').select2({
                                            theme: 'bootstrap4',
                                            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                            placeholder: $(this).data('placeholder'),
                                            allowClear: Boolean($(this).data('allow-clear')),
                                        });
                                    }
                                </script>

                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-package me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Delivery Information</h5>
                                        </div>
                                        <hr />

                                        <div class="row">
                                            <div class="col-5">
                                                <asp:RadioButtonList ID="rbDeliveryType" runat="server" CellPadding="3" AutoPostBack="True" OnSelectedIndexChanged="rbDeliveryType_OnSelectedIndexChanged" RepeatColumns="3" RepeatDirection="Vertical" Font-Bold="True" Font-Size="smaller">
                                                </asp:RadioButtonList>
                                            </div>
                                            <div class="col-7">
                                                <div class="row">

                                                    <div class="col-6">


                                                       
                                                        
                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-4 col-form-label col-form-label-sm">Fabric:<span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>
                                                            <div class="col-sm-8">
                                                                <asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlPackingGeneration" AutoPostBack="True" OnTextChanged="ddlPackingGeneration_OnTextChanged"></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-4 col-form-label col-form-label-sm">PI No:<span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>
                                                            <div class="col-sm-8">
                                                                <asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlPINumber" AutoPostBack="True" OnTextChanged="ddlPINumber_OnTextChanged"></asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-4 col-form-label col-form-label-sm">Del. Date:<span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtDeliveryDate" runat="server" CssClass="form-control form-control-sm datepicker"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-4 col-form-label col-form-label-sm">Order By: </label>
                                                            <div class="col-sm-8">
                                                                <asp:DropDownList ID="ddl_OrderBy" AutoPostBack="True" OnSelectedIndexChanged="ddl_OrderBy_OnSelectedIndexChanged"
                                                                                  runat="server" CssClass="form-control form-control-sm">
                                                                    <asp:ListItem Value="ASC"> Ascending </asp:ListItem>
                                                                    <asp:ListItem Value="DESC"> Descending </asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                    <div class="col-6">
                                                        <div class="form-group row">

                                                            <label class="col-sm-3 col-form-label">
                                                                <a href="../FinishedGoodInventory_UI/CustomerEntry.aspx" style="color:dimgrey">Buyer</a>

                                                            </label>
                                                            <div class="col-sm-9">
                                                                <asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlBuyer" AutoPostBack="True" OnSelectedIndexChanged="ddlBuyer_OnSelectedIndexChanged"></asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-3 col-form-label">Vendor: </label>
                                                            <div class="col-sm-9">
                                                                <asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlVendor" AutoPostBack="True" OnSelectedIndexChanged="ddlVendor_OnSelectedIndexChanged"></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group row">
                                                            <label class="col-sm-3 col-form-label">DO:<span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>
                                                            <div class="col-sm-9">
                                                                <asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlDOCharge" ></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group row">
                                                            <label class="col-sm-3 col-form-label">Remarks: </label>
                                                            <div class="col-sm-9 mt-2">
                                                                <asp:Label ID="lblRemarks" runat="server"  ></asp:Label>  
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <hr />
                                        <div class="row">
                                            <div class="col-7">
                                                <div class="card-title d-flex align-items-center text-center">
                                                    <div>
                                                        <i class="bx bxs-file me-1 font-22 text-secondary"></i>
                                                    </div>
                                                    <h5 class="mb-0 text-secondary ">Roll Description</h5>

                                                </div>

                                            </div>
                                            <div class="col-5">
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-10 col-form-label col-form-label-sm text-right">Total Quantity:</label>
                                                    <div class="col-sm-2 pt-2">
                                                        <asp:Label CssClass="custom-mt-top pt-2 bold text-success" ID="lblTotalQuantity" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <hr />


                                        <div class="row ">
                                            <div id="maingridview" style="text-align: center; height: auto; overflow: scroll; width: 100%; overflow-y: scroll; overflow-x: scroll;">
                                                <asp:GridView ID="productGridView" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-condensed custom-table-style"
                                                    DataKeyNames="IssueMasterId,IssueDetailId,FGStockId,StockQuantity" EmptyDataText="There are no data records to display.">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="# SL">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True" OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" AutoPostBack="True" OnCheckedChanged="chkSelect_CheckedChanged" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <%--<asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="FabricName" HeaderText="Fabric / SIS Code" />--%>
                                                        <asp:BoundField DataField="FabricSet" HeaderText="Set" />
                                                        <asp:BoundField DataField="RollNo" HeaderText="Roll No" />
                                                        <asp:BoundField DataField="StockQuantity" HeaderText="Quantity" />
                                                        <asp:BoundField DataField="RequiredWidth" HeaderText="Req. Width" />
                                                        <asp:BoundField DataField="ActualWidth" HeaderText="Cuttable Width" />
                                                        <asp:BoundField DataField="ShadeGradeing" HeaderText="Shade Grade" />
                                                        <asp:BoundField DataField="PPHSY" HeaderText="PPHSY" />
                                                        <asp:BoundField DataField="Piece" HeaderText="Piece" />
                                                        <asp:BoundField DataField="PWLength" HeaderText="P.W Length" />
                                                        <asp:BoundField DataField="NetWeight" HeaderText="Weight(Kg)" />
                                                       <%-- <asp:BoundField DataField="LocationName" HeaderText="Current Location" />--%>

                                                        
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row mb-4">

                                            <div class="col-5"></div>
                                            <div class="col-4" style="">
                                                <asp:Button ID="Button1" CssClass="btn btn-outline-primary align-center" OnClientClick="return confirm('Are you really aware of this operation?');" runat="server" Text="Confirm Delivery" OnClick="submitButton_Click" />
                                                <asp:LinkButton runat="server" Visible="False" class="btn btn-outline-warning" ID="resetButton" OnClick="resetButton_Click">Back to list</asp:LinkButton>
                                            </div>
                                            <div class="col-3"></div>

                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>



    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                    Height="120" Width="120" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


</asp:Content>

