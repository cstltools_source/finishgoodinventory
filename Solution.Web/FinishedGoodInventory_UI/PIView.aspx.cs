﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_PIView : System.Web.UI.Page
{
    ProformaInvoiceDal aDal = new ProformaInvoiceDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDownlist();
            LoadListInfo(itemsGridView);
        }
    }

    private void LoadDropDownlist()
    {
        aDal.LoadBuyerInfo(ddlBuyer);
        aDal.LoadVendorInfo(ddlVendor);
        aDal.LoadRequiredWidth(ddlWidth);
        aDal.LoadRequiredOunceYerd(ddlOunceYard);
        aDal.LoadRequiredShrinkage(ddlShrinkage);
        aDal.LoadProformaNo(ddlPi);
    }

    private void LoadListInfo(GridView gridView)
    {
        gridView.DataSource = aDal.GetProformaInfo(Generateprameter());
        gridView.DataBind();
    }

    private string Generateprameter()
    {
        string pram = "";

        if (ddlBuyer.SelectedValue != "")
        {
            pram = pram + " AND PI.BuyerId =" + ddlBuyer.SelectedValue;
        }
        
        if (ddlVendor.SelectedValue != "")
        {
            pram = pram + " AND PI.VendorId =" + ddlVendor.SelectedValue;
        }

        if (fromDateTextBox.Text.Trim() != "" && toDateTextBox.Text.Trim() != "")
        {
            pram = pram + " AND PI.ProformaDate BETWEEN '" + fromDateTextBox.Text.Trim() + "' AND '" + toDateTextBox.Text.Trim() + "'";
        }

        if (fromDateTextBox.Text.Trim() != "" && toDateTextBox.Text.Trim() == "")
        {
            pram = pram + "  AND PI.ProformaDate => '" + fromDateTextBox.Text.Trim() + "'";
        }

        if (fromDateTextBox.Text.Trim() == "" && toDateTextBox.Text.Trim() != "")
        {
            pram = pram + " AND PI.ProformaDate <= '" + toDateTextBox.Text.Trim() + "'";
        }

        if (ddlPi.SelectedValue != "")
        {
           // pram = pram + " AND ProformaInvNo LIKE '%" + ddlPi.SelectedValue + "%'";
            pram = pram + " AND PI.ProformaMasterId ='" + ddlPi.SelectedValue + "'";
        }

        if (ddlWidth.SelectedValue != "")
        {
            pram = pram + " AND RequiredWidth ='" + ddlWidth.SelectedValue + "'";
        }
        
        if (ddlOunceYard.SelectedValue != "")
        {
            pram = pram + " AND RequiredOunceOryard ='" + ddlOunceYard.SelectedValue + "'";
        }

        if (ddlShrinkage.SelectedValue != "")
        {
            pram = pram + " AND Shrinkage ='" + ddlShrinkage.SelectedValue + "'";
        }

        return pram;
    }

    protected void itemsGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int row = Convert.ToInt32(e.CommandArgument.ToString());

            Session["ProformaId"] = "";

            Session["ProformaId"] = e.CommandArgument.ToString();
            Response.Redirect("PIEntry.aspx");
        }

        if (e.CommandName == "DeleteData")
        {

            DataTable Dt = aDal.CheckProforma(e.CommandArgument.ToString());

            if (Dt.Rows.Count > 0)
            {
                ShowMessageBox("You can not delete this!!");
            }
            else
            {
                ProformaInvoiceDao aInfo = new ProformaInvoiceDao();

                aInfo.ProformaId = Int32.Parse(e.CommandArgument.ToString());

                if (aDal.DeleteProformaInfoById(aInfo))
                {
                    ShowMessageBox("Successfully Deleted !!");
                }

                this.LoadListInfo(itemsGridView);
            }

        }
    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void loadGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        itemsGridView.PageIndex = e.NewPageIndex;
        this.LoadListInfo(itemsGridView);
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {
        LoadListInfo(itemsGridView);
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        ddlPi.SelectedValue = "";
        fromDateTextBox.Text = "";
        toDateTextBox.Text = "";
        ddlBuyer.SelectedValue = "";
        ddlWidth.SelectedValue = "";
        ddlOunceYard.SelectedValue = "";
        ddlShrinkage.SelectedValue = "";
        LoadListInfo(itemsGridView);
    }

    protected void ddlBuyer_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadListInfo(itemsGridView);
    }

    protected void ddlVendor_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadListInfo(itemsGridView);
    }

    protected void ddlPi_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadListInfo(itemsGridView);
    }

    protected void ddlWidth_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadListInfo(itemsGridView);
    }

    protected void ddlOunceYard_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadListInfo(itemsGridView);
    }

    protected void ddlShrinkage_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadListInfo(itemsGridView);
    }
}