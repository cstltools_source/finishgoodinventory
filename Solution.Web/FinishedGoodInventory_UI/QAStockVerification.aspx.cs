﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;

public partial class FinishedGoodInventory_UI_QAStockVerification : System.Web.UI.Page
{
    QAStockVerificationDal aDal = new QAStockVerificationDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDownlist();
            LoadList(itemsGridView);
        }
    }

    private void LoadDropDownlist()
    {
        aDal.LoadShift(ddlShift);
        aDal.LoadBuyerInfo(ddlBuyer);
        aDal.LoadVendorInfo(ddlVendor);
        aDal.LoadProformaInvoice(ddlPi);
    }

    private void LoadList(GridView gridView)
    {
        gridView.DataSource = aDal.GetStockReceiveInfo(GenerateParameter());
        gridView.DataBind();
    }

    private string GenerateParameter()
    {
        string pram = "";

        if (ddlBuyer.SelectedValue != "")
        {
            pram = pram + " AND BI.BuyerName LIKE '%" + ddlBuyer.SelectedItem.Text + "%'";
        }

        if (ddlVendor.SelectedValue != "")
        {
            pram = pram + " AND VEN.BuyerName LIKE '%" + ddlBuyer.SelectedItem.Text + "%'";
        }

        if (ddlShift.SelectedValue != "")
        {
            pram = pram + " AND M.ShiftId = " + ddlShift.SelectedValue;
        }

        if (fromDateTextBox.Text.Trim() != "" && toDateTextBox.Text.Trim() != "")
        {
            pram = pram + " AND PI.ProformaDate BETWEEN '" + fromDateTextBox.Text.Trim() + "' AND '" + toDateTextBox.Text.Trim() + "'";
        }

        if (fromDateTextBox.Text.Trim() != "" && toDateTextBox.Text.Trim() == "")
        {
            pram = pram + "  AND PI.ProformaDate => '" + fromDateTextBox.Text.Trim() + "'";
        }

        if (fromDateTextBox.Text.Trim() == "" && toDateTextBox.Text.Trim() != "")
        {
            pram = pram + " AND PI.ProformaDate <= '" + toDateTextBox.Text.Trim() + "'";
        }

        if (ddlPi.SelectedValue != "")
        {
            pram = pram + " AND PI.ProformaInvNo LIKE '%" + ddlPi.SelectedValue + "%'";
        }

        if (txtProductionFromDate.Text.Trim() != "" && txtProductionToDate.Text.Trim() != "")
        {
            pram = pram + " AND M.ProductionDate BETWEEN '" + txtProductionFromDate.Text.Trim() + "' AND '" + txtProductionToDate.Text.Trim() + "'";
        }

        if (txtProductionFromDate.Text.Trim() != "" && txtProductionToDate.Text.Trim() == "")
        {
            pram = pram + "  AND M.ProductionDate => '" + txtProductionFromDate.Text.Trim() + "'";
        }

        if (txtProductionFromDate.Text.Trim() == "" && txtProductionToDate.Text.Trim() != "")
        {
            pram = pram + " AND M.ProductionDate <= '" + toDateTextBox.Text.Trim() + "'";
        }

        if (ddlPi.SelectedValue != "")
        {
            pram = pram + " AND PI.ProformaInvNo LIKE '%" + ddlPi.SelectedValue + "%'";
        }

        return pram;
    }

    protected void ddlBuyer_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadList(itemsGridView);
    }

    protected void ddlVendor_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadList(itemsGridView);
    }

    protected void ddlPi_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadList(itemsGridView);
    }

    protected void ddlWidth_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadList(itemsGridView);
    }

    protected void loadGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        itemsGridView.PageIndex = e.NewPageIndex;
        this.LoadList(itemsGridView);
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {
        this.LoadList(itemsGridView);
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        ddlBuyer.SelectedValue = "";
        ddlVendor.SelectedValue = "";
        ddlShift.SelectedIndex = 0;
        txtProductionFromDate.Text = "";
        txtProductionToDate.Text = "";
        fromDateTextBox.Text = "";
        toDateTextBox.Text = "";
        LoadDropDownlist();
        LoadList(itemsGridView);
    }

    protected void gotoinvoiceButton_Click(object sender, EventArgs e)
    {
        Button button = (Button)sender;
        GridViewRow currentRow = (GridViewRow)button.Parent.Parent;
        int rowindex = 0;
        rowindex = currentRow.RowIndex;

        Session["StockReceiveMasterId"] = itemsGridView.DataKeys[rowindex]["StockReceiveMasterId"].ToString();
        Response.Redirect("QAStockVerificationDetails.aspx");
    }
}