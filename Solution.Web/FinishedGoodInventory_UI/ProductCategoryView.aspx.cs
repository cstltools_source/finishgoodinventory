﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_ProductCategoryView : System.Web.UI.Page
{
    private ProductCategory_Dal aCategoryDal = new ProductCategory_Dal();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadShiftInfo(itemsGridView);
        }
    }

    private void LoadShiftInfo(GridView gridView)
    {
        gridView.DataSource = aCategoryDal.GetFabricCategoryInfo(GenerateParameter());
        gridView.DataBind();
    }

    private string GenerateParameter()
    {
        string pram = "";

        //if (ddlShift.SelectedValue != "")
        //{
        //    pram = pram + " And ShiftId =" + ddlShift.SelectedValue;
        //}

        return pram;
    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void loadGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        itemsGridView.PageIndex = e.NewPageIndex;
        this.LoadShiftInfo(itemsGridView);
    }

}