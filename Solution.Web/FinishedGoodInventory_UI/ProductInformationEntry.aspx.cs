﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_ProductInformationEntry : System.Web.UI.Page
{
    FabricDal aDal = new FabricDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDownList();

            if (Session["FabricId"] != null)
            {
                GetOneRecord(Convert.ToInt32(Session["FabricId"].ToString()));
                Session["FabricId"] = null;
            }
        }
    }

    # region Edit
    private void GetOneRecord(int buyerId)
    {
        var aTable = new DataTable();

        aTable = aDal.GetFabricInfoById(buyerId);

        if (aTable.Rows.Count > 0)
        {
            hfFabricId.Value = aTable.Rows[0].Field<int>("FabricId").ToString(CultureInfo.InvariantCulture);
            txtFabricName.Text = aTable.Rows[0].Field<String>("FabricName");
            ddlFabricColor.SelectedValue = aTable.Rows[0].Field<Int32>("FabricColor").ToString(CultureInfo.InvariantCulture);
            txtFebricShortName.Text = aTable.Rows[0].Field<String>("FabricShortName");
            txtFabricDescription.Text = aTable.Rows[0].Field<String>("FebricDescription");
            submitButton.Text = "Update Information";
        }

    }

    #endregion Edit

    private void LoadDropDownList()
    {
        aDal.LoadFabricColor(ddlFabricColor);
    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private bool Validation()
    {
        if (txtFabricName.Text == "")
        {
            ShowMessageBox("You should enter fabric name !!");
            txtFabricName.Focus();
            txtFabricName.BackColor = Color.GhostWhite;
            return false;
        }

        if (txtFebricShortName.Text == "")
        {
            ShowMessageBox("You should enter Febric Short Name !!");
            txtFebricShortName.Focus();
            txtFebricShortName.BackColor = Color.GhostWhite;
            return false;
        }

        if (txtFabricDescription.Text == "")
        {
            ShowMessageBox("You should enter fabric description !!");
            txtFabricDescription.Focus();
            txtFabricDescription.BackColor = Color.GhostWhite;
            return false;
        }

        if (ddlFabricColor.SelectedValue == "")
        {
            ShowMessageBox("You should select fabric color!!");
            ddlFabricColor.Focus();
            ddlFabricColor.BackColor = Color.GhostWhite;
            return false;
        }

        return true;
    }


    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {

            if (hfFabricId.Value == "")
            {
                bool saveId = SaveChanges();

                if (saveId)
                {
                    ShowMessageBox("Fabric info saved successfully !!");
                    Clear();
                }
                else
                {
                    ShowMessageBox("Data does not save successfully !!");
                }
            }
            else
            {
                bool saveId = UpdateChanges();

                if (saveId)
                {
                    ShowMessageBox("Fabric info updated successfully !!");
                    Clear();
                }
                else
                {
                    ShowMessageBox("Data does not updated successfully !!");
                }
            }

        }
    }

    private bool UpdateChanges()
    {
        bool retVal;
        try
        {
            retVal = aDal.UpdateFabricInfo(PrepareDataForUpdatee());

        }
        catch (Exception ex)
        {
            retVal = false;
            throw ex;
        }

        return retVal;
    }

    private FabricDao PrepareDataForUpdatee()
    {
        var aInfo = new FabricDao();

        aInfo.FabricId = Convert.ToInt32(hfFabricId.Value);
        aInfo.FabricName = txtFabricName.Text;
        aInfo.FabricColor = Convert.ToInt32(ddlFabricColor.SelectedValue);
        aInfo.FabricShortName = txtFebricShortName.Text;
        aInfo.FebricDescription = txtFabricDescription.Text;

        return aInfo;
    }


    private bool SaveChanges()
    {
        bool retVal;
        try
        {
            retVal = aDal.SaveFabricInfo(PrepareDataForSave());

        }
        catch (Exception ex)
        {
            retVal = false;
            throw ex;
        }

        return retVal;
    }

    private FabricDao PrepareDataForSave()
    {
        var aInfo = new FabricDao();

        aInfo.FabricName = txtFabricName.Text;
        aInfo.FabricColor = Convert.ToInt32(ddlFabricColor.SelectedValue);
        aInfo.FabricShortName = txtFebricShortName.Text;
        aInfo.FebricDescription = txtFabricDescription.Text;

        return aInfo;
    }

    private void Clear()
    {
        txtFabricName.Text = "";
        ddlFabricColor.SelectedValue = "";
        txtFebricShortName.Text = "";
        txtFabricDescription.Text = "";
        hfFabricId.Value = "";
        submitButton.Text = "Save Information";
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        Clear();
    }
}