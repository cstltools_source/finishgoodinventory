﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/NewMasterPage.master" AutoEventWireup="true" CodeFile="LocationView.aspx.cs" Inherits="FinishedGoodInventory_UI_LocationView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style>
        #ContentPlaceHolder1_rbLocationFor_0,
        #ContentPlaceHolder1_rbLocationFor_1 {
            margin-right: 5px !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
                <ProgressTemplate>
                    <div class="divWaiting">
                        <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                            Height="120" Width="120" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>



            <div id="popDiv">
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Stock Location Setup</div>

                        <div class="ms-auto">
                            <div class="btn-group">
                                <a href="../FinishedGoodInventory_UI/LocationEntry.aspx" class="btn btn-sm btn-outline-info "><i class="fa fa-plus" aria-hidden="true"></i>New Entry</a>
                            </div>
                        </div>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">

                            <div class="card border-top border-0 border-4 border-success">


                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">



                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-search me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Search Options</h5>
                                        </div>
                                        <hr>

                                        <div class="row mb-2">
                                            <label for="" class="col-sm-2 col-form-label">Stock Location Type</label>
                                            <div class="col-sm-5" style="padding-top: 8px">
                                                <asp:RadioButtonList ID="rbLocationFor" runat="server" AutoPostBack="True" OnSelectedIndexChanged="rbLocationFor_OnSelectedIndexChanged" CssClass="chkChoice" CellPadding="3" RepeatDirection="Horizontal" Font-Bold="True" Font-Size="Larger">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>

                                        <div class="row mb-2">
                                            <label for="" class="col-sm-2 col-form-label">Stock Location Name</label>
                                            <div class="col-sm-5">
                                                <asp:TextBox runat="server" class="form-control" ID="txtLocationName" AutoPostBack="True" OnTextChanged="txtLocationName_OnTextChanged" placeholder="Enter Location Name"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="row mb-4">
                                            <label class="col-sm-2 col-form-label"></label>
                                            <div class="col-sm-8">
                                                <asp:LinkButton runat="server" class="btn btn-outline-primary" ID="submitButton" OnClick="searchButton_Click">
                                            
                                            <i class="fa fa-search-plus"></i><span style="font-weight: bold !important">Search Information</span>

                                                </asp:LinkButton>


                                                <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="resetButton" OnClick="resetButton_Click">
                                                  <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Information</span>
                                                </asp:LinkButton>
                                            </div>
                                        </div>


                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bx-list-ul me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Stock Location List</h5>
                                        </div>
                                        <hr>

                                        <div id="MainGradeDiv">
                                            <asp:GridView ID="itemsGridView" runat="server" AutoGenerateColumns="False"
                                                PageIndex="0" CssClass="table table-bordered table-condensed custom-table-style text-center" DataKeyNames="LocationId" OnRowCommand="itemsGridView_RowCommand"
                                                AllowPaging="True" OnPageIndexChanging="loadGridView_PageIndexChanging" PageSize="10" EmptyDataText="There are no data records to display.">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="SL">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                            <asp:HiddenField runat="server" ID="hfBuyerId" Value='<%#Eval("LocationId") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="LocationName" HeaderText="Location" />
                                                    <asp:BoundField DataField="LocationForTitle" HeaderText="Location Type" />

                                                    <asp:BoundField DataField="IsActive" HeaderText="Status" />
                                                    <asp:BoundField DataField="EntryBy" HeaderText="Entry By" />
                                                    <asp:BoundField DataField="EntryDate" HeaderText="Entry Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                    <asp:BoundField DataField="UpdateBy" HeaderText="Update By" />
                                                    <asp:BoundField DataField="UpdateDate" HeaderText="Update Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                    <asp:BoundField DataField="InactiveBy" HeaderText="Inactive By" />
                                                    <asp:BoundField DataField="InactiveDate" HeaderText="Inactive Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                    <asp:TemplateField HeaderText="Actions">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="editImageButton" runat="server" class="btn btn-white btn-sm  " Height="36" Width="38" CommandArgument='<%#Eval("LocationId") %>'
                                                                CommandName="EditData" ImageUrl="~/assets/images/pencil-icon.png" />
                                                            <asp:ImageButton ID="activeInactiveButton" runat="server" class="btn btn-white btn-sm" Height="36" Width="38" CommandArgument='<%#Eval("LocationId") %>'
                                                                CommandName="ActiveInactiveData" ImageUrl='<%#Eval("ActiveInactiveUrl") %>' />
                                                            <asp:ImageButton ID="DeleteImageButton" runat="server" class="btn btn-white btn-sm  " Height="36" Width="38" CommandArgument='<%#Eval("LocationId") %>'
                                                                CommandName="DeleteData" ImageUrl="~/assets/images/delete.png" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="12%"></ItemStyle>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

