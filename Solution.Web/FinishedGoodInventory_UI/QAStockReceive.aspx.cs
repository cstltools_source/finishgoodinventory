﻿using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_QAStockReceive : System.Web.UI.Page
{
    private QAStockReceiveDal aDal = new QAStockReceiveDal();

    private readonly StockMovementDal aMovementDal = new StockMovementDal();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            if( Session["UserId"] != null)
            {
                TxtEntryBy.Text = Session["EmpName"].ToString();
            }

            LoadDropDownList();
            InitialGrid();

            if (Session["StockReceiveMasterId"] != null)
            {
              
                GetOneRecord(Convert.ToInt32(Session["StockReceiveMasterId"].ToString()));
                Session["StockReceiveMasterId"] = null;
            }
            else
            {
                tbxPiece.Text = 1.ToString();
            }
        }

    }

    #region Edit
    private void GetOneRecord(int StockReceiveMasterId)
    {
        var aTable = new DataTable();

        aTable = aDal.GetQAStockReceiveMasterById(StockReceiveMasterId);

        if (aTable.Rows.Count > 0)
        {

            hfStockReceiveMasterId.Value = "";

            hfStockReceiveMasterId.Value = aTable.Rows[0].Field<int>("StockReceiveMasterId").ToString(CultureInfo.InvariantCulture);

            cbxCategory.SelectedValue = null;

            ddlPINumber.SelectedValue = aTable.Rows[0].Field<Int32>("ProformaInvId").ToString(CultureInfo.InvariantCulture);
          
            if (ddlPINumber.SelectedValue != "")
            {
                ddlPINumber_OnSelectedIndexChanged(null, null);
            }
            ddlShift.Text = aTable.Rows[0].Field<Int32>("ShiftId").ToString(CultureInfo.InvariantCulture);
            txtoductionDate.Text = aTable.Rows[0]["ProductionDate"].ToString();
            TxtEntryBy.Text = aTable.Rows[0].Field<String>("EmpName");
            TxtEntryDate.Text = aTable.Rows[0]["EntryDate"].ToString();

            itemsGridView.DataSource = null;
            itemsGridView.DataSource = aTable;
            itemsGridView.DataBind();

        }
    }

    #endregion Edit

    private void LoadDropDownList()
    {

        aDal.LoadBuyerInfo2(ddlBuyer);
        aDal.LoadVendorInfo2(ddlVendor);
        aDal.LoadQALocation(ddlQaLocation);
        aDal.LoadRollNo(ddlRollNo);

        using (DataTable dt = aDal.LoadPiNoAll())
        {
            ddlPINumber.DataSource = dt;
            ddlPINumber.DataValueField = "ProformaMasterId";
            ddlPINumber.DataTextField = "ProformaInvNo";
            ddlPINumber.DataBind();
            ddlPINumber.Items.Insert(0, new ListItem("Select from list", String.Empty));
            ddlPINumber.SelectedIndex = 0;

        }


        aDal.LoadShift(ddlShift);
        aDal.LoadColor(ddlColor);

        DataTable DT = aDal.GetReceiveMovementCategory("Receive");
        if (DT.Rows.Count > 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                ListItem list = new ListItem();
                list.Text = DT.Rows[i]["MovementCategory"].ToString();
                list.Value = DT.Rows[i]["MovementCategoryId"].ToString();
                rbReceiveType.Items.Add(list);
                rbReceiveType.SelectedValue = 1.ToString();
                rbReceiveType_OnSelectedIndexChanged(null, null);

            }
        }


       
            cbxCategory.SelectedIndex = 0;
        

    }




    #region InitailGrid

    private void InitialGrid()
    {
        //DataTable aTable = new DataTable();
        //aTable.Columns.Add("FabricOrSisCodeId");
        //aTable.Columns.Add("FebricDescription");
        //aTable.Columns.Add("UOMId");
        //aTable.Columns.Add("FabricSet");
        //aTable.Columns.Add("Beam");
        //aTable.Columns.Add("GreigeRollNo");
        //aTable.Columns.Add("RollNo");
        //aTable.Columns.Add("DefectPoint");
        //aTable.Columns.Add("Re_Width");
        //aTable.Columns.Add("Width");
        //aTable.Columns.Add("Piece");
        //aTable.Columns.Add("PWLength");
        //aTable.Columns.Add("Quantity");
        //aTable.Columns.Add("Shady");
        //aTable.Columns.Add("OperatorName");
        //aTable.Columns.Add("INSMCNo");
        //aTable.Columns.Add("FL");
        //aTable.Columns.Add("PPHY");
        //aTable.Columns.Add("Weight");
        //aTable.Columns.Add("NetWeight");
        //aTable.Columns.Add("ZoneAndPlate");
        //aTable.Columns.Add("ProblemCause");
        //aTable.Columns.Add("ShadeGradeing");
        //aTable.Columns.Add("Shrinkage");
        //aTable.Columns.Add("QALocationId");
        //aTable.Columns.Add("ProbleamCauseId");

        //DataRow dr;

        //dr = aTable.NewRow();

        //aTable.Rows.Add(dr);

        //QAStockDetails.DataSource = null;
        //QAStockDetails.DataBind();
        //QAStockDetails.DataSource = aTable;
        //QAStockDetails.DataBind();


        //////foreach (GridViewRow row in QAStockDetails.Rows)
        //////{
        //////    TextBox productTextBox = (TextBox)QAStockDetails.Rows[row.RowIndex].Cells[3].FindControl("TxtFabricName");
        //////    AjaxControlToolkit.AutoCompleteExtender modal = (AjaxControlToolkit.AutoCompleteExtender)productTextBox.FindControl("TxtFabricName_AutoCompleteExtender");
        //////}

        //for (int i = 0; i < QAStockDetails.Rows.Count; i++)
        //{
        //    DropDownList UOM = ((DropDownList)QAStockDetails.Rows[i].FindControl("ddlUOM"));
        //    using (DataTable dt = aDal.Get_UOM_All())
        //    {
        //        UOM.DataSource = dt;
        //        UOM.DataValueField = "UOMId";
        //        UOM.DataTextField = "UOM";
        //        UOM.DataBind();
        //      //  UOM.Items.Insert(0, new ListItem("Select from list", String.Empty));
        //        UOM.SelectedValue=1.ToString();
                
        //    }

        //    DropDownList QALocation = ((DropDownList)QAStockDetails.Rows[i].FindControl("ddlQALocation"));
        //    aDal.LoadQALocation(QALocation);

        //    DropDownList ProblemCauses = ((DropDownList)QAStockDetails.Rows[i].FindControl("ddlProblemCouses"));
        //    aDal.LoadProblemCouses(ProblemCauses);


        //    DropDownList ShadeGrade = ((DropDownList)QAStockDetails.Rows[i].FindControl("ddlShadeGradeing"));
        //    aDal.LoadShadeGrade(ShadeGrade);
            

        //}

    }

    protected void ItemAddImageButton_Click(object sender, EventArgs e)
    {

        //int rowIndex = ((GridViewRow)(((LinkButton)sender).Parent.Parent)).RowIndex;
        //DataTable aTable = new DataTable();
        //aTable.Columns.Add("FabricOrSisCodeId");
        //aTable.Columns.Add("FebricDescription");
        //aTable.Columns.Add("UOMId");
        //aTable.Columns.Add("FabricSet");
        //aTable.Columns.Add("Beam");
        //aTable.Columns.Add("GreigeRollNo");
        //aTable.Columns.Add("RollNo");
        //aTable.Columns.Add("DefectPoint");
        //aTable.Columns.Add("Re_Width");
        //aTable.Columns.Add("Width");
        //aTable.Columns.Add("Piece");
        //aTable.Columns.Add("PWLength");
        //aTable.Columns.Add("Quantity");
        //aTable.Columns.Add("Shady");
        //aTable.Columns.Add("OperatorName");
        //aTable.Columns.Add("INSMCNo");
        //aTable.Columns.Add("FL");
        //aTable.Columns.Add("PPHY");
        //aTable.Columns.Add("Weight");
        //aTable.Columns.Add("NetWeight");
        //aTable.Columns.Add("ZoneAndPlate");
        //aTable.Columns.Add("ProblemCause");
        //aTable.Columns.Add("ShadeGradeing");
        //aTable.Columns.Add("Shrinkage");
        //aTable.Columns.Add("QALocationId");
        //aTable.Columns.Add("ProbleamCauseId");

        //DataRow dr;

        //for (int i = 0; i < QAStockDetails.Rows.Count; i++)
        //{
        //    dr = aTable.NewRow();

        //    //  dr["FabricId"] = ((DropDownList)QAStockDetails.Rows[i].FindControl("ddlFabricName")).SelectedValue;
        //    dr["FabricOrSisCodeId"] =
        //        ((TextBox) QAStockDetails.Rows[i].FindControl("txtFabricOrSisCodeId")).Text.Trim();
        //    dr["FebricDescription"] = ((TextBox) QAStockDetails.Rows[i].FindControl("TxtFabricName")).Text.Trim();
        //    dr["FabricSet"] = ((TextBox) QAStockDetails.Rows[i].FindControl("TxtSET")).Text.Trim();
        //    dr["Beam"] = ((TextBox) QAStockDetails.Rows[i].FindControl("TxtBeam")).Text.Trim();
        //    dr["GreigeRollNo"] =
        //        ((TextBox) QAStockDetails.Rows[i].Cells[2].FindControl("TxtGeraigeRollNo")).Text.Trim();
        //    dr["RollNo"] = ((TextBox) QAStockDetails.Rows[i].Cells[2].FindControl("TxtRollNo")).Text.Trim();
        //    dr["DefectPoint"] = ((TextBox) QAStockDetails.Rows[i].FindControl("TxtDefectPoint")).Text.Trim();
        //    dr["Re_Width"] = ((TextBox) QAStockDetails.Rows[i].FindControl("TxtRequiredWidth")).Text.Trim();
        //    dr["Width"] = ((TextBox) QAStockDetails.Rows[i].FindControl("TxtWidth")).Text.Trim();
        //    dr["Piece"] = ((TextBox) QAStockDetails.Rows[i].FindControl("TxtPiece")).Text.Trim();
        //    dr["PWLength"] = ((TextBox) QAStockDetails.Rows[i].FindControl("TxtPWLength")).Text.Trim();
        //    dr["Quantity"] = ((TextBox) QAStockDetails.Rows[i].FindControl("TxtQuantity")).Text.Trim();
        //    dr["Shady"] = ((TextBox) QAStockDetails.Rows[i].FindControl("TxtShady")).Text.Trim();
        //    dr["OperatorName"] = ((TextBox) QAStockDetails.Rows[i].FindControl("TxtOPName")).Text.Trim();
        //    dr["INSMCNo"] = ((TextBox) QAStockDetails.Rows[i].FindControl("TxtINSMCNo")).Text.Trim();
        //    dr["FL"] = ((TextBox) QAStockDetails.Rows[i].FindControl("TxtFL")).Text.Trim();
        //    dr["PPHY"] = ((TextBox) QAStockDetails.Rows[i].FindControl("TxtPPHY")).Text.Trim();
        //    dr["Weight"] = ((TextBox) QAStockDetails.Rows[i].FindControl("TxtWeight")).Text.Trim();
        //    dr["NetWeight"] = ((TextBox) QAStockDetails.Rows[i].FindControl("TxtNetWeight")).Text.Trim();
        //    dr["QALocationId"] = ((DropDownList) QAStockDetails.Rows[i].FindControl("ddlQALocation")).SelectedValue;
        //    dr["ProbleamCauseId"] = ((DropDownList) QAStockDetails.Rows[i].FindControl("ddlProblemCouses")).SelectedValue;
        //    dr["UOMId"] = ((DropDownList) QAStockDetails.Rows[i].FindControl("ddlUOM")).SelectedValue;
        //    dr["ShadeGradeing"] = ((DropDownList) QAStockDetails.Rows[i].FindControl("ddlShadeGradeing")).SelectedValue;
        //    dr["Shrinkage"] = ((TextBox) QAStockDetails.Rows[i].FindControl("TxtShrinkage")).Text.Trim();
        //    aTable.Rows.Add(dr);


        //}




        //dr = aTable.NewRow();

        //aTable.Rows.Add(dr);

        //QAStockDetails.DataSource = aTable;
        //QAStockDetails.DataBind();



        //LinkButton qtyTextBox = (LinkButton)sender;
        //GridViewRow currentRow = (GridViewRow)qtyTextBox.Parent.Parent;
        //int rowindex = 0;
        //rowindex = currentRow.RowIndex;
        //int nextrow = rowindex + 1;

        //var GShrinkage = (TextBox)QAStockDetails.Rows[rowindex].FindControl("TxtShrinkage");
        //var GRequire = (TextBox)QAStockDetails.Rows[rowindex].FindControl("TxtRequiredWidth");

        //var Shrinkage = (TextBox)QAStockDetails.Rows[nextrow].FindControl("TxtShrinkage");
        //var Required = (TextBox)QAStockDetails.Rows[nextrow].FindControl("TxtRequiredWidth");

        //Shrinkage.Text = string.IsNullOrEmpty(GShrinkage.Text) ? null : GShrinkage.Text.Trim();
        //Required.Text = string.IsNullOrEmpty(GRequire.Text) ? null : GRequire.Text.Trim();


        //foreach (GridViewRow row in QAStockDetails.Rows)
        //{
        //    TextBox productTextBox = (TextBox)QAStockDetails.Rows[row.RowIndex].FindControl("TxtFabricName");
        //    AjaxControlToolkit.AutoCompleteExtender modal = (AjaxControlToolkit.AutoCompleteExtender)productTextBox.FindControl("TxtFabricName_AutoCompleteExtender");
        //}

        //for (int i = 0; i < QAStockDetails.Rows.Count; i++)
        //{
        //    DropDownList UOM = ((DropDownList)QAStockDetails.Rows[i].FindControl("ddlUOM"));
        //    HiddenField HFID = (HiddenField)QAStockDetails.Rows[i].FindControl("HFUOMId");
        //    using (DataTable dt = aDal.Get_UOM_All())
        //    {
        //        UOM.DataSource = dt;
        //        UOM.DataValueField = "UOMId";
        //        UOM.DataTextField = "UOM";
        //        UOM.DataBind();
        //        UOM.Items.Insert(0, new ListItem("Select from list", String.Empty));
        //        if (HFID.Value != "")
        //        {
        //            UOM.SelectedValue = HFID.Value;
        //        }
        //        else
        //        {
        //            UOM.SelectedValue = 1.ToString();
        //        }

        //    }

        //    DropDownList QALocation = ((DropDownList)QAStockDetails.Rows[i].FindControl("ddlQALocation"));
        //    aDal.LoadQALocation(QALocation);
        //    HiddenField Location = (HiddenField)QAStockDetails.Rows[i].FindControl("HFQALocationId");
        //    if (Location.Value != "")
        //    {
        //        QALocation.SelectedValue = Location.Value;
        //    }
        //    else
        //    {
        //        QALocation.SelectedIndex = 0;
        //    }


        //    DropDownList ProblemCauses = ((DropDownList)QAStockDetails.Rows[i].FindControl("ddlProblemCouses"));
        //    aDal.LoadProblemCouses(ProblemCauses);

        //    HiddenField Problem = (HiddenField)QAStockDetails.Rows[i].FindControl("HFProblemCausesId");
        //    if (Problem.Value != "")
        //    {
        //        ProblemCauses.SelectedValue = Problem.Value;
        //    }
        //    else
        //    {
        //        ProblemCauses.SelectedIndex = 0;
        //    }


        //    DropDownList ShadeGrade = ((DropDownList)QAStockDetails.Rows[i].FindControl("ddlShadeGradeing"));
        //    aDal.LoadShadeGrade(ShadeGrade);

        //    HiddenField hfShadeGrade = (HiddenField)QAStockDetails.Rows[i].FindControl("hfShadeGrade");
        //    if (hfShadeGrade.Value != "")
        //    {
        //        ShadeGrade.SelectedValue = hfShadeGrade.Value;
        //    }
        //    else
        //    {
        //        ShadeGrade.SelectedIndex = 0;
        //    }



        //}


    }

    protected void itemdeleteImageButton_Click(object sender, EventArgs e)
    {
        //int rowIndex = ((GridViewRow)(((LinkButton)sender).Parent.Parent)).RowIndex;
        //DataTable aTable = new DataTable();
        //aTable.Columns.Add("FabricOrSisCodeId");
        //aTable.Columns.Add("FebricDescription");
        //aTable.Columns.Add("UOMId");
        //aTable.Columns.Add("FabricSet");
        //aTable.Columns.Add("Beam");
        //aTable.Columns.Add("GreigeRollNo");
        //aTable.Columns.Add("RollNo");
        //aTable.Columns.Add("DefectPoint");
        //aTable.Columns.Add("Re_Width");
        //aTable.Columns.Add("Width");
        //aTable.Columns.Add("Piece");
        //aTable.Columns.Add("PWLength");
        //aTable.Columns.Add("Quantity");
        //aTable.Columns.Add("Shady");
        //aTable.Columns.Add("OperatorName");
        //aTable.Columns.Add("INSMCNo");
        //aTable.Columns.Add("FL");
        //aTable.Columns.Add("PPHY");
        //aTable.Columns.Add("Weight");
        //aTable.Columns.Add("NetWeight");
        //aTable.Columns.Add("ZoneAndPlate");
        //aTable.Columns.Add("ProblemCause");
        //aTable.Columns.Add("ShadeGradeing");
        //aTable.Columns.Add("Shrinkage");
        //aTable.Columns.Add("ProbleamCauseId");
        //aTable.Columns.Add("QALocationId");
        //DataRow dr;

        //for (int i = 0; i < QAStockDetails.Rows.Count; i++)
        //{
        //    if (i != rowIndex)
        //    {
        //        dr = aTable.NewRow();

        //        dr["FabricOrSisCodeId"] = ((TextBox)QAStockDetails.Rows[i].FindControl("txtFabricOrSisCodeId")).Text.Trim();
        //        dr["FebricDescription"] = ((TextBox)QAStockDetails.Rows[i].FindControl("TxtFabricName")).Text.Trim();
        //        dr["FabricSet"] = ((TextBox)QAStockDetails.Rows[i].FindControl("TxtSET")).Text.Trim();
        //        dr["Beam"] = ((TextBox)QAStockDetails.Rows[i].FindControl("TxtBeam")).Text.Trim();
        //        dr["GreigeRollNo"] = ((TextBox)QAStockDetails.Rows[i].Cells[2].FindControl("TxtGeraigeRollNo")).Text.Trim();
        //        dr["RollNo"] = ((TextBox)QAStockDetails.Rows[i].Cells[2].FindControl("TxtRollNo")).Text.Trim();
        //        dr["DefectPoint"] = ((TextBox)QAStockDetails.Rows[i].FindControl("TxtDefectPoint")).Text.Trim();
        //        dr["Re_Width"] = ((TextBox)QAStockDetails.Rows[i].FindControl("TxtRequiredWidth")).Text.Trim();
        //        dr["Width"] = ((TextBox)QAStockDetails.Rows[i].FindControl("TxtWidth")).Text.Trim();
        //        dr["Piece"] = ((TextBox)QAStockDetails.Rows[i].FindControl("TxtPiece")).Text.Trim();
        //        dr["PWLength"] = ((TextBox)QAStockDetails.Rows[i].FindControl("TxtPWLength")).Text.Trim();
        //        dr["Quantity"] = ((TextBox)QAStockDetails.Rows[i].FindControl("TxtQuantity")).Text.Trim();
        //        dr["Shady"] = ((TextBox)QAStockDetails.Rows[i].FindControl("TxtShady")).Text.Trim();
        //        dr["OperatorName"] = ((TextBox)QAStockDetails.Rows[i].FindControl("TxtOPName")).Text.Trim();
        //        dr["INSMCNo"] = ((TextBox)QAStockDetails.Rows[i].FindControl("TxtINSMCNo")).Text.Trim();
        //        dr["FL"] = ((TextBox)QAStockDetails.Rows[i].FindControl("TxtFL")).Text.Trim();
        //        dr["PPHY"] = ((TextBox)QAStockDetails.Rows[i].FindControl("TxtPPHY")).Text.Trim();
        //        dr["Weight"] = ((TextBox)QAStockDetails.Rows[i].FindControl("TxtWeight")).Text.Trim();
        //        dr["NetWeight"] = ((TextBox)QAStockDetails.Rows[i].FindControl("TxtNetWeight")).Text.Trim();
        //        dr["QALocationId"] = ((DropDownList)QAStockDetails.Rows[i].FindControl("ddlQALocation")).SelectedValue;
        //        dr["ProbleamCauseId"] = ((DropDownList)QAStockDetails.Rows[i].FindControl("ddlProblemCouses")).SelectedValue;
        //        dr["UOMId"] = ((DropDownList)QAStockDetails.Rows[i].FindControl("ddlUOM")).SelectedValue;
        //        dr["ShadeGradeing"] = ((DropDownList)QAStockDetails.Rows[i].FindControl("ddlShadeGradeing")).SelectedValue;
        //        dr["Shrinkage"] = ((TextBox)QAStockDetails.Rows[i].FindControl("TxtShrinkage")).Text.Trim();
        //        aTable.Rows.Add(dr);
        //    }
        //}

        //QAStockDetails.DataSource = aTable;
        //QAStockDetails.DataBind();

        //foreach (GridViewRow row in QAStockDetails.Rows)
        //{
        //    TextBox productTextBox = (TextBox)QAStockDetails.Rows[row.RowIndex].Cells[3].FindControl("TxtFabricName");
        //    AjaxControlToolkit.AutoCompleteExtender modal = (AjaxControlToolkit.AutoCompleteExtender)productTextBox.FindControl("TxtFabricName_AutoCompleteExtender");
        //}

        //for (int i = 0; i < QAStockDetails.Rows.Count; i++)
        //{

        //    DropDownList UOM = ((DropDownList)QAStockDetails.Rows[i].FindControl("ddlUOM"));
        //    HiddenField HFID = (HiddenField)QAStockDetails.Rows[i].FindControl("HFUOMId");
        //    using (DataTable dt = aDal.Get_UOM_All())
        //    {
        //        UOM.DataSource = dt;
        //        UOM.DataValueField = "UOMId";
        //        UOM.DataTextField = "UOM";
        //        UOM.DataBind();
        //        UOM.Items.Insert(0, new ListItem("Select from list", String.Empty));
        //        if (HFID.Value != "")
        //        {
        //            UOM.SelectedValue = HFID.Value;
        //        }
        //        else
        //        {
        //            UOM.SelectedValue = 1.ToString();
        //        }

        //    }

        //    DropDownList QALocation = ((DropDownList)QAStockDetails.Rows[i].FindControl("ddlQALocation"));
        //    aDal.LoadQALocation(QALocation);
        //    HiddenField Location = (HiddenField)QAStockDetails.Rows[i].FindControl("HFQALocationId");
        //    if(Location.Value !="")
        //    {
        //        QALocation.SelectedValue = Location.Value;
        //    }
        //    else
        //    {
        //        QALocation.SelectedIndex = 0;
        //    }


        //    DropDownList ProblemCauses = ((DropDownList)QAStockDetails.Rows[i].FindControl("ddlProblemCouses"));
        //    aDal.LoadProblemCouses(ProblemCauses);

        //    HiddenField Problem = (HiddenField)QAStockDetails.Rows[i].FindControl("HFProblemCausesId");
        //    if (Problem.Value != "")
        //    {
        //        ProblemCauses.SelectedValue = Problem.Value;
        //    }
        //    else
        //    {
        //        ProblemCauses.SelectedIndex = 0;
        //    }


        //    DropDownList ShadeGrade = ((DropDownList)QAStockDetails.Rows[i].FindControl("ddlShadeGradeing"));
        //    aDal.LoadShadeGrade(ShadeGrade);

        //    HiddenField hfShadeGrade = (HiddenField)QAStockDetails.Rows[i].FindControl("hfShadeGrade");
        //    if (hfShadeGrade.Value != "")
        //    {
        //        ShadeGrade.SelectedValue = hfShadeGrade.Value;
        //    }
        //    else
        //    {
        //        ShadeGrade.SelectedIndex = 0;
        //    }

        //}

    }

    #endregion InitailGrid

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private bool Validation()
    {
         int rcvType = 0;



         //if (rbReceiveType.SelectedValue == "")
         //{
         //    ShowMessageBox("You should select  !!");
         //    ddlPINumber.Focus();
         //    return false;
         //}


        rcvType = int.Parse(rbReceiveType.SelectedValue);

        if (rcvType == 1)
        {
            if (ddlPINumber.SelectedValue == "")
            {
                ShowMessageBox("You should select proforma no. !!");
                ddlPINumber.Focus();
                return false;
            }

        }

        if (ddlFabricInfo.SelectedValue == "")
        {
            ShowMessageBox("You should select fabric. !!");
            ddlFabricInfo.Focus();
            return false;
        }
        
        if (ddlColor.SelectedValue == "")
        {
            ShowMessageBox("You should select color !!");
            ddlColor.Focus();
            return false;
        }

        if (tbxRequiredWidth.Text.Trim() == "")
        {
            ShowMessageBox("You should select required width !!");
            tbxRequiredWidth.Focus();
            return false;
        }
        
        if (tbxOunceYard.Text.Trim() == "")
        {
            ShowMessageBox("You should select required Ounce/Yard !!");
            tbxOunceYard.Focus();
            return false;
        }

        if (txtRequiredShrinkageLenthMin.Text.Trim() == "")
        {
            ShowMessageBox("You should select required shrinkage Length(Min) !!");
           // tbxShrinkageLength.Focus();
            return false;
        }
        
        if (txtRequiredShrinkageLenthMax.Text.Trim() == "")
        {
            ShowMessageBox("You should select required shrinkage Length(Max) !!");
           // tbxShrinkageLength.Focus();
            return false;
        }

        if (txtRequiredShrinkageWidthMin.Text.Trim() == "")
        {
            ShowMessageBox("You should select required shrinkage Width(Min)!!");
          //  tbxShrinkageWidth.Focus();
            return false;
        }
        
        if (txtRequiredShrinkageWidthMax.Text.Trim() == "")
        {
            ShowMessageBox("You should select required shrinkage Width(Max)!!");
           // tbxShrinkageWidth.Focus();
            return false;
        }

        if (rcvType == 1)
        {
            if (txtoductionDate.Text == "")
            {
                ShowMessageBox("You should select poduction date !!");
                txtoductionDate.Focus();
                return false;
            }

            if (ddlShift.SelectedValue == "")
            {
                ShowMessageBox("You should select shift !!");
                ddlShift.Focus();
                return false;
            }


            if (hfStockReceiveMasterId.Value == "")
            {
                if (txtFLNo.Text == "")
                {
                    ShowMessageBox("You should select FL No !!");
                    txtFLNo.Focus();
                    return false;
                }

                if (ddlQaLocation.SelectedValue == "")
                {
                    ShowMessageBox("You should select QA location !!");
                    ddlQaLocation.Focus();
                    return false;
                }
            }

        }

        if (TxtEntryDate.Text == "")
        {
            ShowMessageBox("You should enter Entry Date !!");
            TxtEntryDate.Focus();
            return false;
        }

        if (!Update.Visible)
        {
            if (itemsGridView.Rows.Count == 0)
            {
                ShowMessageBox("You should select at least one roll info !!");
                return false;
            }

        }

       

        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            int saveId = SaveChanges();

                if (saveId > 0)
                {
                    ShowMessageBox("QA Stock Receive saved successfully !!");

                    itemsGridView.DataSource = null;
                    itemsGridView.DataBind();

                    PopUpReport(saveId);

                  //  Clear();
                }
                else
                {
                    ShowMessageBox("Data does not save successfully !!");
                }
            
                
        }
    }


    private void PopUpReport(int masterId)
    {

        string pram = "";

        if (masterId.ToString() != "")
        {
            pram = pram + " AND QARM.StockReceiveMasterId = " + masterId;
        }

        string url = "../FinishedGoodInventory_RPTView/FinishedGoodReportViewer.aspx?rptType=QASR&rpt=" + pram;
        string fullURL = "window.open('" + url + "', '_blank', 'height=600,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }

    private bool UpdateChanges()
    {
        bool retVal;
        try
        {
           retVal = aDal.UpdateQAStockReceive(PrepareDetailsDataForUpdate());

        }
        catch (Exception ex)
        {
            retVal = false;
            throw ex;
        }

        return retVal;
    }

  

    private Int32 SaveChanges()
    {
        Int32 retVal;
        try
        {
            retVal = aDal.SaveStockReceiveMaster(PrepareMasterDataForSave(), PrepareDetailsDataForSave());

        }
        catch (Exception ex)
        {
            retVal = 0;
            throw ex;
        }

        return retVal;
    }

    private QaStockReceiveMasterDao PrepareMasterDataForSave()
    {
        string fabric = ddlFabricInfo.SelectedItem.Text.Trim();
        string[] fabrics = fabric.Split(':');

        var aInfo = new QaStockReceiveMasterDao();
        if (hfStockReceiveMasterId.Value == "")
        {
            aInfo.StockReceiveMasterId = 0;
        }
        else
        {
            aInfo.StockReceiveMasterId = Convert.ToInt32(hfStockReceiveMasterId.Value);
        }

        aInfo.ProformaInvId = ddlPINumber.SelectedIndex > 0 ? int.Parse(ddlPINumber.SelectedValue) : (int?)null;
        aInfo.ProformaDetailId = Convert.ToInt32(0);
        aInfo.ShiftId = ddlShift.SelectedIndex > 0 ? int.Parse(ddlShift.SelectedValue) : (int?)null;
        aInfo.ProductionDate = string.IsNullOrEmpty(txtoductionDate.Text) ? (DateTime?)null : DateTime.Parse(txtoductionDate.Text).Date;
        aInfo.EntryDate = DateTime.Parse(TxtEntryDate.Text);
        aInfo.RcvdTypeId = Int32.Parse(rbReceiveType.SelectedValue);
        aInfo.Remarks = string.IsNullOrEmpty(ReceiveReason.Text.Trim()) ? null : ReceiveReason.Text.Trim();
        
        return aInfo;
    }

    private QaStockReceiveMasterDao PrepareMasterDataForUpdate()
    {
        var aInfo = new QaStockReceiveMasterDao();
        //aInfo.StockReceiveMasterId = Convert.ToInt32(hfStockReceiveMasterId.Value);
        //aInfo.ProformaInvId = ddlPINumber.SelectedIndex > 0 ? int.Parse(ddlPINumber.SelectedValue) : (int?)null;
        //aInfo.ShiftId = ddlShift.SelectedIndex > 0 ? int.Parse(ddlShift.SelectedValue) : (int?)null;
        //aInfo.ProductionDate = string.IsNullOrEmpty(txtoductionDate.Text) ? (DateTime?)null : DateTime.Parse(txtoductionDate.Text).Date;
        //aInfo.EntryDate = DateTime.Parse(TxtEntryDate.Text);
        //aInfo.RcvdTypeId = Int32.Parse(rbReceiveType.SelectedValue);
        //aInfo.RcvdReason = string.IsNullOrEmpty(ReceiveReason.Text.Trim()) ? null : ReceiveReason.Text.Trim();
        //aInfo.RcvdDate = DateTime.Parse(ReceiveDate.Text);
        //aInfo.DeliveryId = ddlDelivery.SelectedIndex > 0 ? int.Parse(ddlDelivery.SelectedValue) : (int?)null;
        //aInfo.ReturnDate = string.IsNullOrEmpty(ReturnDate.Text) ? (DateTime?)null : DateTime.Parse(ReturnDate.Text).Date;
        //aInfo.HECuttingDate = string.IsNullOrEmpty(hECuttingDate.Text) ? (DateTime?)null : DateTime.Parse(hECuttingDate.Text).Date;
        //aInfo.ReasonForCutting = string.IsNullOrEmpty(cuttingReason.Text.Trim()) ? null : cuttingReason.Text.Trim();
        return aInfo;
    }


    private List<QaStockReceiveDetailDao> PrepareDetailsDataForSave()
    {
        List<QaStockReceiveDetailDao> aList = new List<QaStockReceiveDetailDao>();
        QaStockReceiveDetailDao aDao;

        for (int i = 0; i < itemsGridView.Rows.Count; i++)
        {
            aDao = new QaStockReceiveDetailDao();

            aDao.FabricName = itemsGridView.DataKeys[i][0].ToString();
            aDao.FabricCategory = itemsGridView.DataKeys[i][5].ToString();
            aDao.ColorId = Convert.ToInt32(ddlColor.SelectedValue);
            aDao.RequiredWidth = Convert.ToDecimal(tbxRequiredWidth.Text.Trim());
            aDao.RequiredOunceYard = Convert.ToDecimal(tbxOunceYard.Text.Trim());
            aDao.ShrinkageLengthMin = Convert.ToDecimal(txtRequiredShrinkageLenthMin.Text.Trim());
            aDao.ShrinkageLengthMax = Convert.ToDecimal(txtRequiredShrinkageLenthMax.Text.Trim());
            aDao.ShrinkageWidthMin = Convert.ToDecimal(txtRequiredShrinkageWidthMin.Text.Trim());
            aDao.ShrinkageWidthMax = Convert.ToDecimal(txtRequiredShrinkageWidthMax.Text.Trim());
            aDao.FabricSet = itemsGridView.DataKeys[i][1].ToString();
            aDao.Beam = Convert.ToInt32(itemsGridView.Rows[i].Cells[1].Text.Trim());
            aDao.RollNo = Convert.ToInt32(itemsGridView.DataKeys[i][4].ToString());
            aDao.GreigeRollNo = itemsGridView.Rows[i].Cells[2].Text.Trim();
            aDao.DefectPoint = Convert.ToDecimal(itemsGridView.Rows[i].Cells[4].Text.Trim());
            aDao.ActualWidth = Convert.ToDecimal(itemsGridView.Rows[i].Cells[5].Text.Trim());
            aDao.Piece = itemsGridView.Rows[i].Cells[6].Text != "" ? Convert.ToDecimal(itemsGridView.Rows[i].Cells[6].Text.Trim()) : 0;
            aDao.PWLength = itemsGridView.Rows[i].Cells[7].Text.Trim();
            aDao.Quantity = Convert.ToDecimal(itemsGridView.Rows[i].Cells[8].Text.Trim());
            aDao.MCNo = Convert.ToInt32(itemsGridView.Rows[i].Cells[10].Text.Trim());
            aDao.PPHSY = Convert.ToDecimal(itemsGridView.Rows[i].Cells[9].Text.Trim());
            aDao.GrossWeight = Convert.ToDecimal(itemsGridView.Rows[i].Cells[11].Text.Trim());
            aDao.NetWeight = Convert.ToDecimal(itemsGridView.Rows[i].Cells[12].Text.Trim());
            aDao.ProblemCause = itemsGridView.Rows[i].Cells[13].Text.Trim();
            aDao.FL = Convert.ToInt32(itemsGridView.DataKeys[i][2].ToString());
            aDao.QALocationId = Convert.ToInt32(itemsGridView.DataKeys[i][3].ToString());
            aDao.Remarks = itemsGridView.Rows[i].Cells[14].Text;
            aDao.OperatorName = itemsGridView.Rows[i].Cells[15].Text;

            aList.Add(aDao);
        }




        //try
        //{
        //    for (int i = 0; i < QAStockDetails.Rows.Count; i++)
        //    {
        //        QaStockReceiveDetailDao aDao = new QaStockReceiveDetailDao();

        //        //TxtFabricName

        //        TextBox txtFabricOrSisCodeId = (TextBox)QAStockDetails.Rows[i].FindControl("txtFabricOrSisCodeId");

        //        HiddenField HFFabricId = (HiddenField)QAStockDetails.Rows[i].FindControl("HFFabricId");
        //        TextBox FabricSet = (TextBox)QAStockDetails.Rows[i].FindControl("TxtSET");
        //        TextBox TxtBeam = (TextBox)QAStockDetails.Rows[i].FindControl("TxtBeam");
        //        TextBox TxtGeraigeRollNo = (TextBox)QAStockDetails.Rows[i].FindControl("TxtGeraigeRollNo");
        //        TextBox TxtRollNo = (TextBox)QAStockDetails.Rows[i].FindControl("TxtRollNo");
        //        TextBox TxtDefectPoint = (TextBox)QAStockDetails.Rows[i].FindControl("TxtDefectPoint");
        //        TextBox TxtRequiredWidth = (TextBox)QAStockDetails.Rows[i].FindControl("TxtRequiredWidth");
        //        TextBox TxtWidth = (TextBox)QAStockDetails.Rows[i].FindControl("TxtWidth");
        //        TextBox TxtPiece = (TextBox)QAStockDetails.Rows[i].FindControl("TxtPiece");
        //        TextBox TxtPWLength = (TextBox)QAStockDetails.Rows[i].FindControl("TxtPWLength");
        //        TextBox TxtQuantity = (TextBox)QAStockDetails.Rows[i].FindControl("TxtQuantity");
        //        TextBox TxtShady = (TextBox)QAStockDetails.Rows[i].FindControl("TxtShady");
        //        TextBox TxtOPName = (TextBox)QAStockDetails.Rows[i].FindControl("TxtOPName");
        //        TextBox TxtINSMCNo = (TextBox)QAStockDetails.Rows[i].FindControl("TxtINSMCNo");
        //        TextBox TxtFL = (TextBox)QAStockDetails.Rows[i].FindControl("TxtFL");
        //        TextBox TxtPPHY = (TextBox)QAStockDetails.Rows[i].FindControl("TxtPPHY");
        //        TextBox TxtWeight = (TextBox)QAStockDetails.Rows[i].FindControl("TxtWeight");
        //        TextBox TxtNetWeight = (TextBox)QAStockDetails.Rows[i].FindControl("TxtNetWeight");

        //        DropDownList ddlQaLocation = (DropDownList)QAStockDetails.Rows[i].FindControl("ddlQALocation");
        //        DropDownList ddlProblemCauses = (DropDownList)QAStockDetails.Rows[i].FindControl("ddlProblemCouses");
        //        DropDownList ddlUOM = (DropDownList)QAStockDetails.Rows[i].FindControl("ddlUOM");
        //        DropDownList ddlShadeGrade = (DropDownList)QAStockDetails.Rows[i].FindControl("ddlShadeGradeing");
        //        TextBox TxtShrinkage = (TextBox)QAStockDetails.Rows[i].FindControl("TxtShrinkage");

        //        aDao.StockReceiveDetailId = 0;
        //        aDao.FabricOrSisCodeId = string.IsNullOrEmpty(txtFabricOrSisCodeId.Text) ? 0 : int.Parse(txtFabricOrSisCodeId.Text);
        //        aDao.FabricSet = string.IsNullOrEmpty(FabricSet.Text) ? null : FabricSet.Text;
        //        aDao.Beam = string.IsNullOrEmpty(TxtBeam.Text) ? null : TxtBeam.Text;
        //        aDao.GreigeRollNo = string.IsNullOrEmpty(TxtGeraigeRollNo.Text) ? null : TxtGeraigeRollNo.Text;
        //        aDao.RollNo = string.IsNullOrEmpty(TxtRollNo.Text) ? null : TxtRollNo.Text;
        //        aDao.DefectPoint = string.IsNullOrEmpty(TxtDefectPoint.Text) ? null : TxtDefectPoint.Text;
        //        aDao.Re_Width = string.IsNullOrEmpty(TxtRequiredWidth.Text) ? 0 : Decimal.Parse(TxtRequiredWidth.Text);
        //        aDao.Width = string.IsNullOrEmpty(TxtWidth.Text) ? 0 : Decimal.Parse(TxtWidth.Text);
        //        aDao.Piece = string.IsNullOrEmpty(TxtPiece.Text) ? 0 : Decimal.Parse(TxtPiece.Text);
        //        aDao.PWLength = string.IsNullOrEmpty(TxtPWLength.Text) ? null : TxtPWLength.Text;
        //        aDao.Quantity = string.IsNullOrEmpty(TxtQuantity.Text) ? 0 : Decimal.Parse(TxtQuantity.Text);
        //        aDao.Shady = string.IsNullOrEmpty(TxtShady.Text) ? 0 : Decimal.Parse(TxtShady.Text);
        //        aDao.OperatorName = string.IsNullOrEmpty(TxtOPName.Text) ? null : TxtOPName.Text;
        //        aDao.INSMCNo = string.IsNullOrEmpty(TxtINSMCNo.Text.Trim()) ? null : TxtINSMCNo.Text.Trim();
        //        aDao.FL = string.IsNullOrEmpty(TxtFL.Text.Trim()) ? null : TxtFL.Text.Trim();
        //        aDao.PPHY = string.IsNullOrEmpty(TxtPPHY.Text.Trim()) ? 0 : decimal.Parse(TxtPPHY.Text.Trim());
        //        aDao.Weight = string.IsNullOrEmpty(TxtWeight.Text.Trim()) ? 0 : decimal.Parse(TxtWeight.Text.Trim());
        //        aDao.NetWeight = string.IsNullOrEmpty(TxtNetWeight.Text.Trim()) ? 0 : decimal.Parse(TxtNetWeight.Text.Trim());
        //        aDao.ProbleamCauseId = ddlProblemCauses.SelectedIndex > 0 ? int.Parse(ddlProblemCauses.SelectedValue) : (int?)null;
        //        aDao.QALocationId = ddlQaLocation.SelectedIndex > 0 ? int.Parse(ddlQaLocation.SelectedValue) : (int?)null;
        //        aDao.UOMId = ddlUOM.SelectedIndex > 0 ? int.Parse(ddlUOM.SelectedValue) : (int?)null;
        //        aDao.ShadeGradeing = ddlShadeGrade.SelectedIndex > 0 ? ddlShadeGrade.SelectedItem.Text.Trim() : null;
        //        aDao.Shrinkage = string.IsNullOrEmpty(TxtShrinkage.Text) ? null : TxtShrinkage.Text;
        //        aList.Add(aDao);
        //    }
        //}
        //catch (Exception)
        //{

        //}
        return aList;
    }


    private QaStockReceiveDetailDao PrepareDetailsDataForUpdate()
    {
        QaStockReceiveDetailDao aDao;
        aDao = new QaStockReceiveDetailDao();
        aDao.StockReceiveDetailId = int.Parse(ddlRollNo.SelectedValue);
        aDao.FabricName = ddlFabricInfo.SelectedValue;
        aDao.FabricCategory = cbxCategory.SelectedItem.Text.Trim();
            aDao.ColorId = Convert.ToInt32(ddlColor.SelectedValue);
            aDao.RequiredWidth = Convert.ToDecimal(tbxRequiredWidth.Text.Trim());
            aDao.RequiredOunceYard = Convert.ToDecimal(tbxOunceYard.Text.Trim());
            aDao.ShrinkageLengthMin = Convert.ToDecimal(txtRequiredShrinkageLenthMin.Text.Trim());
            aDao.ShrinkageLengthMax = Convert.ToDecimal(txtRequiredShrinkageLenthMax.Text.Trim());
            aDao.ShrinkageWidthMin = Convert.ToDecimal(txtRequiredShrinkageWidthMin.Text.Trim());
            aDao.ShrinkageWidthMax = Convert.ToDecimal(txtRequiredShrinkageWidthMax.Text.Trim());
            aDao.FabricSet = txtSetNo.Text;
            aDao.Beam =  int.Parse(tbxBeamNo.Text);
            aDao.RollNo = int.Parse(tbxRollNo.Text);
            aDao.GreigeRollNo = tbxGrlNo.Text;
            aDao.DefectPoint = decimal.Parse(tbxDefectPoint.Text);
            aDao.ActualWidth = decimal.Parse(tbxActualWidth.Text);
            aDao.Piece = decimal.Parse(tbxPiece.Text);
            aDao.PWLength = string.IsNullOrEmpty(tbxPWLength.Text) ? "": tbxPWLength.Text.Trim();
            aDao.Quantity = decimal.Parse(tbxQuantity.Text);
            aDao.MCNo = int.Parse(tbxMcNo.Text);
            aDao.PPHSY = decimal.Parse(tbxPphsy.Text);
            aDao.GrossWeight = decimal.Parse(tbxGrossWeight.Text);
            aDao.NetWeight = decimal.Parse(tbxNetWeight.Text);
            aDao.ProblemCause = string.IsNullOrEmpty(tbxProblemCause.Text.Trim()) ? "" : tbxProblemCause.Text;
            aDao.FL = int.Parse(txtFLNo.Text);
            aDao.QALocationId = int.Parse(ddlQaLocation.SelectedValue);
            aDao.Remarks = string.IsNullOrEmpty(tbxRemarks.Text.Trim()) ? "" : tbxRemarks.Text;
            aDao.OperatorName = string.IsNullOrEmpty(tbxOperatorName.Text.Trim()) ? "" : tbxOperatorName.Text;

            return aDao;
    }

    private void Clear()
    {
        ddlPINumber.SelectedIndex = 0;
       
        ReceiveReason.Text = "";
        ddlShift.SelectedIndex = 0;
        txtoductionDate.Text = "";
        TxtEntryDate.Text = "";
        InitialGrid();
        submitButton.Text = "Save Information";

        foreach (ListItem aItem in rbReceiveType.Items)
        {
            aItem.Selected = false;
        }

        itemsGridView.DataSource = null;
        itemsGridView.DataBind();
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        Clear();
    }

    protected void ddlPINumber_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ddlColor.SelectedValue = "";
        tbxRequiredWidth.Text = "";
        tbxOunceYard.Text = "";
        //tbxShrinkageLength.Text = "";
        //tbxShrinkageWidth.Text = "";
        txtRequiredShrinkageLenthMin.Text = "";
        txtRequiredShrinkageLenthMax.Text = "";
        txtRequiredShrinkageWidthMin.Text = "";
        txtRequiredShrinkageWidthMax.Text = "";


        if (ddlPINumber.SelectedValue != "")
        {
          
          aDal.LoadSisCodeByPi(ddlFabricInfo, Convert.ToInt32(ddlPINumber.SelectedValue));

          DataTable dt = aDal.LoadBuyerVendor(ddlPINumber.SelectedValue);

          if (dt.Rows.Count > 0)
          {
              ddlBuyer.SelectedValue = dt.Rows[0]["BuyerId"].ToString();
              ddlVendor.SelectedValue = dt.Rows[0]["VendorId"].ToString();


              int fabricCount = 0;
              fabricCount = ddlFabricInfo.Items.Count;

              if (fabricCount == 2)
              {
                  ddlFabricInfo.SelectedIndex = 1;
                  ddlFabric_OnSelectedIndexChanged(null, null);
              }
              else
              {
                  ddlColor.SelectedValue = "";
                  tbxRequiredWidth.Text = "";
                  tbxOunceYard.Text = "";
                 // tbxShrinkageLength.Text = "";
                 // tbxShrinkageWidth.Text = "";
              }       
          }
        }


     
    }

   

    protected void rbReceiveType_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        int rcvType = 0;

        rcvType = int.Parse(rbReceiveType.SelectedValue);

        switch (rcvType)
        {
            case 1:
                //RDate.Visible = false;
                ReceiveReason.Text = "PI Wise";
                break;
            case 2:
                //RDate.Visible = false;
                ddlPINumber.SelectedIndex = 0;
                ReceiveReason.Text = "NOOS";
                break;
            case 3:
                //RDate.Visible = true;
                ReceiveReason.Text = "Return Fabric in YDS";
                break;
            case 5:
                //RDate.Visible = false;
                ReceiveReason.Text = "Rcvd from R & D";
                break;
            case 6:
                //RDate.Visible = false;
                ReceiveReason.Text = "Received from QA After Head Ends Cutting";
                break;
            case 7:
                //RDate.Visible = false;
                ReceiveReason.Text = "Fabric Rcvd after Re-Finish";
                break;
        }
    }

    
    protected void addToListButton_Click(object sender, EventArgs e)
    {
        if (AddToListValidation())
        {
            DataTable aDataTable = new DataTable();

            aDataTable.Columns.Add("SisCode");
            aDataTable.Columns.Add("FabricSet");
            aDataTable.Columns.Add("FLNo");
            aDataTable.Columns.Add("QaLocationId");
            aDataTable.Columns.Add("RollNo");
            aDataTable.Columns.Add("BeamNo");
            aDataTable.Columns.Add("GrlNo");
            aDataTable.Columns.Add("DefectPoints");
            aDataTable.Columns.Add("ActualWidth");
            aDataTable.Columns.Add("Piece");
            aDataTable.Columns.Add("PWLength");
            aDataTable.Columns.Add("Quantity");
            aDataTable.Columns.Add("Pphsy");
            aDataTable.Columns.Add("MCNo");
            aDataTable.Columns.Add("GrossWeight");
            aDataTable.Columns.Add("NetWeight");
            aDataTable.Columns.Add("ProblemCause");
            aDataTable.Columns.Add("Remarks");
            aDataTable.Columns.Add("OPName");
            aDataTable.Columns.Add("Category");

            DataRow dataRow = null;

            if (HasRollNo(Convert.ToInt32(tbxRollNo.Text.Trim())))
            {
                dataRow = aDataTable.NewRow();

                dataRow["SisCode"] = ddlFabricInfo.SelectedItem.Text;
                dataRow["FabricSet"] = txtSetNo.Text.Trim();
                dataRow["FLNo"] = txtFLNo.Text.Trim();
                dataRow["QaLocationId"] = ddlQaLocation.SelectedValue;
                dataRow["RollNo"] = tbxRollNo.Text.Trim();
                dataRow["BeamNo"] = tbxBeamNo.Text.Trim();
                dataRow["GrlNo"] = tbxGrlNo.Text.Trim();
                dataRow["DefectPoints"] = Convert.ToDecimal(tbxDefectPoint.Text.Trim()).ToString("F");
                dataRow["ActualWidth"] =  Convert.ToDecimal(tbxActualWidth.Text.Trim()).ToString("F");

                if (tbxPiece.Text.Trim() == "")
                {
                    dataRow["Piece"] = 0;
                }
                else
                {
                    dataRow["Piece"] = tbxPiece.Text.Trim();
                }

                if (tbxPWLength.Text.Trim() == "")
                {
                    dataRow["PWLength"] = "N/A";
                }
                else
                {
                    dataRow["PWLength"] = tbxPWLength.Text.Trim(); 
                }


                dataRow["Quantity"] = Convert.ToDecimal(tbxQuantity.Text.Trim()).ToString("F");
                dataRow["Pphsy"] = tbxPphsy.Text.Trim();
                dataRow["MCNo"] = tbxMcNo.Text.Trim();
                dataRow["GrossWeight"] = Convert.ToDecimal(tbxGrossWeight.Text.Trim()).ToString("F");
                dataRow["NetWeight"] = tbxNetWeight.Text.Trim();

                if (tbxProblemCause.Text.Trim() == "")
                {
                    dataRow["ProblemCause"] = "0";
                }
                else
                {
                    dataRow["ProblemCause"] = tbxProblemCause.Text.Trim();
                }


                if (tbxRemarks.Text.Trim() == "")
                {
                    dataRow["Remarks"] = "N/A";

                }
                else
                {
                    dataRow["Remarks"] = tbxRemarks.Text.Trim();
                }
                
                dataRow["OPName"] = tbxOperatorName.Text.Trim();
                dataRow["Category"] = cbxCategory.SelectedItem.Value;

                aDataTable.Rows.Add(dataRow);
            }
            else
            {
                ShowMessageBox("Fabric/SisCode already exist !!");
            }

            for (int i = 0; i < itemsGridView.Rows.Count; i++)
            {
                dataRow = aDataTable.NewRow();

                dataRow["SisCode"] = itemsGridView.DataKeys[i][0].ToString();
                dataRow["FabricSet"] = itemsGridView.DataKeys[i][1].ToString();
                dataRow["FLNo"] = itemsGridView.DataKeys[i][2].ToString();
                dataRow["QaLocationId"] = itemsGridView.DataKeys[i][3].ToString();
                dataRow["RollNo"] = itemsGridView.DataKeys[i][4].ToString();

                dataRow["BeamNo"] = itemsGridView.Rows[i].Cells[1].Text;
                dataRow["GrlNo"] = itemsGridView.Rows[i].Cells[2].Text;
                dataRow["DefectPoints"] =  itemsGridView.Rows[i].Cells[4].Text;
                dataRow["ActualWidth"] = itemsGridView.Rows[i].Cells[5].Text;
                dataRow["Piece"] = itemsGridView.Rows[i].Cells[6].Text;
                dataRow["PWLength"] = itemsGridView.Rows[i].Cells[7].Text.Trim();
                dataRow["Quantity"] = itemsGridView.Rows[i].Cells[8].Text;
                dataRow["Pphsy"] = itemsGridView.Rows[i].Cells[9].Text;
                dataRow["MCNo"] = itemsGridView.Rows[i].Cells[10].Text;
                dataRow["GrossWeight"] = itemsGridView.Rows[i].Cells[11].Text;
                dataRow["NetWeight"] = itemsGridView.Rows[i].Cells[12].Text;
                dataRow["ProblemCause"] = itemsGridView.Rows[i].Cells[13].Text.Trim();
                dataRow["Remarks"] = itemsGridView.Rows[i].Cells[14].Text.Trim();
                dataRow["OPName"] = itemsGridView.Rows[i].Cells[15].Text;
                dataRow["Category"] = itemsGridView.DataKeys[i][5].ToString();

                aDataTable.Rows.Add(dataRow);
            }

            itemsGridView.DataSource = aDataTable;
            itemsGridView.DataBind();

            tbxRollNo.Text = "";
           // tbxBeamNo.Text = "";
           // tbxGrlNo.Text = "";
            tbxDefectPoint.Text = "";
            tbxActualWidth.Text = "";
            tbxPiece.Text = "";
            tbxPWLength.Text = "";
            tbxQuantity.Text = "";
            tbxPphsy.Text = "";
            tbxMcNo.Text = "";
            tbxGrossWeight.Text = "";
            tbxNetWeight.Text = "";
            tbxProblemCause.Text = "";
            tbxRemarks.Text = "";
        }

        CalculateTotalQuantity();

    }

    private void CalculateTotalQuantity()
    {

        decimal totalQuantity = 0;

        for (int i = 0; i < itemsGridView.Rows.Count; i++)
        {

            totalQuantity = totalQuantity + Convert.ToDecimal(itemsGridView.Rows[i].Cells[8].Text);
            
        }

        lblTotalQuantity.Text = String.Format("{0:0.00}", totalQuantity);
    }

    public bool HasRollNo(int rollNo)
    {
        for (int i = 0; i < itemsGridView.Rows.Count; i++)
        {
            if (Convert.ToInt32(itemsGridView.DataKeys[i][4].ToString()) == rollNo)
            {
                return false;
                break;

            }
        }
        return true;
    }

    private bool AddToListValidation()
    {

        if (ddlFabricInfo.SelectedValue == "")
        {
            ShowMessageBox("Please select Fabric/Sis Code !!");
            tbxBeamNo.Focus();
            return false;
        }

        if (txtSetNo.Text.Trim() == "")
        {
            ShowMessageBox("Please select Set No !!");
            txtSetNo.Focus();
            return false;
        }

        if (txtFLNo.Text.Trim() == "")
        {
            ShowMessageBox("Please select FL No !!");
            txtFLNo.Focus();
            return false;
        }

        if (ddlQaLocation.SelectedValue == "")
        {
            ShowMessageBox("Please select QA Location !!");
            txtFLNo.Focus();
            return false;
        }

        if (tbxBeamNo.Text.Trim() == "")
        {
            ShowMessageBox("Please select Beam No !!");
            tbxBeamNo.Focus();
            return false;
        }

        if (tbxGrlNo.Text.Trim() == "")
        {
            ShowMessageBox("Please select GRL No !!");
            tbxGrlNo.Focus();
            return false;
        }
        
        if (tbxRollNo.Text.Trim() == "")
        {
            ShowMessageBox("Please select Roll No !!");
            tbxRollNo.Focus();
            return false;
        }
        
        if (tbxDefectPoint.Text.Trim() == "")
        {
            ShowMessageBox("Please select Defect Point !!");
            tbxDefectPoint.Focus();
            return false;
        }
        
        if (tbxActualWidth.Text.Trim() == "")
        {
            ShowMessageBox("Please select Actual Width !!");
            tbxActualWidth.Focus();
            return false;
        }
        
        if (tbxQuantity.Text.Trim() == "")
        {
            ShowMessageBox("Please select Quantity !!");
            tbxQuantity.Focus();
            return false;
        }

        if (tbxPphsy.Text.Trim() == "")
        {
            ShowMessageBox("Please select PPHSY !!");
            tbxPphsy.Focus();
            return false;
        }

        if (tbxMcNo.Text.Trim() == "")
        {
            ShowMessageBox("Please select M.C No !!");
            tbxMcNo.Focus();
            return false;
        }


        if (tbxGrossWeight.Text.Trim() == "")
        {
            ShowMessageBox("Please select Gross Weight !!");
            tbxGrossWeight.Focus();
            return false;
        }

        if (tbxOperatorName.Text.Trim() == "")
        {
            ShowMessageBox("Please select Operator Name !!");
            tbxOperatorName.Focus();
            return false;
        }
        

        return true;
    }

    protected void deleteImageButton_Click(object sender, EventArgs e)
    {
        LinkButton productCodeTextBox = (LinkButton)sender;
        GridViewRow currentRow = (GridViewRow)productCodeTextBox.Parent.Parent;
        int rowindex = 0;
        rowindex = currentRow.RowIndex;

        DataTable aDataTable = new DataTable();

        aDataTable.Columns.Add("SisCode");
        aDataTable.Columns.Add("FabricSet");
        aDataTable.Columns.Add("FLNo");
        aDataTable.Columns.Add("QaLocationId");
        aDataTable.Columns.Add("RollNo");
        aDataTable.Columns.Add("BeamNo");
        aDataTable.Columns.Add("GrlNo");
        aDataTable.Columns.Add("DefectPoints");
        aDataTable.Columns.Add("ActualWidth");
        aDataTable.Columns.Add("Piece");
        aDataTable.Columns.Add("PWLength");
        aDataTable.Columns.Add("Quantity");
        aDataTable.Columns.Add("Pphsy");
        aDataTable.Columns.Add("MCNo");
        aDataTable.Columns.Add("GrossWeight");
        aDataTable.Columns.Add("NetWeight");
        aDataTable.Columns.Add("ProblemCause");
        aDataTable.Columns.Add("Remarks");
        aDataTable.Columns.Add("OPName");
        aDataTable.Columns.Add("Category");

        DataRow dataRow = null;

        for (int i = 0; i < itemsGridView.Rows.Count; i++)
        {
            if (i != rowindex)
            {
                dataRow = aDataTable.NewRow();

                dataRow["SisCode"] = itemsGridView.DataKeys[i][0].ToString();
                dataRow["FabricSet"] = itemsGridView.DataKeys[i][1].ToString();
                dataRow["FLNo"] = itemsGridView.DataKeys[i][2].ToString();
                dataRow["QaLocationId"] = itemsGridView.DataKeys[i][3].ToString();
                dataRow["RollNo"] = itemsGridView.DataKeys[i][4].ToString();

                dataRow["BeamNo"] = itemsGridView.Rows[i].Cells[1].Text;
                dataRow["GrlNo"] = itemsGridView.Rows[i].Cells[2].Text;
                dataRow["DefectPoints"] = itemsGridView.Rows[i].Cells[4].Text;
                dataRow["ActualWidth"] = itemsGridView.Rows[i].Cells[5].Text;
                dataRow["Piece"] = itemsGridView.Rows[i].Cells[6].Text;
                dataRow["PWLength"] = itemsGridView.Rows[i].Cells[7].Text;
                dataRow["Quantity"] = itemsGridView.Rows[i].Cells[8].Text;
                dataRow["Pphsy"] = itemsGridView.Rows[i].Cells[9].Text;
                dataRow["MCNo"] = itemsGridView.Rows[i].Cells[10].Text;
                dataRow["GrossWeight"] = itemsGridView.Rows[i].Cells[11].Text;
                dataRow["NetWeight"] = itemsGridView.Rows[i].Cells[12].Text;
                dataRow["ProblemCause"] = itemsGridView.Rows[i].Cells[13].Text;
                dataRow["Remarks"] = itemsGridView.Rows[i].Cells[14].Text;
                dataRow["OPName"] = itemsGridView.Rows[i].Cells[15].Text;
                dataRow["Category"] = itemsGridView.DataKeys[i][5].ToString();

                aDataTable.Rows.Add(dataRow);

            }

        }

        itemsGridView.DataSource = aDataTable;
        itemsGridView.DataBind();

        CalculateTotalQuantity();
    }

    protected void ddlVendor_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadBuyerVendorWisePI();
    }

    protected void ddlBuyer_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadBuyerVendorWisePI();
    }

    public void LoadBuyerVendorWisePI()
    {
        string pram = "";

        if (ddlBuyer.SelectedValue != "")
        {
            pram = pram + " AND PFI.BuyerId = " + ddlBuyer.SelectedValue;
        }
        
        if (ddlVendor.SelectedValue != "")
        {
            pram = pram + " AND PFI.VendorId = " + ddlVendor.SelectedValue;
        }

        aDal.LoadPiNo(ddlPINumber,pram);
    }

    public void CalculateNetWight()
    {
        decimal grossWeight = 0;
        decimal netWeight = 0;

        if (tbxGrossWeight.Text.Trim() != "")
        {
            grossWeight = Decimal.Parse(tbxGrossWeight.Text.Trim()) * 1000;
            netWeight = (grossWeight - 500) / 1000;
        }

        tbxNetWeight.Text = String.Format("{0:0.00}", netWeight); 
    }

    public void CalculatePphsy()
    {
        decimal pphy = 0;

        decimal defectPoint = string.IsNullOrEmpty(tbxDefectPoint.Text.Trim()) ? 1 : Decimal.Parse(tbxDefectPoint.Text.Trim());
        decimal totalWidth = string.IsNullOrEmpty(tbxActualWidth.Text.Trim()) ? 1 : Decimal.Parse(tbxActualWidth.Text.Trim());
        decimal totalQuantity = string.IsNullOrEmpty(tbxQuantity.Text.Trim()) ? 1 : Decimal.Parse(tbxQuantity.Text.Trim());

        pphy = (defectPoint * 36 * 100) / ((totalQuantity == 0 ? 1 : totalQuantity) * (totalWidth == 0 ? 1 : totalWidth));
        tbxPphsy.Text = String.Format("{0:0.00}", pphy);
    }

    protected void tbxGrossWeight_OnTextChanged(object sender, EventArgs e)
    {
        CalculateNetWight();
        tbxProblemCause.Focus();
    }

    protected void tbxDefectPoint_OnTextChanged(object sender, EventArgs e)
    {
        CalculatePphsy();
        tbxActualWidth.Focus();
    }

    protected void tbxActualWidth_OnTextChanged(object sender, EventArgs e)
    {
        CalculatePphsy();
        tbxPiece.Focus();
    }

    protected void tbxQuantity_OnTextChanged(object sender, EventArgs e)
    {

        if (tbxQuantity.Text.Trim() != "")
        {


            if (Convert.ToDecimal(tbxQuantity.Text) != 0)
            {
               
                CalculatePphsy();
                tbxMcNo.Focus();
            }
            else
            {
                ShowMessageBox("Can not be Zero");
            }

            
        }

       
    }

    protected void ddlFabric_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ddlColor.SelectedValue = "";
        tbxRequiredWidth.Text = "";
        tbxOunceYard.Text = "";
      //  tbxShrinkageLength.Text = "";
       // tbxShrinkageWidth.Text = "";
        txtRequiredShrinkageLenthMin.Text = "";
        txtRequiredShrinkageLenthMax.Text = "";
        txtRequiredShrinkageWidthMin.Text = "";
        txtRequiredShrinkageWidthMax.Text = "";


        if (ddlFabricInfo.SelectedValue != "")
        {
            //string fabric = ddlFabricInfo.SelectedItem.Text.Trim();
            //string[] fabrics = fabric.Split(':');

            DataTable aTable = aDal.GetFabricInfoById(Convert.ToInt32(ddlPINumber.SelectedValue),ddlFabricInfo.SelectedItem.Text.Trim());

            if (aTable.Rows.Count > 0)
            {
                ddlColor.SelectedValue = aTable.Rows[0].Field<Int32>("ColorId").ToString(CultureInfo.InvariantCulture);
                tbxRequiredWidth.Text = aTable.Rows[0].Field<Decimal>("RequiredWidth").ToString(CultureInfo.InvariantCulture);
                tbxOunceYard.Text = aTable.Rows[0].Field<Decimal>("RequiredOunceOryard").ToString(CultureInfo.InvariantCulture);
                txtRequiredShrinkageLenthMin.Text = aTable.Rows[0].Field<Decimal>("ShrinkageLengthMin").ToString(CultureInfo.InvariantCulture);
                txtRequiredShrinkageLenthMax.Text = aTable.Rows[0].Field<Decimal>("ShrinkageLengthMax").ToString(CultureInfo.InvariantCulture);
                txtRequiredShrinkageWidthMin.Text = aTable.Rows[0].Field<Decimal>("ShrinkageWidthMin").ToString(CultureInfo.InvariantCulture);
                txtRequiredShrinkageWidthMax.Text = aTable.Rows[0].Field<Decimal>("ShrinkageWidthMax").ToString(CultureInfo.InvariantCulture);
            }
        }
        else
        {
            ddlColor.SelectedValue = "";
            tbxRequiredWidth.Text = "";
            tbxOunceYard.Text = "";
         //   tbxShrinkageLength.Text = "";
         //   tbxShrinkageWidth.Text = "";
            txtRequiredShrinkageLenthMin.Text = "";
            txtRequiredShrinkageLenthMax.Text = "";
            txtRequiredShrinkageWidthMin.Text = "";
            txtRequiredShrinkageWidthMax.Text = "";
        }
    }




    private void LoadFabric(Int32 PINumber, string Fabric)
    {
        ddlColor.SelectedValue = "";
        tbxRequiredWidth.Text = "";
        tbxOunceYard.Text = "";
        //  tbxShrinkageLength.Text = "";
        // tbxShrinkageWidth.Text = "";
        txtRequiredShrinkageLenthMin.Text = "";
        txtRequiredShrinkageLenthMax.Text = "";
        txtRequiredShrinkageWidthMin.Text = "";
        txtRequiredShrinkageWidthMax.Text = "";


        if (ddlFabricInfo.SelectedValue != "")
        {
            //string fabric = ddlFabricInfo.SelectedItem.Text.Trim();
            //string[] fabrics = fabric.Split(':');

            DataTable aTable = aDal.GetFabricInfoById(Convert.ToInt32(ddlPINumber.SelectedValue), ddlFabricInfo.SelectedItem.Text.Trim());

            if (aTable.Rows.Count > 0)
            {
                ddlColor.SelectedValue = aTable.Rows[0].Field<Int32>("ColorId").ToString(CultureInfo.InvariantCulture);
                tbxRequiredWidth.Text = aTable.Rows[0].Field<Decimal>("RequiredWidth").ToString(CultureInfo.InvariantCulture);
                tbxOunceYard.Text = aTable.Rows[0].Field<Decimal>("RequiredOunceOryard").ToString(CultureInfo.InvariantCulture);
                txtRequiredShrinkageLenthMin.Text = aTable.Rows[0].Field<Decimal>("ShrinkageLengthMin").ToString(CultureInfo.InvariantCulture);
                txtRequiredShrinkageLenthMax.Text = aTable.Rows[0].Field<Decimal>("ShrinkageLengthMax").ToString(CultureInfo.InvariantCulture);
                txtRequiredShrinkageWidthMin.Text = aTable.Rows[0].Field<Decimal>("ShrinkageWidthMin").ToString(CultureInfo.InvariantCulture);
                txtRequiredShrinkageWidthMax.Text = aTable.Rows[0].Field<Decimal>("ShrinkageWidthMax").ToString(CultureInfo.InvariantCulture);
            }
        }
        else
        {
            ddlColor.SelectedValue = "";
            tbxRequiredWidth.Text = "";
            tbxOunceYard.Text = "";
            //   tbxShrinkageLength.Text = "";
            //   tbxShrinkageWidth.Text = "";
            txtRequiredShrinkageLenthMin.Text = "";
            txtRequiredShrinkageLenthMax.Text = "";
            txtRequiredShrinkageWidthMin.Text = "";
            txtRequiredShrinkageWidthMax.Text = "";
        }
    }

    protected void tbxRollNo_OnTextChanged(object sender, EventArgs e)
    {
        if (tbxRollNo.Text.Trim() !="")
        {
            DataTable Dt = aMovementDal.GetRollNoValidation(tbxRollNo.Text.Trim());

            if (Dt.Rows.Count > 0)
            {
                ShowMessageBox("Roll number already exists !!");
                tbxRollNo.Text = "";
                tbxRollNo.Focus();
            }
            else
            {
                if (HasRollNo(Convert.ToInt32(tbxRollNo.Text.Trim())))
                {
                   
                    tbxDefectPoint.Focus();
                }
                else
                {
                    ShowMessageBox("Roll number already exists !!");
                    tbxRollNo.Text = "";
                    tbxRollNo.Focus();
                }
            }

        }
    }

    protected void tbxPWLength_OnTextChanged(object sender, EventArgs e)
    {

        if (tbxPWLength.Text.Trim() != "")
        {
            string productName = tbxPWLength.Text.TrimEnd('+');

            if (productName.Contains('+'))
            {

                int count = 0;

                decimal totalQt = 0;
                string[] pice = productName.Split('+');

                for (int i = 0; i < pice.Length; i++)
                {
                    count++;
                    totalQt += Convert.ToDecimal(pice[i].ToString());

                }

                tbxPiece.Text = count.ToString();
                tbxQuantity.Text = totalQt.ToString();
                tbxQuantity_OnTextChanged(null, null);

            }
            else
            {
                tbxQuantity.Text = tbxPWLength.Text.Trim() != ""
                    ? Convert.ToDecimal(tbxPWLength.Text.Trim()).ToString()
                    : 0.ToString();
                tbxPiece.Text = 1.ToString();
            }

            tbxOperatorName.Focus();
        }
        else
        {
            tbxPiece.Text = 1.ToString();
            tbxQuantity.Text = "";
            tbxPphsy.Text = "";
        }

    }

    protected void ddlRollNo_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        EditByRoll.Visible = false;
        AddToList.Visible = false;
        Update.Visible = true;
        var aTable = new DataTable();
        aTable = aDal.GetQAStockReceiveByRoll(ddlRollNo.SelectedValue);
        if (aTable.Rows.Count > 0)
        {
            hfStockReceiveMasterId.Value = "";
            hfStockReceiveMasterId.Value = aTable.Rows[0].Field<int>("StockReceiveMasterId").ToString(CultureInfo.InvariantCulture);
            cbxCategory.SelectedValue = null;
            ddlPINumber.SelectedValue = aTable.Rows[0].Field<Int32>("ProformaInvId").ToString(CultureInfo.InvariantCulture);
            if (ddlPINumber.SelectedValue != "")
            {
                aDal.LoadSisCodeByPi(ddlFabricInfo, Convert.ToInt32(ddlPINumber.SelectedValue));
                using (DataTable data = aDal.LoadSisCodeByPiD(Convert.ToInt32(ddlPINumber.SelectedValue)))
                {
                    ddlFabricInfo.DataSource = data;
                    ddlFabricInfo.DataValueField = "FabricName";
                    ddlFabricInfo.DataTextField = "FabricDescription";
                    ddlFabricInfo.DataBind();
                    ddlFabricInfo.Items.Insert(0, new ListItem("Select from list", String.Empty));
                    ddlFabricInfo.SelectedValue = "";
                    ddlFabricInfo.SelectedValue = aTable.Rows[0]["FabricName"].ToString().Trim();
                }
                DataTable dt = aDal.LoadBuyerVendor(ddlPINumber.SelectedValue);
                if (dt.Rows.Count > 0)
                {
                    ddlBuyer.SelectedValue = dt.Rows[0]["BuyerId"].ToString();
                    ddlVendor.SelectedValue = dt.Rows[0]["VendorId"].ToString();
                }
                ddlFabricInfo.SelectedValue = aTable.Rows[0]["FabricName"].ToString().Trim();
                cbxCategory.SelectedValue = aTable.Rows[0].Field<string>("CategoryName").ToString(CultureInfo.InvariantCulture);
            }
            ddlShift.Text = aTable.Rows[0].Field<Int32>("ShiftId").ToString(CultureInfo.InvariantCulture);
            txtoductionDate.Text = aTable.Rows[0]["ProductionDate"].ToString();
            TxtEntryDate.Text = aTable.Rows[0]["EntryDate"].ToString();
            txtSetNo.Text = aTable.Rows[0]["FabricSet"].ToString();
            txtFLNo.Text = aTable.Rows[0]["FL"].ToString();
            ddlQaLocation.SelectedValue = aTable.Rows[0]["QALocationId"].ToString();
            tbxBeamNo.Text = aTable.Rows[0]["Beam"].ToString();
            tbxGrlNo.Text = aTable.Rows[0]["GreigeRollNo"].ToString();
            tbxRollNo.Text = aTable.Rows[0]["RollNo"].ToString();
            tbxDefectPoint.Text = aTable.Rows[0]["DefectPoint"].ToString();
            tbxActualWidth.Text = aTable.Rows[0]["ActualWidth"].ToString();
            tbxPiece.Text =  aTable.Rows[0]["Piece"].ToString();
            tbxPWLength.Text = aTable.Rows[0]["PWLength"].ToString();
            tbxOperatorName.Text = aTable.Rows[0]["OperatorName"].ToString();
            tbxQuantity.Text = aTable.Rows[0]["Quantity"].ToString();
            tbxPphsy.Text = aTable.Rows[0]["PPHSY"].ToString();
            tbxMcNo.Text = aTable.Rows[0]["MCNo"].ToString();
            tbxGrossWeight.Text = aTable.Rows[0]["GrossWeight"].ToString();
            tbxNetWeight.Text = aTable.Rows[0]["NetWeight"].ToString();
            tbxProblemCause.Text = aTable.Rows[0]["ProblemCause"].ToString();
            tbxRemarks.Text = aTable.Rows[0]["Remarks"].ToString();
            ddlColor.SelectedValue = aTable.Rows[0].Field<Int32>("ColorId").ToString(CultureInfo.InvariantCulture);
            tbxRequiredWidth.Text = aTable.Rows[0].Field<Decimal>("RequiredWidth").ToString(CultureInfo.InvariantCulture);
            tbxOunceYard.Text = aTable.Rows[0]["RequiredOunceYard"].ToString();
            txtRequiredShrinkageLenthMin.Text = aTable.Rows[0].Field<Decimal>("ShrinkageLengthMin").ToString(CultureInfo.InvariantCulture);
            txtRequiredShrinkageLenthMax.Text = aTable.Rows[0].Field<Decimal>("ShrinkageLengthMax").ToString(CultureInfo.InvariantCulture);
            txtRequiredShrinkageWidthMin.Text = aTable.Rows[0].Field<Decimal>("ShrinkageWidthMin").ToString(CultureInfo.InvariantCulture);
            txtRequiredShrinkageWidthMax.Text = aTable.Rows[0].Field<Decimal>("ShrinkageWidthMax").ToString(CultureInfo.InvariantCulture);
        }
    }

    protected void btnUpdate_OnClick(object sender, EventArgs e)
    {
        if (Validation())
        {
            bool status = UpdateChanges();

            if (status)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "alert",
                    "alert('Operation Successful...!');window.location ='QAStockReceive.aspx';",
                    true);
            }
        }
    }
}