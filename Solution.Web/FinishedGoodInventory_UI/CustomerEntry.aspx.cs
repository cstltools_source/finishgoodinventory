﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAO.FGInvrntory_DAO;

public partial class FinishedGoodInventory_UI_CustomerEntry : System.Web.UI.Page
{
    CustomerDal aDal = new CustomerDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDownList();


            RadioButtonList_OnSelectedIndexChanged(null, null);

            if (Session["BuyerId"] != null)
            {
                GetOneRecord(Convert.ToInt32(Session["BuyerId"].ToString()));
                Session["BuyerId"] = null;
            }
        }
    }

    

    # region Edit
    private void GetOneRecord(int buyerId)
    {
        var aTable = new DataTable();

        aTable = aDal.GetBuyerInfoById(buyerId);

        if (aTable.Rows.Count > 0)
        {
            hfBuyerId.Value = aTable.Rows[0].Field<int>("BuyerId").ToString(CultureInfo.InvariantCulture);
            txtBuyerName.Text = aTable.Rows[0].Field<String>("BuyerName");
            ddlBuyerType.SelectedValue = aTable.Rows[0].Field<Int32>("BuyerTypeId").ToString(CultureInfo.InvariantCulture);
            txtBuyerContactNo.Text = aTable.Rows[0].Field<String>("BuyerContactNo");
            txtBuyerEmail.Text = aTable.Rows[0].Field<String>("BuyerEmail");
            txtBuyerAddress.Text = aTable.Rows[0].Field<String>("BuyerAddress");

            if (aTable.Rows[0].Field<Boolean>("IsBoth"))
            {
                RadioButtonList.SelectedIndex = 2;
            }
            else if (aTable.Rows[0].Field<Boolean>("IsVendor"))
            {
                RadioButtonList.SelectedIndex = 1;
                RadioButtonList.Items[0].Enabled = false;
            }
            else
            {
                RadioButtonList.SelectedIndex = 0;
                RadioButtonList.Items[1].Enabled = false;
            }
            submitButton.Text = "Update Information";
        }

    }

    #endregion Edit
    private void LoadDropDownList()
    {
        aDal.LoadBuyerType(ddlBuyerType);
    }

    private void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private bool Validation()
    {
        if (txtBuyerName.Text == "")
        {
            ShowMessageBox("You should enter buyer name !!");
            txtBuyerName.Focus();
            txtBuyerName.BackColor = Color.GhostWhite;
            return false;
        }
        
        if (ddlBuyerType.SelectedValue == "")
        {
            ShowMessageBox("You should select buyer type!!");
            ddlBuyerType.Focus();
            ddlBuyerType.BackColor = Color.GhostWhite;
            return false;
        }

        if (txtBuyerContactNo.Text == "")
        {
            ShowMessageBox("You should enter buyer contact no !!");
            txtBuyerContactNo.Focus();
            txtBuyerContactNo.BackColor = Color.GhostWhite;
            return false;
        }
        
        if (txtBuyerAddress.Text == "")
        {
            ShowMessageBox("You should enter buyer address !!");
            txtBuyerAddress.Focus();
            txtBuyerAddress.BackColor = Color.GhostWhite;
            return false;
        }

        return true;
    }



    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {

            if (hfBuyerId.Value == "")
            {
                bool saveId = SaveChanges();

                if (saveId)
                {
                    ShowMessageBox("Buyer/Vendor info saved successfully !!");
                    Clear();
                }
                else
                {
                    ShowMessageBox("Data does not save successfully !!");
                }
            }
            else
            {
                bool saveId = UpdateChanges();

                if (saveId)
                {
                    ShowMessageBox("Buyer/Vendor info updated successfully !!");
                    Clear();
                }
                else
                {
                    ShowMessageBox("Data does not updated successfully !!");
                }
            }
            
        }
    }

    private bool UpdateChanges()
    {
        bool retVal;
        try
        {
            retVal = aDal.UpdateBuyerInfo(PrepareDataForUpdatee());

        }
        catch (Exception ex)
        {
            retVal = false;
            throw ex;
        }

        return retVal;
    }

    private BuyerInfo PrepareDataForUpdatee()
    {
        var aInfo = new BuyerInfo();

        aInfo.BuyerId = Convert.ToInt32(hfBuyerId.Value);
        aInfo.BuyerName = txtBuyerName.Text;
        aInfo.BuyerTypeId = Convert.ToInt32(ddlBuyerType.SelectedValue);
        aInfo.BuyerContactNo = txtBuyerContactNo.Text;
        aInfo.BuyerEmail = txtBuyerEmail.Text;
        aInfo.BuyerAddress = txtBuyerAddress.Text;

        string selectedValue = RadioButtonList.SelectedValue;

        if (selectedValue.Trim() == "Is vendor")
        {
            aInfo.IsVendor = true;
            aInfo.IsBuyer = false;
            aInfo.IsBoth = false;
        }
        else if (selectedValue.Trim() == "Is buyer")
        {
            aInfo.IsBuyer = true;
            aInfo.IsVendor = false;
            aInfo.IsBoth = false;
        }
        else
        {
            aInfo.IsBoth = true;
            aInfo.IsBuyer = false;
            aInfo.IsVendor = false;

        }

        return aInfo;
    }


    private bool SaveChanges()
    {
        bool retVal;
        try
        {
            retVal = aDal.SaveBuyerInfo(PrepareDataForSave());

        }
        catch (Exception ex)
        {
            retVal = false;
            throw ex;
        }

        return retVal;
    }



    private BuyerInfo PrepareDataForSave()
    {
        var aInfo = new BuyerInfo();

        aInfo.BuyerName = txtBuyerName.Text;
        aInfo.BuyerTypeId = Convert.ToInt32(ddlBuyerType.SelectedValue);
        aInfo.BuyerContactNo = txtBuyerContactNo.Text;
        aInfo.BuyerEmail = txtBuyerEmail.Text;
        aInfo.BuyerAddress = txtBuyerAddress.Text;

        string selectedValue = RadioButtonList.SelectedValue.Trim();

        if (selectedValue.Trim() == "Is vendor")
        {
            aInfo.IsVendor = true;
            aInfo.IsBuyer = false;
            aInfo.IsBoth = false;
        }
        else if (selectedValue.Trim() == "Is buyer")
        {
            aInfo.IsBuyer = true;
            aInfo.IsVendor = false;
            aInfo.IsBoth = false;
        }
        else
        {
            aInfo.IsBoth = true;
            aInfo.IsBuyer = false;
            aInfo.IsVendor = false;

        }

        return aInfo;
    }

    private void Clear()
    {
        txtBuyerName.Text = "";
        ddlBuyerType.SelectedValue = "";
        txtBuyerContactNo.Text = "";
        txtBuyerEmail.Text = "";
        txtBuyerAddress.Text = "";
        hfBuyerId.Value = "";
        submitButton.Text = "Save Information";
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        Clear();
    }


    protected void RadioButtonList_OnSelectedIndexChanged(object sender, EventArgs e)
    {

        if (RadioButtonList.SelectedValue == "Isbuyer")
        {
            BuyerType.Text = "Buyer Type: <span style='color: red !important;'>[*]</span> ";

            Name.Text = "Buyer Name: <span style='color: red !important;'>[*]</span>";
            ContactNo.Text = "Buyer Cont. No: <span style='color: red !important;'>[*]</span>";
            Email.Text = "Buyer Email: <span style='color: red !important;'>[*]</span>";
            Address.Text = "Buyer Address: <span style='color: red !important;'>[*]</span>";
        }

        if (RadioButtonList.SelectedValue == "Isvendor")
        {
            BuyerType.Text = "Vendor Type: <span style='color: red !important;'>[*]</span>";
            Name.Text = "Vendor Name: <span style='color: red !important;'>[*]</span>";

            ContactNo.Text = "Vendor Cont. No: <span style='color: red !important;'>[*]</span>";
            Email.Text = "Vendor Email: <span style='color: red !important;'>[*]</span>";
            Address.Text = "Vendor Address: <span style='color: red !important;'>[*]</span>";
        }

        if (RadioButtonList.SelectedValue == "Isboth")
        {
            BuyerType.Text = "Buyer/Vendor Type: <span style='color: red !important;'>[*]</span>";
            Name.Text = "Buyer/Vendor Name: <span style='color: red !important;'>[*]</span>";
            ContactNo.Text = "Buyer/Vendor Cont. No: <span style='color: red !important;'>[*]</span>";
            Email.Text = "Buyer/Vendor Email: <span style='color: red !important;'>[*]</span>";
            Address.Text = "Buyer/Vendor Address: <span style='color: red !important;'>[*]</span>";
        }

    }
}