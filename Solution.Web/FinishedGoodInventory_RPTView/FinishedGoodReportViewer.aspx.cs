﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using Library.BLL.SInventory_BLL;
using Library.DAL.FGInventory_DAL;

public partial class FinishedGoodInventory_RPTView_FinishedGoodReportViewer : System.Web.UI.Page
{

    ReportDocument rptdoc = new ReportDocument();

    private FinishedGoodReportDal aReportDal = new FinishedGoodReportDal();

    protected void Page_Init(object sender, EventArgs e)
    {
        string rptType = Request.QueryString["rptType"];
        string param = Request.QueryString["rpt"];
        DataSet mainDS = new DataSet();

        if (rptType.Trim() == "FGRR")
        {

            DataTable allDataTable = new DataTable();
            allDataTable = aReportDal.RPT_QAStockReceive(param).Copy();
            allDataTable.TableName = "QAStockReceiveInfo";
            mainDS.Tables.Add(allDataTable);

            if (mainDS.Tables[0].Rows.Count > 0)
            {
                // mainDS.WriteXmlSchema(MapPath("~\\FGI_Report\\DataSet\\QA_FinishedGoodReceiveInfo.xsd"));
               ShowReport(mainDS, "rptDailyFinishedGoodReceive.rpt");

               // ShowReport(mainDS, "rptQAStockReport.rpt");
            }

        }

        
        if (rptType.Trim() == "QASR")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aReportDal.RPT_QAStockReceive(param).Copy();
            allDataTable.TableName = "QAStockReceiveInfo";
            mainDS.Tables.Add(allDataTable);

            //DataTable master = new DataTable();
            //master = aReportDal.RPT_QAStockReceive(param).Copy();
            //master.TableName = "QAStockReceiveMasterInfo";
            //mainDS.Tables.Add(master);

            if (mainDS.Tables[0].Rows.Count > 0)
            {
               //mainDS.WriteXmlSchema(MapPath("~\\FGI_Report\\DataSet\\QA_FinishedGoodReceiveByQaNew.xsd"));
               ShowReport(mainDS, "crpFinishGdReceiveByQaNew.rpt");
            }

        }

    }
    private void ShowReport(DataSet dsDataSet, string reportName)
    {
        if (dsDataSet.Tables[0].Rows.Count > 0)
        {
            rptdoc.Load(ReportPath(reportName));
            rptdoc.SetDataSource(dsDataSet);
            crReportViewer.ReportSource = rptdoc;
            crReportViewer.DataBind();
        }
        else
        {
            lblMsg.Text = "No Data Found!!!!";
        }

    }
    private string ReportPath(string rptName)
    {
        return Convert.ToString(Server.MapPath("~\\FGI_Report\\CrystalReports\\" + rptName));

    }
    protected void rptViewerBasic_Unload(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crReportViewer.Dispose();
        }
    }

    protected void rptViewerBasic_Disposed(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crReportViewer.Dispose();
        }
    }
}