﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StockReportViewer.aspx.cs" Inherits="HTMLReport_StockReportViewer" %>


<!DOCTYPE html>

<html >
<head runat="server">
    <title>Stock Report</title>
    
    <style type="text/css">
        .alignment
        {
            width: 20%;
            margin: 0 auto;
            color: #008080;
            text-align: center;
        }
        
        table
        {
            text-align: center;
        }

        table td
        {
            padding: 1px 2px !important;
        }
    </style>
    
    
    
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" />
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
    <script>

           $(document).ready(function () {
               // Setup - add a text input to each footer cell


               // DataTable
               var table = $('#loadGridView').DataTable(
                    {
                        "bInfo": true,
                        "bFilter": true,
                        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        pageLength: 10,
                        dom: 'lBfrtip',

                        buttons: [
                            {
                            extend: 'excel',
                            footer: true,
                            text: '<i class="fa fa-file-excel-o" > Excel </i>',
                            titleAttr: 'Export to Excel'
                     ,
                            filename: ' Stock Report',
                            title: 'T',
                            messageTop: ' Stock Report',
                            exportOptions: {
                                columns: [0 , 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29]
                            }
                        }
                        ]
                    }
                   );

               var prm = Sys.WebForms.PageRequestManager.getInstance();
               if (prm != null) {
                   prm.add_endRequest(function (sender, e) {
                       if (sender._postBackSettings.panelsToUpdate != null) {
                           table = $('#loadGridView').DataTable(
                           {
                               "bInfo": true,
                               "bFilter": true,
                               lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                               pageLength: 10,
                               dom: 'lBfrtip',

                               buttons: [


                               {
                                   extend: 'excel',
                                   footer: true,
                                   text: '<i class="fa fa-file-excel-o" > Excel </i>',
                                   titleAttr: 'Export to Excel'
                            ,
                                   filename: ' Stock Report',
                                   title: 'T',
                                   messageTop: ' Stock Report',
                                   exportOptions: {
                                       columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29]

                                   }
                               }
                               ]

                           }
                           );
                       }
                   });
               };

               // Apply the search

           });

      </script>
    <style>
           .table   thead th {
               background-color: #5B799E;
               color: white;
           }
       </style>
    <style>
        .dt-button.buttons-print,
        .dt-button.buttons-excel.buttons-html5,
        .dt-button.buttons-pdf.buttons-html5 {
            
            background-color: #4CAF50;
  border: none;
  color: white;
  padding: 5px 18px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 2px 1px;
  cursor: pointer;
  -webkit-transition-duration: 0.4s; 
  transition-duration: 0.4s;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 3px 10px 0 rgba(0,0,0,0.19);
        }

 
.dt-buttons {
    align-content: center;
    text-align: center;
}
.dt-button.buttons-pdf.buttons-html5:hover {
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
}
</style>
    
    
    

</head>
<body>
    <h2 class="alignment">
        Stock Report
    </h2>
    <hr />
    <tr>
        <td class="TDLeft" width="13%" style="text-align: right; padding-right: 10px;">
            <span style="font-weight: bold">From: </span>
        </td>
        <td class="TDRight" width="20%">
             <asp:Label ID="FromTextBox" runat="server" CssClass="TextBox" ReadOnly="True"></asp:Label> &nbsp;
        </td>
        <td class="TDLeft" width="13%" style="text-align: right; padding-right: 10px;">
             <span style="font-weight: bold">To: </span>
        </td>
        <td class="TDRight" width="20%">
             <asp:Label ID="toTextBox" runat="server" CssClass="TextBox" ReadOnly="True"></asp:Label> &nbsp;
        </td>
        <td class="TDLeft" width="13%" style="text-align: right; padding-right: 10px;">
             <%--<span style="font-weight: bold">Company: </span>--%>
        </td>
        <td class="TDRight" width="20%">
           <asp:Label ID="dcTextBox" runat="server" CssClass="TextBox" ReadOnly="True"></asp:Label> &nbsp;
        </td>
    </tr>
    <form id="form1" runat="server">
    <div>
        <br />
        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../HTML_Report_css/images/excel.png"
            OnClick="ImageButton1_Click" Width="40px" />
        <br />
        <span style="color: #b8860b;">Import Excel</span>
        <br />
        <br />

        <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False" CssClass="responstable">
            <Columns>
               
                <asp:BoundField DataField="FebricDescription" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px" HeaderText="Fabric Name" />
                <asp:BoundField DataField="OpeningStock" HeaderText="Opening Stock" />
                <asp:BoundField DataField="RcvPI" HeaderText="RcvPI" />               
                <asp:BoundField DataField="RcvNoos" HeaderText="RcvNoos" />               
                <asp:BoundField DataField="RcvAtoA1" HeaderText="A to A1" />               
                <asp:BoundField DataField="RcvA1toA" HeaderText="A1 to A" />               
                <asp:BoundField DataField="RcvShtoA" HeaderText="Shaddy to A" />               
                <asp:BoundField DataField="RcvShtoA1" HeaderText="Shaddy to A1" />               
                <asp:BoundField DataField="RcvRnd" HeaderText="Rcv from RnD" />               
                <asp:BoundField DataField="RcvReFinish" HeaderText="Rcv from Re-Finish" />               
                <asp:BoundField DataField="RcvHeadEnds" HeaderText="Rcv from HE Cuttings" />               
                <asp:BoundField DataField="TotalStock" HeaderText="Total Rcv Stock" />
                <asp:BoundField DataField="PIWiseDel" HeaderText="General PI Delivery" />
                <asp:BoundField DataField="IssueAtoA1" HeaderText="Issue A to A1" />
                <asp:BoundField DataField="IssueA1toA" HeaderText="Issue A1 to A" />
                <asp:BoundField DataField="IssueA1toA" HeaderText="Issue A1 to A" />
                <asp:BoundField DataField="IssueShToA" HeaderText="Issue Shaddy to A" />
                <asp:BoundField DataField="IssueShToA1" HeaderText="Issue Shaddy to A1" />
                <asp:BoundField DataField="IssueCash" HeaderText="Local Cash" />
                <asp:BoundField DataField="AdvDel" HeaderText="Advance Delivery" />
                <asp:BoundField DataField="RpcDel" HeaderText="Replace Delivery" />
                <asp:BoundField DataField="CmpsDel" HeaderText="Compensation" />
                <asp:BoundField DataField="FocDel" HeaderText="F.O.C" />
                <asp:BoundField DataField="RndDel" HeaderText="Issue to RnD" />
                <asp:BoundField DataField="HecDel" HeaderText="Issue to HE" />
                <asp:BoundField DataField="RefDel" HeaderText="Issue to Re-Finish" />
                <asp:BoundField DataField="TotalDelivery" HeaderText="Total Delivery" />
                <asp:BoundField DataField="ClosingStock" HeaderText="Closing Stock" />
                <asp:BoundField DataField="LastReceiveDate" HeaderText="Last Receive Date" />
                <asp:BoundField DataField="LastDeliveryDate" HeaderText="Last Delivery Date" />

            </Columns>
        </asp:GridView>
    
    </div>
    </form>
</body>
</html>
