﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StockDeliveryReport.aspx.cs" Inherits="HTMLReport_StockDeliveryReport" %>

<!DOCTYPE html>

<html >
<head runat="server">
    <title>Stock Delivery Report</title>
    
    <style type="text/css">
        .alignment
        {
            width: 20%;
            margin: 0 auto;
            color: #008080;
            text-align: center;
        }
        
        table
        {
            text-align: center;
        }

        table td
        {
            padding: 1px 2px !important;
        }
    </style>
    
    
   
    
    
    

</head>
<body>
    <h2 class="alignment">
        Stock Delivery Report
    </h2>
    <hr />

    <tr>
       <%-- <td class="TDLeft" width="13%" style="text-align: right; padding-right: 10px;">
            <span style="font-weight: bold">From: </span>
        </td>
        <td class="TDRight" width="20%">
             <asp:Label ID="FromTextBox" runat="server" CssClass="TextBox" ReadOnly="True"></asp:Label> &nbsp;
        </td>
        <td class="TDLeft" width="13%" style="text-align: right; padding-right: 10px;">
             <span style="font-weight: bold">To: </span>
        </td>--%>

        <td class="TDRight" width="20%">
             <asp:Label ID="toTextBox" runat="server" CssClass="TextBox" ReadOnly="True"></asp:Label> &nbsp;
        </td>
        <td class="TDLeft" width="13%" style="text-align: right; padding-right: 10px;">
            
        </td>
        <td class="TDRight" width="20%">
           <asp:Label ID="dcTextBox" runat="server" CssClass="TextBox" ReadOnly="True"></asp:Label> &nbsp;
        </td>
    </tr>
    <form id="form1" runat="server">
    <div>

      <%--  <br />
        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../HTML_Report_css/images/excel.png"
            OnClick="ImageButton1_Click" Width="40px" />
        <br />
        <span style="color: #b8860b;">Import Excel</span>
        <br />
        <br />--%>

        <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False" CssClass="responstable">
            <Columns>
               
                <asp:BoundField DataField="FebricDescription" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px" HeaderText="Fabric Code" />
                <asp:BoundField DataField="ShiftTitle" HeaderText="Shift " />
                <asp:BoundField DataField="ProformaInvNo" HeaderText="PI No." />               
                <asp:BoundField DataField="FabricSet" HeaderText="SET No." />               
                <asp:BoundField DataField="RollNo" HeaderText="Roll No." />
                <asp:BoundField DataField="Quantity" HeaderText="Qty in (Yds)" />               
                <asp:BoundField DataField="DefectPoint" HeaderText="Defect Point" />               
                <asp:BoundField DataField="RequiredWidth" HeaderText="Req Width" />               
                <asp:BoundField DataField="ActualWidth" HeaderText="Width()" />               
                <asp:BoundField DataField="NetWeight" HeaderText="Weight(Kg)" />               
                <asp:BoundField DataField="Piece" HeaderText="Pice" />               
                <asp:BoundField DataField="PWLength" HeaderText="P.W Length" />
                <asp:BoundField DataField="LocationFor" HeaderText="Location Plate" />
                <asp:BoundField DataField="ProductionDate" HeaderText="Production Date" />
                <asp:BoundField DataField="EmpName" HeaderText="Received By" />
                <asp:BoundField DataField="DeliveryDate" HeaderText="Delivery Date & PI"/>
                <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
            </Columns>
        </asp:GridView>
    
    </div>
    </form>
</body>
</html>