﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;
using Library.DAL.InternalCls;

public partial class HTMLReport_BeenCardReportViewer : System.Web.UI.Page
{
   BinCardDal aDal = new BinCardDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        
        string fromDate = Request.QueryString["fromDate"];
        string toDate = Request.QueryString["toDate"];
        string pram = "";

        FromTextBox.Text = Convert.ToDateTime(fromDate).ToString("dd-MMM-yyyy");
        toTextBox.Text = Convert.ToDateTime(toDate).ToString("dd-MMM-yyyy");

        DataTable mainDataTable = aDal.GetBinCarDataTable(Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), pram).Copy();

        if (mainDataTable.Rows.Count > 0)
        {
            loadGridView.DataSource = mainDataTable;
            loadGridView.DataBind();
        }
        else
        {
            loadGridView.DataSource = null;
            loadGridView.DataBind();
        }
 
    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
       // throw new NotImplementedException();
    }
}