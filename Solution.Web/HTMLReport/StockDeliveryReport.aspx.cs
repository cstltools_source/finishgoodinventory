﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.FGInventory_DAL;

public partial class HTMLReport_StockDeliveryReport : System.Web.UI.Page
{
    private ReportDal aReportDal = new ReportDal();
    protected void Page_Load(object sender, EventArgs e)
    {

        string ID = Request.QueryString["fromDate"];

        DataTable mainDataTable = aReportDal.Get_StockDelivery(ID).Copy();

        if (mainDataTable.Rows.Count > 0)
        {
            loadGridView.DataSource = mainDataTable;
            loadGridView.DataBind();
        }
        else
        {
            loadGridView.DataSource = null;
            loadGridView.DataBind();
        }

    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        // throw new NotImplementedException();
    }
}