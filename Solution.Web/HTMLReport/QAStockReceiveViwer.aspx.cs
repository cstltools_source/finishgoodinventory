﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using Library.DAL.FGInventory_DAL;

public partial class HTMLReport_QAStockReceiveViwer : System.Web.UI.Page
{

    ReportDocument rptdoc = new ReportDocument();

    private FinishedGoodReportDal aReportDal = new FinishedGoodReportDal();

    protected void Page_Init(object sender, EventArgs e)
    {
        string rptType = Request.QueryString["rptType"];
        string param = Session["Param"].ToString();
        DataSet mainDS = new DataSet();

        if (rptType.Trim() == "QA")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aReportDal.RPT_QAStockReceive_Report(param).Copy();
            allDataTable.TableName = "QAStockReceive";
            mainDS.Tables.Add(allDataTable);
            if (mainDS.Tables[0].Rows.Count > 0)
            {
                //  mainDS.WriteXmlSchema(MapPath("~\\FGI_Report\\DataSet\\QA_QAStockReceive.xsd"));
                ShowReport(mainDS, "CrpQAStockReport.rpt");
                // ShowReport(mainDS, "rptQAStockReport.rpt");
            }
        }
    }
    private void ShowReport(DataSet dsDataSet, string reportName)
    {
        if (dsDataSet.Tables[0].Rows.Count > 0)
        {
            rptdoc.Load(ReportPath(reportName));
            rptdoc.SetDataSource(dsDataSet);
            crReportViewer.ReportSource = rptdoc;
            crReportViewer.DataBind();
        }
        else
        {
            lblMsg.Text = "No Data Found!!!!";
        }

    }
    private string ReportPath(string rptName)
    {
        return Convert.ToString(Server.MapPath("~\\FGI_Report\\CrystalReports\\" + rptName));

    }
    protected void rptViewerBasic_Unload(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crReportViewer.Dispose();
        }
    }

    protected void rptViewerBasic_Disposed(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crReportViewer.Dispose();
        }
    }

}