﻿<%@ page language="C#" autoeventwireup="true" inherits="HTMLReport_BeenCardReportViewer, App_Web_rblp5veh" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title> Been Card Report</title>
    
    <style type="text/css">
        .alignment
        {
            width: 20%;
            margin: 0 auto;
            color: #008080;
            text-align: center;
        }
        
        table
        {
            text-align: center;
        }

        table td
        {
            padding: 1px 2px !important;
        }
    </style>
</head>
<body>
    <h2 class="alignment">
        Stock Bin Card 
    </h2>
    <hr />
    <tr>
        <td class="TDLeft" width="13%" style="text-align: right; padding-right: 10px;">
            <span style="font-weight: bold">From: </span>
        </td>
        <td class="TDRight" width="20%">
             <asp:Label ID="FromTextBox" runat="server" CssClass="TextBox" ReadOnly="True"></asp:Label> &nbsp;
        </td>
        <td class="TDLeft" width="13%" style="text-align: right; padding-right: 10px;">
             <span style="font-weight: bold">To: </span>
        </td>
        <td class="TDRight" width="20%">
             <asp:Label ID="toTextBox" runat="server" CssClass="TextBox" ReadOnly="True"></asp:Label> &nbsp;
        </td>
        <td class="TDLeft" width="13%" style="text-align: right; padding-right: 10px;">
             <%--<span style="font-weight: bold">Company: </span>--%>
        </td>
        <td class="TDRight" width="20%">
           <asp:Label ID="dcTextBox" runat="server" CssClass="TextBox" ReadOnly="True"></asp:Label> &nbsp;
        </td>
    </tr>
    <form id="form1" runat="server">
    <div>
        <br />
        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../HTML_Report_css/images/excel.png"
            OnClick="ImageButton1_Click" Width="40px" />
        <br />
        <span style="color: #b8860b;">Import Excel</span>
        <br />
        <br />
        <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False" CssClass="responstable">
            <Columns>
               
                <asp:BoundField DataField="FabricName" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px" HeaderText="Fabric Name" />
                <%--<asp:BoundField DataField="CategoryName" HeaderText="Category" />--%>
                <asp:BoundField DataField="OpeningStock" HeaderText="Opening Stock" />
                <%--<asp:BoundField DataField="ReceivedStock" HeaderText="Received Stock" />--%>               
                <asp:BoundField DataField="RcvPI" HeaderText="RcvPI" />               
                <asp:BoundField DataField="RcvNoos" HeaderText="RcvNoos" />               
                <asp:BoundField DataField="RcvAtoA1" HeaderText="A to A1" />               
                <asp:BoundField DataField="RcvA1toA" HeaderText="A1 to A" />               
                <asp:BoundField DataField="RcvShtoA" HeaderText="Shaddy to A" />               
                <asp:BoundField DataField="RcvShtoA1" HeaderText="Shaddy to A1" />               
                <asp:BoundField DataField="RcvRnd" HeaderText="Rcv from RnD" />               
                <asp:BoundField DataField="RcvReFinish" HeaderText="Rcv from Re-Finish" />               
                <asp:BoundField DataField="RcvHeadEnds" HeaderText="Rcv from HE Cuttings" />               
                <asp:BoundField DataField="TotalStock" HeaderText="Total Rcv Stock" />
                <asp:BoundField DataField="PIWiseDel" HeaderText="General PI Delivery" />
                <asp:BoundField DataField="IssueAtoA1" HeaderText="Issue A to A1" />
                <asp:BoundField DataField="IssueA1toA" HeaderText="Issue A1 to A" />
                <asp:BoundField DataField="IssueA1toA" HeaderText="Issue A1 to A" />
                <asp:BoundField DataField="IssueShToA" HeaderText="Issue Shaddy to A" />
                <asp:BoundField DataField="IssueShToA1" HeaderText="Issue Shaddy to A1" />
                <asp:BoundField DataField="IssueCash" HeaderText="Local Cash" />
                <asp:BoundField DataField="AdvDel" HeaderText="Advance Delivery" />
                <asp:BoundField DataField="RpcDel" HeaderText="Replace Delivery" />
                <asp:BoundField DataField="CmpsDel" HeaderText="Compensation" />
                <asp:BoundField DataField="FocDel" HeaderText="F.O.C" />
                <asp:BoundField DataField="RndDel" HeaderText="Issue to RnD" />
                <asp:BoundField DataField="HecDel" HeaderText="Issue to HE" />
                <asp:BoundField DataField="RefDel" HeaderText="Issue to Re-Finish" />
                <asp:BoundField DataField="TotalDelivery" HeaderText="Total Delivery" />
                <asp:BoundField DataField="ClosingStock" HeaderText="Closing Stock" />
                <asp:BoundField DataField="LastReceiveDate" HeaderText="Last Receive Date" />
                <asp:BoundField DataField="LastDeliveryDate" HeaderText="Last Delivery Date" />
                
                 
            </Columns>
        </asp:GridView>
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CssClass="responstable">
            <Columns>
                <asp:BoundField DataField="ProductCode" HeaderText="Product Code" />
                <asp:BoundField DataField="ProductName" HeaderText="Product Description " />
                <%--<asp:BoundField DataField="DelivaryInvoiceNo" HeaderText="DelivaryInvoiceNo" />--%>
                <%--<asp:BoundField DataField="UpdateDate" HeaderText="UpdateDate" />--%>
                <asp:BoundField DataField="PackSize" HeaderText="Pack Size" />
                <asp:BoundField DataField="AQty" HeaderText="Available Qty" />
                <asp:BoundField DataField="BookFDel" HeaderText="Book For Delv" />
                <%--<asp:BoundField DataField="VatAmount" HeaderText="VatAmount" />--%>
                <asp:BoundField DataField="Tqty" HeaderText="Transit Qty" />
                <asp:BoundField DataField="RQty" HeaderText="Restricted Qty" />
                <%--<asp:BoundField DataField="ReturnReason" HeaderText="ReturnReason" />--%>
                <asp:BoundField DataField="BQty" HeaderText="Blocked Qty" />
            </Columns>
        </asp:GridView>
    </div>
    </form>
</body>
</html>
