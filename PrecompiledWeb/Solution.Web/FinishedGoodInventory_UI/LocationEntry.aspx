﻿<%@ page title="" language="C#" masterpagefile="~/MasterPages/NewMasterPage.master" autoeventwireup="true" inherits="FinishedGoodInventory_UI_LocationEntry, App_Web_ekfqsqgd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
    <style>
        
         #ContentPlaceHolder1_rbLocationFor_0,
         #ContentPlaceHolder1_rbLocationFor_1 {
            
             margin-right: 5px !important; 

         }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>


            <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
                <ProgressTemplate>
                    <div class="divWaiting">
                        <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                            Height="120" Width="120" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>


            <div id="popDiv">
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Stock Location Setup</div>

                        <div class="ms-auto">
                            <div class="btn-group">


                                <a href="../FinishedGoodInventory_UI/LocationView.aspx" class="btn btn-sm btn-sm btn-outline-info"><i class="fa fa-backward"></i>&nbsp;Back to List</a>

                            </div>
                        </div>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">

                            <div class="card border-top border-0 border-4 border-success">


                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-map me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Stock Location Information</h5>
                                        </div>
                                        <hr>

                                        <div class="row mb-3">
                                            <label for="" class="col-sm-2 col-form-label">Stock Location Type:<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>
                                            <div class="col-sm-10" style="padding-top: 8px">


                                                <asp:RadioButtonList ID="rbLocationFor" runat="server" CssClass="chkChoice" CellPadding="3" RepeatDirection="Horizontal" Font-Bold="True" Font-Size="Larger">
                                                </asp:RadioButtonList>


                                            </div>
                                        </div>

                                        <div class="row mb-2">
                                            <label for="" class="col-sm-2 col-form-label">Stock Location Name:<span style="color: red !important;">&nbsp;[*]&nbsp;</span></label>
                                            <div class="col-sm-10">

                                                <asp:TextBox runat="server" CssClass="form-control" ID="txtLocationName" placeholder="Enter Location Name"></asp:TextBox>
                                                <asp:HiddenField runat="server" ID="HFLocationId" />
                                            </div>
                                        </div>

                                        <br />
                                        <div class="row">
                                            <label class="col-sm-2 col-form-label"></label>
                                            <div class="col-sm-10">

                                                <asp:LinkButton runat="server" class="btn btn-outline-primary" ID="submitButton" OnClick="submitButton_Click">
                                            
                                            <i class="bx bxs-save mr-1"></i><span style="font-weight: bold !important">Save Information</span>

                                                </asp:LinkButton>

                                                <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="btnReset" OnClick="resetButton_Click">
                                            
                                            <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Information</span>

                                                </asp:LinkButton>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

