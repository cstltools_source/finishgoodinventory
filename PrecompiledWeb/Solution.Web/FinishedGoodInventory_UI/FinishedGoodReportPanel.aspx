﻿<%@ page title="" language="C#" masterpagefile="~/MasterPages/NewMasterPage.master" autoeventwireup="true" inherits="FinishedGoodInventory_UI_FinishedGoodReportPanel, App_Web_ekfqsqgd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.3.0/jspdf.umd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/3.5.6/jspdf.plugin.autotable.min.js"></script>

    <%--<script src="../Assets/js/html2canvas.min.js"></script>--%>

    <style>
        .table > tbody > tr > td {
            padding: 5px 0 5px 0 !important;
        }

        .table > thead > tr > th {
            padding: 5px 0 5px 0 !important;
        }

        .form-group {
            margin-bottom: -5px !important;
        }

            .form-group label {
                text-align: right;
            }

        .btn-container {
            min-width: 300px;
            margin: 0 auto;
        }

        .btn-container-right {
            width: 98%;
            text-align: right;
        }


        thead th {
            font-weight: 500;
            color: white !important;
            align-items: center !important;
            background-color: #07619D !important;
        }

        .table > thead > tr > th {
            padding: 7px 0 9px 0 !important;
        }

        .table > tbody > tr > td {
            padding: 5px 0 5px 0 !important;
        }

        .table > tbody > tr:nth-child(even) {
            background-color: #C7D3D4FF;
        }

        table {
            text-align: center !important;
            border: 1px solid #E2E5E8 !important;
        }

        .table thead th {
            background-color: #7a9ebd !important;
            font-size: 11px !important;
            color: #fff !important;
        }

        .table .table-xs th {
            padding: 0.3rem 2rem !important;
        }

        .table tbody tr {
            border: 1px solid #6b799c !important;
        }

        .table tbody td {
            font-size: 11px !important;
        }

        .table > tbody > tr:not(th):nth-child(even) {
            background-color: #d7e3ee !important;
        }

        .table .table-xs td {
            padding: 0.1rem 2.5rem !important;
        }

        .table thead th {
            background-color: #7a9ebd !important;
            font-size: 14px !important;
            color: #fff !important;
        }


        .head {
            background-color: #d7e3ee !important;
            font-weight: bold;
            font-size: 1.2em;
        }

        .center {
            position: absolute;
            display: block;
            margin-left: auto;
            margin-right: auto;
            top: 40%;
            left: 45%;
        }

        .btn-container-right {
            width: 98%;
            text-align: right;
        }

        .bold {
            font-weight: bold !important;
        }

        tr.Spacer {
            line-height: 20px;
        }


        #ContentPlaceHolder1_rblRecordStatus_0,
        #ContentPlaceHolder1_rblRecordStatus_1,
        #ContentPlaceHolder1_rblRecordStatus_2,
        #ContentPlaceHolder1_rblRecordStatus_3,
        #ContentPlaceHolder1_rblRecordStatus_4,
        #ContentPlaceHolder1_rblRecordStatus_5,
        #ContentPlaceHolder1_rblRecordStatus_6,
        #ContentPlaceHolder1_rblRecordStatus_7,
        #ContentPlaceHolder1_rblRecordStatus_8 
        {
            margin-right: 5px !important;
        }

    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>


            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Finished Good Report Panel</div>


                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">

                            <div class="card border-top border-0 border-4 border-success">

                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">

                                        <div class="row mb-5">

                                            <div class="col-md-3" style="padding-top: 3px">

                                                <asp:RadioButtonList runat="server" ID="rblRecordStatus" RepeatDirection="Vertical" Font-Bold="True" Font-Size="Small" CssClass="inline-rb">
                                                    <asp:ListItem Value="FGRR"> Finished Good Receive Report </asp:ListItem>
                                                    <%--<asp:ListItem Value="FGPKG">Finished Good Packing Report </asp:ListItem>--%>
                                                </asp:RadioButtonList>

                                            </div>

                                            <div class="col-md-4">
                                                <script type="text/javascript">
                                                    function pageLoad() {
                                                        $('.datepicker').pickadate({
                                                            selectMonths: true,
                                                            selectYears: true
                                                        });
                                                        $('.mySelect2').select2({
                                                            theme: 'bootstrap4',
                                                            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                                            placeholder: $(this).data('placeholder'),
                                                            allowClear: Boolean($(this).data('allow-clear')),
                                                        });
                                                    }
                                                </script>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Buyer Name: </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList runat="server" ID="ddlBuyer" CssClass="form-control form-control-sm mySelect2" />
                                                    </div>
                                                </div>

                                                <div style="padding-top: 5px"></div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Proforma No: </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList runat="server" ID="ddlProforma" CssClass="form-control form-control-sm mySelect2" />
                                                    </div>
                                                </div>

                                                <div style="padding-top: 5px"></div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Shift No: </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList runat="server" ID="ddlShift" CssClass="form-control form-control-sm mySelect2" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">From Date: </label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control form-control-sm  datepicker"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div style="padding-top: 5px"></div>
                                                <div style="padding-top: 5px"></div>
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">To Date: </label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control form-control-sm datepicker"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <hr />
                                        <div class="row ">

                                            <div class="btn-container" style="padding-left: 500px">

                                                <asp:LinkButton runat="server" class="btn btn-outline-primary" ID="submitButton" OnClick="searchButton_Click">
                                            
                        <i class="fa fa-search-plus"></i><span style="font-weight: bold !important">Search Information</span>

                                                </asp:LinkButton>


                                                <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="resetButton" OnClick="resetButton_Click">
                                            
                        <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Information</span>

                                                </asp:LinkButton>
                                            </div>



                                        </div>
                                        <div class="ml-3">
                                        </div>
                                        <hr />


                                        <hr />




                                        <div class="row" runat="server" id="data">
                                        </div>





                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
                <ProgressTemplate>
                    <div class="divWaiting">
                        <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                            Height="120" Width="120" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

        </ContentTemplate>
    </asp:UpdatePanel>



</asp:Content>

