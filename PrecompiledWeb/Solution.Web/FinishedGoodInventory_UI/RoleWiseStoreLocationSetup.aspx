﻿<%@ page title="" language="C#" masterpagefile="~/MasterPages/NewMasterPage.master" autoeventwireup="true" inherits="FinishedGoodInventory_UI_RoleWiseStoreLocationSetup, App_Web_ekfqsqgd" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=3.0.20820.28364, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="popDiv">
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Roll Wise Location Allocation (For Store)</div>

                        <%--<div class="ms-auto">
                    <div class="btn-group">


                        <a href="../FinishedGoodInventory_UI/PIView.aspx" class="btn btn-sm btn-sm btn-outline-info"><i class="fa fa-backward"></i>&nbsp;Back to List</a>


                    </div>
                </div>--%>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">

                            <div class="card border-top border-0 border-4 border-success">


                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-search me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Search Options</h5>
                                        </div>
                                        <hr>

                                        <script type="text/javascript">
                                            function pageLoad() {
                                                $('.datepicker').pickadate({
                                                    selectMonths: true,
                                                    selectYears: true
                                                });
                                                $('.mySelect2').select2({
                                                    theme: 'bootstrap4',
                                                    width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                                    placeholder: $(this).data('placeholder'),
                                                    allowClear: Boolean($(this).data('allow-clear')),
                                                });
                                            }
                                        </script>

                                        <div class="row mb-1">
                                            
                                            <div class="col-6">
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Fabric Code: </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList runat="server" ID="ddlFabric" AutoPostBack="True" OnSelectedIndexChanged="ddlFabric_OnSelectedIndexChanged" CssClass="form-control form-control-sm mySelect2" />
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Set No: </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList runat="server" ID="ddlSet" AutoPostBack="True" OnSelectedIndexChanged="ddlSet_OnSelectedIndexChanged" CssClass="form-control form-control-sm mySelect2" />
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-6">
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Roll No: </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList runat="server" ID="ddlRoll" AutoPostBack="True" OnSelectedIndexChanged="ddlRoll_OnSelectedIndexChanged" CssClass="form-control form-control-sm mySelect2" />
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Location: </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList runat="server" ID="ddlLocation" AutoPostBack="True" OnSelectedIndexChanged="ddlLocation_OnSelectedIndexChanged" CssClass="form-control form-control-sm mySelect2" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr />

                                        <div class="row mb-4">

                                            <div class="col-md-3"></div>
                                            <div class="col-md-6" style="">
                                                <asp:LinkButton runat="server" class="btn btn-outline-success" ID="LinkButton1" OnClick="searchButton_Click">                                            
                                                            <i class="fa fa-search-plus"></i><span style="font-weight: bold !important">Search Information</span>
                                                </asp:LinkButton>
                                                <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="LinkButton2" OnClick="resetButton_Click">                                            
                                                            <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Information</span>
                                                </asp:LinkButton>
                                            </div>
                                            <div class="col-md-3"></div>

                                        </div>
                                        <hr />
                                        
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="card-title d-flex align-items-center text-center"><div>
                                                    <i class="bx bxs-file me-1 font-22 text-secondary"></i>
                                                </div>
                                                <h5 class="mb-0 text-secondary ">Roll Description</h5>

                                            </div>
                                                
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                <label for="" class="col-sm-4 col-form-label col-form-label-sm">Location: </label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList runat="server" ID="ddlStockLocation"  CssClass="form-control form-control-sm mySelect2" />
                                                </div>
                                                
                                                <div class="col-sm-2" style="padding-bottom: 5px">

                                                    <asp:LinkButton  runat="server" class="btn btn-primary btn-sm" ID="BtnApply"  OnClick="BtnApply_OnClick">

                                                        Apply

                                                    </asp:LinkButton>
                                                  
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <hr />
                                        

                                        <div class="row mb-4">


                                            <div id="maingridview" style="text-align: center; height: auto; overflow: scroll; width: 100%; overflow-y: scroll; overflow-x: scroll;">
                                                <asp:GridView ID="productGridView" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-condensed custom-table-style"
                                                    AllowPaging="True" OnPageIndexChanging="loadGridView_PageIndexChanging" PageSize="10"
                                                    DataKeyNames="FGStockId" EmptyDataText="There are no data records to display.">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="# SL">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                    <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="FabricName" HeaderText="Fabric / SIS Code"/>
                                                    <asp:BoundField DataField="FabricSet" HeaderText="Set"/>
                                                    <asp:BoundField DataField="Beam" HeaderText="Beam"/>
                                                    <asp:BoundField DataField="GreigeRollNo" HeaderText="Greige Roll No" />
                                                    <asp:BoundField DataField="RollNo" HeaderText="Roll No"/>
                                                    <asp:BoundField DataField="DefectPoint" HeaderText="Defect Point"/>
                                                    <asp:BoundField DataField="ActualWidth" HeaderText="Actual Width"/>
                                                    <asp:BoundField DataField="Piece" HeaderText="Piece"/>
                                                    <asp:BoundField DataField="PWLength" HeaderText="P.W Length"/>
                                                    <asp:BoundField DataField="PPHSY" HeaderText="PPHSY"/>
                                                    <asp:BoundField DataField="StockQuantity" HeaderText="Quantity"/>
                                                    <asp:BoundField DataField="LocationName" HeaderText="Current Location"/>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True" OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" AutoPostBack="True" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <asp:TemplateField HeaderText="Store Location">
                                                            <ItemTemplate>
                                                                <asp:DropDownList class="form-select form-control-sm mySelect2"  runat="server" ID="ddlStoreLocation"></asp:DropDownList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        
                                        <div class="row mb-4">

                                            <div class="col-md-4"></div>
                                            <div class="col-md-4">
                                                <asp:LinkButton Style="margin: 0 auto !important" runat="server" class="btn btn-outline-primary" ID="submitButton" OnClientClick="return confirm('Are you really aware of this operation?');" OnClick="submitButton_Click">
                                            
                                                    <i class="bx bxs-save mr-1"></i><span style="font-weight: bold !important">Update Stock Location</span>

                                                </asp:LinkButton>
                                            </div>
                                            <div class="col-md-4"></div>

                                        </div>

                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>



    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                    Height="120" Width="120" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>

