﻿<%@ page title="" language="C#" masterpagefile="~/MasterPages/NewMasterPage.master" autoeventwireup="true" inherits="FinishedGoodInventory_UI_QAStockReceive, App_Web_ekfqsqgd" enableeventvalidation="false" enableviewstate="true" %>




<%@ Register TagPrefix="cc1" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=3.0.20820.28364, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
    
   

    <style type="text/css">
        /*AutoComplete flyout */
        .autocomplete_completionListElement {
            margin: 0px !important;
            background-color: White;
            color: windowtext;
            border: buttonshadow;
            border-width: 1px;
            border-style: solid;
            cursor: default;
            overflow: auto;
            font-family: Calibri;
            font-size: 12px;
            text-align: left;
            list-style-type: none;
            margin-left: 0px;
            padding-left: 0px;
            max-height: 350px;
            width: 40% !important;
        }


        .autocomplete_highlightedListItem {
            background-color: yellow;
            color: black;
            padding: 1px;
        }



        .autocomplete_listItem {
            background-color: white;
            color: blue;
            padding: 0px;
        }



        .custom-mt-top {
            vertical-align: middle !important;
        }

        #ContentPlaceHolder1_rbReceiveType_0,
        #ContentPlaceHolder1_rbReceiveType_1,
        #ContentPlaceHolder1_rbReceiveType_2,
        #ContentPlaceHolder1_rbReceiveType_3,
        #ContentPlaceHolder1_rbReceiveType_4,
        #ContentPlaceHolder1_rbReceiveType_5,
        #ContentPlaceHolder1_rbReceiveType_6 {
            margin-right: 5px !important;
            
        }

        #ContentPlaceHolder1_cbxCategory_0,
        #ContentPlaceHolder1_cbxCategory_1,
        #ContentPlaceHolder1_cbxCategory_2,
        #ContentPlaceHolder1_cbxCategory_3 {
            margin-right: 5px !important;
            margin-left: 5px !important;
        }

        fieldset {
            border: 1px solid grey !important;
            padding: 10px !important;
        }

        .output {
            font: 1rem 'Fira Sans', sans-serif;
        }

        .v1 {
            margin: 0 auto !important;
        }
    </style>
    
    




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    


<%--<script type="text/javascript">
    $(function () {
        $("[id*=ddlPINumber]").select2();
    });
</script>--%>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            
        


            <div id="popDiv">
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>QA Stock Receive</div>

                        <div class="ms-auto">
                            <div class="btn-group">


                                <a href="../FinishedGoodInventory_UI/QAStockReceiveView.aspx" class="btn btn-sm btn-sm btn-outline-info"><i class="fa fa-backward"></i>&nbsp;Back to List</a>


                            </div>
                        </div>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">

                            <div class="card border-top border-0 border-4 border-success">
                                

                          
                            


                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-file me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Master Information</h5>
                                        </div>
                                        <hr>

                                      

                                        <div class="row mb-2">



                                            <div class="col-4 text-lg-start">
                                                <div class="form-group row ">

                                                    <div class="col-12 ">
                                                        <asp:RadioButtonList ID="rbReceiveType" runat="server" CellPadding="2" AutoPostBack="True" OnSelectedIndexChanged="rbReceiveType_OnSelectedIndexChanged" RepeatDirection="Vertical" Font-Bold="True" Font-Size="Small">
                                                        </asp:RadioButtonList>

                                                    </div>
                                                </div>

                                            </div>
                                            
                                            
                                            

                                            <div class="col-4">
                                                
                                                <div class="form-group row mb-1">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Proforma No: </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList CssClass="form-select mySelect2" runat="server" ID="ddlPINumber" AutoPostBack="True" OnSelectedIndexChanged="ddlPINumber_OnSelectedIndexChanged"></asp:DropDownList>

                                                     
                                                        <script type="text/javascript">
                                                            function pageLoad() {
                                                                $('.datepicker').pickadate({
                                                                    selectMonths: true,
                                                                    selectYears: true
                                                                });
                                                                $('.mySelect2').select2({

                                                                    theme: 'bootstrap4',
                                                                    width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                                                    placeholder: $(this).data('placeholder'),
                                                                    allowClear: Boolean($(this).data('allow-clear'))
                                                                });
                                                            }
                                                        </script>
                                                        
                                                        
                                                        <script type="text/javascript">

                                                            $(document).ready(function () {
                                                                $('.datepicker').pickadate({
                                                                    selectMonths: true,
                                                                    selectYears: true
                                                                });
                                                                $('.mySelect2').select2({

                                                                    theme: 'bootstrap4',
                                                                    width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                                                    placeholder: $(this).data('placeholder'),
                                                                    allowClear: Boolean($(this).data('allow-clear'))
                                                                });
                                                            });
                                                            
                                                        </script>
                                                        
                                                     <%--   <script type="text/javascript">
                                                            $(function () {
                                                                $("[id*=ddlPINumber]").select2();
                                                                $("[id*=ddlBuyer]").select2();
                                                                $("[id*=ddlVendor]").select2();
                                                                $("[id*=ddlColor]").select2();
                                                            });
                                                        </script>--%>
      
                                                       
                                                        <asp:HiddenField runat="server" ID="hfStockReceiveMasterId" />
                                                        
                                                    
                                                    </div>
                                                </div>
                                                


                                                <div class="form-group row mb-1">
                                                    <label class="col-sm-4 col-form-label">Buyer Name: </label>
                                                    <div class="col-sm-8">
                                                        <asp:HiddenField runat="server" ID="hfProformaId" />
                                                        <asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlBuyer" AutoPostBack="True" OnSelectedIndexChanged="ddlBuyer_OnSelectedIndexChanged"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group row mb-1">
                                                    <label class="col-sm-4 col-form-label">Vendor Name: </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlVendor" AutoPostBack="True" OnSelectedIndexChanged="ddlVendor_OnSelectedIndexChanged"></asp:DropDownList>
                                                    </div>
                                                </div>

                                               

                                                <div class="form-group row mb-1">
                                                    <label class="col-sm-4 col-form-label">Fabric Name: </label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList class="form-select " AutoPostBack="True" OnSelectedIndexChanged="ddlFabric_OnSelectedIndexChanged" runat="server" ID="ddlFabricInfo"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-4">

                                                <div class="form-group row mb-1">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Color: </label>
                                                    <div class="col-sm-8" style="">
                                                        <asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlColor"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Required Width: </label>
                                                    <div class="col-sm-8" style="">
                                                        <asp:TextBox ID="tbxRequiredWidth" runat="server" placeholder="Numeric" CssClass="form-control form-control-sm "></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="tbxRequiredWidth" FilterType="Custom, Numbers" ValidChars="." />
                                                    </div>
                                                </div>

                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Ounce/Yard<sup>2</sup>: </label>
                                                    <div class="col-sm-8" style="">
                                                        <asp:TextBox ID="tbxOunceYard" runat="server" placeholder="Numeric" CssClass="form-control form-control-sm "></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="tbxOunceYard" FilterType="Custom, Numbers" ValidChars="." />
                                                    </div>
                                                </div>
                                                
                                                
                                                
                                                <div class="form-group row">


                                                    <label class="col-sm-4 col-form-label">Shrinkage(L):</label>

                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="txtRequiredShrinkageLenthMin" runat="server" placeholder="min" CssClass="form-control form-control-sm "></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="freqQtyTextBox1" runat="server" TargetControlID="txtRequiredShrinkageLenthMin" FilterType="Custom, Numbers" ValidChars="." />
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="txtRequiredShrinkageLenthMax" runat="server" placeholder="max" CssClass="form-control form-control-sm "></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" TargetControlID="txtRequiredShrinkageLenthMax" FilterType="Custom, Numbers" ValidChars="." />
                                                    </div>


                                                </div>
                                                
                                                <div class="form-group row">


                                                    <label class="col-sm-4 col-form-label">Shrinkage(W):</label>

                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="txtRequiredShrinkageWidthMin" runat="server" placeholder="min" CssClass="form-control form-control-sm "></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" TargetControlID="txtRequiredShrinkageWidthMin" FilterType="Custom, Numbers" ValidChars="." />
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="txtRequiredShrinkageWidthMax" runat="server" placeholder="max" CssClass="form-control form-control-sm "></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" TargetControlID="txtRequiredShrinkageWidthMax" FilterType="Custom, Numbers" ValidChars="." />
                                                    </div>


                                                </div>

                                               <!-- <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Shrinkage(%): </label>
                                                    <div class="col-sm-8" style="">
                                                        
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <asp:TextBox ID="tbxShrinkageLength" runat="server" placeholder="Numeric" CssClass="form-control form-control-sm "></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="tbxShrinkageLength" FilterType="Custom, Numbers" ValidChars="." />
                                                            </div>
                                                            <div class="col-6">
                                                                <asp:TextBox ID="tbxShrinkageWidth" runat="server" placeholder="Numeric" CssClass="form-control form-control-sm "></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" TargetControlID="tbxShrinkageWidth" FilterType="Custom, Numbers" ValidChars="." />
                                                            </div>
                                                        </div>

                                                        
                                                    </div>
                                                </div>-->
                                            </div>

                                        </div>
                                        <br />

                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-file me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Roll Wise Information</h5>
                                        </div>
                                        <hr>

                                        <div class="row mb-2">

                                            <div class="col-4">
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-5 col-form-label col-form-label-sm">Production Date: </label>
                                                    <div class="col-sm-7">
                                                        <asp:TextBox ID="txtoductionDate" runat="server" CssClass="form-control form-control-sm datepicker"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-5 col-form-label col-form-label-sm">Production Shift: </label>
                                                    <div class="col-sm-7">
                                                        <asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlShift"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-5 col-form-label col-form-label-sm">SET No: </label>
                                                    <div class="col-sm-7">
                                                        <asp:TextBox ID="txtSetNo" runat="server" CssClass="form-control form-control-sm "></asp:TextBox>
                                                        
                                                        
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" TargetControlID="txtSetNo" FilterType="Custom, Numbers" ValidChars="-" />
                                                        
                                                        

                                      
                                                        
                                                        

                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-5 col-form-label col-form-label-sm">FL No: </label>
                                                    <div class="col-sm-7">
                                                        <asp:TextBox ID="txtFLNo" runat="server" placeholder="Numeric" CssClass="form-control form-control-sm "></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" TargetControlID="txtFLNo" FilterType="Custom, Numbers"  />
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-5 col-form-label col-form-label-sm">QA Location: </label>
                                                    <div class="col-sm-7">
                                                        <asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlQaLocation"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-5 col-form-label col-form-label-sm">Entry By:<span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>
                                                    <div class="col-sm-7">
                                                        <asp:TextBox class="form-control" ReadOnly="True" CssClass="form-control form-control-sm" runat="server" ID="TxtEntryBy"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-5 col-form-label col-form-label-sm">Entry Date:<span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>
                                                    <div class="col-sm-7">
                                                        <asp:TextBox ID="TxtEntryDate" runat="server" class="form-control form-control-sm datepicker"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-5 col-form-label col-form-label-sm">Reason/Remarks: </label>
                                                    <div class="col-sm-7">
                                                        <asp:TextBox ID="ReceiveReason" runat="server" TextMode="MultiLine" Rows="3" Columns="4" CssClass="form-control form-control-sm "></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-8">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-5 col-form-label col-form-label-sm">BEAM NO: </label>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="tbxBeamNo" runat="server" placeholder="Numeric" CssClass="form-control form-control-sm "></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="tbxBeamNo" FilterType="Custom, Numbers"  />
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-5 col-form-label col-form-label-sm">GRL NO: </label>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="tbxGrlNo" runat="server" CssClass="form-control form-control-sm "></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-5 col-form-label col-form-label-sm">ROLL NO: </label>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="tbxRollNo" placeholder="Numeric" AutoPostBack="True" OnTextChanged="tbxRollNo_OnTextChanged" runat="server" CssClass="form-control form-control-sm "></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="tbxRollNo" FilterType="Custom, Numbers" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-5 col-form-label col-form-label-sm">DEFECT POINTS: </label>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="tbxDefectPoint" placeholder="Numeric" AutoPostBack="True" OnTextChanged="tbxDefectPoint_OnTextChanged" runat="server" CssClass="form-control form-control-sm "></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="tbxDefectPoint" FilterType="Custom, Numbers" ValidChars="." />
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-5 col-form-label col-form-label-sm">ACTUAL WIDTH: </label>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="tbxActualWidth" placeholder="Numeric" runat="server" AutoPostBack="True" OnTextChanged="tbxActualWidth_OnTextChanged" CssClass="form-control form-control-sm "></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="tbxActualWidth" FilterType="Custom, Numbers" ValidChars="." />
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-5 col-form-label col-form-label-sm">PIECE: </label>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="tbxPiece" runat="server" placeholder="Numeric" CssClass="form-control form-control-sm "></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="tbxPiece" FilterType="Custom, Numbers" ValidChars="." />
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-5 col-form-label col-form-label-sm">P.W. LENGTH: </label>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="tbxPWLength" runat="server" AutoPostBack="True" OnTextChanged="tbxPWLength_OnTextChanged" CssClass="form-control form-control-sm "></asp:TextBox>

                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-5 col-form-label col-form-label-sm">OP Name: </label>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="tbxOperatorName" runat="server" CssClass="form-control form-control-sm "></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-6">

                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-5 col-form-label col-form-label-sm">QUANTITY (YDS): </label>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="tbxQuantity" runat="server" AutoPostBack="True" placeholder="Numeric" OnTextChanged="tbxQuantity_OnTextChanged" CssClass="form-control form-control-sm "></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="tbxQuantity" FilterType="Custom, Numbers" ValidChars="." />
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-5 col-form-label col-form-label-sm">PPHSY: </label>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="tbxPphsy" ReadOnly="True" runat="server" CssClass="form-control form-control-sm "></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-5 col-form-label col-form-label-sm">Category: </label>
                                                            <div class="col-sm-7">     
                                                                <asp:RadioButtonList id="cbxCategory" RepeatColumns="2" RepeatDirection="Horizontal" runat="server">
                                                                    <asp:ListItem>Elite-A</asp:ListItem>
                                                                    <asp:ListItem>Elite-A1</asp:ListItem>
                                                                    <asp:ListItem>Shady</asp:ListItem>
                                                                    <asp:ListItem>Insta</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-5 col-form-label col-form-label-sm">M.C. NO: </label>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="tbxMcNo" runat="server" placeholder="Numeric" CssClass="form-control form-control-sm "></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server"  TargetControlID="tbxMcNo" FilterType="Custom, Numbers"  />
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-5 col-form-label col-form-label-sm">GROSS WIGHT (KG): </label>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="tbxGrossWeight" runat="server" AutoPostBack="True" placeholder="Numeric" OnTextChanged="tbxGrossWeight_OnTextChanged" CssClass="form-control form-control-sm "></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="tbxGrossWeight" FilterType="Custom, Numbers" ValidChars="." />
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-5 col-form-label col-form-label-sm">NET WIGHT (KG): </label>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="tbxNetWeight" ReadOnly="True" runat="server" CssClass="form-control form-control-sm "></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-5 col-form-label col-form-label-sm">PROBLEM CAUSE: </label>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="tbxProblemCause" runat="server" placeholder="Numeric" CssClass="form-control form-control-sm "></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="tbxProblemCause" FilterType="Custom, Numbers" ValidChars="." />
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-5 col-form-label col-form-label-sm">ROLL REMARKS: </label>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="tbxRemarks" TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control form-control-sm "></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr />
                                                <div class="row">
                                                    <div class="col-4"></div>
                                                    <div class="col-8">
                                                        <div class="row">
                                                            <label class="col-sm-3 col-form-label">&nbsp;</label>
                                                            <div class="col-sm-9">
                                                                <asp:LinkButton runat="server" class="btn btn-outline-success" ID="addToListButton" OnClick="addToListButton_Click">
                                                               <i class="bx bxs-plus-circle mr-1"></i><span style="font-weight: bold !important">Add to list</span>
                                                                </asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                    <hr />
                                    <div class="row">
                                        <div class="col-7">
                                            <div class="card-title d-flex align-items-center text-center">
                                                <div>
                                                    <i class="bx bxs-file me-1 font-22 text-secondary"></i>
                                                </div>
                                                <h5 class="mb-0 text-secondary ">Roll Description</h5>

                                            </div>

                                        </div>
                                        <div class="col-5">
                                            <div class="form-group row">
                                                <label for="" class="col-sm-10 col-form-label col-form-label-sm text-right">Total Quantity:</label>
                                                <div class="col-sm-2 pt-2">
                                                    <asp:Label CssClass="custom-mt-top pt-2 bold text-success" ID="lblTotalQuantity" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <hr />
                                        <div class="row">
                                            <div class="col-12">
                                                <div id="MainGradeDiv" style="text-align: center !important">
                                                    <asp:GridView ID="itemsGridView" runat="server" AutoGenerateColumns="False"
                                                        PageIndex="0" CssClass="table table-bordered text-center table-condensed custom-table-style" DataKeyNames="SisCode,FabricSet,FLNo,QaLocationId,RollNo,Category" EmptyDataText="There are no data records to display.">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="SL">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="BeamNo" HeaderText="Beam No" />
                                                            <asp:BoundField DataField="GrlNo" HeaderText="GRL No" />
                                                            <asp:BoundField DataField="RollNo" HeaderText="ROLL NO" />
                                                            <asp:BoundField DataField="DefectPoints" HeaderText="DEFECT POINT" />
                                                            <asp:BoundField DataField="ActualWidth" HeaderText="ACTUAL WIDTH" />
                                                            <asp:BoundField DataField="Piece" HeaderText="PIECE" />
                                                            <asp:BoundField DataField="PWLength" HeaderText="P.W. LENGTH" />
                                                            <asp:BoundField DataField="Quantity" HeaderText="QUANTITY" />
                                                            <asp:BoundField DataField="Pphsy" HeaderText="PPHSY" />
                                                            <asp:BoundField DataField="MCNo" HeaderText="M.C. NO" />
                                                            <asp:BoundField DataField="GrossWeight" HeaderText="GROSS WEIGHT(KG)" />
                                                            <asp:BoundField DataField="NetWeight" HeaderText="NET WEIGHT(KG)" />
                                                            <asp:BoundField DataField="ProblemCause" HeaderText="PROBLEM CAUSE" />
                                                            <asp:BoundField DataField="Remarks" HeaderText="REMARKS" />
                                                            <asp:BoundField DataField="OPName" HeaderText="OP Name" />
                                                            <asp:TemplateField HeaderText="Remove">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="itemdeleteImageButton" runat="server" class="btn btn-danger btn-sm btnTextShadow" CommandName="DeleteData" OnClick="deleteImageButton_Click"><i class="fa fa-trash" aria-hidden="true"></i>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>

                                        <hr />
                                        <div class="row mb-4">

                                            <div class="col-3"></div>
                                            <div class="col-6 pl-2">
                                                <asp:LinkButton ID="submitButton" runat="server" OnClick="submitButton_Click" Style="width: 200px !important; margin-right: 4px !important; margin: 0 auto !important;" class="btn btn-outline-primary">
                                            <i class="bx bxs-save mr-1"></i><span style="font-weight: bold !important">Save Information</span>
                                                </asp:LinkButton>


                                                <asp:LinkButton ID="btnReset" runat="server" OnClick="resetButton_Click" Style="width: 200px !important; margin-right: 4px !important; margin: 0 auto !important;" class="btn btn-outline-warning">
                                            <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Information</span>
                                                </asp:LinkButton>
                                            </div>
                                            <div class="col-3"></div>

                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
            <ProgressTemplate>
                <div class="divWaiting">
                    <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                               Height="120" Width="120" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        </ContentTemplate>
    </asp:UpdatePanel>






</asp:Content>

