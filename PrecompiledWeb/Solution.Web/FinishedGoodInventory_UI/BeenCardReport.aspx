﻿<%@ page title="" language="C#" masterpagefile="~/MasterPages/NewMasterPage.master" autoeventwireup="true" inherits="FinishedGoodInventory_UI_BeenCardReport, App_Web_ekfqsqgd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            
            
            <div id="popDiv">
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Delivery to Customer  </div>
                        <%--<div class="ms-auto">
                    <div class="btn-group">
                        <a href="../FinishedGoodInventory_UI/PIView.aspx" class="btn btn-sm btn-sm btn-outline-info"><i class="fa fa-backward"></i>&nbsp;Back to List</a>
                    </div>
                </div>--%>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">


                            <div class="card border-top border-0 border-4 border-success">
                                <script type="text/javascript">
                                    function pageLoad() {
                                        $('.datepicker').pickadate({
                                            selectMonths: true,
                                            selectYears: true
                                        });
                                        $('.mySelect2').select2({
                                            theme: 'bootstrap4',
                                            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                            placeholder: $(this).data('placeholder'),
                                            allowClear: Boolean($(this).data('allow-clear')),
                                        });
                                    }
                            </script>

                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-search me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Search Options</h5>
                                        </div>
                                        <hr>
                                        <div class="row mb-2">
                                            <label for="" class="col-sm-2 col-form-label">From Date</label>
                                            <div class="col-sm-10">
                                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control form-control-sm  datepicker"></asp:TextBox>
                                            </div>
                                        </div>


                                        <div class="row mb-2">
                                            <label for="" class="col-sm-2 col-form-label">To Date</label>
                                            <div class="col-sm-10">
                                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control form-control-sm datepicker"></asp:TextBox>
                                            </div>
                                        </div>

                                        <hr />
                                        <div class="row mb-4">

                                            <div class="col-4"></div>
                                            <div class="col-4" style="">
                                                <asp:Button ID="Button1" CssClass="btn btn-outline-success align-center" runat="server" Text="Search Information" OnClick="Button1_Click" />
                                                <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="resetButton" OnClick="resetButton_Click">
                                            
                                                    <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Information</span>

                                                </asp:LinkButton>
                                            </div>
                                            <div class="col-4"></div>

                                        </div>
                                        <hr />


                                        <div class="row ">

                                            <div id="maingridview" style="text-align: center; height: auto; overflow: scroll; width: 100%; overflow-y: scroll; overflow-x: scroll;">
                                                <asp:GridView ID="productGridView" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-condensed"
                                                    DataKeyNames="IssueMasterId" EmptyDataText="There are no data records to display.">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="# SL">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:BoundField DataField="PINo" HeaderText="Proforma No " />
                                                        <asp:BoundField DataField="PackegingIssueCode" HeaderText="Packeging Issue Code" />
                                                        <asp:BoundField DataField="IssueBy" HeaderText="IssueBy" />
                                                        <asp:BoundField DataField="IssueDate" HeaderText="Issue Date" DataFormatString="{0:dd-MMM-yyyy}" />

                                                        

                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </ContentTemplate>
    </asp:UpdatePanel>



    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                    Height="120" Width="120" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


</asp:Content>

