﻿<%@ page title="" language="C#" masterpagefile="~/MasterPages/NewMasterPage.master" autoeventwireup="true" inherits="FinishedGoodInventory_UI_ShiftView, App_Web_ekfqsqgd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js" integrity="sha512-rMGGF4wg1R73ehtnxXBt5mbUfN9JUJwbk21KMlnLZDJh7BkPmeovBuddZCENJddHYYMkCh9hPFnPmS9sspki8g==" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css" integrity="sha512-yVvxUQV0QESBt1SyZbNJMAwyKvFTLMyXSyBHDO4BG5t7k/Lw34tyqlSDlKIrIENIzCl+RVUNjmCPG+V/GMesRw==" crossorigin="anonymous" />


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" integrity="sha512-nNlU0WK2QfKsuEmdcTwkeh+lhGs6uyOxuUs+n+0oXSYDok5qy0EI0lt01ZynHq6+p/tbgpZ7P+yUb+r71wqdXg==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js" integrity="sha512-j7/1CJweOskkQiS5RD9W8zhEG9D9vpgByNGxPIqkO5KrXrwyDAroM9aQ9w8J7oRqwxGyz429hPVk/zR6IOMtSA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

   


    <style>
        .shrinkToFit {
            width: 100% !important;
            height: 100% !important;
        }

        .star {
            color: red;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>


            <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
                <ProgressTemplate>
                    <div class="divWaiting">
                        <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                            Height="120" Width="120" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>


            <div id="popDiv">
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Shift Setup</div>

                        <div class="ms-auto">
                            <div class="btn-group">
                                <a href="../FinishedGoodInventory_UI/ShiftEntry.aspx" class="btn btn-sm btn-outline-info "><i class="fa fa-plus" aria-hidden="true"></i>New Entry</a>
                            </div>
                        </div>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">

                            <div class="card border-top border-0 border-4 border-success">


                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">


                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-search me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Search Options</h5>
                                        </div>
                                        <script type="text/javascript">
                                            function pageLoad() {
                                                $('.datepicker').pickadate({
                                                    selectMonths: true,
                                                    selectYears: true
                                                });
                                                $('.mySelect2').select2({
                                                    theme: 'bootstrap4',
                                                    width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                                    placeholder: $(this).data('placeholder'),
                                                    allowClear: Boolean($(this).data('allow-clear')),
                                                });
                                            }
                                        </script>
                                        <hr>
                                        <div class="row mb-2">
                                            <label for="" class="col-sm-2 col-form-label">Shift Name</label>
                                            <div class="col-sm-5" style="padding-top: 5px">

                                                <asp:DropDownList class="form-select mySelect2" runat="server" ID="ddlShift"></asp:DropDownList>

                                            </div>
                                        </div>

                                        <div class="row mb-4">
                                            <label class="col-sm-2 col-form-label"></label>
                                            <div class="col-sm-8">

                                                <asp:LinkButton runat="server" class="btn btn-outline-primary" ID="submitButton" OnClick="searchButton_Click">
                                            
                                            <i class="fa fa-search-plus"></i><span style="font-weight: bold !important">Search Information</span>

                                                </asp:LinkButton>


                                                <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="resetButton" OnClick="resetButton_Click">
                                            
                                            <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Information</span>

                                                </asp:LinkButton>

                                            </div>
                                        </div>


                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-time me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Shift List</h5>
                                        </div>
                                        <hr>

                                        <div id="MainGradeDiv">
                                            <asp:GridView ID="itemsGridView" runat="server" AutoGenerateColumns="False"
                                                PageIndex="0" CssClass="table table-bordered table-condensed text-center custom-table-style" DataKeyNames="ShiftId" OnRowCommand="itemsGridView_RowCommand"
                                                AllowPaging="True" OnPageIndexChanging="loadGridView_PageIndexChanging" PageSize="10" EmptyDataText="There are no data records to display.">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="SL">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                            <asp:HiddenField runat="server" ID="hfBuyerId" Value='<%#Eval("ShiftId") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="ShiftTitle" HeaderText="Shift Name" />
                                                    <asp:BoundField DataField="ShiftTitleInTime" HeaderText="Shift In Time" />
                                                    <asp:BoundField DataField="ShiftTitleInEndTime" HeaderText="Shift End Time" />
                                                    <asp:BoundField DataField="IsActive" HeaderText="Status" />
                                                    <asp:BoundField DataField="EntryBy" HeaderText="Entry By" />
                                                    <asp:BoundField DataField="EntryDate" HeaderText="Entry Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                    <asp:BoundField DataField="UpdateBy" HeaderText="Update By" />
                                                    <asp:BoundField DataField="UpdateDate" HeaderText="Update Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                    <asp:BoundField DataField="InactiveBy" HeaderText="Inactive By" />
                                                    <asp:BoundField DataField="InactiveDate" HeaderText="Inactive Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                    <asp:TemplateField HeaderText="Actions">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="editImageButton" runat="server" class="btn btn-white btn-sm  " Height="36" Width="38" CommandArgument='<%#Eval("ShiftId") %>'
                                                                CommandName="EditData" ImageUrl="~/assets/images/pencil-icon.png" />
                                                            <asp:ImageButton ID="activeInactiveButton" runat="server" class="btn btn-white btn-sm" Height="36" Width="38" CommandArgument='<%#Eval("ShiftId") %>'
                                                                CommandName="ActiveInactiveData" ImageUrl='<%#Eval("ActiveInactiveUrl") %>' />
                                                            <asp:ImageButton ID="DeleteImageButton" runat="server" class="btn btn-white btn-sm  " Height="36" Width="38" CommandArgument='<%#Eval("ShiftId") %>'
                                                                CommandName="DeleteData" ImageUrl="~/assets/images/delete.png" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="12%"></ItemStyle>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>




                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

