﻿<%@ page title="" language="C#" masterpagefile="~/MasterPages/NewMasterPage.master" autoeventwireup="true" inherits="FinishedGoodInventory_UI_StockMovement, App_Web_ekfqsqgd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        fieldset {
            /*border:1px solid grey !important;*/
            padding: 10px !important;
        }

        #ContentPlaceHolder1_rbStockType_0,
        #ContentPlaceHolder1_rbStockType_1,
        #ContentPlaceHolder1_rbStockType_2,
        #ContentPlaceHolder1_rbStockType_3,
        #ContentPlaceHolder1_rbStockType_4,
        #ContentPlaceHolder1_rbStockType_5,
        #ContentPlaceHolder1_rbStockType_6,
        #ContentPlaceHolder1_rbStockType_7 {
            margin-right: 5px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="popDiv">
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Stock Movement </div>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">
                            <div class="card border-top border-0 border-4 border-success">
                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <script type="text/javascript">                                            function pageLoad() {
                                                $('.datepicker').pickadate({
                                                    selectMonths: true,                                                    selectYears: true
                                                });                                                $('.mySelect2').select2({
                                                    theme: 'bootstrap4',                                                    width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',                                                    placeholder: $(this).data('placeholder'),                                                    allowClear: Boolean($(this).data('allow-clear')),
                                                });
                                            }                            </script>
                                        <div class="row mb-2">
                                            <div class="col-5">
                                                <div class="card border-top border-0 border-4 pb-2">
                                                    <div class="card-body" style="padding-bottom: 1px !important;">
                                                        <div class="card-title">
                                                            <h6 class="mb-0 text-secondary">Delivery/Issue Info </h6>
                                                            <hr />
                                                        </div>
                                                        <div class="row form-group">
                                                            <label for="" class="col-sm-4 col-form-label col-form-label-sm">Movement Type: <span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>
                                                            <div class="col-sm-8">
                                                                <asp:RadioButtonList ID="rbStockType" runat="server" AutoPostBack="True" OnTextChanged="rbStockType_OnTextChanged" Style="font-size: 12px;" CssClass="chkChoice" CellPadding="4" RepeatColumns="2" RepeatDirection="Vertical" Font-Bold="True" Font-Size="Small">
                                                                </asp:RadioButtonList>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <label for="" class="col-sm-4 col-form-label col-form-label-sm">Reason:<span style="color: red !important;">&nbsp;[*]&nbsp;</span> </label>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtRemarks" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-7">
                                                <div class="card border-top border-0 border-4">
                                                    <div class="card-body">
                                                        <div class="card-title">
                                                            <h6 class="mb-0 text-secondary ">Search options</h6>
                                                            <hr />
                                                        </div>

                                                        <div class="row mb-1">

                                                            <div class="col-6">
                                                                <div class="form-group row">
                                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Fabric Code: </label>
                                                                    <div class="col-sm-8">
                                                                        <asp:DropDownList runat="server" ID="ddlFabric" AutoPostBack="True" OnSelectedIndexChanged="ddlFabric_OnSelectedIndexChanged" CssClass="form-control form-control-sm mySelect2" />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Set No: </label>
                                                                    <div class="col-sm-8">
                                                                        <asp:DropDownList runat="server" ID="ddlSet" AutoPostBack="True" OnSelectedIndexChanged="ddlSet_OnSelectedIndexChanged" CssClass="form-control form-control-sm mySelect2" />
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-6">
                                                                <div class="form-group row">
                                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Roll No: </label>
                                                                    <div class="col-sm-8">
                                                                        <asp:DropDownList runat="server" ID="ddlRoll" AutoPostBack="True" OnSelectedIndexChanged="ddlRoll_OnSelectedIndexChanged" CssClass="form-control form-control-sm mySelect2" />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Order By: </label>
                                                                    <div class="col-sm-8">

                                                                        <asp:DropDownList ID="ddlOrderBy"
                                                                            AutoPostBack="True"
                                                                            OnSelectedIndexChanged="ddlOrderBy_OnSelectedIndexChanged"
                                                                            runat="server" CssClass="form-control form-control-sm mySelect2">

                                                                            <asp:ListItem Value="ASC"> Ascending </asp:ListItem>
                                                                            <asp:ListItem Value="DESC"> Descending </asp:ListItem>
                                                                        </asp:DropDownList>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <hr />
                                                        <div class="row mb-4">
                                                            <div class="col-4"></div>
                                                            <div class="col-4" style="margin: 0 auto !important;">
                                                                <asp:Button ID="btn_Search" CssClass="btn btn-outline-success align-center" runat="server" Text="Search" OnClick="searchButton_Click" />
                                                                <asp:LinkButton runat="server" class="btn btn-outline-warning" ID="resetButton"><span style="font-weight: bold !important">Reset</span>                                                                </asp:LinkButton>
                                                            </div>
                                                            <div class="col-4"></div>
                                                        </div>
                                                        <hr />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <div id="maingridview" style="text-align: center; height: auto; overflow: scroll; width: 100%; overflow-y: scroll; overflow-x: scroll;">
                                                <asp:GridView ID="productGridView" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-condensed custom-table-style"
                                                    AllowPaging="True" OnPageIndexChanging="loadGridView_PageIndexChanging" PageSize="10"
                                                    DataKeyNames="FGStockId,FabricCodeId,StockQuantity" EmptyDataText="There are no data records to display.">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="# SL">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                       <%-- <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="FabricName" HeaderText="Fabric / SIS Code" />--%>

                                                        
                                                        <asp:TemplateField HeaderText="Fabric / SIS Code">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="LblFabricSISCode"  Text='<%#Eval("FabricName") %>'  ></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <%-- <div runat="server" Visible="False" ID="Fabric">--%>
                                                        <asp:TemplateField HeaderText="Change to Fabric/SIS Code">
                                                            <ItemTemplate>
                                                                <asp:DropDownList Width="220px" class="form-select form-select-sm mySelect2" AutoPostBack="True" OnSelectedIndexChanged="ddlFabric_OnSelectedIndexChanged" runat="server" ID="ddlFabric"></asp:DropDownList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="FabricSet" HeaderText="Set" />
                                                        <asp:BoundField DataField="Beam" HeaderText="Beam" />
                                                        <asp:BoundField DataField="GreigeRollNo" HeaderText="Greige Roll No" />
                                                        <asp:BoundField DataField="RollNo" HeaderText="Roll No" />
                                                        <asp:TemplateField HeaderText="Change to Roll No">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" ID="txtChangeRollNo" AutoPostBack="True" OnTextChanged="txtChangeRollNo_OnTextChanged" CssClass="form-control form-control-sm"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="DefectPoint" HeaderText="Defect Point" />
                                                        <asp:BoundField DataField="ActualWidth" HeaderText="Actual Width" />
                                                        <asp:BoundField DataField="Piece" HeaderText="Piece" />
                                                        <asp:BoundField DataField="PWLength" HeaderText="P.W Length" />
                                                        <asp:BoundField DataField="PPHSY" HeaderText="PPHSY" />
                                                        <asp:BoundField DataField="StockQuantity" HeaderText="Quantity" />

                                                        <%-- <asp:TemplateField HeaderText="Change Quantity">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" ID="txtQuantity" CssClass="form-control form-control-sm"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        <asp:BoundField DataField="LocationName" HeaderText="Current Location" />
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True" OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" AutoPostBack="True" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6">
                                                <asp:LinkButton ID="submitButton" runat="server" OnClick="submitButton_Click" Style="width: 200px !important; margin-right: 4px !important; margin: 0 auto !important;" class="btn btn-outline-primary">                                        <i class="bx bxs-save mr-1"></i><span style="font-weight: bold !important">Save Information</span>                                    </asp:LinkButton>
                                                <asp:LinkButton ID="btnReset" runat="server" OnClick="resetButton_Click" Style="width: 200px !important; margin-right: 4px !important; margin: 0 auto !important;" class="btn btn-outline-warning">                                        <i class="bx bxs-rewind-circle mr-1"></i><span style="font-weight: bold !important">Reset Information</span>                                    </asp:LinkButton>
                                            </div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                    Height="120" Width="120" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>

