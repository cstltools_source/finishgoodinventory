﻿<%@ page title="" language="C#" masterpagefile="~/MasterPages/NewMasterPage.master" autoeventwireup="true" inherits="FinishedGoodInventory_UI_StoreApprovalDetail, App_Web_ekfqsqgd" %>
<%@ Register TagPrefix="cc1" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=3.0.20820.28364, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>


            <div id="popDiv">
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <!--breadcrumb-->
                    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                        <div class="breadcrumb-title pe-3"><i class="bx bx-customize"></i>Stock Receive</div>

                        <div class="ms-auto">
                            <div class="btn-group">


                                <a href="../FinishedGoodInventory_UI/QAStockVerification.aspx" class="btn btn-sm btn-sm btn-outline-info"><i class="fa fa-backward"></i>&nbsp;Back to List</a>


                            </div>
                        </div>
                    </div>
                    <!--end breadcrumb-->
                    <div class="row">
                        <div class="col">

                            <div class="card border-top border-0 border-4 border-success">


                                <div class="card-body custom-height">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-file me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Master Information</h5>
                                        </div>
                                        <hr>


                                        <div class="row mb-2">

                                            <div class="col-md-4">
                                                <script type="text/javascript">
                                                    function pageLoad() {
                                                        $('.datepicker').pickadate({
                                                            selectMonths: true,
                                                            selectYears: true
                                                        });
                                                        $('.mySelect2').select2({
                                                            theme: 'bootstrap4',
                                                            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                                                            placeholder: $(this).data('placeholder'),
                                                            allowClear: Boolean($(this).data('allow-clear')),
                                                        });
                                                    }
                                                </script>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Receive Type: </label>
                                                   <div class="col-sm-8 pt-2">
                                                            <asp:Label CssClass="custom-mt-top" ID="lblReceiveType" runat="server" />
                                                            <asp:HiddenField runat="server" ID="QAStockRId" />
                                                        </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Prod. Date: </label>
                                                    <div class="col-sm-8 pt-2">
                                                        <asp:Label CssClass="custom-mt-top" ID="lblProductiondate" runat="server" />
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Prod. Shift: </label>
                                                    <div class="col-sm-8">
                                                        <asp:Label CssClass="custom-mt-top" ID="lblProductionShift" runat="server" />
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Entry By: </label>
                                                    <div class="col-sm-8 pt-2">
                                                        <asp:Label CssClass="custom-mt-top" ID="lblEntryBy" runat="server" />
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Entry Date: </label>
                                                    <div class="col-sm-8 pt-2">
                                                        <asp:Label CssClass="custom-mt-top" ID="lblEntryDate" runat="server" />
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-4">
                                                
                                                
                                                <div class="form-group row" runat="server" Visible="False">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Fabric Name: </label>
                                                    <div class="col-sm-8 pt-2">
                                                        <asp:Label CssClass="custom-mt-top" ID="lblFabricName" runat="server" />
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Fabric Color: </label>
                                                    <div class="col-sm-8 pt-2">
                                                        <asp:Label CssClass="custom-mt-top" ID="lblColor" runat="server" />
                                                    </div>
                                                </div>
                                                

                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Required Width: </label>
                                                    <div class="col-sm-8 pt-2">
                                                        <asp:Label CssClass="custom-mt-top" ID="lblRequiredWidth" runat="server" />
                                                    </div>
                                                </div>

                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Ounce/Yard<sup>2</sup>: </label>
                                                    <div class="col-sm-8 pt-2">
                                                        <asp:Label CssClass="custom-mt-top" ID="lblOunceYard" runat="server" />
                                                    </div>
                                                </div>

                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Shrinkage(L): </label>
                                                    <div class="col-sm-8 pt-2">
                                                        <asp:Label CssClass="custom-mt-top" ID="lblShrinkage" runat="server" />
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Shrinkage(W): </label>
                                                    <div class="col-sm-8 pt-2">
                                                        <asp:Label CssClass="custom-mt-top" ID="lblShrinkageWidth" runat="server" />
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-4">

                                                 <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">SET No: </label>
                                                    <div class="col-sm-8 pt-2">
                                                        <asp:Label CssClass="custom-mt-top" ID="lblSetNo" runat="server" />
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">FL No: </label>
                                                    <div class="col-sm-8 pt-2">
                                                        <asp:Label CssClass="custom-mt-top" ID="lblFLNo" runat="server" />
                                                    </div>
                                                </div>
                                                

                                                <div class="form-group row ">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">QA Location: </label>
                                                    <div class="col-sm-8 pt-2">
                                                        <asp:Label CssClass="custom-mt-top" ID="lblQaLocation" runat="server" />
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row" style="display: none">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Verified By: </label>
                                                    <div class="col-sm-8 pt-2">
                                                        <asp:Label CssClass="custom-mt-top" ID="lblVerifiedBy" runat="server" />
                                                    </div>
                                                </div>

                                                <div class="form-group row" style="display: none">
                                                    <label for="" class="col-sm-4 col-form-label col-form-label-sm">Verified Date: </label>
                                                    <div class="col-sm-8 pt-2">
                                                        <asp:Label CssClass="custom-mt-top" ID="lblVerifiedDate" runat="server" />
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <br />

                                        <div class="card-title d-flex align-items-center text-center">
                                            <div>
                                                <i class="bx bxs-file me-1 font-22 text-secondary"></i>
                                            </div>
                                            <h5 class="mb-0 text-secondary ">Roll Description</h5>

                                            

                                        </div>
                                        
                                        <hr />
                                        <asp:GridView ID="QAStockDetails" runat="server" ShowFooter="True" AutoGenerateColumns="false" DataKeyNames="Quantity,StockReceiveMasterId" CssClass="table table-bordered text-center thead-dark custom-table-style">
                                            <Columns>
                                                <asp:TemplateField HeaderText="# SL">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="FabricSet" HeaderText="SET" />
                                                <asp:BoundField DataField="Beam" HeaderText="Beam" />
                                                <asp:BoundField DataField="RollNo" HeaderText="Roll No" />
                                                <asp:BoundField DataField="DefectPoint" HeaderText="Defect Point" />
                                                <asp:BoundField DataField="ActualWidth" HeaderText="Actual Width" />
                                                <asp:BoundField DataField="Piece" HeaderText="Piece" />
                                                <asp:BoundField DataField="PWLength" HeaderText="P.W Length" />
                                                <asp:BoundField DataField="GrossWeight" HeaderText="Gross Weight (kg)" />
                                                <asp:BoundField DataField="NetWeight" HeaderText="Net Weight (kg)" />
                                                <asp:BoundField DataField="Quantity" HeaderText="Receive Quantity (Yds)" />

                                                <asp:TemplateField HeaderText="Physical Quantity (Yds)">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TxtQuantity" AutoPostBack="True" OnTextChanged="TxtQuantity_OnTextChanged" runat="server" Text='<%#Eval("Quantity") %>' CssClass="form-control form-control-sm"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                        <hr>
                                        <br />


                                        <div class="row mb-4">
                                            <div class="col-4"></div>
                                            <div class="col-4">
                                                <asp:LinkButton runat="server" OnClientClick="return confirm('Are you really aware of this operation?');" class="btn btn-outline-primary" ID="LinkButton2" OnClick="submitButton_Click">                                          
                                                    <i class="fa fa-check-square-o"></i><span style="font-weight: bold !important">Approve Information</span>
                                                </asp:LinkButton>


                                            </div>
                                            <div class="col-4"></div>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/assets/images/progress-bar-opt.gif"
                    Height="120" Width="120" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    
 


</asp:Content>

