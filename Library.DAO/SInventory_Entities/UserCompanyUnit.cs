﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.SInventory_Entities
{
    public class UserCompanyUnit
    {
        public int UserComId { get; set; }
        public int UserId { get; set; }
        public int CompanyUnitId { get; set; }
      
    }
}
