﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.Panal_Entities
{
   public class ObjPanal
    {
       public int SL { get; set; }
       public string ManuName { get; set; }
       public string URL { get; set; }
       public string ParantId { get; set; }
     
    }
}
