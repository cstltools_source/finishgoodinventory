﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.FGInvrntory_DAO
{
    public class ProformaDetailDao
    {
        public int ProformaDetailId { get; set; }

        public int? ProformaMasterId { get; set; }

        public int? SisCodeId { get; set; }

        public int? ColorId { get; set; }

        public decimal? RequiredWidth { get; set; }

        public decimal? RequiredOunceOryard { get; set; }

        public decimal? ShrinkageLengthMin { get; set; }
        public decimal? ShrinkageLengthMax { get; set; }

        public decimal? ShrinkageWidthMin { get; set; }
        public decimal? ShrinkageWidthMax { get; set; }

        public string FabricName { get; set; }
        public string ColorName { get; set; }
        public string SisCode { get; set; }

        public int EntryBy { get; set; }

        public DateTime EntryDate { get; set; }


        public decimal? Quantity { get; set; }
        public decimal? UnitPrice { get; set; }

    }
}
