﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.FGInvrntory_DAO
{
    public class BuyerInfo
    {
        public Int32 BuyerId { get; set; }
        public Int32 BuyerTypeId { get; set; }
        public String BuyerCode { get; set; }
        public String BuyerName { get; set; }
        public String BuyerContactNo { get; set; }
        public String BuyerEmail { get; set; }
        public String BuyerAddress { get; set; }
        public Int32 EntryBy { get; set; }
        public Int32 UpdateBy { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool IsActive { get; set; }
        public Int32 InactiveBy { get; set; }
        public DateTime InactiveDate { get; set; }
        public bool IsBuyer { get; set; }
        public bool IsVendor { get; set; }
        public bool IsBoth { get; set; }

    }
}
