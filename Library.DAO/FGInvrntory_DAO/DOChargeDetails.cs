﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.FGInvrntory_DAO
{
   public class DOChargeDetails
    {
        public int DOChargeId { get; set; }
        public int DOChargeDetailsId { get; set; }
        public string ItemDescription { get; set; }
        public int ProformaMasterId { get; set; }
        public string PINO { get; set; }
        public DateTime PIDate { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal DOAmount { get; set; }
        public int CurrencyId { get; set; }
    }
}
