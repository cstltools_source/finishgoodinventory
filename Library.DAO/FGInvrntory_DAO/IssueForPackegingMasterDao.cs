﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.FGInvrntory_DAO
{
    public class IssueForPackegingMasterDao
    {
        public int IssueMasterId { get; set; }
        public int ProformaId { get; set; }
        public int DeliveryTypeId { get; set; }

        public DateTime? IssueDate { get; set; }

        public decimal? TotalQuantity { get; set; }
        public decimal? DeliveryQuantity { get; set; }

        public int? IssueBy { get; set; }
        public string Reason { get; set; }


        public DateTime? DeliveryDate { get; set; }

        public int? DeliveryBy { get; set; }


        public string DeliveryStatus { get; set; }


        public int DOChargeId { get; set; }


    }
}
