﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.FGInvrntory_DAO
{
   public class ShiftDao
    {
        public int ShiftId { get; set; }

        public string ShiftTitle { get; set; }

        public TimeSpan? ShiftTitleInTime { get; set; }

        public TimeSpan? ShiftTitleInEndTime { get; set; }

        public int? EntryBy { get; set; }

        public DateTime? EntryDate { get; set; }

        public int? UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int? ApproveBy { get; set; }

        public DateTime? ApproveDate { get; set; }

        public bool? IsActive { get; set; }

        public int? InactiveBy { get; set; }

        public DateTime? InactiveDate { get; set; }
    }
}
