﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.FGInvrntory_DAO
{
   public class DOCharge
    {
       public int DOChargeId { get; set; }
       public string DOCode { get; set; }
       public string DONo { get; set; }
       public DateTime DODate { get; set; }
       public string LCNo { get; set; }
       public DateTime LCDate { get; set; }
       public DateTime ShipmentDate { get; set; }
       public int MarketingPersonId { get; set; }
       public int BuyerId { get; set; }
       public int CurrencyId { get; set; }
       public Boolean IsActive { get; set; }
       public int EntryBy { get; set; }
       public DateTime EntryDate { get; set; }
       public int UpdateBy { get; set; }
       public DateTime UpdateDate { get; set; } 

    }
}
