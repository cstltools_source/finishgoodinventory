﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.FGInvrntory_DAO
{
   public class QaStockReceiveDetailDao
    {
        public int StockReceiveDetailId { get; set; }

        public int? StockReceiveMasterId { get; set; }

        public int? FabricCodeId { get; set; }

        public int? ColorId { get; set; }

        public decimal? RequiredWidth { get; set; }

        public decimal? RequiredOunceYard { get; set; }

        public decimal? ShrinkageLengthMin { get; set; }
        public decimal? ShrinkageLengthMax { get; set; }
        public decimal? ShrinkageWidthMin { get; set; }
        public decimal? ShrinkageWidthMax { get; set; }

        public string FabricSet { get; set; }

        public int? Beam { get; set; }

        public string GreigeRollNo { get; set; }

        public int? RollNo { get; set; }

        public decimal? DefectPoint { get; set; }

        public decimal? ActualWidth { get; set; }

        public decimal? Piece { get; set; }

        public string PWLength { get; set; }

        public decimal? Quantity { get; set; }

        public string OperatorName { get; set; }

        public int? MCNo { get; set; }

        public int? FL { get; set; }

        public decimal? PPHSY { get; set; }

        public decimal? GrossWeight { get; set; }

        public decimal? NetWeight { get; set; }

        public string ProblemCause { get; set; }

        public int? QALocationId { get; set; }

        public string Remarks { get; set; }

        public string ProblemCauseReason { get; set; }
        public string FabricName { get; set; }
        public string FabricCategory { get; set; }

        public string ShadeGradeing { get; set; }
    }
}
