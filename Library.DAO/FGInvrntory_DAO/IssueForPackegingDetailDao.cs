﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.FGInvrntory_DAO
{
    public class IssueForPackegingDetailDao
    {
        public int IssueDetailId { get; set; }

        public int? IssueMasterId { get; set; }

        public int? FGStockId { get; set; }

        public bool? IsDelivery { get; set; }
        public decimal? TotalQuantity { get; set; }

        public int DeliveryTypeId { get; set; }
    }
}
