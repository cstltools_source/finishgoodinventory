﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.FGInvrntory_DAO
{
    public class ProformaInvoiceDao
    {
        public Int32 ProformaId { get; set; }
        public Int32 BuyerId { get; set; }
        public Int32 VendorId { get; set; }
        public Int32 ColorId { get; set; }
        public String ProformaInvNo { get; set; }
        public String ProformaCode { get; set; }
        public DateTime ProformaDate { get; set; }
        public String RequiredWidth { get; set; }
        public String RequiredOunceOryard { get; set; }
        public String Shrinkage { get; set; }
        public Int32 EntryBy { get; set; }
        public Int32 UpdateBy { get; set; }
        public Int32 ApproveBy { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime ApproveDate { get; set; }
    }
}
