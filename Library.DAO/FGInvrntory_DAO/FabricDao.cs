﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.FGInvrntory_DAO
{
    public class FabricDao
    {
        public Int32 FabricId { get; set; }
        public Int32 FabricColor { get; set; }
        public String FabricCode { get; set; }
        public String FabricName { get; set; }
        public String FabricShortName { get; set; }
        public String FebricDescription { get; set; }
        public Int32 EntryBy { get; set; }
        public Int32 UpdateBy { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool IsActive { get; set; }
        public Int32 InactiveBy { get; set; }
        public DateTime InactiveDate { get; set; }

    }
}
