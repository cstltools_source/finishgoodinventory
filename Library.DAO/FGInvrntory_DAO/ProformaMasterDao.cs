﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;

namespace Library.DAO.FGInvrntory_DAO
{
    public class ProformaMasterDao
    {

        public int ProformaMasterId { get; set; }

        public string ProformaCode { get; set; }

        public int? BuyerId { get; set; }

        public int? VendorId { get; set; }

        public string ProformaInvNo { get; set; }

        public DateTime? ProformaDate { get; set; }

        public string Remarks { get; set; }

        public int? EntryBy { get; set; }

        public DateTime? EntryDate { get; set; }

        public int? UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public string BuyerName { get; set; }
        public string VendorName { get; set; }


        public string MarketingPersonId { get; set; }
        

    }
}
