﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.FGInvrntory_DAO
{
   public class StockMovement
    {
        public int StockId { get; set; }

        public double StockQty { get; set; }

        public int fabricId { get; set; }

        public string RollChange { get; set; }

        public double NewStockQty { get; set; }

        public int MovementTypeId { get; set; }

        public string MovementType { get; set; }

    }
}
