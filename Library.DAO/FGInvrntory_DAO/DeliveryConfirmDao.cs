﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.FGInvrntory_DAO
{
    public class DeliveryConfirmDao
    {
        public int? DeliveryConfirmId { get; set; }

        public DateTime? ConfirmationDate { get; set; }

        public int? ConfirmBy { get; set; }

        public int? IssueMasterId { get; set; }
    }
}
