﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.FGInvrntory_DAO
{
   public class QaStockReceiveMasterDao
    {
        public int StockReceiveMasterId { get; set; }

        public string StockReceiveCode { get; set; }

        public int? ProformaInvId { get; set; }

        public int? ProformaDetailId { get; set; }

        public int? ShiftId { get; set; }

        public DateTime? ProductionDate { get; set; }

        public int? EntryBy { get; set; }

        public DateTime? EntryDate { get; set; }

        public int? UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int? ApproveBy { get; set; }

        public DateTime? ApproveDate { get; set; }

        public string ApprovalStatus { get; set; }

        public int? VerifiedBy { get; set; }

        public DateTime? VerifiedDate { get; set; }

        public int? RcvdTypeId { get; set; }

        public DateTime? RcvdDate { get; set; }

        public string Remarks { get; set; }

    }
}
