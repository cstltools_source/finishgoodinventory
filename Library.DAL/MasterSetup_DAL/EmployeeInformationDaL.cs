﻿using Library.DAL.DataManager;
using SalesSolution.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Library.DAL.MasterSetup_DAL
{
   public class EmployeeInformationDaL
    {
        private DataAccessManager accessManager = new DataAccessManager();



        public ResultInfo SaveEmployeeInformation(EmployeeInformation employee, string MonthlyArray, string sessionUser)
        {
            int pk = 0;
            ResultInfo aInformation = new ResultInfo();
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                List<SqlParameter> gSqlParameterList = new List<SqlParameter>();
                DateTime entryDtae = DateTime.Now;

                gSqlParameterList.Add(new SqlParameter("@EmpInfoId", employee.EmpInfoId ));
                gSqlParameterList.Add(new SqlParameter("@CompanyId", employee.CompanyId ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@EmpName", employee.EmpName ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@EmpMasterCode", employee.EmpMasterCode ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@FatherName", employee.FatherName ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@MotherName", employee.MotherName ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@Religion", employee.Religion ?? (object)DBNull.Value));

                gSqlParameterList.Add(new SqlParameter("@Nationality", employee.Nationality ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@NationalIdNo", employee.NationalIdNo ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@DateOfBirth", employee.DateOfBirth ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@BloodGroup", employee.BloodGroup ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@Gender", employee.Gender ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@AddressPresent", employee.AddressPresent ?? (object)DBNull.Value));

                gSqlParameterList.Add(new SqlParameter("@AddressPermanent", employee.AddressPermanent ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@CellNumber", employee.CellNumber ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@Email", employee.Email ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@EmrgContactNo", employee.EmrgContactNo ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@MaritalStatus", employee.MaritalStatus ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@RefName", employee.RefName ?? (object)DBNull.Value));

                gSqlParameterList.Add(new SqlParameter("@RefContactNo", employee.RefContactNo ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@DesignationId", employee.DesignationId ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@DepartmentId", employee.DepartmentId ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@ShiftId", employee.ShiftId ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@JoiningDate", employee.JoiningDate ?? (object)DBNull.Value));

                gSqlParameterList.Add(new SqlParameter("@JobLeftDate", employee.JobLeftDate ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@LastCompanyName", employee.LastCompanyName ?? (object)DBNull.Value));
                gSqlParameterList.Add(new SqlParameter("@LastJobLocation", employee.LastJobLocation ?? (object)DBNull.Value));

                gSqlParameterList.Add(new SqlParameter("@EmployeeStatus", employee.EmployeeStatus ?? (object)DBNull.Value));

                if (employee.EmpInfoId > 0)
                {
                    List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();

                    aSqlParameterlist.Add(new SqlParameter("@id", employee.EmpInfoId));
                    aSqlParameterlist.Add(new SqlParameter("@Name", employee.EmpMasterCode));
                    DataTable dt = accessManager.GetDataTable("sp_check_EmployeeInfo", aSqlParameterlist);
                    if (dt.Rows.Count == 0)
                    {
                        gSqlParameterList.Add(new SqlParameter("@UpdateBy", sessionUser));



                        bool result = accessManager.UpdateData("sp_Update_EmployeeInformation", gSqlParameterList);
                        aInformation.isSuccess = result;
                        pk = employee.EmpInfoId;
                    }
                }
                else
                {
                    gSqlParameterList.Add(new SqlParameter("@EntryBy", sessionUser));
                    pk = accessManager.SaveDataReturnPrimaryKey("sp_Save_EmployeeInformation", gSqlParameterList);
                    if (pk > 0)
                    {
                        aInformation.isSuccess = true;
                    }
                }


                if (aInformation.isSuccess)
                {
                    if (pk > 0)
                    {
                        if (MonthlyArray != "")
                        {
                            string[] degreeList = MonthlyArray.Split(',');

                            foreach (String item in degreeList)
                            {
                                if (item != "")
                                {
                                    List<SqlParameter> aSQL = new List<SqlParameter>();
                                    aSQL.Add(new SqlParameter("@EmpInfoId", pk));
                                    aSQL.Add(new SqlParameter("@AllowanceId", item ?? (object)DBNull.Value));
                                    aInformation.isSuccess = accessManager.SaveData("sp_Save_EmployeeAllowanceDetail", aSQL);
                                }
                            }



                        }
                    }
                }

               

            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                aInformation.isSuccess = false;
                aInformation.ErrorMessage = exception.Message;

                throw exception;
            }
            finally
            {

                accessManager.SqlConnectionClose();
            }

            return aInformation;
        }

        public ResultInfo SaveNoticeImage(string path, string name)
        {
            int pk = 0;
            ResultInfo aInformation = new ResultInfo();
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                List<SqlParameter> gSqlParameterList = new List<SqlParameter>();
                DateTime entryDtae = DateTime.Now;

                //gSqlParameterList.Add(new SqlParameter("@NoticeImageId", master.NoticeId));
                //gSqlParameterList.Add(new SqlParameter("@ImageName", master.NoticeTitle));
                //gSqlParameterList.Add(new SqlParameter("@ImagePath", master.Announcement));

                //if (master.NoticeId > 0)
                //{
                //    gSqlParameterList.Add(new SqlParameter("@UpdateBy", sessionUser));
                //    bool result = accessManager.UpdateData("sp_Update_NoticeMaster", gSqlParameterList);
                //    aInformation.isSuccess = result;
                //    pk = master.NoticeId;
                //}
                //else
                //{
                //    gSqlParameterList.Add(new SqlParameter("@EntryBy", sessionUser));
                //    pk = accessManager.SaveDataReturnPrimaryKey("sp_Save_NoticeMaster", gSqlParameterList);
                //    if (pk > 0)
                //    {
                //        aInformation.isSuccess = true;
                //    }
                //}
            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                aInformation.isSuccess = false;
                aInformation.ErrorMessage = exception.Message;

                throw exception;
            }
            finally
            {

                accessManager.SqlConnectionClose();
            }

            return aInformation;
        }





        public DataTable GetEmployeeInformationList()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
                DataTable dt = accessManager.GetDataTable("sp_Get_EmployeeInformationList");
                return dt;
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public ResultInfo Delete_EmployeeInformation(Int32 DeleteId)
        {
            int pk = 0;

            ResultInfo aInformation = new ResultInfo();
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                List<SqlParameter> gSqlParameterList = new List<SqlParameter>();
                DateTime entryDtae = DateTime.Now;
                gSqlParameterList.Add(new SqlParameter("@EmpInfoId", DeleteId));

                bool result = accessManager.DeleteData("sp_Delete_EmployeeInformation", gSqlParameterList);
                pk = DeleteId;
            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                aInformation.isSuccess = false;
                aInformation.ErrorMessage = exception.Message;

                throw exception;
            }
            finally
            {
                aInformation.isSuccess = true;
                accessManager.SqlConnectionClose();
            }

            return aInformation;
        }


        public DataTable GetEmployeeInfoForEdit(int id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                EmployeeInformation master = new EmployeeInformation();
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                aSqlParameters.Add(new SqlParameter("@id", id));
                DataTable dt = accessManager.GetDataTable("sp_Get_EmployeeInformation_ById", aSqlParameters);
                //while (dr.Read())
                //{
                //    master.EmpInfoId = (int)dr["EmpInfoId"];
                //    master.CompanyId = (int)dr["CompanyId"];
                //    master.EmpName = dr["EmpName"].ToString();
                //    master.FatherName = dr["FatherName"].ToString();
                //    master.MotherName = dr["MotherName"].ToString();
                //    master.DateOfBirth = (DateTime)dr["DateOfBirth"];
                //    master.AddressPermanent = dr["AddressPermanent"].ToString();
                //    master.AddressPresent = dr["AddressPresent"].ToString();
                //    master.Gender = dr["Gender"].ToString();
                //    master.Religion = dr["Religion"].ToString();
                //    master.Nationality = dr["Nationality"].ToString();

                //    master.Gender = dr["Gender"].ToString();
                //    master.Religion = dr["Religion"].ToString();
                //    master.Nationality = dr["Nationality"].ToString();



                //    master.BloodGroup = dr["BloodGroup"].ToString();
                //    master.MaritalStatus = dr["MaritalStatus"].ToString();
                //    master.NationalIdNo = dr["NationalIdNo"].ToString();
                //    master.JoiningDate = (DateTime)dr["JoiningDate"];


                //    master.DepartmentId = (int)dr["DepartmentId"];
                //    master.DesignationId = (int)dr["DesignationId"];
                //    master.ShiftId = (int)dr["ShiftId"];

                //    master.CellNumber = dr["CellNumber"].ToString();
                //    master.Email = dr["Email"].ToString();
                //    master.RefName = dr["RefName"].ToString();
                //    master.RefContactNo = dr["RefContactNo"].ToString();
                //    master.EmrgContactNo = dr["EmrgContactNo"].ToString();

                //}
                return dt;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


    }
}