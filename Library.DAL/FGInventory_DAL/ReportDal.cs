﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Library.DAL.InternalCls;

namespace Library.DAL.FGInventory_DAL
{
  public class ReportDal
    {
        private readonly ClsCommonInternalDAL _aCommonInternalDal = new ClsCommonInternalDAL();

        public DataTable Get_StockDelivery(string pram)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ID", pram));
            return _aCommonInternalDal.GetDataTableAction("sp_Report_StockDelevary", aSqlParameterlist, "SSIDB");
        }
    }
}
