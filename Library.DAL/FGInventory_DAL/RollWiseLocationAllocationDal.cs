﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.DataManager;
using Library.DAL.InternalCls;

namespace Library.DAL.FGInventory_DAL
{
    public class RollWiseLocationAllocationDal
    {

        private readonly ClsCommonInternalDAL _aCommonInternalDal = new ClsCommonInternalDAL();

        private DataAccessManager accessManager = new DataAccessManager();
        public void LoadFabricSetInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT FabricSet FROM tblFinishGoodStock WHERE FabricSet IS NOT NULL";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "FabricSet", "FabricSet", queryStr);
        }

        public void LoadRollNoInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT RollNo FROM tblFinishGoodStock WHERE RollNo IS NOT NULL";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "RollNo", "RollNo", queryStr);
        }

        public void LoadRequiredWidthInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT Re_Width FROM tblFinishGoodStock WHERE Re_Width IS NOT NULL";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "Re_Width", "Re_Width", queryStr);
        }

        public void LoadRequiredOunceOryardInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT RequiredOunceOryard FROM tblProformaInvoice WHERE RequiredOunceOryard IS NOT NULL";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "RequiredOunceOryard", "RequiredOunceOryard", queryStr);
        }

        public void LoadShadeGradeingInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT ShadeGradeing FROM tblFinishGoodStock WHERE ShadeGradeing IS NOT NULL";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ShadeGradeing", "ShadeGradeing", queryStr);
        }
        
        public void LoadShadeGradeInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT ShadeGradeId,ShadeGrade FROM tblShadeGrades ORDER BY ShadeGrade";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ShadeGrade", "ShadeGradeId", queryStr);
        }

        public void LoadShrinkageInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT Shrinkage FROM tblFinishGoodStock WHERE Shrinkage IS NOT NULL";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "Shrinkage", "Shrinkage", queryStr);
        }

        public void LoadPPHYInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT PPHY FROM tblFinishGoodStock WHERE PPHY IS NOT NULL";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "PPHY", "PPHY", queryStr);
        }

        public void LoadPWLengthInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT PWLength FROM tblFinishGoodStock WHERE PWLength IS NOT NULL";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "PWLength", "PWLength", queryStr);
        }

        public DataTable GetShadeGrades()
        {
            var aSqlParameterlist = new List<SqlParameter>();
            return _aCommonInternalDal.GetDataTableAction("sp_ShadeGrades_CheckboxList", aSqlParameterlist, "SSIDB");
        }

        public DataTable GetFinishedGoodStock(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Parm", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_StockLocation_RollWiseList", aSqlParameterlist, "SSIDB");
        }


        public void LoadStoreLocation(DropDownList ddl)
        {
             string queryStr = @"SELECT LocationId,LocationName + ' ( ' + LocationForTitle + ' ) ' LocationName FROM tblLocationInfo AS LI
                                 LEFT JOIN tblLocationForInfo AS LFI ON LI.LocationFor = LFI.LocationForId WHERE LocationFor = 2 ORDER BY LocationName";
             _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "LocationName", "LocationId", queryStr);
        }

        public bool UpdateStockLocation(string fgStockId, string locationId, int userId)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                var aSqlParameterlist = new List<SqlParameter>();

                aSqlParameterlist.Add(new SqlParameter("@FgStockId", Convert.ToInt32(fgStockId)));
                aSqlParameterlist.Add(new SqlParameter("@LocationId", Convert.ToInt32(locationId)));
                aSqlParameterlist.Add(new SqlParameter("@ChangeBy", userId));

                return accessManager.UpdateData("sp_StockLocation_RollWiseAllocationEntry", aSqlParameterlist);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

            
        }

        public void LoadFabricInfo(DropDownList ddl)
        {
            string queryStr = @"SELECT DISTINCT FabricId,FBCI.FebricDescription AS Fabric FROM tblFinishGoodStock AS FGS
                                LEFT JOIN tblFabricInfo AS FBCI ON FGS.FabricCodeId = FBCI.FabricId ORDER BY FabricId";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "Fabric", "FabricId", queryStr);
        }


        public DataTable LoadFabricInfoForShadeGradeUpdate()
        {
            string queryStr = @"SELECT  DISTINCT FabricName FROM tblFabricInfo WHERE FabricName IS NOT NULL";
            return _aCommonInternalDal.DataContainerDataTable(queryStr, "SSIDB");
        }

        public void LoadSearchStoreLocation(DropDownList ddl)
        {
            string queryStr = @"SELECT LocationId,LocationName + ' ( ' + LocationForTitle + ' ) ' LocationName FROM tblLocationInfo AS LI
                                 LEFT JOIN tblLocationForInfo AS LFI ON LI.LocationFor = LFI.LocationForId ORDER BY LocationName";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "LocationName", "LocationId", queryStr);
        }

        public DataTable GetRemarks(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Parm", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_ShadeGrades_RemarksCheckboxList", aSqlParameterlist, "SSIDB");
        }

        public DataTable GetRollList( )
        {
           // var aSqlParameterlist = new List<SqlParameter>();

            return _aCommonInternalDal.GetDataTableAction("sp_ShadeGrades_RollCheckboxList", "SSIDB");
        }
    }
}
