﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.DataManager;
using Library.DAL.InternalCls;
using Library.DAO.FGInvrntory_DAO;
using Library.DAO.SInventory_Entities;

namespace Library.DAL.FGInventory_DAL
{
    public class CustomerDal
    {
        private readonly ClsCommonInternalDAL _aCommonInternalDal = new ClsCommonInternalDAL();
        private DataAccessManager accessManager = new DataAccessManager();

        public void LoadBuyerType(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerTypeId,BuyerTypeName FROM tblBuyerTypeInfo";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerTypeName", "BuyerTypeId", queryStr);
        }

        public DataTable CheckBuyerExistOrNot(string buyerName)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@BuyerName", buyerName));
            return _aCommonInternalDal.GetDataTableAction("@BuyerName", aSqlParameterlist, "SolutionConnectionStringSSIDB");
        }

        public bool SaveBuyerInfo(BuyerInfo aInfo)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                var aSqlParameterlist = new List<SqlParameter>();

                aSqlParameterlist.Add(new SqlParameter("@BuyerTypeId", aInfo.BuyerTypeId));
                aSqlParameterlist.Add(new SqlParameter("@BuyerName", aInfo.BuyerName));
                aSqlParameterlist.Add(new SqlParameter("@BuyerContactNo", aInfo.BuyerContactNo));
                aSqlParameterlist.Add(new SqlParameter("@BuyerAddress", aInfo.BuyerAddress));
                aSqlParameterlist.Add(new SqlParameter("@BuyerEmail", aInfo.BuyerEmail));
                aSqlParameterlist.Add(new SqlParameter("@EntryBy", HttpContext.Current.Session["UserId"].ToString()));
                aSqlParameterlist.Add(new SqlParameter("@EntryDate", DateTime.Today));
                aSqlParameterlist.Add(new SqlParameter("@IsActive", true));
                aSqlParameterlist.Add(new SqlParameter("@IsBuyer", aInfo.IsBuyer));
                aSqlParameterlist.Add(new SqlParameter("@IsVendor", aInfo.IsVendor));
                aSqlParameterlist.Add(new SqlParameter("@IsBoth", aInfo.IsBoth));

                return accessManager.SaveData("sp_Buyer_Insert", aSqlParameterlist);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }



            
        }

        public DataTable GetBuyerInfo(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_Buyer_View", aSqlParameterlist, "SSIDB"); 
        }


        public DataTable GetBuyerInfoById(Int32 buyerId)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@BuyerId", buyerId));
            return _aCommonInternalDal.GetDataTableAction("sp_Buyer_EditById", aSqlParameterlist, "SSIDB");
        }


        public bool UpdateBuyerInfo(BuyerInfo aInfo)
        {


            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                
                var aSqlParameterlist = new List<SqlParameter>();

                aSqlParameterlist.Add(new SqlParameter("@BuyerId", aInfo.BuyerId));
                aSqlParameterlist.Add(new SqlParameter("@BuyerTypeId", aInfo.BuyerTypeId));
                aSqlParameterlist.Add(new SqlParameter("@BuyerName", aInfo.BuyerName));
                aSqlParameterlist.Add(new SqlParameter("@BuyerContactNo", aInfo.BuyerContactNo));
                aSqlParameterlist.Add(new SqlParameter("@BuyerAddress", aInfo.BuyerAddress));
                aSqlParameterlist.Add(new SqlParameter("@BuyerEmail", aInfo.BuyerEmail));
                aSqlParameterlist.Add(new SqlParameter("@UpdateBy", HttpContext.Current.Session["UserId"].ToString()));
                aSqlParameterlist.Add(new SqlParameter("@UpdateDate", DateTime.Today));
                aSqlParameterlist.Add(new SqlParameter("@IsBuyer", aInfo.IsBuyer));
                aSqlParameterlist.Add(new SqlParameter("@IsVendor", aInfo.IsVendor));
                aSqlParameterlist.Add(new SqlParameter("@IsBoth", aInfo.IsBoth));

                return accessManager.UpdateData("sp_Buyer_Update", aSqlParameterlist);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            
            
            
        }


        public bool ActiveInactiveBuyerInfo(BuyerInfo aInfo)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);

                var aSqlParameterlist = new List<SqlParameter>();

                aSqlParameterlist.Add(new SqlParameter("@BuyerId", aInfo.BuyerId));
                aSqlParameterlist.Add(new SqlParameter("@ActiveInactiveBy", HttpContext.Current.Session["UserId"].ToString()));

                return accessManager.UpdateData("sp_Buyer_ActiveInactiveById", aSqlParameterlist);

            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

            
        }

        public bool DeleteBuyerInfoById(BuyerInfo aInfo)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@BuyerId", aInfo.BuyerId));
            return _aCommonInternalDal.UpdateAction("sp_Buyer_DeleteById", aSqlParameterlist);
        }



    }
}
