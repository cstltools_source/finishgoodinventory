﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.DataManager;
using Library.DAL.InternalCls;
using Library.DAO.FGInvrntory_DAO;

namespace Library.DAL.FGInventory_DAL
{
    public class ProformaInvoiceDal
    {
        private readonly ClsCommonInternalDAL _aCommonInternalDal = new ClsCommonInternalDAL();
        private DataAccessManager accessManager = new DataAccessManager();

        public void LoadBuyerInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerId,BuyerName FROM tblBuyerInfo WHERE IsBuyer = 1 OR IsBoth = 1";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerName", "BuyerId", queryStr);
        }

        public void Load_Buyer_Info(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerId,BuyerName FROM tblBuyerInfo WHERE IsBuyer = 1 AND IsActive=1";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerName", "BuyerId", queryStr);
        }


        
        public void LoadBuyerInfo2(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerId,BuyerName FROM tblBuyerInfo WHERE (IsBuyer = 1 OR IsBoth = 1) AND IsActive = 1";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerName", "BuyerId", queryStr);
        }

        public void LoadMarketingPerson(DropDownList ddl)
        {
            const string queryStr = "Select * from tbl_marketingPerson";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "MarketingPersonName", "MarketingPersonId", queryStr);
        }
        
        public void LoadVendorInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerId,BuyerName FROM tblBuyerInfo WHERE IsVendor = 1 OR IsBoth = 1";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerName", "BuyerId", queryStr);
        }
        
        public void LoadVendorInfo2(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerId,BuyerName FROM tblBuyerInfo WHERE (IsVendor = 1 OR IsBoth = 1) AND IsActive = 1";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerName", "BuyerId", queryStr);
        } 
        
        public void LoadRequiredWidth(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT RequiredWidth FROM tblProformaInvoice";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "RequiredWidth", "RequiredWidth", queryStr);
        }
        public void LoadRequiredOunceYerd(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT RequiredOunceOryard FROM tblProformaInvoice";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "RequiredOunceOryard", "RequiredOunceOryard", queryStr);
        }
        
        public void LoadRequiredShrinkage(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT Shrinkage FROM tblProformaInvoice";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "Shrinkage", "Shrinkage", queryStr);
        }
        
        public void LoadProformaNo(DropDownList ddl)
        {
            const string queryStr = "SELECT  ProformaMasterId,ProformaInvNo FROM tblProformaMaster";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ProformaInvNo", "ProformaMasterId", queryStr);
        }


        //public bool SaveInfo(ProformaInvoiceDao aInfo)
        //{

        //    try
        //    {
        //        accessManager.SqlConnectionOpen(DataBase.SalesDB);
                
        //        var aSqlParameterlist = new List<SqlParameter>();

        //        aSqlParameterlist.Add(new SqlParameter("@BuyerId", aInfo.BuyerId));
        //        aSqlParameterlist.Add(new SqlParameter("@VendorId", aInfo.VendorId));
        //        aSqlParameterlist.Add(new SqlParameter("@ColorId", aInfo.ColorId));
        //        aSqlParameterlist.Add(new SqlParameter("@ProformaInvNo", aInfo.ProformaInvNo));
        //        aSqlParameterlist.Add(new SqlParameter("@ProformaDate", aInfo.ProformaDate));
        //        aSqlParameterlist.Add(new SqlParameter("@RequiredWidth", aInfo.RequiredWidth));
        //        aSqlParameterlist.Add(new SqlParameter("@RequiredOunceOryard", aInfo.RequiredOunceOryard));
        //        aSqlParameterlist.Add(new SqlParameter("@Shrinkage", aInfo.Shrinkage));
        //        aSqlParameterlist.Add(new SqlParameter("@EntryBy", HttpContext.Current.Session["UserId"].ToString()));
        //        aSqlParameterlist.Add(new SqlParameter("@EntryDate", DateTime.Today));

        //        return accessManager.SaveData("sp_ProformaInvoice_Insert", aSqlParameterlist);
        //    }
        //    catch (Exception e)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        accessManager.SqlConnectionClose();
        //    }
            
            
            
        //}

        public DataTable GetProformaInfo(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_ProformaInvoice_View", aSqlParameterlist, "SSIDB");
        }
        //

        public DataTable CheckProforma(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@ProformaInvoiceId", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_ProformaInvoice_Check", aSqlParameterlist, "SSIDB");
        }


        public DataTable StockVerification(string ProformaInvoiceMId, string ProformaInvoiceDId)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ProformaInvoiceMId", (object)ProformaInvoiceMId ?? DBNull.Value));
            aSqlParameterlist.Add(new SqlParameter("@ProformaInvoiceDId", (object)ProformaInvoiceDId ?? DBNull.Value));
            return _aCommonInternalDal.GetDataTableAction("sp_ProformaInvoice_StockCheck", aSqlParameterlist, "SSIDB");
        }



        public DataTable GetProformaInfoById(Int32 buyerId)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ProformaId", buyerId));
            return _aCommonInternalDal.GetDataTableAction("sp_ProformaInvoice_EditById", aSqlParameterlist, "SSIDB");
        }


        public bool UpdateInfo(ProformaMasterDao aInfo, List<ProformaDetailDao> aDetailDaos)
        {
            bool status = false;

            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                var aSqlParameterlist = new List<SqlParameter>();

                aSqlParameterlist.Add(new SqlParameter("@ProformaId", aInfo.ProformaMasterId));
                aSqlParameterlist.Add(new SqlParameter("@BuyerId", aInfo.BuyerId));
                aSqlParameterlist.Add(new SqlParameter("@VendorId", aInfo.VendorId));
                aSqlParameterlist.Add(new SqlParameter("@ProformaInvNo", aInfo.ProformaInvNo));
                aSqlParameterlist.Add(new SqlParameter("@ProformaDate", aInfo.ProformaDate));

                aSqlParameterlist.Add(new SqlParameter("@BuyerName", (object)aInfo.BuyerName ?? DBNull.Value));
                aSqlParameterlist.Add(new SqlParameter("@VendorName", (object)aInfo.VendorName ?? DBNull.Value));

                aSqlParameterlist.Add(new SqlParameter("@Remarks", (object)aInfo.Remarks ?? DBNull.Value));
                aSqlParameterlist.Add(new SqlParameter("@UpdateBy", HttpContext.Current.Session["UserId"].ToString()));
                aSqlParameterlist.Add(new SqlParameter("@UpdateDate", DateTime.Today));

                status =  accessManager.UpdateData("sp_ProformaInvoice_Update", aSqlParameterlist);

                bool status2 = DeleteProformaDetailById(aInfo.ProformaMasterId);

                if (status)
                {
                    foreach (var aDaos in aDetailDaos)
                    {
                        var detailSqlParameterlist = new List<SqlParameter>();

                        detailSqlParameterlist.Add(new SqlParameter("@SisCodeId", aDaos.SisCodeId));
                        detailSqlParameterlist.Add(new SqlParameter("@ProformaMasterId", aInfo.ProformaMasterId));
                        detailSqlParameterlist.Add(new SqlParameter("@RequiredOunceOryard", aDaos.RequiredOunceOryard));
                        detailSqlParameterlist.Add(new SqlParameter("@RequiredWidth", aDaos.RequiredWidth));
                        detailSqlParameterlist.Add(new SqlParameter("@ShrinkageLengthMin", aDaos.ShrinkageLengthMin));
                        detailSqlParameterlist.Add(new SqlParameter("@ShrinkageLengthMax", aDaos.ShrinkageLengthMax));
                        detailSqlParameterlist.Add(new SqlParameter("@ShrinkageWidthMin", aDaos.ShrinkageWidthMin));
                        detailSqlParameterlist.Add(new SqlParameter("@ShrinkageWidthMax", aDaos.ShrinkageWidthMax));
                        detailSqlParameterlist.Add(new SqlParameter("@ColorId", aDaos.ColorId));
                        detailSqlParameterlist.Add(new SqlParameter("@FabricName", aDaos.FabricName));
                        detailSqlParameterlist.Add(new SqlParameter("@ColorName", aDaos.ColorName));
                        detailSqlParameterlist.Add(new SqlParameter("@EntryBy", HttpContext.Current.Session["UserId"].ToString()));
                        detailSqlParameterlist.Add(new SqlParameter("@EntryDate", DateTime.Today));

                        status = accessManager.SaveData("sp_Proforma_DetailInsert", detailSqlParameterlist);
                    }
                }

              
            }
            catch (Exception e)
            {
                status = false;
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

            return status;
        }


        public bool UpdateSingleInfo( ProformaDetailDao aDetailDaos)
        {
            bool status = false;

            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
             
                        var detailSqlParameterlist = new List<SqlParameter>();

                        detailSqlParameterlist.Add(new SqlParameter("@SisCodeId", aDetailDaos.SisCodeId));
                        detailSqlParameterlist.Add(new SqlParameter("@ProformaMasterId", aDetailDaos.ProformaMasterId));
                        detailSqlParameterlist.Add(new SqlParameter("@RequiredOunceOryard", aDetailDaos.RequiredOunceOryard));
                        detailSqlParameterlist.Add(new SqlParameter("@RequiredWidth", aDetailDaos.RequiredWidth));
                        detailSqlParameterlist.Add(new SqlParameter("@ShrinkageLengthMin", aDetailDaos.ShrinkageLengthMin));
                        detailSqlParameterlist.Add(new SqlParameter("@ShrinkageLengthMax", aDetailDaos.ShrinkageLengthMax));
                        detailSqlParameterlist.Add(new SqlParameter("@ShrinkageWidthMin", aDetailDaos.ShrinkageWidthMin));
                        detailSqlParameterlist.Add(new SqlParameter("@ShrinkageWidthMax", aDetailDaos.ShrinkageWidthMax));
                        detailSqlParameterlist.Add(new SqlParameter("@ColorId", aDetailDaos.ColorId));
                        detailSqlParameterlist.Add(new SqlParameter("@FabricName", aDetailDaos.FabricName));
                        detailSqlParameterlist.Add(new SqlParameter("@ColorName", aDetailDaos.ColorName));
                        detailSqlParameterlist.Add(new SqlParameter("@Quantity", (object)aDetailDaos.Quantity ?? DBNull.Value));
                        detailSqlParameterlist.Add(new SqlParameter("@UnitPrice", (object)aDetailDaos.UnitPrice ?? DBNull.Value));
                        detailSqlParameterlist.Add(new SqlParameter("@EntryBy", HttpContext.Current.Session["UserId"].ToString()));
                        detailSqlParameterlist.Add(new SqlParameter("@EntryDate", DateTime.Today));

                        status = accessManager.SaveData("sp_Proforma_single_Update", detailSqlParameterlist);
                    
               
            }
            catch (Exception e)
            {
                status = false;
                throw;
            }
            finally
            {
                status = true;
                accessManager.SqlConnectionClose();
            }

            return status;
        }


        public bool RowWise_UpdateInfo(ProformaDetailDao aInfo)
        {
            bool status = false;

            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                var aSqlParameterlist = new List<SqlParameter>();
                aSqlParameterlist.Add(new SqlParameter("@ProformaMasterId", aInfo.ProformaMasterId));
                aSqlParameterlist.Add(new SqlParameter("@SisCode", aInfo.SisCode));
                aSqlParameterlist.Add(new SqlParameter("@RequiredWidth", (object)aInfo.RequiredWidth ?? DBNull.Value));
                aSqlParameterlist.Add(new SqlParameter("@RequiredOunceOryard", (object)aInfo.RequiredOunceOryard ?? DBNull.Value));
                aSqlParameterlist.Add(new SqlParameter("@ShrinkageLengthMax", (object)aInfo.ShrinkageLengthMax ??DBNull.Value));
                aSqlParameterlist.Add(new SqlParameter("@ShrinkageLengthMin", (object)aInfo.ShrinkageLengthMin ?? DBNull.Value));
                aSqlParameterlist.Add(new SqlParameter("@ShrinkageWidthMax", (object)aInfo.ShrinkageWidthMax ?? DBNull.Value));
                aSqlParameterlist.Add(new SqlParameter("@ShrinkageWidthMin", (object)aInfo.ShrinkageWidthMin ?? DBNull.Value));
                status = accessManager.UpdateData("sp_Proforma_Detail_Update", aSqlParameterlist);

            }
            catch (Exception e)
            {
                status = false;
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

            return status;
        }


        public bool DeleteProformaDetailById(int proformMasterid)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@ProformaId", proformMasterid));
            return _aCommonInternalDal.UpdateAction("sp_ProformaInvoice_DetailDeleteById", aSqlParameterlist);
        }

        public bool DeleteProformaInfoById(ProformaInvoiceDao aInfo)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@ProformaId", aInfo.ProformaId));
            return _aCommonInternalDal.UpdateAction("sp_ProformaInvoice_DeleteById", aSqlParameterlist);
        }

        public void LoadColor(DropDownList ddl)
        {
            const string queryStr = "SELECT ColorId,ColorName FROM tblFabricColor";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ColorName", "ColorId", queryStr);
        }

        public void LoadFabric(DropDownList ddl)
        {
            const string queryStr = "SELECT FabricId,FabricShortName+ ' : ' + FebricDescription AS Fabric FROM tblFabricInfo ORDER BY FabricId";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "Fabric", "FabricId", queryStr);
        }

        public Int32 SaveInfo(ProformaMasterDao aInfo, List<ProformaDetailDao> aDetailDaos)
        {

            int masterId = 0;
            int detailId = 0;

            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);

                var aSqlParameterlist = new List<SqlParameter>();

                aSqlParameterlist.Add(new SqlParameter("@BuyerId", aInfo.BuyerId));
                aSqlParameterlist.Add(new SqlParameter("@VendorId", aInfo.VendorId));
                aSqlParameterlist.Add(new SqlParameter("@ProformaInvNo", aInfo.ProformaInvNo));
                aSqlParameterlist.Add(new SqlParameter("@ProformaDate", aInfo.ProformaDate));

                aSqlParameterlist.Add(new SqlParameter("@BuyerName", (object)aInfo.BuyerName ?? DBNull.Value));
                aSqlParameterlist.Add(new SqlParameter("@VendorName", (object)aInfo.VendorName ?? DBNull.Value));

                aSqlParameterlist.Add(new SqlParameter("@Remarks", (object)aInfo.Remarks ?? DBNull.Value));
                aSqlParameterlist.Add(new SqlParameter("@EntryBy", HttpContext.Current.Session["UserId"].ToString()));
                aSqlParameterlist.Add(new SqlParameter("@EntryDate", DateTime.Today));
                aSqlParameterlist.Add(new SqlParameter("@MarketingPersonId", (object)aInfo.MarketingPersonId ?? DBNull.Value));

                masterId = accessManager.SaveDataReturnPrimaryKey("sp_Proforma_MasterInsert", aSqlParameterlist);

                foreach (var aDaos in aDetailDaos)
                {
                    var detailSqlParameterlist = new List<SqlParameter>();

                    detailSqlParameterlist.Add(new SqlParameter("@SisCodeId", aDaos.SisCodeId));
                    detailSqlParameterlist.Add(new SqlParameter("@ProformaMasterId", masterId));
                    detailSqlParameterlist.Add(new SqlParameter("@RequiredOunceOryard", aDaos.RequiredOunceOryard));
                    detailSqlParameterlist.Add(new SqlParameter("@RequiredWidth", aDaos.RequiredWidth));
                    detailSqlParameterlist.Add(new SqlParameter("@ShrinkageLengthMin", aDaos.ShrinkageLengthMin));
                    detailSqlParameterlist.Add(new SqlParameter("@ShrinkageLengthMax", aDaos.ShrinkageLengthMax));
                    detailSqlParameterlist.Add(new SqlParameter("@ShrinkageWidthMin", aDaos.ShrinkageWidthMin));
                    detailSqlParameterlist.Add(new SqlParameter("@ShrinkageWidthMax", aDaos.ShrinkageWidthMax));
                    detailSqlParameterlist.Add(new SqlParameter("@ColorId", aDaos.ColorId));
                    detailSqlParameterlist.Add(new SqlParameter("@FabricName", aDaos.FabricName));
                    detailSqlParameterlist.Add(new SqlParameter("@ColorName", aDaos.ColorName));
                    detailSqlParameterlist.Add(new SqlParameter("@Quantity", (object)aDaos.Quantity ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@UnitPrice", (object)aDaos.UnitPrice ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@EntryBy", HttpContext.Current.Session["UserId"].ToString()));
                    detailSqlParameterlist.Add(new SqlParameter("@EntryDate", DateTime.Today));

                    detailId = accessManager.SaveDataReturnPrimaryKey("sp_Proforma_DetailInsert", detailSqlParameterlist);
                }
            }
            catch (Exception e)
            {
                masterId = 0;
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

            return masterId;
        }


        public bool Delete_Proforma_DetailById(string proformMasterid, string ProforDetailsId)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ProformaId", proformMasterid));
            aSqlParameterlist.Add(new SqlParameter("@ProforDetailsId", ProforDetailsId));
            return _aCommonInternalDal.DeleteAction("sp_ProformaInvoice_Delete_ById", aSqlParameterlist);
        }

    }
}
