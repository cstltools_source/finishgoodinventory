﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Library.DAL.InternalCls;

namespace Library.DAL.FGInventory_DAL
{
    public class FinishedGoodDeliveryDal
    {
        private readonly ClsCommonInternalDAL _aCommonInternalDal = new ClsCommonInternalDAL();
        public DataTable RPT_FinishedGoodDelivery(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_Report_FinishedGoodDelivery", aSqlParameterlist, "SSIDB");
        }
    }
}
