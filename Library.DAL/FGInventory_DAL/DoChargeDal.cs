﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.DataManager;
using Library.DAL.InternalCls;
using Library.DAO.FGInvrntory_DAO;

namespace Library.DAL.FGInventory_DAL
{
    public class DoChargeDal
    {

        private readonly ClsCommonInternalDAL _aCommonInternalDal = new ClsCommonInternalDAL();
        private DataAccessManager accessManager = new DataAccessManager();



        public DataTable RPT_DeliveryAndDoStatus(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_Report_DeliveryAndDOStatus", aSqlParameterlist, "SSIDB");
        }

        public DataTable GetDOChargeInfo(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_DOCharge_View", aSqlParameterlist, "SSIDB");
        }


        public DataTable RPT_DOCharge(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_DOCharge_Report", aSqlParameterlist, "SSIDB");
        }


        public void LoadPIInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT PM.ProformaMasterId, PM.ProformaInvNo FROM tblIssueForPackegingMaster IM LEFT JOIN tblProformaMaster PM ON IM.IssueMasterId = PM.ProformaMasterId WHERE PM.ProformaMasterId IS NOT NULL";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ProformaInvNo", "ProformaMasterId", queryStr);
        }

        public int SaveFabricInfo(DOCharge aInfo)
        {
            int id = 0;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                var aSqlParameterlist = new List<SqlParameter>();
                aSqlParameterlist.Add(new SqlParameter("@BuyerId", aInfo.BuyerId));
                aSqlParameterlist.Add(new SqlParameter("@DONo", aInfo.DONo));
                aSqlParameterlist.Add(new SqlParameter("@DODate", aInfo.DODate));
                aSqlParameterlist.Add(new SqlParameter("@LCNo", aInfo.LCNo));
                aSqlParameterlist.Add(new SqlParameter("@LCDate", aInfo.LCDate));
                aSqlParameterlist.Add(new SqlParameter("@ShipmentDate", aInfo.ShipmentDate));
                aSqlParameterlist.Add(new SqlParameter("@MarketingPersonId", aInfo.MarketingPersonId));
                aSqlParameterlist.Add(new SqlParameter("@BuyerId", aInfo.BuyerId));
                aSqlParameterlist.Add(new SqlParameter("@CurrencyId", aInfo.CurrencyId));
                aSqlParameterlist.Add(new SqlParameter("@EntryBy", HttpContext.Current.Session["UserId"].ToString()));
                aSqlParameterlist.Add(new SqlParameter("@EntryDate", DateTime.Today));
                aSqlParameterlist.Add(new SqlParameter("@IsActive", true));

                id = accessManager.SaveDataReturnPrimaryKey("sp_Fabric_Insert", aSqlParameterlist);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

            return id;
        }


        public Int32 SaveDOCharge(DOCharge aInfo, List<DOChargeDetails> aDetailDaos)
        {
            int masterId = 0;
            int detailId = 0;
            bool status = false;

            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                var aSqlParameterlist = new List<SqlParameter>();

                aSqlParameterlist.Add(new SqlParameter("@DOChargeId", aInfo.DOChargeId));
                aSqlParameterlist.Add(new SqlParameter("@DONo", aInfo.DONo));
                aSqlParameterlist.Add(new SqlParameter("@DODate", aInfo.DODate));
                aSqlParameterlist.Add(new SqlParameter("@LCNo", aInfo.LCNo));
                aSqlParameterlist.Add(new SqlParameter("@LCDate", aInfo.LCDate));
                aSqlParameterlist.Add(new SqlParameter("@ShipmentDate", aInfo.ShipmentDate));
                aSqlParameterlist.Add(new SqlParameter("@MarketingPersonId", aInfo.MarketingPersonId));
                aSqlParameterlist.Add(new SqlParameter("@BuyerId", aInfo.BuyerId));
                //aSqlParameterlist.Add(new SqlParameter("@CurrencyId", aInfo.CurrencyId));
                aSqlParameterlist.Add(new SqlParameter("@EntryBy", HttpContext.Current.Session["UserId"].ToString()));
 
                aSqlParameterlist.Add(new SqlParameter("@IsActive", true));

                masterId = accessManager.SaveDataReturnPrimaryKey("sp_DOMaster_Insert", aSqlParameterlist);

                foreach (var aDaos in aDetailDaos)
                {
                    var detailSqlParameterlist = new List<SqlParameter>();

                    detailSqlParameterlist.Add(new SqlParameter("@DOChargeId", (object)masterId ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@ItemDescription", (object)aDaos.ItemDescription ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@ProformaMasterId", (object)aDaos.ProformaMasterId ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@PIDate", (object)aDaos.PIDate ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@Quantity", (object)aDaos.Quantity ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@UnitPrice", (object)aDaos.UnitPrice ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@DOAmount", (object)aDaos.DOAmount ?? DBNull.Value));
                    detailId = accessManager.SaveDataReturnPrimaryKey("sp_DODetails_Insert", detailSqlParameterlist);
                }

            }
            catch (Exception e)
            {
                masterId = 0;
                accessManager.SqlConnectionClose();
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

            return masterId;
        }


    }
}
