﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.DataManager;
using Library.DAL.InternalCls;
using Library.DAO.FGInvrntory_DAO;

namespace Library.DAL.FGInventory_DAL
{
   public class QAStockReceiveDal
    {
        private readonly ClsCommonInternalDAL _aCommonInternalDal = new ClsCommonInternalDAL();


        private DataAccessManager accessManager = new DataAccessManager();

        public void LoadBuyerInfo2(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerId,BuyerName FROM tblBuyerInfo WHERE (IsBuyer = 1 OR IsBoth = 1) AND IsActive = 1";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerName", "BuyerId", queryStr);
        }



        public void LoadRollNo(DropDownList ddl)
        {
            const string queryStr = "SELECT StockReceiveDetailId, RollNo FROM tblQaStockReceiveDetail WITH (NOLOCk)";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "RollNo", "StockReceiveDetailId", queryStr);
        }

        public void LoadVendorInfo2(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerId,BuyerName FROM tblBuyerInfo WHERE (IsVendor = 1 OR IsBoth = 1) AND IsActive = 1";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerName", "BuyerId", queryStr);
        }

        public void LoadProformaInvoice(DropDownList ddl)
        {
             string queryStr = "SELECT ProformaId,ProformaInvNo + '(' + ProformaCode + ' : ' + BuyerName + ' )' AS PINo FROM tblProformaInvoice AS PI LEFT JOIN tblBuyerInfo AS BI ON PI.BuyerId = BI.BuyerId";
             _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "PINo", "ProformaId", queryStr);
        }

        public void LoadShadeGrade(DropDownList ddl)
        {
            const string queryStr = "SELECT * FROM tblShadeGrades";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ShadeGrade", "ShadeGrade", queryStr);
        }


        public DataTable LoadFabricInfo()
        {
            string queryStr = @"SELECT  DISTINCT FabricName FROM tblFabricInfo WHERE FabricName IS NOT NULL";
            return _aCommonInternalDal.DataContainerDataTable(queryStr, "SSIDB");
        }


        public void LoadFabricName(DropDownList ddl)
        {
            const string queryStr = "SELECT FabricId,FabricName FROM tblFabricInfo ";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "FabricName", "FabricId", queryStr);
        }

        public void LoadRollName(DropDownList ddl)
        {
            const string queryStr = "SELECT RollNo FROM tblQaStockReceiveDetail";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "RollNo", "RollNo", queryStr);
        }

        public void LoadFabricSet(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT FabricSet FROM tblQaStockReceiveDetail";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "FabricSet", "FabricSet", queryStr);
        }


        public void LoadQALocation(DropDownList ddl)
        {
            const string queryStr = "SELECT LocationId, LocationName FROM tblLocationInfo WHERE LocationFor=1 AND IsActive=1";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "LocationName", "LocationId", queryStr);
        }


        public void LoadProblemCouses(DropDownList ddl)
        {
            const string queryStr = " SELECT ProbleamCauseId, ProbleamCause FROM tblProbleamCauses";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ProbleamCause", "ProbleamCauseId", queryStr);
        }


        public void LoadShift(DropDownList ddl)
        {
            const string queryStr = "SELECT ShiftId,ShiftTitle FROM tblShift WHERE IsActive=1";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ShiftTitle", "ShiftId", queryStr);
        }

        public void LoadBuyerInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerId,BuyerName FROM tblBuyerInfo WHERE IsBuyer = 1";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerName", "BuyerId", queryStr);
        }

        public void LoadVendorInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerId,BuyerName FROM tblBuyerInfo WHERE IsVendor = 1 ";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerName", "BuyerId", queryStr);
        }

        public DataTable GetPIInfo(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@ProformaId", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_ProformaInvoice_InfoById", aSqlParameterlist, "SSIDB");
        }


        public DataTable GetReceiveMovementCategory(string Category)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@CategoriesGroup", Category));
            return _aCommonInternalDal.GetDataTableAction("sp_Movement_ListForDDL", aSqlParameterlist, "SSIDB");
        }


        public DataTable GetLocationName( )
        {

            return _aCommonInternalDal.GetDataTableAction("sp_Get_Location", "SSIDB");
        }

        public DataTable Get_UOM_All( )
        {
            return _aCommonInternalDal.GetDataTableAction("sp_UOM_GetAll", "SSIDB");
        }


        public DataTable GetQAStockReceiveMasterById(Int32 StockReceiveMasterId)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@StockReceiveMasterId", StockReceiveMasterId));
            return _aCommonInternalDal.GetDataTableAction("sp_StockReceiveMaster_EditById", aSqlParameterlist, "SSIDB");
        }


        public DataTable GetQAStockReceiveByRoll(string StockReceiveDetailsId)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@StockReceiveDetailsId", StockReceiveDetailsId));
            return _aCommonInternalDal.GetDataTableAction("sp_StockReceiveDetails_EditByRoll", aSqlParameterlist, "SSIDB");
        }

        public Int32 SaveStockReceiveMaster(QaStockReceiveMasterDao aInfo, List<QaStockReceiveDetailDao> aDetailDaos)
        {
            int masterId = 0;
            int detailId = 0;
            bool status = false;

            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                var aSqlParameterlist = new List<SqlParameter>();
                aSqlParameterlist.Add(new SqlParameter("@StockReceiveMasterId", (object)aInfo.StockReceiveMasterId ?? DBNull.Value));
                aSqlParameterlist.Add(new SqlParameter("@ProformaInvId", (object)aInfo.ProformaInvId ?? DBNull.Value));
                aSqlParameterlist.Add(new SqlParameter("@ProformaDetailId", (object)aInfo.ProformaDetailId ?? DBNull.Value));
                aSqlParameterlist.Add(new SqlParameter("@ShiftId", (object)aInfo.ShiftId ?? DBNull.Value));
                aSqlParameterlist.Add(new SqlParameter("@ProductionDate", (object)aInfo.ProductionDate ?? DBNull.Value));
                aSqlParameterlist.Add(new SqlParameter("@EntryBy", HttpContext.Current.Session["UserId"].ToString()));
                aSqlParameterlist.Add(new SqlParameter("@EntryDate", aInfo.EntryDate));
                aSqlParameterlist.Add(new SqlParameter("@RcvdTypeId", (object)aInfo.RcvdTypeId ?? DBNull.Value));
                aSqlParameterlist.Add(new SqlParameter("@Remarks", (object)aInfo.Remarks ?? DBNull.Value));
                if (aInfo.StockReceiveMasterId == 0)
                {
                    masterId = accessManager.SaveDataReturnPrimaryKey("sp_StockReceiveMaster_Insert", aSqlParameterlist);
                }
                else
                {
                    status = accessManager.UpdateData("sp_StockReceiveMaster_Update", aSqlParameterlist);
                    if (status)
                    {
                        masterId = aInfo.StockReceiveMasterId;
                    }
                }

                foreach (var aDaos in aDetailDaos)
                {
                    var detailSqlParameterlist = new List<SqlParameter>();

                    detailSqlParameterlist.Add(new SqlParameter("@StockReceiveMasterId", (object)masterId ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@FabricName", (object)aDaos.FabricName ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@ColorId", (object)aDaos.ColorId ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@RequiredWidth", (object)aDaos.RequiredWidth ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@RequiredOunceYard", (object)aDaos.RequiredOunceYard ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@ShrinkageLengthMin", (object)aDaos.ShrinkageLengthMin ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@ShrinkageLengthMax", (object)aDaos.ShrinkageLengthMax ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@ShrinkageWidthMin", (object)aDaos.ShrinkageWidthMin ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@ShrinkageWidthMax", (object)aDaos.ShrinkageWidthMax ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@FabricSet", (object)aDaos.FabricSet ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@Beam", (object)aDaos.Beam ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@GreigeRollNo", (object)aDaos.GreigeRollNo ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@RollNo", (object)aDaos.RollNo ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@DefectPoint", (object)aDaos.DefectPoint ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@ActualWidth", (object)aDaos.ActualWidth ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@Piece", (object)aDaos.Piece ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@PWLength", (object)aDaos.PWLength ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@Quantity", (object)aDaos.Quantity ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@OperatorName", (object)aDaos.OperatorName ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@MCNo", (object)aDaos.MCNo ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@FL", (object)aDaos.FL ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@PPHSY", (object)aDaos.PPHSY ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@GrossWeight", (object)aDaos.GrossWeight ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@NetWeight", (object)aDaos.NetWeight ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@ProblemCause", (object)aDaos.ProblemCause ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@QALocationId", (object)aDaos.QALocationId ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@Remarks", (object)aDaos.Remarks ?? DBNull.Value));
                    detailSqlParameterlist.Add(new SqlParameter("@FabricCategory", (object)aDaos.FabricCategory ?? DBNull.Value));
                    detailId = accessManager.SaveDataReturnPrimaryKey("sp_StockReceiveDetail_Insert", detailSqlParameterlist);
                }

            }
            catch (Exception e)
            {
                masterId = 0;
                accessManager.SqlConnectionClose();
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

            return masterId;
        }

        public bool UpdateQAStockReceive(QaStockReceiveDetailDao aDaos)
        {
            bool status = false;

            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);

                var detailSqlParameterlist = new List<SqlParameter>();

                detailSqlParameterlist.Add(new SqlParameter("@StockReceiveMasterId", aDaos.StockReceiveDetailId));
                detailSqlParameterlist.Add(new SqlParameter("@FabricName", (object) aDaos.FabricName ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@ColorId", (object) aDaos.ColorId ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@RequiredWidth", (object) aDaos.RequiredWidth ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@RequiredOunceYard", (object) aDaos.RequiredOunceYard ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@ShrinkageLengthMin", (object) aDaos.ShrinkageLengthMin ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@ShrinkageLengthMax", (object) aDaos.ShrinkageLengthMax ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@ShrinkageWidthMin", (object) aDaos.ShrinkageWidthMin ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@ShrinkageWidthMax", (object) aDaos.ShrinkageWidthMax ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@FabricSet", (object) aDaos.FabricSet ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@Beam", (object) aDaos.Beam ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@GreigeRollNo", (object) aDaos.GreigeRollNo ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@RollNo", (object) aDaos.RollNo ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@DefectPoint", (object) aDaos.DefectPoint ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@ActualWidth", (object) aDaos.ActualWidth ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@Piece", (object) aDaos.Piece ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@PWLength", (object) aDaos.PWLength ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@Quantity", (object) aDaos.Quantity ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@OperatorName", (object) aDaos.OperatorName ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@MCNo", (object) aDaos.MCNo ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@FL", (object) aDaos.FL ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@PPHSY", (object) aDaos.PPHSY ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@GrossWeight", (object) aDaos.GrossWeight ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@NetWeight", (object) aDaos.NetWeight ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@ProblemCause", (object) aDaos.ProblemCause ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@QALocationId", (object) aDaos.QALocationId ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@Remarks", (object) aDaos.Remarks ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@FabricCategory", (object) aDaos.FabricCategory ?? DBNull.Value));
                status= accessManager.UpdateData("sp_StockReceiveDetail_Update", detailSqlParameterlist);
            }
            catch (Exception)
            {
                status = false;
                accessManager.SqlConnectionClose();

            }
            finally
            {
                status = true;
                accessManager.SqlConnectionClose();
            }

            return status;
        }

        public bool DetailsUpdate(QaStockReceiveDetailDao aDaos)
        {
            bool Status = false;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                var detailSqlParameterlist = new List<SqlParameter>();
                detailSqlParameterlist.Add(new SqlParameter("@StockReceiveDetailId", (object)aDaos.StockReceiveDetailId ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@FabricSet", (object)aDaos.FabricSet ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@Beam", (object)aDaos.Beam ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@GreigeRollNo", (object)aDaos.GreigeRollNo ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@DefectPoint", (object)aDaos.DefectPoint ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@ActualWidth", (object)aDaos.ActualWidth ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@Piece", (object)aDaos.Piece ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@PWLength", (object)aDaos.PWLength ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@Quantity", (object)aDaos.Quantity ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@OperatorName", (object)aDaos.OperatorName ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@MCNo", (object)aDaos.MCNo ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@FL", (object)aDaos.FL ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@PPHSY", (object)aDaos.PPHSY ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@GrossWeight", (object)aDaos.GrossWeight ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@NetWeight", (object)aDaos.NetWeight ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@ProblemCause", (object)aDaos.ProblemCause ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@QALocationId", (object)aDaos.QALocationId ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@Remarks", (object)aDaos.Remarks ?? DBNull.Value));
                detailSqlParameterlist.Add(new SqlParameter("@UserID", HttpContext.Current.Session["UserId"].ToString()));
                Status = accessManager.UpdateData("sp_StockReceiveDetails_Update", detailSqlParameterlist);

            }
            catch (Exception)
            {
                Status = false;
                accessManager.SqlConnectionClose();
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

            return Status;
        }



        public DataTable GetStockReceiveInfo(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_StockReceiveMaster_View", aSqlParameterlist, "SSIDB");
        }


        public DataTable GetStockReceiveCheck(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_StockReceiveCHECK_View", aSqlParameterlist, "SSIDB");
        }

        public bool DeleteQAStockMasterById(QaStockReceiveMasterDao aInfo)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@StockReceiveMasterId", aInfo.StockReceiveMasterId));
            return _aCommonInternalDal.DeleteAction("sp_StockReceiveMaster_DeleteById", aSqlParameterlist);
        }


        public void LoadPiNo(DropDownList ddl, string pram)
        {
            string queryStr = @"SELECT ProformaMasterId,ProformaInvNo + ' ( ' + CASE WHEN BI.BuyerName IS NULL THEN VEN.BuyerName ELSE BI.BuyerName END  + ' ) ' AS ProformaInvNo 
            FROM tblProformaMaster AS PFI
            LEFT JOIN tblBuyerInfo AS BI ON PFI.BuyerId = BI.BuyerId 
            LEFT JOIN tblBuyerInfo AS VEN ON PFI.VendorId = VEN.BuyerId WHERE PFI.ProformaMasterId IS NOT NULL " + pram + "";

            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ProformaInvNo", "ProformaMasterId", queryStr);
        }



       public void LoadSisCodeByPi(DropDownList ddl, Int32 piId)
       {
           string queryStr = @"SELECT DISTINCT CAST(ProformaMasterId AS nvarchar)+':'+FabricName AS FabricDescription,FabricName FROM tblProformaDetail AS PD
                               LEFT JOIN tblFabricInfo AS FBCI ON PD.SisCodeId = FBCI.FabricId
                               WHERE FabricName IS NOT NULL AND ProformaMasterId = " + piId;
           _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "FabricName", "FabricDescription", queryStr);
       }



       public DataTable LoadSisCodeByPiD( Int32 piId)
       {
           string queryStr = @"SELECT DISTINCT FabricName AS FabricDescription,FabricName FROM tblProformaDetail AS PD
                               LEFT JOIN tblFabricInfo AS FBCI ON PD.SisCodeId = FBCI.FabricId
                               WHERE FabricName IS NOT NULL AND ProformaMasterId = " + piId;
         return  _aCommonInternalDal.DataContainerDataTable(queryStr);
       }


       public void LoadColor(DropDownList ddl)
       {

           string queryStr = @"SELECT ColorId,ColorName FROM tblFabricColor ORDER BY ColorName";
           _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ColorName", "ColorId", queryStr);
       }

       public DataTable GetFabricInfoById(int proformaMasterId,string fabricName)
       {
           var aSqlParameterlist = new List<SqlParameter>();

           aSqlParameterlist.Add(new SqlParameter("@ProformaMasterId", proformaMasterId));
           aSqlParameterlist.Add(new SqlParameter("@FabricName", fabricName));

           return _aCommonInternalDal.GetDataTableAction("sp_Proforma_DetailById", aSqlParameterlist, "SSIDB");
       }

       public DataTable LoadPiNoAll( )
       {
           return _aCommonInternalDal.GetDataTableAction("sp_ProformaInvoice_ALL_DDL", "SSIDB");
       }

       public DataTable LoadBuyerVendor(string id)
       {

           var aSqlParameterlist = new List<SqlParameter>();

           aSqlParameterlist.Add(new SqlParameter("@ProformaId", id));
           return _aCommonInternalDal.GetDataTableAction("sp_Proforma_BuyerVendorId", aSqlParameterlist, "SSIDB");


           
       }

    }
}
