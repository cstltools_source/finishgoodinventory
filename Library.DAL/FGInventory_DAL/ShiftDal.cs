﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.FGInvrntory_DAO;

namespace Library.DAL.FGInventory_DAL
{
   public class ShiftDal
    {
        private readonly ClsCommonInternalDAL _aCommonInternalDal = new ClsCommonInternalDAL();

        public void LoadBuyerType(DropDownList ddl)
        {
            const string queryStr = "SELECT ShiftId,ShiftTitle FROM tblShift";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ShiftTitle", "ShiftId", queryStr);
        }

        public DataTable CheckBuyerExistOrNot(string buyerName)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@BuyerName", buyerName));
            return _aCommonInternalDal.GetDataTableAction("@BuyerName", aSqlParameterlist, "SSIDB");
        }

        public Int32 SaveShift(ShiftDao aInfo)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            //aSqlParameterlist.Add(new SqlParameter("@ShiftId", aInfo.ShiftId));
            aSqlParameterlist.Add(new SqlParameter("@ShiftTitle", aInfo.ShiftTitle));
            aSqlParameterlist.Add(new SqlParameter("@ShiftTitleInTime", aInfo.ShiftTitleInTime));
            aSqlParameterlist.Add(new SqlParameter("@ShiftTitleInEndTime", aInfo.ShiftTitleInEndTime));
            aSqlParameterlist.Add(new SqlParameter("@EntryBy", HttpContext.Current.Session["UserId"].ToString()));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", DateTime.Now));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", true));
            return _aCommonInternalDal.SaveAction("sp_Shift_Insert", aSqlParameterlist, "@ShiftId");
        }

        public bool UpdateShiftInfo(ShiftDao aInfo)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@ShiftId", aInfo.ShiftId));
            aSqlParameterlist.Add(new SqlParameter("@ShiftTitle", aInfo.ShiftTitle));
            aSqlParameterlist.Add(new SqlParameter("@ShiftTitleInTime", aInfo.ShiftTitleInTime));
            aSqlParameterlist.Add(new SqlParameter("@ShiftTitleInEndTime", aInfo.ShiftTitleInEndTime));
            aSqlParameterlist.Add(new SqlParameter("@UpdateBy", HttpContext.Current.Session["UserId"].ToString()));
            aSqlParameterlist.Add(new SqlParameter("@UpdateDate", DateTime.Now));
            return _aCommonInternalDal.UpdateAction("sp_Shift_Update", aSqlParameterlist);
        }

        public DataTable GetShiftInfo(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_Shift_View", aSqlParameterlist, "SSIDB");
        }

        public bool ActiveInactiveShiftInfo(ShiftDao aInfo)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@ShiftId", aInfo.ShiftId));
            aSqlParameterlist.Add(new SqlParameter("@ActiveInactiveBy", HttpContext.Current.Session["UserId"].ToString()));

            return _aCommonInternalDal.UpdateAction("sp_Shift_ActiveInactiveById", aSqlParameterlist);
        }


        public DataTable GetShiftInfoById(Int32 buyerId)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ShiftId", buyerId));
            return _aCommonInternalDal.GetDataTableAction("sp_Shift_EditById", aSqlParameterlist, "SSIDB");
        }


        public bool DeleteShiftInfo(ShiftDao aInfo)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ShiftId", aInfo.ShiftId));
            return _aCommonInternalDal.DeleteAction("sp_Shift_DeleteById", aSqlParameterlist);
        }

    }
}
