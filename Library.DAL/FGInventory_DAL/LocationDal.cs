﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.DataManager;
using Library.DAL.InternalCls;
using Library.DAO.FGInvrntory_DAO;

namespace Library.DAL.FGInventory_DAL
{
   public class LocationDal
    {
        private readonly ClsCommonInternalDAL _aCommonInternalDal = new ClsCommonInternalDAL();
        private DataAccessManager accessManager = new DataAccessManager();

        public void LoadBuyerType(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerTypeId,BuyerTypeName FROM tblBuyerTypeInfo";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerTypeName", "BuyerTypeId", queryStr);
        }

        public DataTable CheckBuyerExistOrNot(string buyerName)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@BuyerName", buyerName));
            return _aCommonInternalDal.GetDataTableAction("@BuyerName", aSqlParameterlist, "SSIDB");
        }

        public bool SaveLocationInfo(LocationDao aInfo)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                
                var aSqlParameterlist = new List<SqlParameter>();

                // aSqlParameterlist.Add(new SqlParameter("@LocationId", aInfo.LocationId));
                aSqlParameterlist.Add(new SqlParameter("@LocationName", aInfo.LocationName));
                aSqlParameterlist.Add(new SqlParameter("@LocationFor", aInfo.LocationFor));
                aSqlParameterlist.Add(new SqlParameter("@EntryBy", HttpContext.Current.Session["UserId"].ToString()));
                aSqlParameterlist.Add(new SqlParameter("@EntryDate", DateTime.Now));
                aSqlParameterlist.Add(new SqlParameter("@IsActive", true));

                return accessManager.SaveData("sp_Location_Insert", aSqlParameterlist);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

            
        }

        public DataTable GetLocationInfo(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_Location_View", aSqlParameterlist, "SSIDB");
        }


        public DataTable GetLocationFor( )
        {
            return _aCommonInternalDal.GetDataTableAction("sp_LocationFor_Get", "SSIDB");
        }

        public DataTable GetLocationInfoById(Int32 buyerId)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LocationId", buyerId));
            return _aCommonInternalDal.GetDataTableAction("sp_Location_EditById", aSqlParameterlist, "SSIDB");
        }


        public bool UpdateLocationInfo(LocationDao aInfo)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                var aSqlParameterlist = new List<SqlParameter>();

                aSqlParameterlist.Add(new SqlParameter("@LocationId", aInfo.LocationId));
                aSqlParameterlist.Add(new SqlParameter("@LocationName", aInfo.LocationName));
                aSqlParameterlist.Add(new SqlParameter("@LocationFor", aInfo.LocationFor));
                aSqlParameterlist.Add(new SqlParameter("@UpdateDate", DateTime.Now));
                aSqlParameterlist.Add(new SqlParameter("@UpdateBy", HttpContext.Current.Session["UserId"].ToString()));
                return accessManager.UpdateData("sp_Location_Update", aSqlParameterlist);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            
        }


        public bool ActiveInactiveLocationInfo(LocationDao aInfo)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                var aSqlParameterlist = new List<SqlParameter>();

                aSqlParameterlist.Add(new SqlParameter("@LocationId", aInfo.LocationId));
                aSqlParameterlist.Add(new SqlParameter("@ActiveInactiveBy", HttpContext.Current.Session["UserId"].ToString()));

                return accessManager.UpdateData("sp_Location_ActiveInactiveById", aSqlParameterlist);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

            
        }


        public bool DeleteLocationInfo(LocationDao aInfo)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                var aSqlParameterlist = new List<SqlParameter>();

                aSqlParameterlist.Add(new SqlParameter("@LocationId", aInfo.LocationId));
                return accessManager.DeleteData("sp_Location_DeleteById", aSqlParameterlist);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            
        }
    }
}
