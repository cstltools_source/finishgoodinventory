﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.DataManager;
using Library.DAL.InternalCls;

namespace Library.DAL.FGInventory_DAL
{
   public class FinishedGoodReportDal
    {

        private readonly ClsCommonInternalDAL _aCommonInternalDal = new ClsCommonInternalDAL();


        public DataTable Get_LocationWiseFabricStatus(string param)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@Pram", param));
            return _aCommonInternalDal.GetDataTableAction("sp_LocationWiseStock_RPT", aSqlParameterlist, "SSIDB");
        }

        public void LoadLocationInfoForddl(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT L.LocationName,QALocationId from tblFinishGoodStock FGS LEFT JOIN tblLocationInfo L ON FGS.QALocationId = L.LocationId";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "LocationName", "QALocationId", queryStr);
        }

        public void LoadBuyerInfoForddl(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerId, BuyerName FROM tblBuyerInfo";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerName", "BuyerId", queryStr);
        }


        public DataTable RPT_QAStockReceive(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_Report_QAStockReceive", aSqlParameterlist, "SSIDB");
        }

        public DataTable RPT_QAStockReceive_Report(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_Report_QA_StockReceive_R", aSqlParameterlist, "SSIDB");
        }


        public DataTable RPT_StoreStock_Report(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_Report_FG_StockReceive", aSqlParameterlist, "SSIDB");
        }

        public DataTable RPT_QAStockReceiveMaster(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_Report_StockReceiveMaster", aSqlParameterlist, "SSIDB");
        }

    }
}
