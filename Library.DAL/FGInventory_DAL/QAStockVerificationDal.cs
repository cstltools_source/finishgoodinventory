﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.DataManager;
using Library.DAL.InternalCls;
using Library.DAO.FGInvrntory_DAO;

namespace Library.DAL.FGInventory_DAL
{
    public class QAStockVerificationDal
    {

        private readonly ClsCommonInternalDAL _aCommonInternalDal = new ClsCommonInternalDAL();
        private DataAccessManager accessManager = new DataAccessManager();

        public void LoadProformaInvoice(DropDownList ddl)
        {
            string queryStr = "SELECT ProformaId,ProformaInvNo + '(' + ProformaCode + ' : ' + BuyerName + ' )' AS PINo FROM tblProformaInvoice AS PI LEFT JOIN tblBuyerInfo AS BI ON PI.BuyerId = BI.BuyerId";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "PINo", "ProformaId", queryStr);
        }

        public void LoadShift(DropDownList ddl)
        {
            const string queryStr = "SELECT ShiftId,ShiftTitle FROM tblShift WHERE IsActive=1";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ShiftTitle", "ShiftId", queryStr);
        }

        public void LoadBuyerInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerId,BuyerName FROM tblBuyerInfo WHERE IsBuyer = 1";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerName", "BuyerId", queryStr);
        }

        public void LoadVendorInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerId,BuyerName FROM tblBuyerInfo WHERE IsVendor = 1 ";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerName", "BuyerId", queryStr);
        }

        public DataTable GetStockReceiveInfo(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_StockReceiveMaster_VerificationList", aSqlParameterlist, "SSIDB");
        }

        public DataTable GetQAStockReceiveMasterById(int stockReceiveMasterId,string status)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@StockReceiveMasterId", stockReceiveMasterId));
            aSqlParameterlist.Add(new SqlParameter("@ApprovalStatus", status));
            return _aCommonInternalDal.GetDataTableAction("sp_StockReceiveMaster_ForVerificationById", aSqlParameterlist, "SSIDB");
        }

        public bool QAStockVerified(QaStockReceiveMasterDao aInfo)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                var aSqlParameterlist = new List<SqlParameter>();
                aSqlParameterlist.Add(new SqlParameter("@StockReceiveMasterId", aInfo.StockReceiveMasterId));
                aSqlParameterlist.Add(new SqlParameter("@VerifiedBy", HttpContext.Current.Session["UserId"].ToString()));
                aSqlParameterlist.Add(new SqlParameter("@VerifiedDate", DateTime.Now));

                return accessManager.UpdateData("sp_StockReceiveMaster_VerifiedById", aSqlParameterlist);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

            
        }
    }
}
