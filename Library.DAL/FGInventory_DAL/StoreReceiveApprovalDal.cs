﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.DataManager;
using Library.DAL.InternalCls;
using Library.DAO.FGInvrntory_DAO;

namespace Library.DAL.FGInventory_DAL
{
    public class StoreReceiveApprovalDal
    {

        private readonly ClsCommonInternalDAL _aCommonInternalDal = new ClsCommonInternalDAL();
        private DataAccessManager accessManager = new DataAccessManager();

        public void LoadProformaInvoice(DropDownList ddl)
        {
            string queryStr = "SELECT ProformaMasterId,ProformaInvNo + '(' + ProformaCode + ' : ' + BuyerName + ' )' AS PINo FROM tblProformaMaster AS PI LEFT JOIN tblBuyerInfo AS BI ON PI.BuyerId = BI.BuyerId";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "PINo", "ProformaMasterId", queryStr);
        }

        public void LoadShift(DropDownList ddl)
        {
            const string queryStr = "SELECT ShiftId,ShiftTitle FROM tblShift WHERE IsActive=1";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ShiftTitle", "ShiftId", queryStr);
        }

        public void LoadBuyerInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerId,BuyerName FROM tblBuyerInfo WHERE IsBuyer = 1";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerName", "BuyerId", queryStr);
        }

        public void LoadVendorInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerId,BuyerName FROM tblBuyerInfo WHERE IsVendor = 1 ";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerName", "BuyerId", queryStr);
        }
        public DataTable GetStockReceiveApprovalList(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_StockReceiveApproval_View", aSqlParameterlist, "SSIDB");
        }

        public DataTable GetStockReceiveInfoById(int stockReceiveMasterId)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@StockReceiveMasterId", stockReceiveMasterId));
            return _aCommonInternalDal.GetDataTableAction("sp_StockReceiveApproval_View", aSqlParameterlist, "SSIDB");
        }

        public bool Approved_QAStockReceive(QaStockReceiveMasterDao aInfo)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);

                var aSqlParameterlist = new List<SqlParameter>();
                aSqlParameterlist.Add(new SqlParameter("@StockReceiveMasterId", aInfo.StockReceiveMasterId));
                aSqlParameterlist.Add(new SqlParameter("@ReceiveBy", HttpContext.Current.Session["UserId"].ToString()));
                return accessManager.UpdateData("sp_FinishGoodStock_Insert", aSqlParameterlist);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }


        public bool Approved_QAStockReceive_Multiple(string aInfo)
        {
            bool status = false;

            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                var aSqlParameterlist = new List<SqlParameter>();
                aSqlParameterlist.Add(new SqlParameter("@StockReceiveMasterId", aInfo));
                aSqlParameterlist.Add(new SqlParameter("@ReceiveBy", HttpContext.Current.Session["UserId"].ToString()));
                status = accessManager.UpdateData("sp_FinishGoodStock_Insert_Multiple", aSqlParameterlist);
            }
            catch (Exception e)
            {
                status = false;
                accessManager.SqlConnectionClose();
            }
            finally
            {
                status = true;
                accessManager.SqlConnectionClose();
            }

            return status;
        }

    }
}
