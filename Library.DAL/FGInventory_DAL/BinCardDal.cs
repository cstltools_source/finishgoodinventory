﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Library.DAL.InternalCls;

namespace Library.DAL.FGInventory_DAL
{
    public class BinCardDal
    {
        private readonly ClsCommonInternalDAL _aCommonInternalDal = new ClsCommonInternalDAL();

        public DataTable GetBinCarDataTable(DateTime fromDate, DateTime toDate, string pram)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@FromDate", fromDate));
            aSqlParameterlist.Add(new SqlParameter("@ToDate", toDate));
            aSqlParameterlist.Add(new SqlParameter("@Pram", pram));

            return _aCommonInternalDal.GetDataTableAction("sp_Report_StockBeenCard", aSqlParameterlist, "SSIDB");
        }


        public DataTable GetStockReport(DateTime fromDate, DateTime toDate, string pram)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FromDate", fromDate));
            aSqlParameterlist.Add(new SqlParameter("@ToDate", toDate));
            aSqlParameterlist.Add(new SqlParameter("@Pram", pram));
            return _aCommonInternalDal.GetDataTableAction("sp_Report_StockReport", aSqlParameterlist, "SSIDB");
        }

        public DataTable GetFabricCategory()
        {
            const string queryStr = "SELECT CategoryName FROM tblFabricCategory";
            return _aCommonInternalDal.DataContainerDataTable(queryStr);
        }
    }
}
