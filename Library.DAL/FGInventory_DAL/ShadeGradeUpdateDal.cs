﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.DataManager;
using Library.DAL.InternalCls;
using Library.DAO.FGInvrntory_DAO;

namespace Library.DAL.FGInventory_DAL
{
   public class ShadeGradeUpdateDal
    {
        private readonly ClsCommonInternalDAL _aCommonInternalDal = new ClsCommonInternalDAL();


        private readonly DataAccessManager accessManager = new DataAccessManager();

        public DataTable GetFinishedGoodStock(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Parm", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_ShadeGrade_UpdateList", aSqlParameterlist, "SSIDB");
        }


        public void LoadShadeGrade(DropDownList ddl)
        {
            const string queryStr = "SELECT * FROM tblShadeGrades";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ShadeGrade", "ShadeGradeId", queryStr);
        }


        public void LoadShift(DropDownList ddl)
        {
            const string queryStr = "SELECT ShiftId,ShiftTitle FROM tblShift WHERE IsActive=1";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ShiftTitle", "ShiftId", queryStr);
        }


        public void LoadSisCodeByPi_ShadeGrade(DropDownList ddl, Int32 piId)
        {
            string queryStr = @"SELECT DISTINCT FabricName FROM tblProformaDetail AS PD
                               LEFT JOIN tblFabricInfo AS FBCI ON PD.SisCodeId = FBCI.FabricId
                               WHERE FabricName IS NOT NULL AND ProformaMasterId = " + piId;
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "FabricName", "FabricName", queryStr);
        }



        public bool UpdateShadeGrade(List<QaStockReceiveDetailDao> alist)
        {
            bool status = false;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
           
                foreach (var agrade in alist)
                {
                    var aSqlParameterlist = new List<SqlParameter>();
                    aSqlParameterlist.Add(new SqlParameter("@StockReceiveMasterId", agrade.StockReceiveMasterId));
                    aSqlParameterlist.Add(new SqlParameter("@StockReceiveDetailId", agrade.StockReceiveDetailId));
                    aSqlParameterlist.Add(new SqlParameter("@ShadeGrade", agrade.ShadeGradeing));
                    aSqlParameterlist.Add(new SqlParameter("@UpdateBy", HttpContext.Current.Session["UserId"].ToString()));
                    status= accessManager.UpdateData("sp_StockReceiveMaster_ShadeGradeUpdate", aSqlParameterlist);
                }
            }
            catch (Exception e)
            {
                status = false;
                accessManager.SqlConnectionClose();

            }
            finally
            {
                status = true;
                accessManager.SqlConnectionClose();
            }


            return status;

        }

    }
}
