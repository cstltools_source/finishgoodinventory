﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.DataManager;
using Library.DAL.InternalCls;
using Library.DAO.FGInvrntory_DAO;

namespace Library.DAL.FGInventory_DAL
{
    public class DeliveryAgainestPackingListDal
    {
        private readonly ClsCommonInternalDAL _aCommonInternalDal = new ClsCommonInternalDAL();
        private DataAccessManager accessManager = new DataAccessManager();

        public DataTable GetReceiveMovementCategory(string Category)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@CategoriesGroup", Category));
            return _aCommonInternalDal.GetDataTableAction("sp_Movement_ListForDDL", aSqlParameterlist, "SSIDB");
        }

        public void LoadDOCharge(DropDownList ddl)
        {
            const string queryStr = "SELECT DOChargeId, DONo FROM tblDOCharge";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "DONo", "DOChargeId", queryStr);
        }

        public void LoadBuyerInfo2(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerId,BuyerName FROM tblBuyerInfo WHERE (IsBuyer = 1 OR IsBoth = 1)";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerName", "BuyerId", queryStr);
        }

        public void LoadPackegingCode(DropDownList ddl)
        {
            const string queryStr = @"SELECT DISTINCT IssueMasterId,FBC.FabricName FROM tblIssueForPackegingDetail AS ISPD
                                      LEFT JOIN tblFinishGoodStock AS FGST ON ISPD.FGStockId = FGST.FGStockId
                                      LEFT JOIN tblFabricInfo FBC ON FGST.FabricCodeId = FBC.FabricId ORDER BY FBC.FabricName";

            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "FabricName", "IssueMasterId", queryStr);
        }


        public void LoadVendorInfo2(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerId,BuyerName FROM tblBuyerInfo WHERE (IsVendor = 1 OR IsBoth = 1)";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerName", "BuyerId", queryStr);
        }
        
        public void LoadMovementCatagories(DropDownList ddl)
        {
            const string queryStr = "SELECT MovementCategoryId,MovementCategory FROM tblMovementCatagories WHERE MovementGroup = 'Delivery'";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "MovementCategory", "MovementCategoryId", queryStr);
        }

        public void LoadPIByuyer(DropDownList ddl, int buyerId)
        {
            string queryStr = "SELECT ProformaId,ProformaInvNo + ' ( ' + BI.BuyerName + ' ) ' AS ProformaInvNo FROM tblProformaInvoice AS PRI " +
                              "LEFT JOIN tblBuyerInfo AS BI ON PRI.BuyerId = BI.BuyerId WHERE PRI.BuyerId = " + buyerId;
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ProformaInvNo", "ProformaId", queryStr);
        }

        public DataTable GetPackingListForDelivery(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_Delivery_PackingList", aSqlParameterlist, "SSIDB");
        }

        public DataTable GetPackingListForDeliveryById(int issueMasterId,string Order)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@IssueMasterId", issueMasterId));
            aSqlParameterlist.Add(new SqlParameter("@OrderBy", Order));
            return _aCommonInternalDal.GetDataTableAction("sp_Delivery_PackingListById", aSqlParameterlist, "SSIDB");
        }

        public bool SaveDeliveryConfirmation(int stockIssueId, int deliveryQuantity, int deliveryConfId)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@StockIssueId", stockIssueId));
            aSqlParameterlist.Add(new SqlParameter("@DeliveryQuantity", deliveryQuantity));
            aSqlParameterlist.Add(new SqlParameter("@DeliveryConfId", deliveryConfId));

            return _aCommonInternalDal.UpdateAction("sp_DeliveryConfirmation_Save", aSqlParameterlist);
        }

        public void LoadPiNo(DropDownList ddl, string pram)
        {
            string queryStr = @"SELECT ProformaMasterId,ProformaInvNo + ' ( ' + CASE WHEN BI.BuyerName = 'N/A' THEN VEN.BuyerName ELSE BI.BuyerName END  + ' ) ' AS ProformaInvNo FROM tblProformaMaster AS PFI 
LEFT JOIN tblBuyerInfo AS BI ON PFI.BuyerId = BI.BuyerId 
LEFT JOIN tblBuyerInfo AS VEN ON PFI.VendorId = VEN.BuyerId WHERE  ProformaMasterId IS NOT NULL" + pram + " ORDER BY ProformaInvNo";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ProformaInvNo", "ProformaMasterId", queryStr);
        }

        public Int32 SaveDeliveryInfo(DeliveryConfirmDao aDao)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                var aSqlParameterlist = new List<SqlParameter>();

                aSqlParameterlist.Add(new SqlParameter("@ConfirmBy", aDao.ConfirmBy));
                aSqlParameterlist.Add(new SqlParameter("@IssueMasterId", aDao.IssueMasterId));

                return accessManager.SaveDataReturnPrimaryKey("sp_DeliveryConfirmation_Insert", aSqlParameterlist);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }




        // ja


        /// <summary>
        /// /
        /// </summary>
        /// <param name="aInfo"></param>
        /// <param name="aDetailDaos"></param>
        /// <returns></returns>
        public bool SaveInfo(IssueForPackegingMasterDao aInfo, List<IssueForPackegingDetailDao> aDetailDaos)
        {
            bool masterId = false;
            bool detailId = false;

            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);

                var aSqlParameterlist = new List<SqlParameter>();

                aSqlParameterlist.Add(new SqlParameter("@IssueMasterId", aInfo.IssueMasterId));
                aSqlParameterlist.Add(new SqlParameter("@DeliveryQuantity", aInfo.DeliveryQuantity));
                aSqlParameterlist.Add(new SqlParameter("@ProformaId", aInfo.ProformaId));
                aSqlParameterlist.Add(new SqlParameter("@DeliveryTypeId", aInfo.DeliveryTypeId));
                aSqlParameterlist.Add(new SqlParameter("@DeliveryBy", aInfo.DeliveryBy));
                aSqlParameterlist.Add(new SqlParameter("@DeliveryDate", aInfo.DeliveryDate));
                aSqlParameterlist.Add(new SqlParameter("@DOChargeId", (object)aInfo.DOChargeId ?? DBNull.Value));

                masterId = accessManager.UpdateData("sp_DeliveryConfirmation_Insert", aSqlParameterlist);

                foreach (var aDaos in aDetailDaos)
                {
                    var detailSqlParameterlist = new List<SqlParameter>();

                    detailSqlParameterlist.Add(new SqlParameter("@FGStockId", aDaos.FGStockId));
                    detailSqlParameterlist.Add(new SqlParameter("@TotalQuantity", aDaos.TotalQuantity));
                    detailSqlParameterlist.Add(new SqlParameter("@IssueDetailId", aDaos.IssueDetailId));
                    detailSqlParameterlist.Add(new SqlParameter("@IsDelivery", aDaos.IsDelivery));
                    detailSqlParameterlist.Add(new SqlParameter("@DeliveryTypeId", aDaos.DeliveryTypeId));

                    detailId = accessManager.UpdateData("sp_DeliveryConfirmation_DetailUpdate", detailSqlParameterlist);
                }
            }
            catch (Exception e)
            {
                masterId = false;
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

            return masterId;
        }
    }
}
