﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.DataManager;
using Library.DAL.InternalCls;
using Library.DAO.FGInvrntory_DAO;

namespace Library.DAL.FGInventory_DAL
{
    public class FabricDal
    {

        private readonly ClsCommonInternalDAL _aCommonInternalDal = new ClsCommonInternalDAL();
        private DataAccessManager accessManager = new DataAccessManager();

        public void LoadFabricColor(DropDownList ddl)
        {
            const string queryStr = "SELECT ColorId,ColorName FROM tblFabricColor";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ColorName", "ColorId", queryStr);
        }

        public void LoadFabricCategory(DropDownList ddl)
        {
            const string queryStr = "SELECT CategoryId,CategoryName FROM tblFabricCategory";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "CategoryName", "CategoryId", queryStr);
        }

        public bool SaveFabricInfo(FabricDao aInfo)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
               
                var aSqlParameterlist = new List<SqlParameter>();

                aSqlParameterlist.Add(new SqlParameter("@FabricName", aInfo.FabricName));
                aSqlParameterlist.Add(new SqlParameter("@FabricShortName", aInfo.FabricShortName));
                aSqlParameterlist.Add(new SqlParameter("@FebricDescription", aInfo.FebricDescription));
                aSqlParameterlist.Add(new SqlParameter("@FabricColor", aInfo.FabricColor));
                aSqlParameterlist.Add(new SqlParameter("@EntryBy", HttpContext.Current.Session["UserId"].ToString()));
                aSqlParameterlist.Add(new SqlParameter("@EntryDate", DateTime.Today));
                aSqlParameterlist.Add(new SqlParameter("@IsActive", true));

                return accessManager.SaveData("sp_Fabric_Insert", aSqlParameterlist);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            
            
            
        }

        public DataTable GetFabricInfo(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_Fabric_View", aSqlParameterlist, "SSIDB");
        }

        public DataTable GetFabricInfoById(Int32 fabricId)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FabricId", fabricId));
            return _aCommonInternalDal.GetDataTableAction("sp_Fabric_EditById", aSqlParameterlist, "SSIDB");
        }

        public bool UpdateFabricInfo(FabricDao aInfo)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);

                var aSqlParameterlist = new List<SqlParameter>();

                aSqlParameterlist.Add(new SqlParameter("@FabricId", aInfo.FabricId));
                aSqlParameterlist.Add(new SqlParameter("@FabricName", aInfo.FabricName));
                aSqlParameterlist.Add(new SqlParameter("@FabricShortName", aInfo.FabricShortName));
                aSqlParameterlist.Add(new SqlParameter("@FebricDescription", aInfo.FebricDescription));
                aSqlParameterlist.Add(new SqlParameter("@FabricColor", aInfo.FabricColor));
                aSqlParameterlist.Add(new SqlParameter("@UpdateBy", HttpContext.Current.Session["UserId"].ToString()));
                aSqlParameterlist.Add(new SqlParameter("@UpdateDate", DateTime.Today));

                return accessManager.UpdateData("sp_Fabric_Update", aSqlParameterlist);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }


            
        }


        public bool ActiveInactiveBuyerInfo(FabricDao aInfo)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);

                var aSqlParameterlist = new List<SqlParameter>();

                aSqlParameterlist.Add(new SqlParameter("@FabricId", aInfo.FabricId));
                aSqlParameterlist.Add(new SqlParameter("@ActiveInactiveBy", HttpContext.Current.Session["UserId"].ToString()));

                return accessManager.UpdateData("sp_Fabric_ActiveInactiveById", aSqlParameterlist);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            
            
        }

        public bool DeleteFabricInfoById(FabricDao aInfo)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@FabricId", aInfo.FabricId));
            return _aCommonInternalDal.UpdateAction("sp_Fabric_DeleteById", aSqlParameterlist);
        }

        public DataTable GetFabricWebService(string prefixText)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@PrefixText", prefixText));
            return _aCommonInternalDal.GetDataTableAction("sp_WebService_GetFabricInfo", aSqlParameterlist, "SSIDB");
        }

        public DataTable GetFabricWebService_PI(string prefixText)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@PrefixText", prefixText));
            return _aCommonInternalDal.GetDataTableAction("sp_WebService_GetFabricInfo_PI", aSqlParameterlist, "SSIDB");
        }



        public DataTable GetColorWebService(string prefixText)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@PrefixText", prefixText));
            return _aCommonInternalDal.GetDataTableAction("sp_WebService_GetFabricColor", aSqlParameterlist, "SSIDB");
        }


        public DataTable GetColorWebService_PI(string prefixText)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@PrefixText", prefixText));
            return _aCommonInternalDal.GetDataTableAction("sp_WebService_GetFabricColor_PI", aSqlParameterlist, "SSIDB");
        }
    }
}
