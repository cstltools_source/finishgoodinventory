﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.FGInvrntory_DAO;

namespace Library.DAL.FGInventory_DAL
{
    public class StockMovementDal
    {
        private readonly ClsCommonInternalDAL _aCommonInternalDal = new ClsCommonInternalDAL();


        public void LoadFabricInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT FabricId,FebricDescription FROM tblFabricInfo WHERE IsActive=1";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "FebricDescription", "FabricId", queryStr);
        }


        public DataTable Get_Fabric_Info(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@Parm", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_FabricInfo_DDL_Like", aSqlParameterlist, "SSIDB");
        }


        public DataTable GetStockMovement(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Parm", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_StockMovement_RollWiseList", aSqlParameterlist, "SSIDB");
        }



        public DataTable GetRollNoValidation(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@RollNo", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_Validation_UniqueRollNumber", aSqlParameterlist, "SSIDB");
        }

        public bool StockMovement_ProgramChange(List<StockMovement> aList)
        {
            bool Status = false;
            
                    foreach (var list in aList)
                    {
                        List<SqlParameter> gSqlParameterlist = new List<SqlParameter>();
                        StockMovement aStock= new StockMovement();
                        aStock.StockId = list.StockId;
                        aStock.fabricId = list.fabricId;
                        aStock.StockQty = list.StockQty;
                        aStock.MovementTypeId = list.MovementTypeId;
                        aStock.MovementType = list.MovementType;
                        aStock.RollChange = list.RollChange;
                        gSqlParameterlist.Add(new SqlParameter("@StockId", aStock.StockId));
                        gSqlParameterlist.Add(new SqlParameter("@FabricId", (object)aStock.fabricId ?? DBNull.Value));
                        gSqlParameterlist.Add(new SqlParameter("@RollNo", (object)aStock.RollChange ?? DBNull.Value));
                        gSqlParameterlist.Add(new SqlParameter("@MovementTypeId", (object)aStock.MovementTypeId ?? DBNull.Value));
                        gSqlParameterlist.Add(new SqlParameter("@MovementType", (object)aStock.MovementType ?? DBNull.Value));
                        gSqlParameterlist.Add(new SqlParameter("@MovementBy", HttpContext.Current.Session["UserId"].ToString()));
                        Status=  _aCommonInternalDal.UpdateAction("sp_StockMovement_ProgramChange", gSqlParameterlist);
                    }
                
            
            return Status;
        }



        public bool StockMovement_RollNoChange(List<StockMovement> aList)
        {
            bool Status = true;

            foreach (var list in aList)
            {
                List<SqlParameter> gSqlParameterlist = new List<SqlParameter>();
                StockMovement aStock = new StockMovement();
                aStock.StockId = list.StockId;
                aStock.RollChange = list.RollChange;
                aStock.StockQty = list.StockQty;
                aStock.MovementTypeId = list.MovementTypeId;
                aStock.MovementType = list.MovementType;
                gSqlParameterlist.Add(new SqlParameter("@StockId", aStock.StockId));
                gSqlParameterlist.Add(new SqlParameter("@NewRollNo", (object)aStock.RollChange ?? DBNull.Value));
                gSqlParameterlist.Add(new SqlParameter("@StockQty", (object)aStock.StockQty ?? DBNull.Value));
                gSqlParameterlist.Add(new SqlParameter("@MovementTypeId", (object)aStock.MovementTypeId ?? DBNull.Value));
                gSqlParameterlist.Add(new SqlParameter("@MovementType", (object)aStock.MovementType ?? DBNull.Value));
                gSqlParameterlist.Add(new SqlParameter("@MovementBy", HttpContext.Current.Session["UserId"].ToString()));
                Status = _aCommonInternalDal.UpdateAction("sp_StockMovement_RollNoChange", gSqlParameterlist);
            }


            return Status;
        }

    }
}
