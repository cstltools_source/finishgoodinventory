﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.DataManager;
using Library.DAL.InternalCls;
using Library.DAO.FGInvrntory_DAO;

namespace Library.DAL.FGInventory_DAL
{
    public class QAPackingGenerationDal
    {

        private readonly ClsCommonInternalDAL _aCommonInternalDal = new ClsCommonInternalDAL();
        private DataAccessManager accessManager = new DataAccessManager();

        public DataTable GetShadeGrades()
        {
            var aSqlParameterlist = new List<SqlParameter>();
            return _aCommonInternalDal.GetDataTableAction("sp_ShadeGrades_CheckboxList", aSqlParameterlist, "SSIDB");
        }
        public void LoadBuyerInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerId,BuyerName FROM tblBuyerInfo WHERE IsBuyer = 1";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerName", "BuyerId", queryStr);
        }

        public void LoadVendorInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT BuyerId,BuyerName FROM tblBuyerInfo WHERE IsVendor = 1 ";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "BuyerName", "BuyerId", queryStr);
        }
        public void LoadProformaInvoice(DropDownList ddl)
        {
            string queryStr = "SELECT ProformaId,ProformaInvNo + '(' + ProformaCode + ' : ' + BuyerName + ' )' AS PINo FROM tblProformaInvoice AS PI LEFT JOIN tblBuyerInfo AS BI ON PI.BuyerId = BI.BuyerId ORDER BY ProformaId DESC ";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "PINo", "ProformaId", queryStr);
        }

        public void LoadFabricSetInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT FabricSet FROM tblFinishGoodStock WHERE FabricSet IS NOT NULL";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "FabricSet", "FabricSet", queryStr);
        }
        
        public void LoadRollNoInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT RollNo FROM tblFinishGoodStock WHERE RollNo IS NOT NULL";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "RollNo", "RollNo", queryStr);
        }
        
        public void LoadRequiredWidthInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT Re_Width FROM tblFinishGoodStock WHERE Re_Width IS NOT NULL";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "Re_Width", "Re_Width", queryStr);
        }

        public void LoadRequiredOunceOryardInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT RequiredOunceOryard FROM tblProformaInvoice WHERE RequiredOunceOryard IS NOT NULL";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "RequiredOunceOryard", "RequiredOunceOryard", queryStr);
        }

        public void LoadShadeGradeingInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT ShadeGradeing FROM tblFinishGoodStock WHERE ShadeGradeing IS NOT NULL";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ShadeGradeing", "ShadeGradeing", queryStr);
        }

        public void LoadShrinkageInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT Shrinkage FROM tblFinishGoodStock WHERE Shrinkage IS NOT NULL";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "Shrinkage", "Shrinkage", queryStr);
        }

        public void LoadPPHYInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT PPHY FROM tblFinishGoodStock WHERE PPHY IS NOT NULL";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "PPHY", "PPHY", queryStr);
        }

        public void LoadPWLengthInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT DISTINCT PWLength FROM tblFinishGoodStock WHERE PWLength IS NOT NULL";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "PWLength", "PWLength", queryStr);
        }
        public void LoadFabricInfo(DropDownList ddl)
        {
            string queryStr = @"SELECT DISTINCT FabricId,FabricName AS Fabric FROM tblFinishGoodStock AS FGS
LEFT JOIN tblFabricInfo AS FBCI ON FGS.FabricCodeId = FBCI.FabricId ORDER BY FabricName";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "Fabric", "FabricId", queryStr);
        }

        public void LoadShadeGradeInfo(DropDownList ddl)
        {
            const string queryStr = "SELECT ShadeGradeId,ShadeGrade FROM tblShadeGrades ORDER BY ShadeGrade";
            _aCommonInternalDal.LoadDropDownValueWithoutDataBase(ddl, "ShadeGrade", "ShadeGradeId", queryStr);
        }

        public DataTable GetFGStockView(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@Pram", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_PacketGeneration_ListFromView", aSqlParameterlist, "SSIDB");
        }

        public bool SaveIssueForPackeging(int fgStockId, int issueQuantity)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@FgStockId", Convert.ToInt32(fgStockId)));
            aSqlParameterlist.Add(new SqlParameter("@IssueQuantity", issueQuantity));
            aSqlParameterlist.Add(new SqlParameter("@IssueBy", HttpContext.Current.Session["UserId"].ToString()));
            aSqlParameterlist.Add(new SqlParameter("@IssueDate", DateTime.Now));

            return _aCommonInternalDal.UpdateAction("sp_IssueForPackeging_Insert", aSqlParameterlist);
        }

        public Int32 SaveIssueForPackeging(IssueForPackegingMasterDao aDao)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@IssueQuantity", aDao.TotalQuantity));
            aSqlParameterlist.Add(new SqlParameter("@IssueBy", aDao.IssueBy));
            aSqlParameterlist.Add(new SqlParameter("@ProformaId", aDao.ProformaId));
            aSqlParameterlist.Add(new SqlParameter("@IssueDate", aDao.IssueDate));
            aSqlParameterlist.Add(new SqlParameter("@DeliveryTypeId", aDao.DeliveryTypeId));
            aSqlParameterlist.Add(new SqlParameter("@Reason", aDao.Reason));

            return _aCommonInternalDal.SaveAction("sp_IssueForPackeging_Insert", aSqlParameterlist, "@IssueMasterId");

        }

        //public Int32 SaveIssueForPackegingDetail(IssueForPackegingDetailDao aDao)
        //{
        //    var aSqlParameterlist = new List<SqlParameter>();

        //    aSqlParameterlist.Add(new SqlParameter("@FGStockId", aDao.FGStockId));
        //    aSqlParameterlist.Add(new SqlParameter("@IssueMasterId", aDao.IssueMasterId));
        //    aSqlParameterlist.Add(new SqlParameter("@Quantity", aDao.Quantity));

        //    return _aCommonInternalDal.SaveAction("sp_IssueForPackeging_DetailInsert", aSqlParameterlist, "@IssueDetailId");
        //}

        public bool InsertIntoPackegingTranscationTable(int masterId)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@IssueMasterId", masterId));

            return _aCommonInternalDal.UpdateAction("sp_IssueForPackeging_InsertIntoTranscation", aSqlParameterlist);
        }

        public int SaveInfo(IssueForPackegingMasterDao aInfo, List<IssueForPackegingDetailDao> aDetailDaos)
        {
            int masterId = 0;
            int detailId = 0;

            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);

                var aSqlParameterlist = new List<SqlParameter>();

                aSqlParameterlist.Add(new SqlParameter("@TotalQuantity", aInfo.TotalQuantity));
                aSqlParameterlist.Add(new SqlParameter("@Reason", aInfo.Reason));
                aSqlParameterlist.Add(new SqlParameter("@IssueBy", aInfo.IssueBy));
                aSqlParameterlist.Add(new SqlParameter("@IssueDate", aInfo.IssueDate));

                masterId = accessManager.SaveDataReturnPrimaryKey("sp_IssueForPackeging_Insert", aSqlParameterlist);

                foreach (var aDaos in aDetailDaos)
                {
                    var detailSqlParameterlist = new List<SqlParameter>();

                    detailSqlParameterlist.Add(new SqlParameter("@IssueMasterId", masterId));
                    detailSqlParameterlist.Add(new SqlParameter("@FGStockId", aDaos.FGStockId));
                    detailSqlParameterlist.Add(new SqlParameter("@IsDelivery", DBNull.Value));

                    detailId = accessManager.SaveDataReturnPrimaryKey("sp_IssueForPackeging_DetailInsert", detailSqlParameterlist);
                }
            }
            catch (Exception e)
            {
                masterId = 0;
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

            return masterId;
        }

        public DataTable GetRemarks(string parameter)
        {
            var aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Parm", parameter));
            return _aCommonInternalDal.GetDataTableAction("sp_ShadeGrades_RemarksCheckboxList", aSqlParameterlist, "SSIDB");
        }

    }
}
